package com.boxters.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;

import com.boxters.model.User;
import com.boxters.android.test.TravelList;
import com.boxters.android.test.Pacco;
import com.boxters.android.test.Viaggio;
import com.google.firebase.auth.FirebaseUser;

public class Global {
    private static Global instance;

    public MainActivity mainActivity;



    public Pacco attributiPacco;
    public Viaggio attributiViaggio;

    public FirebaseUser currentUser;





    public TravelList listaViaggi;
    private User user;

    private Global() {
        attributiPacco = new Pacco();
        attributiViaggio = new Viaggio();
        listaViaggi= null;
    }

    public static Global getInstance() {
        if (instance == null) instance = new Global();

        return instance;
    }

    public Context context() {
        return instance.mainActivity.getApplicationContext();
    }


    public Bitmap getImmagineProfilo() {
        Bitmap bm = null;

        String fn = Global.getInstance().currentUser.getUid() + ".png";

        if (Utility.fileExists(Global.getInstance().context(), "users", fn)) {
            bm = Utility.ReadBitmapFromInternalStorage(Global.getInstance().context(), "users", Global.getInstance().currentUser.getUid());
        }

        return bm;
    }

    public void CreaImmagineProfilo() {
        String fn = Global.getInstance().currentUser.getUid() + ".png";


        // creiamo un'immagine locale solo se non esiste.
        if (!Utility.fileExists(Global.getInstance().context(), "users", fn)) {
            // siccome android non consente di effettuare operazioni di networking (come per esempio scaricare i dati di un'immagine online)
            // nel thread dove gira la UI dell'applicazione, occorre scaricare l'immagine in modo asincrono.
            Uri imageUri = Global.getInstance().currentUser.getPhotoUrl();
            new DownloadImageTask().execute(imageUri.toString());
        }

    }

    private class DownloadImageTask extends AsyncTask<String, Integer, Bitmap> {
        protected Bitmap doInBackground(String... urls) {
            Bitmap bm = Utility.getBitmapFromURL(urls[0]);
            return bm;
        }

        protected void onPostExecute(Bitmap bm) {
            if (bm != null) {
                Bitmap circledBm = Utility.clipInCircle(bm, bm.getWidth() / 2);
                Utility.saveToInternalStorage(Global.getInstance().context(), "users", Global.getInstance().currentUser.getUid(), circledBm);
            }
        }
    }


}
