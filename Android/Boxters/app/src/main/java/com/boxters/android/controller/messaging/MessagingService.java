package com.boxters.android.controller.messaging;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.boxters.android.BoxterConfirm;
import com.boxters.android.ProprietaPaccoActivity;
import com.boxters.android.R;
import com.boxters.android.ReceiverConfirm;
import com.boxters.android.SenderConfirm;
import com.boxters.android.test.ProprietaFeedbackActivity;
import com.boxters.model.AuthorizedBox;
import com.boxters.model.BoxState;
import com.boxters.model.Travel;
import com.boxters.model.messaging.BoxArrivedPayload;
import com.boxters.model.messaging.BoxtersPayload;
import com.boxters.model.messaging.MessageType;
import com.boxters.model.messaging.RequestBoxInfoPayload;
import com.boxters.model.messaging.RequestToBeReceiverPayload;
import com.boxters.model.messaging.SelectedAsPossibleBoxterPayload;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Random;

/**
 * Created by daniele on 12/02/17.
 */

public class MessagingService extends FirebaseMessagingService {
    private final static String TAG = "Boxters";

    public final static String SENDER_KEY = "sender";
    public final static String BOX_ID_KEY = "boxId";
    public final static String BOXTER_KEY = "boxter";
    public final static String TRAVEL_ID_KEY = "travelId";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        // Check if message contains a data payload.
        Map<String, String> data = remoteMessage.getData();
        if (data.size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            MessageType type = MessageType.valueOf(data.get(Key.TYPE));

            String sender = data.get(BoxtersPayload.SENDER_KEY);
            String receiver_token = data.get(BoxtersPayload.RECEIVER_TOKEN_KEY);

            if(sender == null) {
                FirebaseCrash.log("Messaggio con dati non validi, sender assente: " + remoteMessage.getMessageId());
                return;
            }

            if(receiver_token == null) {
                FirebaseCrash.log("Messaggio con dati non validi, receiver token assente: " + remoteMessage.getMessageId());
                return;
            }

            switch (type) {
                case REQUEST_TO_BE_RECEIVER:
                    RequestToBeReceiverPayload payloadR =
                            new RequestToBeReceiverPayload(sender, receiver_token,
                                    data.get(RequestToBeReceiverPayload.BOX_ID_KEY));
                    //todo: qui si deve salvare la notifica
                    notification(payloadR);
                    break;
                case REQUEST_BOX_INFO:
                    String travelId = data.get(RequestBoxInfoPayload.TRAVEL_ID_KEY);
                    RequestBoxInfoPayload payloadB =
                            new RequestBoxInfoPayload(sender, receiver_token,
                                    data.get(RequestToBeReceiverPayload.BOX_ID_KEY), travelId);
                    notification(payloadB);
                    break;
                case BOX_ARRIVED:
                    BoxArrivedPayload payloadA =
                            new BoxArrivedPayload(sender, receiver_token,
                                    data.get(RequestToBeReceiverPayload.BOX_ID_KEY));
                    notification(payloadA);
                    break;
                case SELECTED_AS_POSSIBLE_BOXTER:
                    FirebaseUser I = FirebaseAuth.getInstance().getCurrentUser();
                    if(I != null) {
                        String uid = I.getUid();
                        travelId = data.get(SelectedAsPossibleBoxterPayload.TRAVEL_ID_KEY);
                        String boxId = data.get(SelectedAsPossibleBoxterPayload.BOX_ID_KEY);
                        Travel.load(uid, travelId, (Travel travel) ->
                                        AuthorizedBox.load(sender, boxId, (AuthorizedBox box) ->{
                                            travel.addBox(sender, boxId,
                                                    new Travel.TravelBoxProj(box.getReceiver(), BoxState.CREATED), null);
                                                },false),
                                false);

                        SelectedAsPossibleBoxterPayload payloadS =
                                new SelectedAsPossibleBoxterPayload(sender, receiver_token,
                                        boxId, travelId);
                        notification(payloadS);
                    }
            }
        }
    }

    private void notification(@NotNull SelectedAsPossibleBoxterPayload payload) {
        Intent intent = new Intent(this, BoxterConfirm.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(BOX_ID_KEY, payload.getBoxId());
        intent.putExtra(SENDER_KEY, payload.getSender());
        intent.putExtra(TRAVEL_ID_KEY, payload.getTravelId());
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 746, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_boxters_notification_icon)
                .setContentTitle(getString(R.string.selected_as_possible_boxter_title))
                .setContentText(getString(R.string.selected_as_possible_boxter_body))
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(new Random().nextInt() /* ID of notification */, notificationBuilder.build());


    }

    private void notification(@NotNull BoxArrivedPayload payload) {
        //Intent intent = new Intent(this, ProprietaPaccoActivity.class);
        Intent intent = new Intent(this, ProprietaFeedbackActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(BOX_ID_KEY, payload.getBoxId());
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 746, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_boxters_notification_icon)
                .setContentTitle(getString(R.string.box_arrived_title))
                .setContentText(getString(R.string.box_arrived_body))
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(new Random().nextInt() /* ID of notification */, notificationBuilder.build());
    }

    private void notification(@NotNull RequestToBeReceiverPayload payload) {
        Intent intent = new Intent(this, ReceiverConfirm.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(SENDER_KEY, payload.getSender());
        intent.putExtra(BOX_ID_KEY, payload.getBoxId());
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 746, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_boxters_notification_icon)
                .setContentTitle(getString(R.string.request_receiver_title))
                .setContentText(getString(R.string.request_receiver_body))
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(new Random().nextInt() /* ID of notification */, notificationBuilder.build());
    }

    private void notification(@NotNull RequestBoxInfoPayload payload) {
        Intent intent = new Intent(this, SenderConfirm.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Bundle extra = new Bundle();
        extra.putString(BOXTER_KEY, payload.getSender());
        extra.putString(BOX_ID_KEY, payload.getBoxId());
        extra.putString(TRAVEL_ID_KEY, payload.getTravelId());
        intent.putExtras(extra);
        //intent.putExtra(BOX_ID_KEY, payload.getBoxId());
        //intent.putExtra(BOXTER_KEY, payload.getSender());
        //intent.putExtra(TRAVEL_ID_KEY, payload.getTravelId());
        /* TODO: [ @Stefano ] non dovrebbe esserci il TRAVEL_ID_KEY?
        *
        *  Proprio come il boxter vede le info della scatola che deve portare,
        *  secondo me l'utente che ha la scatola non vuole vedere le info
        *  del pacco che affiderebbe al boxter (bensì le info del viaggio
        *  della potenziale spedizione / viaggio).
        *
        * */
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 485, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_boxters_notification_icon)
                .setContentTitle(getString(R.string.request_box_info_title))
                .setContentText(getString(R.string.request_box_info_body))
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(new Random().nextInt() /* ID of notification */, notificationBuilder.build());
    }
}
