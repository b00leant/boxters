package com.boxters.android;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class TestActivity extends AppCompatActivity implements PacchiFragment.PacchiMain {

    private ViewPager viewPager;
    private PacchiFragment pf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        viewPager = (ViewPager) findViewById(R.id.viewpagerT);
        setupViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        this.pf = new PacchiFragment();
        this.pf.pm=this;

        adapter.addFragment(pf, getResString(R.string.title_boxes));
        viewPager.setAdapter(adapter);
    }

    public String getResString(int id) {
        return (String) getResources().getText(id);
    }

    @Override
    public void onFrammentoCreato(PacchiFragment pf) {
         pf.aa.loadAllUserBoxes(MainActivity.user);
    }
}
