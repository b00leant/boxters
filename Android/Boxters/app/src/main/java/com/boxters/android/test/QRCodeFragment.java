package com.boxters.android.test;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.boxters.android.MainActivity;
import com.boxters.android.ProprietaPaccoActivity;
import com.boxters.android.R;
import com.boxters.android.Utility;
import com.boxters.model.Box;
import com.boxters.model.BoxState;
import com.boxters.model.IllegalActionException;
import com.boxters.model.PublicTravel;
import com.google.android.gms.fitness.data.Value;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import static android.content.Context.WINDOW_SERVICE;

/**
 * Created by giuse on 23/02/2017.
 */

public class QRCodeFragment extends android.support.v4.app.Fragment {
    private Box box;
    private Context mContext;
    private Context c;
    private TextView header;
    private ImageButton button;
    private String qr;
    private ImageView qr_code;

    public QRCodeFragment(){

    }

    public void setBox(Context _c, Box _box) {
        box=_box;
        c=_c;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_qr, container, false);
        qr_code = (ImageView) view.findViewById(R.id.qr);
        header = (TextView) view.findViewById(R.id.receive_header);
        qr = ProprietaPaccoActivity.boxAttuale.getQRCode().trim().toUpperCase();
        button = (ImageButton) view.findViewById(R.id.assegnaPacco);
        if(ProprietaPaccoActivity.boxAttuale.getTravelId() != null &&
                ProprietaPaccoActivity.boxAttuale.getState() == BoxState.CONFIRMED){
            button.setVisibility(View.VISIBLE);
            header.setVisibility(View.VISIBLE);
            header.setText(getContext().getString(R.string.Give_Box));
        }else if(ProprietaPaccoActivity.boxAttuale.getTravelId() != null &&
                ProprietaPaccoActivity.boxAttuale.getState() == BoxState.TRAVELLING){
            header.setVisibility(View.VISIBLE);
            header.setText(getContext().getString(R.string.Box_Assigned_));
        }else{
            button.setVisibility(View.GONE);
        }

        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(getActivity().getString(R.string.QR_Scan));
                    alert.setMessage(getActivity().getString(R.string.Question_Assigned));
                    alert.setNegativeButton(getActivity().getString(R.string.cancel),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(
                                        DialogInterface dialog,
                                        int whichButton) {
                                    dialog.cancel();
                                }
                            });
                    alert.setPositiveButton(getActivity().getString(R.string.Sure),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(
                                        DialogInterface dialog,
                                        int whichButton) {
                                    PublicTravel.load(ProprietaPaccoActivity.boxAttuale.getBoxter(),ProprietaPaccoActivity.boxAttuale.getTravelId(), (t) -> {
                                        t.waitTravellingConfirm(MainActivity.user.getId(),ProprietaPaccoActivity.boxAttuale.getId(), (res) -> {
                                            try{
                                                ProprietaPaccoActivity.boxAttuale.setState(BoxState.TRAVELLING,null);
                                                Box.load(ProprietaPaccoActivity.boxAttuale.getSender(),ProprietaPaccoActivity.boxAttuale.getId(),(Box box)->{
                                                    ProprietaPaccoActivity.boxAttuale = box;
                                                },false);
                                                Toast.makeText(getContext(),getActivity().getString(R.string.Box_Assigned_),Toast.LENGTH_LONG).show();
                                            }catch(IllegalActionException e){
                                                Toast.makeText(getContext(),"Error",Toast.LENGTH_LONG).show();
                                                Log.e("ILLEGAL_ACTION","Molto probabilmente non si può modificare lo stato del pacco");
                                            }
                                            dialog.cancel();
                                            ((Activity) getContext()).recreate();
                                        });
                                    }, false);
                                   //((Activity) getContext()).finish();
                                }
                            });
                    alert.create().show();
            }
        });

        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(qr, BarcodeFormat.QR_CODE,width/2,width/2);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            qr_code.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mContext = getActivity();
    }

    public void setBox(Context _c, String qr) {

        c=_c;

        String s= Utility.readResourceTextFileEx(c, R.raw.qr_code);
        this.qr = qr.trim().toUpperCase();
    }
}
