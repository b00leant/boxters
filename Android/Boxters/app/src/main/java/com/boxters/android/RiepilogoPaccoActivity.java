package com.boxters.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.boxters.android.controller.messaging.MessageSender;
import com.boxters.model.BasicUser;
import com.boxters.android.test.Pacco;
import com.boxters.model.Box;
import com.boxters.model.PublicUser;
import com.boxters.model.User;
import com.boxters.model.messaging.RequestToBeReceiverPayload;
import com.boxters.model.messaging.SelectedAsPossibleBoxterPayload;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;


public class RiepilogoPaccoActivity extends AppCompatActivity {

    public Global G = Global.getInstance();
    private TextView nomePacco;
    private TextView descrizionePacco;
    private TextView emailDestinatario;
    private TextView pesoPacco;
    private double pesoc;
    private CircleImageView iconR;
    private ImageView preview;
    private Bitmap imageBitmap;
    private Bundle bundle;
    private TextView receiverName;
    private double SIZE;
    private TextView spazioPacco;
    private TextView scadenzaPacco;
    private TextView posizionePartenza;
    private TextView posizioneArrivo;
    private GoogleApiClient mGoogleApiClient;




    public Bitmap convert2Bitmap(String base64Str) throws IllegalArgumentException
    {
        byte[] decodedBytes = Base64.decode(
                base64Str.substring(base64Str.indexOf(",")  + 1),
                Base64.DEFAULT
        );

        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    public String convert2String(Bitmap bitmap)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riepilogo_pacco);
        receiverName = (TextView) findViewById(R.id.sender_name);
        preview = (ImageView) findViewById(R.id.image_layout);
        iconR = (CircleImageView) findViewById(R.id.iconSR);
        bundle = getIntent().getExtras();
        if(bundle != null) {
            if(bundle.getString("image_bitmap")!= null){
                imageBitmap = convert2Bitmap(bundle.getString("image_bitmap"));
                if(imageBitmap != null){
                    preview.setImageBitmap(imageBitmap);
                }else{
                    Log.e("IMAGE_PREVIEW","STRINGA_BITMAP_VUOTA!");
                }
            }
        }
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage((FragmentActivity) this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        // your code here
                    }
                })
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();

        Places.GeoDataApi.getPlaceById(mGoogleApiClient, G.attributiPacco.arrivo.getName())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);
                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                            posizioneArrivo = (TextView) findViewById(R.id.locationA);
                            posizioneArrivo.setText(myPlace.getName());
                        } else {
                            Log.e("PLACES DETAILS", "Place not found");
                        }
                        places.release();
                    }
                });
        Places.GeoDataApi.getPlaceById(mGoogleApiClient, G.attributiPacco.partenza.getName())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);
                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                            posizionePartenza = (TextView) findViewById(R.id.locationDa);
                            posizionePartenza.setText(myPlace.getName());
                        } else {
                            Log.e("PLACES DETAILS", "Place not found");
                        }
                        places.release();
                    }
                });

        nomePacco = (TextView) findViewById(R.id.nomePacco);
        nomePacco.setText(G.attributiPacco.nome_pacco);

        descrizionePacco = (TextView) findViewById(R.id.descrizionePacco);
        descrizionePacco.setText(G.attributiPacco.descrizione_pacco);

        emailDestinatario = (TextView) findViewById(R.id.receiverEmail);
        emailDestinatario.setText(G.attributiPacco.email_dest);

        pesoPacco = (TextView) findViewById(R.id.peso);
        pesoPacco.setText(G.attributiPacco.peso_pacco);

        spazioPacco = (TextView) findViewById(R.id.spazio);
        spazioPacco.setText(G.attributiPacco.spazio_pacco);

        scadenzaPacco = (TextView) findViewById(R.id.scadenza);
        scadenzaPacco.setText(G.attributiPacco.string_scadenza_pacco);

        User.loadBasicUserByEmail(G.attributiPacco.email_dest,(user) -> {
            if (user != null) {
                PublicUser.load(user.getId(),(_user)->{
                    if(_user != null){
                        if (_user.getPhoto() != null) {
                            receiverName.setText(_user.getName());
                            if (iconR != null)
                                new DownloadImageTask(iconR).execute(_user.getPhoto());
                        }
                    }
                },false);
            }
        });

        switch (G.attributiPacco.peso_pacco.toString()){
            case "1":
                pesoPacco.setText("<1");
                break;
            case "2":
                pesoPacco.setText(">1");
                break;
            case "3":
                pesoPacco.setText(">5");
                break;
            case "4":
                pesoPacco.setText(">10");
                break;
        }
        SIZE = 5.0;
        switch (G.attributiPacco.spazio_pacco.toString()){
            case "S":
                spazioPacco.setText("S");
                break;
            case "M":
                spazioPacco.setText("M");
                break;
            case "L":
                spazioPacco.setText("L");
                break;
        }
        double s = 1.0;
        double m = 2.0;
        double l = 3.0;
    }

    public void startActivity(Class cls) {
        Intent startNewActivity = new Intent(this, cls);
        startActivity(startNewActivity);
    }


    public void onConfirm(View view) {


        /*
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        String dateInString = "18-02-2017 10:20:56";
        Date date = sdf.parse(dateInString);


        if (G.listaPacchi == null) {
            G.listaPacchi = new PublicBoxList();
        }

*/
        //G.listaPacchi.add(G.attributiPacco);
        String dave = "FPmRzKAW2wPwpSAkHMSmfVcxNKL2";
        String daniele = "zmFBE4zWu3VzNMxNd1FRPmXz1BN2";
        String elena = "A4tFzW6cuHdwvEmYd6wjQaA9jGA3";

        pesoc = 1.00;
        switch (G.attributiPacco.peso_pacco.toString()){
            case "1":
                pesoc = 1.00;
                break;
            case "2":
                pesoc = 2.00;
                break;
            case "3":
                pesoc = 3.00;
                break;
            case "4":
                pesoc = 4.00;
                break;
        }
        SIZE = 5.0;
        switch (G.attributiPacco.spazio_pacco.toString()){
            case "S":
                SIZE = 1.0;
                break;
            case "M":
                SIZE = 2.0;
                break;
            case "L":
                SIZE = 3.0;
                break;
        }
        User test;
        User.loadBasicUserByEmail(G.attributiPacco.email_dest,(user) -> {
            if(user != null){
                System.out.println("l'utente esiste e si chiama "+user.getName());
                Box.create(G.currentUser.getUid(),user.getId(),pesoc, G.attributiPacco.descrizione_pacco, "fotolillolallo",
                        G.attributiPacco.nome_pacco,G.attributiPacco.scadenza_pacco, SIZE, G.attributiPacco.partenza,
                        G.attributiPacco.arrivo,
                        (box) -> {
                            if(box != null){
                                if(imageBitmap!=null){
                                    box.setPhoto(convert2String(imageBitmap),(boxp)->{});
                                }
                                System.out.println(box.getId());
                                ListaViaggiatoriQueryActivity.currentBox = box;
                                G.attributiPacco = new Pacco();
                                Intent startNewActivity = new Intent( this, ListaViaggiatoriQueryActivity.class);
                                finishAffinity();
                                startActivity(startNewActivity);
                                String recTokenDan= "dcS_pvT8lNA:APA91bE9e-1WsLlvhyED6yQoJrbPzY0xPamhGMCVWtFpReV_V53Rp6NCPQ_cBuc9CJTxXCG-n3r36fSHk-x-YQwgxdidJfLfGCgKdYu5pt9-wegujennzLbtThiQO0CRpmig6dOxNe7f";
                                String recTokenElena= "cYAa1CSP708:APA91bGgCs4MNoNigpE591a_pefG_7RyZ8N44if3geiBa-CLPDBO2EsJHh_YpPUv65PkhUb7wnTrP9wfP5oYn2cfmiDO4ieCLO6XS4mUZC9esS7V2j8_4q-omQIRRm9_cZWEDOt-mHwq";

                                PublicUser.load(user.getId(),(publicUser) -> {
                                    MessageSender.send(new RequestToBeReceiverPayload(MainActivity.user.getId(),publicUser.getMessagingToken(), box.getId()));

                                },false);

                                // MessageSender.send(new RequestToBeReceiverPayload(MainActivity.user.getId(),"d_wEkbT_ZNI:APA91bF6jWtHFE8Ng4RUuVy5e6J7sWIcvaIOI7dKpuTXFFGmC9Bg4teFUHDlXmFVK7v9MY_MSwMIWvzT5x8Z-mi_x6NreyDq_H0ZeWGLNr6_CYSeVuDH_tBMDmwgJzKJCBAKz4xLwi3H", box.getId()));

                            }else{
                                System.out.println("Il pacco è null!!");
                            }


                        },true );
            }else{
                Toast.makeText(RiepilogoPaccoActivity.this, getString(R.string.error_receiver_mail),Toast.LENGTH_SHORT).show();
            }
        });




    }

    public void writeCityName(Context ctx, TextView tv, double lat, double lng){
        Geocoder geoCoder = new Geocoder(ctx, Locale.getDefault());

        try {
            List<Address> addresses = geoCoder.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0)
            {
                tv.setText(addresses.get(0).getLocality().toString());
                System.out.println(addresses.get(0).getLocality());
            }
            else
            {
                // do your staff
            }

        } catch (IOException e) {
            ;
        }
    }

}


//Location(@Nullable String name, @NonNull Double lat, @NonNull Double lon)

/*create(@NonNull String sender, @NotNull String receiver,
                              @NotNull Double weight, @Nullable String description,
                              @Nullable String photo, @NotNull String object, @NotNull Date expiration,
                              @NotNull Location departure, @NotNull Location arrival,
                              @Nullable BoxCompletion completion, boolean autoUpdate)
*/