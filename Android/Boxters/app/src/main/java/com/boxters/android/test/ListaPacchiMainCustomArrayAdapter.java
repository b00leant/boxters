package com.boxters.android.test;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.boxters.android.DownloadImageTask;
import com.boxters.android.MainActivity;
import com.boxters.android.PacchiFragment;
import com.boxters.android.R;
import com.boxters.android.controller.messaging.MessageSender;
import com.boxters.model.AuthorizedBox;
import com.boxters.model.Box;
import com.boxters.model.PublicBox;
import com.boxters.model.PublicUser;
import com.boxters.model.Travel;
import com.boxters.model.User;
import com.boxters.model.messaging.RequestBoxInfoPayload;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.vision.text.Text;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by giuse on 18/02/2017.
 */

public class ListaPacchiMainCustomArrayAdapter extends ArrayAdapter<AuthorizedBox> {

    public PacchiFragment ma;


    public void writeCityName(Context ctx, TextView tv, double lat, double lng){
        Geocoder geoCoder = new Geocoder(ctx, Locale.getDefault());

        try {
            List<Address> addresses = geoCoder.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0)
            {
                tv.setText(addresses.get(0).getLocality().toString());
                System.out.println(addresses.get(0).getLocality());
            }
            else
            {
                // do your staff
            }

        } catch (IOException e) {
            ;
        }
    }

    public ListaPacchiMainCustomArrayAdapter(PacchiFragment _ma, AuthorizedBoxList lp) {
        super(_ma.getActivity().getApplicationContext(), R.layout.pacchi_listview_item_main);
        ma = _ma;
        if (lp != null){
            importa(lp);
        }

    }



    protected void importa(AuthorizedBoxList pb) {
        for (AuthorizedBox b : pb) {
            this.add(b);
        }
    }

    protected AuthorizedBox getElement(int pos) {
        return this.getItem(pos);
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        AuthorizedBox b = this.getItem(position);
        if(b != null){
            User item_receiver;
            convertView = ma.getActivity().getLayoutInflater().inflate(R.layout.pacchi_listview_item_main, null);
            TextView expiredBanner = (TextView) convertView.findViewById(R.id.expired_banner);
            TextView expirationBanner = (TextView) convertView.findViewById(R.id.expiration_banner);
            ImageView boxState= (ImageView) convertView.findViewById(R.id.box_state);
            TextView receiverName= (TextView) convertView.findViewById(R.id.receiver_name);
            TextView boxName= (TextView) convertView.findViewById(R.id.box_name);
            TextView secondLineA = (TextView) convertView.findViewById(R.id.secondLineA);
            TextView secondLineB = (TextView) convertView.findViewById(R.id.secondLineB);
            CircleImageView iconSender = (CircleImageView) convertView.findViewById(R.id.iconSender);
            CircleImageView iconReceiver = (CircleImageView) convertView.findViewById(R.id.iconReceiver);
        /*TextView dimensionLine = (TextView) convertView.findViewById(R.id.dimensionLine);
        TextView peso = (TextView) convertView.findViewById(R.id.peso_pacco);
        TextView expiration = (TextView) convertView.findViewById(R.id.expiration_date);*/

            View finalConvertView = convertView;



            //sono receiver

            if(b.getReceiver() == MainActivity.user.getId()){

                PublicUser.load(b.getSender(),(user) -> {
                    receiverName.setText(user.getName());
                    if (user.getPhoto() != null) {
                        if (iconSender != null)
                            new DownloadImageTask(iconSender).execute(user.getPhoto());
                    }

                },false);

                //iconReceiver.setBackgroundColor(Color.parseColor("#125688"));
                iconReceiver.setImageResource(R.drawable.compat_white_box_grey_background);
                receiverName.setVisibility(View.GONE);

            }else{ //sono sender
                if(b.getReceiver()!= null){
                    PublicUser.load(b.getReceiver(),(user) -> {
                        receiverName.setText(user.getName());
                        if (user.getPhoto() != null) {
                            if (iconReceiver != null)
                                new DownloadImageTask(iconReceiver).execute(user.getPhoto());
                        }

                    },false);
                }

                iconSender.setImageResource(R.drawable.compat_white_box_grey_background);
//            senderName.setVisibility(View.GONE);
            }
            String week = getContext().getString(R.string.Week);
            String days = getContext().getString(R.string.Days);
            String day = getContext().getString(R.string.Day);

            SimpleDateFormat localDateFormat = new SimpleDateFormat("MM/dd/yyyy");
            Calendar calCurr = Calendar.getInstance();
            Calendar expirationDate = Calendar.getInstance();
            Date expDate = b.getExpiration();
            expirationDate.setTime(b.getExpiration());
            String expiration = getContext().getString(R.string.Expiration);
            int daysLeft = 0;
            if(expDate.after(calCurr.getTime())){
                daysLeft = ((expirationDate.get(Calendar.DAY_OF_MONTH)) - (calCurr.get(Calendar.DAY_OF_MONTH)));
                System.out.println("Days Left: " + (expirationDate.get(Calendar.DAY_OF_MONTH) -(calCurr.get(Calendar.DAY_OF_MONTH))) );
                Log.d("DAYSLEFT",daysLeft+" days to "+localDateFormat.format(b.getExpiration()));
                if(daysLeft == 1){
                    expirationBanner.setText(expiration+": -1 "+day);
                }else if(daysLeft <= 14 && daysLeft >= 2){
                    if(daysLeft%7 == 0){
                        expirationBanner.setText(expiration+": -"+(daysLeft/7)+" "+week);
                    }else{
                        expirationBanner.setText(expiration+": -"+daysLeft+" "+days);
                    }
                }else{
                    expirationBanner.setText(expiration+": "+localDateFormat.format(b.getExpiration()));
                }
            }else{
                expiredBanner.setVisibility(View.VISIBLE);
                expirationBanner.setVisibility(View.GONE);
            }


            //nomePacco.setText(b.getObject());

            //dimensionLine.setText("S/M/L"); // INSERIRE DIMENSIONE!!!
            //secondLineA.setText(b.getDeparture().getName().split(",")[0]);
            //secondLineB.setText(b.getArrival().getName().split(",")[0]);

            boxName.setText(b.getObject());


            Places.GeoDataApi.getPlaceById(MainActivity.mGoogleApiClient, b.getArrival().getName())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                final Place myPlace = places.get(0);
                                Log.i("PLACES DETAILS", "Place found: " + myPlace.getLocale());
                                secondLineB.setText(myPlace.getName());
                            } else {
                                Log.e("PLACES DETAILS", "Place not found");
                                Log.e("PLACES DETAILS",places.getStatus().getStatusMessage());
                            }
                            places.release();
                        }
                    });
            Places.GeoDataApi.getPlaceById(MainActivity.mGoogleApiClient, b.getDeparture().getName())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                final Place myPlace = places.get(0);
                                Log.i("PLACES DETAILS", "Place found: " + myPlace.getLocale());
                                secondLineA.setText(myPlace.getName());
                            } else {
                                Log.e("PLACES DETAILS", "Place not found");
                            }
                            places.release();
                        }
                    });
            //writeCityName(ma.getContext(),secondLineA,b.getDeparture().getLat(),b.getDeparture().getLon());
            //writeCityName(ma.getContext(),secondLineB,b.getArrival().getLat(),b.getArrival().getLon());


            switch (b.getState()){
                case CREATED:
                    boxState.setImageResource(R.drawable.ic_hourglass_full_blue_24dp);
                    break;
                case CONFIRMED:
                    //boxState.setBackgroundResource(R.drawable.ic_circle_blue);
                    boxState.setImageResource(R.drawable.ic_done_blue_24dp);
                    break;
                case TRAVELLING:
                    //boxState.setBackgroundResource(R.drawable.ic_circle_blue);
                    boxState.setImageResource(R.drawable.ic_my_location_blue_24dp);
                    break;
                case RECEIVED:
                    //boxState.setBackgroundResource(R.drawable.ic_circle_blue);
                    boxState.setImageResource(R.drawable.ic_done_all_blue_24dp);
                    break;
            }
            //expiration.setText(localDateFormat.format(b.getExpiration()));


        }

        return convertView;

    }

    @Override
    public void notifyDataSetChanged(){
        super.notifyDataSetChanged();
    }

    public void loadAllUserBoxes(User u ) {
        this.clear();
        notifyDataSetChanged();

        if (u != null) {
            Box.loadAsSender(u.getId(), (box) -> {
                if (box != null) {
                    this.add(box);
                    notifyDataSetChanged();
                }
            }, false);

            Box.loadAsReceiver(u,
                    (box) -> {
                if (box != null) {
                    this.add(box);
                    notifyDataSetChanged();
                }else{
                }
            }, false);
            ma.hideProgressBar();
            ma.hideSwipeUpdate();
        }
    }
}