package com.boxters.android.test;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.boxters.android.MainActivity;
import com.boxters.android.ProprietaPaccoActivity;
import com.boxters.android.ProprietaViaggioActivity;
import com.boxters.android.R;
import com.boxters.android.controller.messaging.MessageSender;
import com.boxters.model.AuthorizedBox;
import com.boxters.model.Box;
import com.boxters.model.BoxState;
import com.boxters.model.PublicTravel;
import com.boxters.model.PublicUser;
import com.boxters.model.messaging.BoxArrivedPayload;
import com.boxters.model.messaging.RequestBoxInfoPayload;

public class RicezionePaccoFragment extends Fragment {
    ImageButton btnScan;

    public static final int RC_GET_FROM_BOXTER_CODE = 250;

    public RicezionePaccoFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= (View) inflater.inflate(R.layout.fragment_ricezione_pacco, container, false);
        btnScan = (ImageButton) view.findViewById(R.id.start_scan );

        btnScan.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent startNewActivity = new Intent(v.getContext(), QRScannerActivityB.class);
                String QR = ProprietaReceiverActivity.boxAttuale.getQRCode().trim().toUpperCase();
                startNewActivity.putExtra("QR",QR);
                startNewActivity.putExtra("Box", ProprietaReceiverActivity.boxAttuale.getId());
                startNewActivity.putExtra("TravelId",ProprietaReceiverActivity.boxAttuale.getTravelId());
                startNewActivity.putExtra("BoxterId",ProprietaReceiverActivity.boxAttuale.getBoxter());
                startActivityForResult(startNewActivity, RicezionePaccoFragment.RC_GET_FROM_BOXTER_CODE);
            }
        });
         return view;
    }



    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode ){


            case RC_GET_FROM_BOXTER_CODE: {
                if (resultCode == -1) {
                    PublicUser.load(ProprietaReceiverActivity.boxAttuale.getSender(),(user)->{
                        if(user != null){
                            String userToken = user.getMessagingToken();
                            if(userToken != null){
                                if (userToken != null) {
                                    MessageSender.send(new BoxArrivedPayload(MainActivity.user.getId(), userToken, ProprietaReceiverActivity.boxAttuale.getId()));
                                    Intent startNewActivity = new Intent(getContext(),MainActivity.class);
                                    getActivity().finishAffinity();
                                    getActivity().startActivity(startNewActivity);
                                }
                            }
                        }
                    },false);
                    // TODO:cambio stato dell'authorizedBox, CHE NON CAMBIA PERCHÈ LE AUTHORIZED BOX NON HANNO SETSTATE()
                }
                break;
            }


        }
    }
}