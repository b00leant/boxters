package com.boxters.android;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import com.boxters.android.R;
import com.boxters.model.RandomCode;
import com.boxters.model.RandomString;
import com.boxters.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.Manifest.permission.READ_CONTACTS;

public class VerificationActivity extends AppCompatActivity{
    private Bundle bundle;
    private String number;
    private String code;
    private static final String TAG = "SMS_GATEWAY";
    private static final String SMS_GATEWAY_BASE = "http://smsgateway.me/api/v3/messages/send";
    private static final String PIPPUS = "boxters.app@gmail.com";
    private static final String LILLUS = "tre200720022";
    private static final String DEVICE = "46722";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        bundle = getIntent().getExtras();
        number = bundle.getString("phone");
        Log.d("NUMBER", number);
        code = RandomCode.next(6);
        StringBuilder sb = new StringBuilder(SMS_GATEWAY_BASE);
        sb.append("?email=" + PIPPUS);
        sb.append("&device=" + DEVICE);
        sb.append("&password=" + LILLUS);
        number = "+39" + number;
        try {
            sb.append("&number=" + URLEncoder.encode(number, "utf8")); //URLEncoder.encode(number, "utf8"));
            StringBuilder myurl = sb;
            Log.d("URL", myurl.toString());
        }catch (IOException e) {
        }
        String message = "Boxters PIN: "+code;
        sb.append("&message=" + message);
        new SendSMS().execute(sb.toString());



        Button verifyButton = (Button) findViewById(R.id.verify_button);
        EditText first_number = (EditText) findViewById(R.id.firstnumber);
        EditText second_number = (EditText) findViewById(R.id.secondnumber);
        EditText third_number = (EditText) findViewById(R.id.thirdnumber);
        EditText fourth_number = (EditText) findViewById(R.id.fourthnumber);
        EditText fifth_number = (EditText) findViewById(R.id.fifthnumber);
        EditText sixth_number = (EditText) findViewById(R.id.sixthnumber);
        first_number.addTextChangedListener(new TextWatcher() {

            int mPreviousLength;
            boolean mBackSpace;

            public void onTextChanged(CharSequence s, int start,int before, int count)  {
                if(first_number.getText().toString().length() == 2) {
                    String sn = first_number.getText().toString().substring(1,2);
                    second_number.setText(sn);
                    String fn = first_number.getText().toString();
                    first_number.setText(fn.substring(0,1));
                    second_number.requestFocus();
                    second_number.setSelection(second_number.length());
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {}

            public void afterTextChanged(Editable s) {
            }

        });


        second_number.addTextChangedListener(new TextWatcher() {


            public void onTextChanged(CharSequence s, int start,int before, int count)  {
                if(second_number.getText().toString().length() == 2) {
                    third_number.setText(second_number.getText().toString().substring(1,2));
                    String fn = second_number.getText().toString();
                    second_number.setText(fn.substring(0,1));
                    third_number.requestFocus();
                    third_number.setSelection(third_number.length());
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {}

            public void afterTextChanged(Editable s) {
                if(s.length() == 0){
                    first_number.requestFocus();
                    first_number.setSelection(first_number.length());
                }else{
                    third_number.requestFocus();
                }
            }

        });
        third_number.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)  {
                if(third_number.getText().toString().length() == 2) {
                    fourth_number.setText(third_number.getText().toString().substring(1,2));
                    String fn = third_number.getText().toString();
                    third_number.setText(fn.substring(0,1));
                    fourth_number.requestFocus();
                    fourth_number.setSelection(fourth_number.length());
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {}

            public void afterTextChanged(Editable s) {
                if(s.length() == 0){
                    second_number.requestFocus();
                    second_number.setSelection(second_number.length());
                }else{
                    fourth_number.requestFocus();
                }
            }

        });
        fourth_number.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)  {
                if(fourth_number.getText().toString().length() == 2) {
                    fifth_number.setText(fourth_number.getText().toString().substring(1,2));
                    String fn = fourth_number.getText().toString();
                    fourth_number.setText(fn.substring(0,1));
                    fifth_number.requestFocus();
                    fifth_number.setSelection(fifth_number.length());
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {}

            public void afterTextChanged(Editable s) {
                if(s.length() == 0){
                    third_number.requestFocus();
                    third_number.setSelection(third_number.length());
                }else{
                    fifth_number.requestFocus();
                }
            }

        });
        fifth_number.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)  {
                if(fifth_number.getText().toString().length() == 2) {
                    sixth_number.setText(fifth_number.getText().toString().substring(1,2));
                    String fn = fifth_number.getText().toString();
                    fifth_number.setText(fn.substring(0,1));
                    sixth_number.requestFocus();
                    sixth_number.setSelection(sixth_number.length());
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {}

            public void afterTextChanged(Editable s) {
                if(s.length() == 0){
                    fourth_number.requestFocus();
                    fourth_number.setSelection(fourth_number.length());
                }else{
                    sixth_number.requestFocus();
                }
            }

        });
        sixth_number.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)  {
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {}

            public void afterTextChanged(Editable s) {
                if(s.length() == 0){
                    fifth_number.requestFocus();
                    fifth_number.setSelection(fifth_number.length());
                }else{
                }
            }

        });

        verifyButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuilder test = new StringBuilder();
                test.append(first_number.getText().toString().trim());
                test.append(second_number.getText().toString().trim());
                test.append(third_number.getText().toString().trim());
                test.append(fourth_number.getText().toString().trim());
                test.append(fifth_number.getText().toString().trim());
                test.append(sixth_number.getText().toString().trim());
                Log.d("CODE",code);
                Log.d("TEST",test.toString());
                Log.d("NUMBER",number);
                if(test.toString().trim().equals(code.trim())){
                    Toast.makeText(VerificationActivity.this, "CODE IS CORRECT", Toast.LENGTH_LONG).show();
                    if(MainActivity.user != null){
                        User.load(MainActivity.user.getId(),(User user) -> {
                            if(user != null){
                                user.setPhone(number, c ->{
                                    Intent startNewActivity = new Intent(VerificationActivity.this,MainActivity.class);
                                    finishAffinity();
                                    startActivity(startNewActivity);
                                });
                            }
                        },false);
                    }else{
                        Log.e("USER_MAINACTIVITY","USER_IS_NULL");
                    }

                }else{
                    Toast.makeText(VerificationActivity.this, "CODE IS NOT CORRECT", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    private class SendSMS extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String url = params[0];
            Log.i(TAG, "doInBackground");
            String response =  calculate(url);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("SMS_RESPONSE", result);
        }

        @Override
        protected void onPreExecute() {
            Log.i(TAG, "onPreExecute");
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            Log.i(TAG, "onProgressUpdate");
        }

    }

    public String calculate(String string)
    {
        HttpURLConnection conn = null;
        boolean success = false;
        StringBuilder jsonResults = new StringBuilder();
        try {
            Log.d("URL",string);
            URL url = new URL(string);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e("SMS_GATEWAY", "Error processing Places API URL", e);
            //return resultList;
        } catch (IOException e) {
            Log.e("SMS_GATEWAY", "Error connecting to Places API", e);
            //return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        try{
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            success = jsonObj.getBoolean("success");
        } catch (JSONException e) {
            Log.e("LOG", "Cannot process JSON results", e);
        }

        if(success){
            return "done";
        }else{
            return "not done";
        }
    }
}

