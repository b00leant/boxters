package com.boxters.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.boxters.android.controller.messaging.MessagingService;
import com.boxters.model.Box;
import com.boxters.model.Location;
import com.boxters.model.PublicTravel;
import com.boxters.model.PublicUser;
import com.boxters.model.Travel;
import com.boxters.model.User;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;


public class SenderConfirm extends AppCompatActivity implements OnMapReadyCallback {
    private PublicUser boxter;
    private GoogleApiClient mGoogleApiClient;
    private Bundle extras;
    public final static String SENDER_KEY = "sender";
    public final static String BOX_ID_KEY = "boxId";
    public final static String BOXTER_KEY = "boxter";
    public final static String TRAVEL_ID_KEY = "travelId";
    private String travelId;
    private String boxterId;
    private String boxId;
    private TextView locationDa;
    private TextView dataPartenza;
    private SupportMapFragment supportMapFragment;
    public GoogleMap mMap;
    private Polyline percorso;
    public Marker daMark;
    public Marker aMark;
    private TextView dataArrivo;
    private Location da;
    private Location a;
    private TextView locationA;
    private TextView spazioDisponibile;


    @Override
    public void onMapReady(GoogleMap googleMap) {
        boolean success = googleMap.setMapStyle(new MapStyleOptions(getResources()
                .getString(R.string.map_style_json)));
        if (!success) {
            Log.e("MAP_STYLAH", "Style parsing failed.");
        }

        PublicTravel.load(boxterId,travelId,(PublicTravel travel)-> {
            if (travel != null) {
                Location daMap = travel.getDeparture();
                Location aMap = travel.getArrival();

                mMap = googleMap;
                LatLng from = new LatLng(daMap.getLat(),daMap.getLon());
                daMark = mMap.addMarker(new MarkerOptions().position(from).title(getString(R.string.Origin)));
                LatLng to = new LatLng(aMap.getLat(),aMap.getLon());
                aMark = mMap.addMarker(new MarkerOptions().position(to).title(getString(R.string.Destination)));
                percorso = mMap.addPolyline(new PolylineOptions().add(from,to).width(8).color(Color.rgb(18, 96, 147)));
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(daMark.getPosition());
                builder.include(aMark.getPosition());
                LatLngBounds bounds = builder.build();
                int width = getResources().getDisplayMetrics().widthPixels;
                int height = getResources().getDisplayMetrics().heightPixels;
                int padding = (int) (width * 0.33);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
                mMap.moveCamera(cameraUpdate);
                float zoom = mMap.getCameraPosition().zoom;
                mMap.animateCamera( CameraUpdateFactory.zoomTo(zoom - 1));
                mMap.getUiSettings().setScrollGesturesEnabled(false);
                mMap.getUiSettings().setZoomControlsEnabled(false);
            }
        },false);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sender_confirm);

        Intent intent = getIntent();
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage((FragmentActivity) this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        // your code here
                    }
                })
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        SimpleDateFormat localDateFormat = new SimpleDateFormat("MM/dd/yyyy");


        extras = intent.getExtras();
        extras = getIntent().getExtras();
        if(extras != null){
            travelId = extras.getString(TRAVEL_ID_KEY);
            boxterId = extras.getString(BOXTER_KEY);
            boxId = extras.getString(BOX_ID_KEY);

            PublicTravel.load(boxterId,travelId,(PublicTravel travel)->{
                if(travel != null){
                    da = travel.getDeparture();
                    a = travel.getArrival();
                    FragmentManager fm = getSupportFragmentManager();
                    supportMapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
                    if (supportMapFragment == null) {
                        supportMapFragment = SupportMapFragment.newInstance();
                        fm.beginTransaction().replace(R.id.map_container, supportMapFragment).commit();
                    }
                    supportMapFragment.getMapAsync(this);

                    Places.GeoDataApi.getPlaceById(mGoogleApiClient, a.getName())
                            .setResultCallback(new ResultCallback<PlaceBuffer>() {
                                @Override
                                public void onResult(PlaceBuffer places) {
                                    if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                        final Place myPlace = places.get(0);
                                        Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                                        locationA = (TextView) findViewById(R.id.posizioneArrivo);
                                        locationA.setText(myPlace.getName());
                                    } else {
                                        Log.e("PLACES DETAILS", "Place not found");
                                        Log.e("PLACES DETAILS",places.getStatus().getStatusMessage());
                                    }
                                    places.release();
                                }
                            });
                    Places.GeoDataApi.getPlaceById(mGoogleApiClient, da.getName())
                            .setResultCallback(new ResultCallback<PlaceBuffer>() {
                                @Override
                                public void onResult(PlaceBuffer places) {
                                    if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                        final Place myPlace = places.get(0);
                                        Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                                        locationDa = (TextView) findViewById(R.id.posizionePartenza);
                                        locationDa.setText(myPlace.getName());
                                    } else {
                                        Log.e("PLACES DETAILS", "Place not found");
                                    }
                                    places.release();
                                }
                            });

                    dataPartenza = (TextView) findViewById(R.id.dataPartenza);
                    dataPartenza.setText(localDateFormat.format(travel.getStartDate()));


                    dataArrivo = (TextView) findViewById(R.id.dataArrivo);
                    dataArrivo.setText(localDateFormat.format(travel.getEndDate()));

                    ImageView travelState = (ImageView) findViewById(R.id.travel_state_img);
                    switch (travel.getState()){
                        case CREATED:
                            travelState.setImageResource(R.drawable.ic_hourglass_full_blue_24dp);
                            //statoPacco.setText(getString(R.string.Created));
                            break;
                        case STARTED:
                            travelState.setImageResource(R.drawable.ic_my_location_blue_24dp);
                            //statoPacco.setText(getString(R.string.Travelling));
                            break;
                    }

                    spazioDisponibile = (TextView) findViewById(R.id.spazioDisponibile);

                    ImageView mezzoImg = (ImageView) findViewById(R.id.transImg);


        /*
        Temporaneo e NON SCALABILE. Dobbiamo parametrizzare gli strings array  multilingua
         */

                    switch(travel.getTransports().toString().replace("[","").replace("]","")){
                        case "PULLMAN":
                            mezzoImg.setImageResource(R.drawable.ic_directions_bus_blue_24dp);
                            break;
                        case "BUS":
                            mezzoImg.setImageResource(R.drawable.ic_directions_bus_blue_24dp);
                            break;
                        case "AEREO":
                            mezzoImg.setImageResource(R.drawable.ic_flight_takeoff_blue);
                            break;
                        case "AIRPLANE":
                            mezzoImg.setImageResource(R.drawable.ic_flight_takeoff_blue);
                            break;
                        case "AUTO":
                            mezzoImg.setImageResource(R.drawable.ic_directions_car_blue_24dp);
                            break;
                        case "CAR":
                            mezzoImg.setImageResource(R.drawable.ic_directions_car_blue_24dp);
                            break;
                        case "TRENO":
                            mezzoImg.setImageResource(R.drawable.ic_train_blue_24dp);
                            break;
                        case "TRAIN":
                            mezzoImg.setImageResource(R.drawable.ic_train_blue_24dp);
                            break;
                    }

                    if(travel.getSpace() == 10.00){
                        spazioDisponibile.setText("S");
                        //spazioDisponibile.setText(getResources().getStringArray(R.array.spazio)[0].replace("[","").replace("]",""));
                    }
                    if(travel.getSpace() == 50.00){
                        spazioDisponibile.setText("M");
                        //spazioDisponibile.setText(getResources().getStringArray(R.array.spazio)[1].replace("[","").replace("]",""));
                    }
                    if(travel.getSpace() == 100.00){
                        spazioDisponibile.setText("L");
                        //spazioDisponibile.setText(getResources().getStringArray(R.array.spazio)[2].replace("[","").replace("]",""));
                    }
                }
            },false);

        }
        CircleImageView iconBoxter = (CircleImageView) findViewById(R.id.boxter_profile_photo);

        String boxterString = extras.getString(MessagingService.BOXTER_KEY);
        PublicUser.load(boxterString, (PublicUser user) -> {
            if(user != null){
                this.boxter = user;
                TextView nome = (TextView) findViewById(R.id.boxter_profile_name);
                nome.setText(boxter.getName());

                TextView email = (TextView) findViewById(R.id.user_profile_name);
                email.setText(boxter.getEmail());

                RatingBar rating = (RatingBar) findViewById(R.id.ratingBar);
                rating.setMax(5);
                if(boxter.getRating()==null){
                    double rating_s1 = 0;
                    rating.setRating( (float) rating_s1);
                }else{
                    double rating_s2 = boxter.getRating();
                    rating.setRating( (float) rating_s2);
                }
                if (user.getPhoto() != null) {
                    if (iconBoxter != null)
                        new DownloadImageTask(iconBoxter).execute(user.getPhoto());
                }
            }
        }, false );
    }


   @Override
    protected void onNewIntent(Intent intent) {


       mGoogleApiClient = new GoogleApiClient
               .Builder(this)
               .enableAutoManage((FragmentActivity) this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                   @Override
                   public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                       // your code here
                   }
               })
               .addApi(Places.GEO_DATA_API)
               .addApi(Places.PLACE_DETECTION_API)
               .build();
       SimpleDateFormat localDateFormat = new SimpleDateFormat("MM/dd/yyyy");


       extras = intent.getExtras();
       extras = getIntent().getExtras();
       if(extras != null){
           travelId = extras.getString(TRAVEL_ID_KEY);
           boxterId = extras.getString(BOXTER_KEY);
           boxId = extras.getString(BOX_ID_KEY);

           Travel.load(boxterId,travelId,(Travel travel)->{
               if(travel != null){
                   da = travel.getDeparture();
                   a = travel.getArrival();
                   FragmentManager fm = getSupportFragmentManager();
                   supportMapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
                   if (supportMapFragment == null) {
                       supportMapFragment = SupportMapFragment.newInstance();
                       fm.beginTransaction().replace(R.id.map_container, supportMapFragment).commit();
                   }
                   supportMapFragment.getMapAsync(this);

                   Places.GeoDataApi.getPlaceById(mGoogleApiClient, a.getName())
                           .setResultCallback(new ResultCallback<PlaceBuffer>() {
                               @Override
                               public void onResult(PlaceBuffer places) {
                                   if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                       final Place myPlace = places.get(0);
                                       Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                                       locationA = (TextView) findViewById(R.id.posizioneArrivo);
                                       locationA.setText(myPlace.getName());
                                   } else {
                                       Log.e("PLACES DETAILS", "Place not found");
                                       Log.e("PLACES DETAILS",places.getStatus().getStatusMessage());
                                   }
                                   places.release();
                               }
                           });
                   Places.GeoDataApi.getPlaceById(mGoogleApiClient, da.getName())
                           .setResultCallback(new ResultCallback<PlaceBuffer>() {
                               @Override
                               public void onResult(PlaceBuffer places) {
                                   if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                       final Place myPlace = places.get(0);
                                       Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                                       locationDa = (TextView) findViewById(R.id.posizionePartenza);
                                       locationDa.setText(myPlace.getName());
                                   } else {
                                       Log.e("PLACES DETAILS", "Place not found");
                                   }
                                   places.release();
                               }
                           });


                   dataPartenza = (TextView) findViewById(R.id.dataPartenza);
                   dataPartenza.setText(localDateFormat.format(travel.getStartDate()));


                   dataArrivo = (TextView) findViewById(R.id.dataArrivo);
                   dataArrivo.setText(localDateFormat.format(travel.getEndDate()));

                   ImageView travelState = (ImageView) findViewById(R.id.travel_state_img);
                   switch (travel.getState()){
                       case CREATED:
                           travelState.setImageResource(R.drawable.ic_hourglass_full_blue_24dp);
                           //statoPacco.setText(getString(R.string.Created));
                           break;
                       case STARTED:
                           travelState.setImageResource(R.drawable.ic_my_location_blue_24dp);
                           //statoPacco.setText(getString(R.string.Travelling));
                           break;
                   }

                   spazioDisponibile = (TextView) findViewById(R.id.spazioDisponibile);

                   ImageView mezzoImg = (ImageView) findViewById(R.id.transImg);


        /*
        Temporaneo e NON SCALABILE. Dobbiamo parametrizzare gli strings array  multilingua
         */

                   switch(travel.getTransports().toString().replace("[","").replace("]","")){
                       case "PULLMAN":
                           mezzoImg.setImageResource(R.drawable.ic_directions_bus_blue_24dp);
                           break;
                       case "BUS":
                           mezzoImg.setImageResource(R.drawable.ic_directions_bus_blue_24dp);
                           break;
                       case "AEREO":
                           mezzoImg.setImageResource(R.drawable.ic_flight_takeoff_blue);
                           break;
                       case "AIRPLANE":
                           mezzoImg.setImageResource(R.drawable.ic_flight_takeoff_blue);
                           break;
                       case "AUTO":
                           mezzoImg.setImageResource(R.drawable.ic_directions_car_blue_24dp);
                           break;
                       case "CAR":
                           mezzoImg.setImageResource(R.drawable.ic_directions_car_blue_24dp);
                           break;
                       case "TRENO":
                           mezzoImg.setImageResource(R.drawable.ic_train_blue_24dp);
                           break;
                       case "TRAIN":
                           mezzoImg.setImageResource(R.drawable.ic_train_blue_24dp);
                           break;
                   }

                   if(travel.getSpace() == 10.00){
                       spazioDisponibile.setText("S");
                       //spazioDisponibile.setText(getResources().getStringArray(R.array.spazio)[0].replace("[","").replace("]",""));
                   }
                   if(travel.getSpace() == 50.00){
                       spazioDisponibile.setText("M");
                       //spazioDisponibile.setText(getResources().getStringArray(R.array.spazio)[1].replace("[","").replace("]",""));
                   }
                   if(travel.getSpace() == 100.00){
                       spazioDisponibile.setText("L");
                       //spazioDisponibile.setText(getResources().getStringArray(R.array.spazio)[2].replace("[","").replace("]",""));
                   }
               }
           },false);

       }
       CircleImageView iconBoxter = (CircleImageView) findViewById(R.id.boxter_profile_photo);

       String boxterString = extras.getString(MessagingService.BOXTER_KEY);
       User.load(boxterString, (User user) -> {
           this.boxter = user;
           TextView nome = (TextView) findViewById(R.id.boxter_profile_name);
           nome.setText(boxter.getName());

           TextView email = (TextView) findViewById(R.id.user_profile_name);
           email.setText(boxter.getEmail());

           RatingBar rating = (RatingBar) findViewById(R.id.ratingBar);
           rating.setMax(5);
           if(boxter.getRating()==null){
               double rating_s1 = 0;
               rating.setRating( (float) rating_s1);
           }else{
               double rating_s2 = boxter.getRating();
               rating.setRating( (float) rating_s2);
           }
           if (user.getPhoto() != null) {
               if (iconBoxter != null)
                   new DownloadImageTask(iconBoxter).execute(user.getPhoto());
           }
       }, false );



    }

    public void writeCityName(Context ctx, TextView tv, double lat, double lng){
        Geocoder geoCoder = new Geocoder(ctx, Locale.getDefault());

        try {
            List<Address> addresses = geoCoder.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0)
            {
                tv.setText(addresses.get(0).getLocality().toString());
                System.out.println(addresses.get(0).getLocality());
            }
            else
            {
                // do your staff
            }

        } catch (IOException e) {
            ;
        }
    }

    public void onDeny(View view){
        Intent startNewActivity = new Intent(this, MainActivity.class);
        finishAffinity();
        startActivity(startNewActivity);
    }

    public void onAccetta(View view) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();

        if (user != null) {
            Log.d("LOGIN","ALREADY SIGNED IN");
            User.load(user.getUid(),(User _user) -> {
                if(_user != null){
                    PublicTravel.load(boxterId,travelId,(PublicTravel travel)->{
                        if(travel != null){
                            Box.load(_user.getId(),boxId,(Box box)->{
                                if(box != null){
                                    Set<PublicTravel> travels = new HashSet<PublicTravel>();
                                    travels.add(travel);
                                    box.addPossibleTravels(travels,(save) -> {
                                        if (save) {
                                            int i=10;
                                            System.out.println(i);
                                            String newBoxter = getString(R.string.NewBoxter);
                                            Toast.makeText(this,newBoxter,Toast.LENGTH_SHORT).show();
                                            Intent startNewActivity = new Intent(this, MainActivity.class);
                                            finishAffinity();
                                            startActivity(startNewActivity);
                                        }else {
                                            Toast.makeText(this,getString(R.string.GoneWrong),Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            },false);
                        }
                    },false);
                }
            },false);
        }else{
            Log.d("USER_IN_NOTIFICATION","FIREBASE USER NOT FIND");
        }
    }
}
