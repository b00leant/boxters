package com.boxters.android;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Region;
import android.net.Uri;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Utility {

    public static void checkGooglePlayServicesAvailability(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if(status != ConnectionResult.SUCCESS) {
            GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(activity);
        }
    }

    public static Bitmap getBitmapFromURL(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap loadBitmap(Context context, Uri uri) {
        Bitmap bm = null;
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            bm = BitmapFactory.decodeStream(inputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return bm;
    }

    public static Bitmap clipInCircle(Bitmap bm, float r) {
        if (r == 0) r = bm.getWidth() / 2;

        Bitmap bc = Bitmap.createBitmap(bm.getWidth(), bm.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bc);
        Paint p = new Paint();
        p.setAlpha(255); //0x80
        Path mPath = new Path();
        mPath.addCircle(bm.getWidth() / 2, bm.getHeight() / 2, r, Path.Direction.CCW);

        //canvas.clipPath(mPath, Region.Op.UNION);
        //canvas.clipPath(mPath, Region.Op.DIFFERENCE);
        //canvas.clipPath(mPath, Region.Op.REPLACE);
        //canvas.clipPath(mPath, Region.Op.XOR);
        canvas.clipPath(mPath, Region.Op.INTERSECT);
        canvas.drawBitmap(bm, 0, 0, p);

        return bc;
    }

    private static File getAppDir(Context C, String dir) {
        ContextWrapper cw = new ContextWrapper(C);
        return cw.getDir(dir, Context.MODE_PRIVATE);
    }

    private static File getAppFile(Context C, String dir, String fileName) {
        return new File(getAppDir(C, dir), fileName);
    }

    private static String getAppFileAbsolutePath(Context C, String dir, String fileName) {
        return new File(getAppDir(C, dir), fileName).getAbsolutePath();
    }

    public static boolean fileExists(Context C, String dir, String fileName) {
        File f = getAppFile(C, dir, fileName);
        return f.exists();
    }

    public static String saveToInternalStorage(Context C, String dir, String fileNameNoExtension, Bitmap bitmapImage) {

        File f = getAppFile(C, dir, fileNameNoExtension + ".png");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return f.getAbsolutePath();
    }

    public static Bitmap ReadBitmapFromInternalStorage(Context C, String dir, String fileNameNoExtension) {

        try {
            File f = getAppFile(C, dir, fileNameNoExtension + ".png");
            return BitmapFactory.decodeStream(new FileInputStream(f));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static String readRawTextFile(Context ctx, int resId)
    {
        InputStream inputStream = ctx.getResources().openRawResource(resId);

        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader buffreader = new BufferedReader(inputreader);
        String line;
        StringBuilder text = new StringBuilder();

        try {
            while (( line = buffreader.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
        } catch (IOException e) {
            return null;
        }
        return text.toString();
    }


    public static String readResourceTextFileEx(Context c, Integer id) {
        String s= Utility.readRawTextFile(c,id);
        return Utility.processIncludeDirective(c,s);
    }



    public static String processIncludeDirective(Context c, String s) {
        int i = s.indexOf("<include>",0);
        while (i>0) {
            int k = s.indexOf("</include>",i);
            if (k>i) {
                String innerText = s.substring(i+9, k);
                String outerText = s.substring(i, k+10);
                String[] sp=innerText.split("\\.");
                int resourceId = c.getResources().getIdentifier(sp[2] , sp[1], c.getPackageName());
                String include = readRawTextFile( c, resourceId);
                s=s.replace(outerText,include);
            }
            i = s.indexOf("<include>",0);
        }
        return s;
    }
}