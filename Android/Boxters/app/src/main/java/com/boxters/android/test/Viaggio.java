package com.boxters.android.test;

import com.boxters.model.Location;
import com.boxters.model.Transport;

import java.util.Date;
import java.util.Set;

/**
 * Created by giuse on 10/02/2017.
 */

public class Viaggio {

    public String trasporto;

    public String spazio_disponibile;
    public Date inizio_viaggio, fine_viaggio;
    public String string_inizio_viaggio, string_fine_viaggio;

    public Location partenza, arrivo;

    public Viaggio() {
        trasporto = null;
        spazio_disponibile = null;
        inizio_viaggio = null;
        fine_viaggio = null;
        partenza = null;
        arrivo = null;
        string_inizio_viaggio = null;
        string_fine_viaggio = null;
    }

    public Viaggio(String trasporto, String spazio_disponibile, Date inizio_viaggio,
                   Date fine_viaggio, Location partenza, Location arrivo,String string_inizio_viaggio, String string_fine_viaggio) {
        this.trasporto = trasporto;
        this.spazio_disponibile = spazio_disponibile;
        this.inizio_viaggio = inizio_viaggio;
        this.fine_viaggio = fine_viaggio;
        this.partenza = partenza;
        this.arrivo = arrivo;
        this.string_inizio_viaggio = string_inizio_viaggio;
        this.string_fine_viaggio = string_fine_viaggio;
    }
}


//super(boxter, id, startDate, endDate, space, transports, departure, arrival, state);