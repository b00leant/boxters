package com.boxters.android;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.transition.Visibility;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.boxters.android.test.ProprietaReceiverActivity;
import com.boxters.model.AuthorizedBox;
import com.boxters.model.Box;
import com.boxters.android.test.ListaPacchiMainCustomArrayAdapter;
import com.boxters.model.PublicTravel;
import com.boxters.model.Travel;
import com.boxters.model.User;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;

public class PacchiFragment extends Fragment {
    public static ListaPacchiMainCustomArrayAdapter aa;
    public PacchiMain pm;
    private GoogleApiClient mGoogleApiClient;
    private ProgressBar pb;
    private ListView lv;
    private TextView tv;
    private SwipeRefreshLayout swipeRefreshLayout;
    public PacchiFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_lista_pacchi_main, container, false);
        lv = (ListView) view.findViewById(R.id.listViewPacchiMain);
        aa = new ListaPacchiMainCustomArrayAdapter(this, null);
        tv = (TextView) view.findViewById(R.id.tvFragmentOne);
        pb = (ProgressBar) view.findViewById(R.id.prgbar);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        aa.loadAllUserBoxes(MainActivity.user);
                    }
                }
        );
        lv.setEmptyView(tv);
        lv.setAdapter(aa);
      //  updateList();


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3)
            {

                AuthorizedBox b = (AuthorizedBox) adapter.getItemAtPosition(position);

                if (b.getSender().equals(MainActivity.user.getId())) {
                    ProprietaPaccoActivity.boxAttuale= (Box) adapter.getItemAtPosition(position);
                    if(b.getBoxter() != null){
                        if(b.getTravelId() != null){
                            PublicTravel.load(b.getBoxter(),b.getTravelId(), (PublicTravel travel) ->{
                                if(travel != null){
                                    ProprietaPaccoActivity.mytravel = travel;
                                    Intent startNewActivity = new Intent(v.getContext(), ProprietaPaccoActivity.class);
                                    //getActivity().finish(); //?
                                    startActivity(startNewActivity);
                                }
                            },false);
                        }
                    }else{
                        Intent startNewActivity = new Intent(v.getContext(), ProprietaPaccoActivity.class);
                        //getActivity().finish(); //?
                        startActivity(startNewActivity);
                    }
                }
                else {
                    Log.d("TRAVEL_ID","TRAVEL_ID: "+b.getTravelId());
                    Log.d("BOXTER_ID","BOXTER_ID: "+b.getBoxter());
                    ProprietaReceiverActivity.boxAttuale= (AuthorizedBox) adapter.getItemAtPosition(position);
                    if(b.getBoxter() != null){
                        if(b.getTravelId() != null){
                            PublicTravel.load(b.getBoxter(),b.getTravelId(), (PublicTravel travel) ->{
                                if(travel != null){
                                    ProprietaReceiverActivity.mytravel = travel;
                                    Intent startNewActivity = new Intent(v.getContext(), ProprietaReceiverActivity.class);
                                    //getActivity().finish(); //?
                                    startActivity(startNewActivity);
                                }
                            },false);
                        }
                    }else{
                        Intent startNewActivity = new Intent(v.getContext(), ProprietaReceiverActivity.class);
                        //getActivity().finish(); //?
                        startActivity(startNewActivity);
                    }
                }
                /*
                ProprietaPaccoActivity.boxAttuale= (Box) adapter.getItemAtPosition(position);
                Intent startNewActivity = new Intent(v.getContext(), ProprietaPaccoActivity.class);
                startActivity(startNewActivity);
            */
            }
        });

        //Codice David
        lv.setLongClickable(true);


        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View v,
                                           int index, long arg3) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                AuthorizedBox b = (AuthorizedBox) arg0.getItemAtPosition(index);
                View dialogView;
                if(b.getState().toString() == "CREATED" && b.getSender().equals(MainActivity.user.getId())){
                    dialogView = inflater.inflate(R.layout.item_dialog, null);
                    dialogBuilder.setView(dialogView);
                    AlertDialog alertDialog = dialogBuilder.create();
                    ImageButton yes = (ImageButton) dialogView.findViewById(R.id.btn_yes);
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Box b = (Box) arg0.getItemAtPosition(index);
                            Bundle bundle = new Bundle();
                            bundle.putString("from_id",b.getDeparture().getName());
                            bundle.putString("to_id",b.getArrival().getName());
                            bundle.putString("box_id",b.getId());
                            bundle.putString("travel_id",null);
                            bundle.putDouble("from_lat",b.getDeparture().getLat());
                            bundle.putDouble("from_lon",b.getDeparture().getLon());
                            bundle.putDouble("to_lat",b.getArrival().getLat());
                            bundle.putDouble("to_lon",b.getArrival().getLon());
                            bundle.putString("user_id",b.getSender());
                            /*if(b.getPhoto() == null){
                                bundle.putString("image_bitmap",null);
                            }else{
                                bundle.putString("image_bitmap",b.getPhoto());
                            }*/
                            bundle.putString("mode","boxes");
                            //MapActivity.boxAttuale = (Box) arg0.getItemAtPosition(index);
                            Intent startNewActivity = new Intent(v.getContext(), MapActivity.class);
                            startNewActivity.putExtras(bundle);
                            getActivity().finish();
                            startActivity(startNewActivity);
                            alertDialog.cancel();
                        }
                    });
                    ImageButton no = (ImageButton) dialogView.findViewById(R.id.btn_no);
                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Box box_to_delete = (Box) b;
                            box_to_delete.delete((box_deleted) -> {
                                ListaPacchiMainCustomArrayAdapter adapter = (ListaPacchiMainCustomArrayAdapter) arg0.getAdapter();
                                adapter.remove(b);
                                alertDialog.cancel();
                                Toast.makeText(getContext(), "Box deleted! (maybe)",Toast.LENGTH_SHORT);
                            });
                        }
                    });
                    alertDialog.show();
                }if(b.getState().toString() == "CONFIRMED"){
                    /*
                    dialogView = inflater.inflate(R.layout.item_dialog_only_delete, null);
                    ImageButton yes = (ImageButton) dialogView.findViewById(R.id.btn_delete);
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //TODO: funzione per eliminare il pacco direttamente da main list view
                        }
                    });
                    dialogBuilder.setView(dialogView);
                    AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.show();
                    */
                }if(b.getState().toString() == "TRAVELLING"){

                }if(b.getState().toString() == "RECEIVED"){/*

                    //TODO: controllare se il rating della scatola/boxter/viaggio è stato inserito e nel caso positivo non far apparire il dialog.

                    dialogView = inflater.inflate(R.layout.item_dialog_only_rating, null);
                    ImageButton yes = (ImageButton) dialogView.findViewById(R.id.btn_ok);
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //TODO: settare il rating
                        }
                    });
                    dialogBuilder.setView(dialogView);
                    AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.show();*/
                }
                return true;
            }});



        if (pm != null)
            pm.onFrammentoCreato(this);

        return view;


    }
    public void showProgressBar(){
        pb.setVisibility(View.VISIBLE);
    }
    public void hideProgressBar(){
        pb.setVisibility(View.GONE);
    }
    public void hideSwipeUpdate() { swipeRefreshLayout.setRefreshing(false);}
    public void showListView() {lv.setVisibility(View.VISIBLE);}
    public void setEmptyView() {lv.setEmptyView(tv);}
    public void showTV() {tv.setVisibility(View.VISIBLE);}

    public void creaNuovoPacco() {

        Intent startNewActivity = new Intent( this.getContext(), MapActivity.class);
        startNewActivity.putExtra("tipoProcesso", MapActivity.TIPO_PROCESSO_PACCO );

        startActivity(startNewActivity);
    }

    public interface PacchiMain {
        void onFrammentoCreato(PacchiFragment pf);
    }

}

        // NON TOCCARE. DAVE
 /*
        aa = new ListaPacchiQueryCustomArrayAdapter(this.getActivity(), Global.getInstance().listaPacchi);
        lv.setAdapter(aa);

        if (Global.getInstance().listaPacchi != null && Global.getInstance().listaPacchi.size() > 0) {
            TextView tv = (TextView) view.findViewById(R.id.tvFragmentOne);
            tv.setVisibility(View.INVISIBLE);

        }
    */