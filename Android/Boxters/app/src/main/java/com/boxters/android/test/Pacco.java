package com.boxters.android.test;

import com.boxters.model.Location;

import java.util.Date;

/**
 * Created by David on 02/02/2017.
 */

public class Pacco {
    public String nome_pacco, descrizione_pacco, email_dest;
    public String peso_pacco, spazio_pacco;
    public String foto_pacco;
    public Date scadenza_pacco;
    public Location partenza, arrivo;
    public String string_scadenza_pacco;


    public Pacco() {
        nome_pacco = null;
        descrizione_pacco = null;
        email_dest = null;
        peso_pacco = null;
        foto_pacco = null;
        spazio_pacco = null;
        scadenza_pacco = null;
        partenza = null;
        arrivo = null;
        string_scadenza_pacco = null;
    }

    public Pacco(String nome_pacco, String descrizione_pacco, String email_dest, String peso_pacco, String spazio_pacco, Date scadenza_pacco,
                 Location partenza, Location arrivo,String string_scadenza_pacco) {
        this.nome_pacco = nome_pacco;
        this.descrizione_pacco = descrizione_pacco;
        this.email_dest = email_dest;
        this.peso_pacco = peso_pacco;
        this.spazio_pacco = spazio_pacco;
        this.scadenza_pacco = scadenza_pacco;
        this.partenza = partenza;
        this.arrivo = arrivo;
        this.string_scadenza_pacco = string_scadenza_pacco;
    }

}
