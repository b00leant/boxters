package com.boxters.android;

import android.app.Activity;
import android.support.annotation.StringRes;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import javax.microedition.khronos.opengles.GL;

public class CustomActionBarToggle extends ActionBarDrawerToggle {
    public Boolean isOpen = false;

    public CustomActionBarToggle(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar, @StringRes int openDrawerContentDescRes, @StringRes int closeDrawerContentDescRes) {
        super(activity, drawerLayout, toolbar, openDrawerContentDescRes, closeDrawerContentDescRes);
        drawerLayout.addDrawerListener(this);
    }


    @Override
    public void onDrawerClosed(View drawerView) {
        super.onDrawerClosed(drawerView);
        this.isOpen = false;
    }

    @Override
    public void onDrawerOpened(View drawerView) {
        super.onDrawerOpened(drawerView);
        this.isOpen = true;
    }

    protected void onStartOpening(View drawerView) {

        TextView tv = (TextView) drawerView.findViewById(R.id.h_userName);
        TextView tv2 = (TextView) drawerView.findViewById(R.id.h_userDetails);
        ImageView iv = (ImageView) drawerView.findViewById(R.id.h_userImage);

        NavigationView navigationView = (NavigationView) drawerView.findViewById(R.id.nav_view);
        Menu menu = navigationView.getMenu();
        //     MenuItem mi_login = menu.findItem(R.id.nav_login);
        MenuItem mi_logout = menu.findItem(R.id.nav_logout);

        tv2.setText(""); // non usiamo questa textview

        if (Global.getInstance().currentUser != null) {
            tv2.setText(Global.getInstance().currentUser.getEmail());
            tv.setText(Global.getInstance().currentUser.getDisplayName());
            iv.setImageBitmap(Global.getInstance().getImmagineProfilo());
            //       mi_login.setVisible(false);
            mi_logout.setVisible(true);
        } else {
            tv.setText("...");
            iv.setImageDrawable(null);
            //     mi_login.setVisible(true);
            mi_logout.setVisible(false);
        }
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
        // intercettiamo l'evento quando 'inizia' ad aprirsi
        if (!isOpen && slideOffset > 0 && slideOffset < 1)
            onStartOpening(drawerView);

        super.onDrawerSlide(drawerView, slideOffset);
    }

    @Override
    public void onDrawerStateChanged(int newState) {
        super.onDrawerStateChanged(newState);
    }

}
