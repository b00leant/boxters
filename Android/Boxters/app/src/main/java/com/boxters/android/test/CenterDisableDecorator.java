package com.boxters.android.test;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import com.boxters.android.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by undeclinable on 11/04/17.
 */

public class CenterDisableDecorator implements DayViewDecorator {
    private final Drawable dc;
    private final ArrayList<Date> dates;

    public CenterDisableDecorator(List<Date> dates, Activity context) {
        dc = context.getResources().getDrawable(R.drawable.ic_d_just_center);
        this.dates = new ArrayList<>(dates);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        if(this.dates.size() > 2){
            ArrayList<Date> middle_dates = new ArrayList<>(this.dates.subList(1, this.dates.size() - 1));
            return middle_dates.contains(day.getDate());
        }else{
            return false;
        }
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setDaysDisabled(true);
        view.setSelectionDrawable(dc);
    }
}