package com.boxters.android.test;

import android.content.Context;

import com.boxters.android.R;
import com.boxters.android.Utility;
import com.boxters.model.Box;

/**
 * Created by giuse on 23/02/2017.
 */

public class InfoPaccoFragment extends WebViewFragment {
    private Box box;
    private Context c;

    public void setBox(Context _c, Box _box) {
        box=_box;
        c=_c;

        String s= Utility.readResourceTextFileEx(c, R.raw.info_pacco);
        s = s.replace("{partenza}", box.getArrival().getName() );

        setHTML(s);
    }

    @android.webkit.JavascriptInterface
    public void jschiudi() {
        this.getActivity().finish();
    }
}
