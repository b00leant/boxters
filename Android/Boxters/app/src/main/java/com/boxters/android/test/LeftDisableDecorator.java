package com.boxters.android.test;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import com.boxters.android.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by undeclinable on 11/04/17.
 */

public class LeftDisableDecorator implements DayViewDecorator {
    private final ArrayList<Date> dates;
    private final Drawable dl;

    public LeftDisableDecorator(List<Date> disabled_dates, Activity context) {
        dl = context.getResources().getDrawable(R.drawable.ic_d_just_start_started);
        this.dates = new ArrayList<>(disabled_dates);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        Calendar cal = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(day.getDate());
        cal.setTime(day.getDate());
        cal2.add( Calendar.DAY_OF_MONTH, 1);
        cal.add( Calendar.DAY_OF_MONTH, -1 );
        Date next = cal2.getTime();
        Date previous = cal.getTime();
        return dates.size()>1 && !dates.contains(previous) && dates.contains(day.getDate()) && dates.contains(next);
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setDaysDisabled(true);
        view.setSelectionDrawable(dl);
    }
}