package com.boxters.android.test;

import com.boxters.android.MainActivity;
import com.boxters.model.Box;
import com.boxters.model.User;

import java.util.ArrayList;

/**
 * Created by giuse on 18/02/2017.
 */

public class BoxList extends ArrayList<Box> {

    public BoxList() {
    }

    public void loadAllUserBoxes(User u ) {
        this.clear();

        if (u != null) {
            Box.loadAsSender(u.getId(), (box) -> {
                if (box != null) {
                    this.add(box);
                }
                else
                    onBoxesLoaded();
            }, false);
        }
    }

    public void onBoxesLoaded() {

    }

}