package com.boxters.android.test;


import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.boxters.android.MainActivity;
import com.boxters.android.R;
import com.boxters.android.ViaggiFragment;
import com.boxters.model.Box;
import com.boxters.model.Transport;
import com.boxters.model.Travel;
import com.boxters.model.User;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import static com.boxters.model.Transport.AIRPLANE;
import static com.boxters.model.Transport.CAR;
import static com.boxters.model.Transport.FEET;
import static com.boxters.model.Transport.TRAIN;

public class ListaViaggiMainCustomArrayAdapter extends ArrayAdapter<Travel> {

    public ViaggiFragment vf;
    private int possibleBoxes;
    public void writeCityName(Context ctx, TextView tv, double lat, double lng){
        Geocoder geoCoder = new Geocoder(ctx, Locale.getDefault());

        try {
            List<Address> addresses = geoCoder.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0)
            {
                tv.setText(addresses.get(0).getLocality().toString());
                System.out.println(addresses.get(0).getLocality());
            }
            else
            {
                // do your staff
            }

        } catch (IOException e) {
            ;
        }
    }

    public ListaViaggiMainCustomArrayAdapter(ViaggiFragment vf, TravelList lv) {
        super(vf.getActivity().getApplicationContext(), R.layout.viaggi_listview_item_main);
        this.vf= vf;
        if (lv != null){
            importa(lv);
        }
    }

    protected void importa(TravelList lv) {
        for (Travel t : lv) {
            this.add(t);
        }
    }


    protected Travel getElement(int pos) {
        return this.getItem(pos);
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        Travel travel = this.getItem(position);


        convertView = vf.getActivity().getLayoutInflater().inflate(R.layout.viaggi_listview_item_main, null);
        SimpleDateFormat localDateFormat = new SimpleDateFormat("MM/dd/yyyy");
        TextView firstLineA = (TextView) convertView.findViewById(R.id.ViaggioFirstLineA);
        TextView firstLineB = (TextView) convertView.findViewById(R.id.ViaggioFirstLineB);

        TextView departureDate = (TextView) convertView.findViewById(R.id.depDate);
        TextView arrivalDate = (TextView) convertView.findViewById(R.id.arrDate);

        departureDate.setText(localDateFormat.format(travel.getStartDate()));
        arrivalDate.setText(localDateFormat.format(travel.getEndDate()));

        Places.GeoDataApi.getPlaceById(MainActivity.mGoogleApiClient, travel.getArrival().getName())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);
                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getLocale());
                            firstLineB.setText(myPlace.getName());
                        } else {
                            Log.e("PLACES DETAILS", "Place not found");
                            Log.e("PLACES DETAILS",places.getStatus().getStatusMessage());
                        }
                        places.release();
                    }
                });
        Places.GeoDataApi.getPlaceById(MainActivity.mGoogleApiClient, travel.getDeparture().getName())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);
                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getLocale());
                            firstLineA.setText(myPlace.getName());
                        } else {
                            Log.e("PLACES DETAILS", "Place not found");
                        }
                        places.release();
                    }
                });
        //writeCityName(vf.getContext(),firstLineB,travel.getArrival().getLat(),travel.getArrival().getLon());
        //writeCityName(vf.getContext(),firstLineA,travel.getDeparture().getLat(),travel.getDeparture().getLon());
        ImageView at = (ImageView) convertView.findViewById(R.id.arrowTravel);

        HashSet<Transport> trset = (HashSet)travel.getTransports();
        if(trset.size()>0){
            Transport firstOfASet = trset.iterator().next();
            switch (firstOfASet){
                case CAR:
                    at.setImageResource(R.drawable.ic_directions_car_blue_24dp);
                    break;
                case FEET:
                    at.setImageResource(R.drawable.ic_directions_run_blue_24dp);
                    break;
                case TRAIN:
                    at.setImageResource(R.drawable.ic_train_blue_24dp);
                    break;
                case AIRPLANE:
                    at.setImageResource(R.drawable.ic_flight_takeoff_blue);
                    break;
                case BUS:
                    at.setImageResource(R.drawable.ic_directions_bus_blue_24dp);
                    break;
            }
        }
        TextView possibleBoxesText = (TextView) convertView.findViewById(R.id.possibleBoxes);
        possibleBoxes = 0;
        possibleBoxesText.setText(possibleBoxes+"");
        travel.loadBoxes((box) ->{
            if(box != null){
                possibleBoxes++;
                possibleBoxesText.setText(possibleBoxes+"");
            }
        },false);

        //TextView secondLine = (TextView) convertView.findViewById(R.id.ViaggioSecondLine);
        //firstLineA.setText(travel.getDeparture().getName().split(",")[0]);
        //firstLineB.setText(travel.getArrival().getName().split(",")[0]);

        //secondLine.setText(localDateFormat.format(travel.getStartDate()));

        return convertView;
    }

    public void loadAllUserTravels(User u ) {
        this.clear();
        notifyDataSetChanged();

        if (u != null) {
            Travel.loadAll(u.getId(), (travel) -> {
                if (travel != null) {

                    this.add(travel);

                    notifyDataSetChanged();
                }else{
                    notifyDataSetChanged();
                }
            }, false);
            vf.hideSwipeUpdate();
            vf.hideProgressBar();

        }
    }
}
