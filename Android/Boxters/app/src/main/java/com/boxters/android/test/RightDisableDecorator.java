package com.boxters.android.test;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import com.boxters.android.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by undeclinable on 11/04/17.
 */

public class RightDisableDecorator implements DayViewDecorator {
    private final Drawable dr;
    private final ArrayList<Date> dates;

    public RightDisableDecorator(List<Date> dates, Activity context) {
        dr = context.getResources().getDrawable(R.drawable.ic_d_just_ended);
        this.dates = new ArrayList<>(dates);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        Calendar cal = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal.setTime(day.getDate());
        cal2.setTime(day.getDate());
        cal2.add( Calendar.DAY_OF_MONTH, -1);
        cal.add( Calendar.DAY_OF_MONTH, 1);
        Date next = cal.getTime();
        Date previous = cal2.getTime();
        return dates.size()>1 && !dates.contains(next) && dates.contains(day.getDate()) && dates.contains(previous);
    }

    @Override
    public void decorate(DayViewFacade view){
        view.setDaysDisabled(true);
        view.setSelectionDrawable(dr);
    }
}