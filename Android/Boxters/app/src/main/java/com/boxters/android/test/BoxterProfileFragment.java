package com.boxters.android.test;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.boxters.android.DownloadImageTask;
import com.boxters.android.ProprietaPaccoActivity;
import com.boxters.android.R;
import com.boxters.model.PublicUser;
import com.boxters.model.Travel;
import com.boxters.model.User;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.SimpleDateFormat;



public class BoxterProfileFragment extends Fragment  implements OnMapReadyCallback {

    private PublicUser boxter;
    private String boxterId;
    private TextView dataPartenza;
    private TextView luogoPartenza;
    private TextView dataArrivo;
    private TextView luogoArrivo;
    private TextView spazioDisponibile;
    private SupportMapFragment supportMapFragment;
    public GoogleMap mMap;
    private Polyline percorso;
    public Marker daMark;
    public Marker aMark;
    private Activity a;
    private TextView mezzoUtilizzato;
    private static final String OUT_JSON = "/json";
    private Context mContext;
    private static final String RADIUS = "10";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place/nearbysearch";
    private static String API_KEY = "AIzaSyDh7RzrrF5TR0Hi-AtHGtvUAVMc-lIbjUI";
    LinearLayout fromLayout;
    LinearLayout toLayout;
    public BoxterProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMap == null) {
            supportMapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMap == null) {
            supportMapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mContext = getActivity();

        FragmentManager fm = getActivity().getSupportFragmentManager();/// getChildFragmentManager();
        supportMapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
        if (supportMapFragment == null) {
            supportMapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map_container, supportMapFragment).commit();
        }
        supportMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        boolean success = googleMap.setMapStyle(new MapStyleOptions(getResources()
                .getString(R.string.map_style_json)));
        if (!success) {
            Log.e("MAP_STYLAH", "Style parsing failed.");
        }
        mMap = googleMap;
        if(ProprietaPaccoActivity.mytravel!= null){
            LatLng from = new LatLng(ProprietaPaccoActivity.mytravel.getDeparture().getLat(),ProprietaPaccoActivity.mytravel.getDeparture().getLon());
            daMark = mMap.addMarker(new MarkerOptions().position(from).title(getString(R.string.Origin)));
            LatLng to = new LatLng(ProprietaPaccoActivity.mytravel.getArrival().getLat(),ProprietaPaccoActivity.mytravel.getArrival().getLon());
            aMark = mMap.addMarker(new MarkerOptions().position(to).title(getString(R.string.Destination)));
            percorso = mMap.addPolyline(new PolylineOptions().add(from,to).width(8).color(Color.rgb(18, 96, 147)));
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(daMark.getPosition());
            builder.include(aMark.getPosition());
            LatLngBounds bounds = builder.build();
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;
            int padding = (int) (width * 0.33);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
            mMap.moveCamera(cameraUpdate);
            float zoom = mMap.getCameraPosition().zoom;
            mMap.animateCamera( CameraUpdateFactory.zoomTo(zoom - 1));
            mMap.getUiSettings().setScrollGesturesEnabled(false);
            mMap.getUiSettings().setZoomControlsEnabled(false);
        }else{

            Log.d("MY_TRAVEL_FOR_MAP","TRAVEL IN RECEIVER ACTIVITY IS NULL");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_boxter_profile, container, false);

        if(ProprietaPaccoActivity.boxAttuale != null){
            boxterId = ProprietaPaccoActivity.boxAttuale.getBoxter();
        }

        if(boxterId != null){
            PublicUser.load(boxterId, (PublicUser u) -> {
                if(u != null){
                    boxter = u;
                    ImageView boxter_icon = (ImageView) view.findViewById(R.id.boxter_profile_photo);
                    if (boxter.getPhoto() != null) {
                        if (boxter_icon != null)
                            new DownloadImageTask(boxter_icon).execute(boxter.getPhoto());
                    }
                    RatingBar rating = (RatingBar) view.findViewById(R.id.ratingBar);
                    rating.setMax(5);
                    if(boxter.getRating()==null){
                        double rating_s1 = 0;
                        rating.setRating( (float) rating_s1);
                    }else{
                        double rating_s2 = boxter.getRating();
                        rating.setRating( (float) rating_s2);
                    }
                    TextView nome_utente = (TextView) view.findViewById(R.id.user_profile_name);
                    TextView email_utente = (TextView) view.findViewById(R.id.user_profile_email);
                    nome_utente.setText(boxter.getName());
                    email_utente.setText(boxter.getEmail());
                    if(ProprietaPaccoActivity.mytravel != null){
                        LatLng arr = new LatLng(ProprietaPaccoActivity.mytravel.getArrival().getLat(),ProprietaPaccoActivity.mytravel.getArrival().getLon());
                        LatLng dep = new LatLng(ProprietaPaccoActivity.mytravel.getDeparture().getLat(),ProprietaPaccoActivity.mytravel.getDeparture().getLon());

                        luogoArrivo = (TextView) view.findViewById(R.id.posizioneArrivo);
                        luogoPartenza = (TextView) view.findViewById(R.id.posizionePartenza);

                        SimpleDateFormat localDateFormat = new SimpleDateFormat("MM/dd/yyyy");


                        Places.GeoDataApi.getPlaceById(ProprietaPaccoActivity.mGoogleApiClient, ProprietaPaccoActivity.mytravel.getArrival().getName())
                                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                                    @Override
                                    public void onResult(PlaceBuffer places) {
                                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                            final Place myPlace = places.get(0);
                                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getAddress());
                                            luogoArrivo.setText(myPlace.getName().toString());
                                        } else {
                                            Log.e("PLACES DETAILS", "Place not found");
                                        }
                                        places.release();
                                    }
                                });
                        Places.GeoDataApi.getPlaceById(ProprietaPaccoActivity.mGoogleApiClient, ProprietaPaccoActivity.mytravel.getDeparture().getName())
                                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                                    @Override
                                    public void onResult(PlaceBuffer places) {
                                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                            final Place myPlace = places.get(0);
                                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getAddress());
                                            luogoPartenza.setText(myPlace.getName().toString());
                                        } else {
                                            Log.e("PLACES DETAILS", "Place not found");
                                        }
                                        places.release();
                                    }
                                });
                        dataPartenza = (TextView) view.findViewById(R.id.dataPartenza);
                        dataPartenza.setText(localDateFormat.format(ProprietaPaccoActivity.mytravel.getStartDate()));

                        fromLayout = (LinearLayout) view.findViewById(R.id.from_layout);
                        toLayout = (LinearLayout) view.findViewById(R.id.to_layout);

                        dataArrivo = (TextView) view.findViewById(R.id.dataArrivo);
                        dataArrivo.setText(localDateFormat.format(ProprietaPaccoActivity.mytravel.getEndDate()));

                        LatLng arrVicinity = new LatLng(0,0);
                        LatLng depVicinity = new LatLng(0,0);
                        String vicinity_from = "";
                        String vicinity_to = "";
                        new FindVicinityTask(vicinity_to).execute(arr);
                        new FindVicinityTask(vicinity_from).execute(dep);
                        String arrId = "";
                        String depId = "";
                        new FindIdTask(arrId).execute(arr);
                        new FindIdTask(depId).execute(dep);
                        LinearLayout card_layout = (LinearLayout) view.findViewById(R.id.arr_dep_layout);
                        //ImageView from_bkg = (ImageView) view.findViewById(R.id.from_bkg);
                        //ImageView to_bkg = (ImageView) view.findViewById(R.id.to_bkg);
                        ViewTreeObserver vto =  card_layout.getViewTreeObserver();
                        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                card_layout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            }
                        });

                        ImageView travelState = (ImageView) view.findViewById(R.id.travel_state_img);

                        switch (ProprietaPaccoActivity.mytravel.getState()){
                            case CREATED:
                                travelState.setImageResource(R.drawable.ic_hourglass_full_blue_24dp);
                                break;
                            case STARTED:
                                travelState.setImageResource(R.drawable.ic_my_location_blue_24dp);
                                break;
                        }

                        spazioDisponibile = (TextView) view.findViewById(R.id.spazioDisponibile);

                        ImageView mezzoImg = (ImageView) view.findViewById(R.id.transImg);


        /*
        Temporaneo e NON SCALABILE. Dobbiamo parametrizzare gli strings array  multilingua
         */

                        switch(ProprietaPaccoActivity.mytravel.getTransports().toString().replace("[","").replace("]","")){
                            case "PULLMAN":
                                mezzoImg.setImageResource(R.drawable.ic_directions_bus_blue_24dp);
                                break;
                            case "BUS":
                                mezzoImg.setImageResource(R.drawable.ic_directions_bus_blue_24dp);
                                break;
                            case "AEREO":
                                mezzoImg.setImageResource(R.drawable.ic_flight_takeoff_blue);
                                break;
                            case "AIRPLANE":
                                mezzoImg.setImageResource(R.drawable.ic_flight_takeoff_blue);
                                break;
                            case "AUTO":
                                mezzoImg.setImageResource(R.drawable.ic_directions_car_blue_24dp);
                                break;
                            case "CAR":
                                mezzoImg.setImageResource(R.drawable.ic_directions_car_blue_24dp);
                                break;
                            case "TRENO":
                                mezzoImg.setImageResource(R.drawable.ic_train_blue_24dp);
                                break;
                            case "TRAIN":
                                mezzoImg.setImageResource(R.drawable.ic_train_blue_24dp);
                                break;
                        }

                        if(ProprietaPaccoActivity.mytravel.getSpace() == 10.00){
                            spazioDisponibile.setText("S");
                            //spazioDisponibile.setText(getResources().getStringArray(R.array.spazio)[0].replace("[","").replace("]",""));
                        }
                        if(ProprietaPaccoActivity.mytravel.getSpace() == 50.00){
                            spazioDisponibile.setText("M");
                            //spazioDisponibile.setText(getResources().getStringArray(R.array.spazio)[1].replace("[","").replace("]",""));
                        }
                        if(ProprietaPaccoActivity.mytravel.getSpace() == 100.00){
                            spazioDisponibile.setText("L");
                            //spazioDisponibile.setText(getResources().getStringArray(R.array.spazio)[2].replace("[","").replace("]",""));
                        }
                    }else{
                        Log.e("MY_TRAVEL","MY TRAVEL E' NULL");
                    }

                }
            }, false);
        }
        return view;
    }


}
