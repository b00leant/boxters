package com.boxters.android.test;

import android.content.Intent;
import android.media.Rating;
import android.media.effect.Effect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.boxters.android.DownloadImageTask;
import com.boxters.android.MainActivity;
import com.boxters.android.R;
import com.boxters.android.controller.messaging.MessagingService;
import com.boxters.model.Box;
import com.boxters.model.BoxState;
import com.boxters.model.PublicUser;
import com.boxters.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProprietaFeedbackActivity extends AppCompatActivity {
    private RatingBar ratingBar;
    private EditText comment;
    private CircleImageView boxterImg;
    private Box pacco;
    private User boxter;
    private Button btnInviaFeedback;
    private static final int CONST_RATING = 2;
    private String boxID;
    private TextView boxter_profile_name;
    private TextView user_profile_name;

    @Override
    public void onBackPressed() {
            Intent startNewActivity = new Intent( this,MainActivity.class);
            finishAffinity();
            startActivity(startNewActivity);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();

        setContentView(R.layout.activity_proprieta_feedback);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar2);
        comment = (EditText) findViewById(R.id.feedback_text);
        boxterImg = (CircleImageView) findViewById(R.id.boxter_profile_photo);
        user_profile_name = (TextView) findViewById(R.id.user_profile_name);
        boxter_profile_name = (TextView) findViewById(R.id.boxter_profile_name);
        btnInviaFeedback = (Button) findViewById(R.id.btnInviaFeedback);
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();
        Bundle extras = intent.getExtras();
        boxID = extras.getString(MessagingService.BOX_ID_KEY);
        if (user != null) {
            User.load(user.getUid(),(User _user) -> {
                Box.load(_user.getId(),boxID,(Box box)->{
                    if(box != null){
                        pacco = box;
                        box.setState(BoxState.RECEIVED,(completion)->{

                        });
                        User.load(box.getBoxter(),(User _puser)->{
                            if(_puser != null){
                                boxter = _puser;
                                btnInviaFeedback.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if(boxter.getRating()== null){
                                            double newr = (0 * CONST_RATING) + (1 - CONST_RATING);
                                            boxter.setRating(newr, (completion)->{
                                                Toast.makeText(ProprietaFeedbackActivity.this,getString(R.string.Tnanks_for_feedback),Toast.LENGTH_LONG).show();
                                            });
                                        }else{
                                            double oldr = boxter.getRating();
                                            double newr = (oldr * CONST_RATING) + (1 - CONST_RATING);
                                            boxter.setRating(newr, (completion)->{
                                                Toast.makeText(ProprietaFeedbackActivity.this,getString(R.string.Tnanks_for_feedback),Toast.LENGTH_LONG).show();
                                            });
                                        }
                                        Intent startNewActivity = new Intent(ProprietaFeedbackActivity.this, MainActivity.class);
                                        finishAffinity();
                                        startActivity(startNewActivity);
                                    }
                                });
                                boxter_profile_name.setText(_puser.getName());
                                user_profile_name.setText(_puser.getEmail());
                                if(_puser.getPhoto() != null){
                                    if (boxterImg != null)
                                        new DownloadImageTask(boxterImg).execute(_puser.getPhoto());
                                }
                            }
                        },false);
                    }
                },false);
            },false);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setContentView(R.layout.activity_proprieta_feedback);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar2);
        comment = (EditText) findViewById(R.id.feedback_text);
        boxterImg = (CircleImageView) findViewById(R.id.boxter_profile_photo);
        user_profile_name = (TextView) findViewById(R.id.user_profile_name);
        boxter_profile_name = (TextView) findViewById(R.id.boxter_profile_name);
        btnInviaFeedback = (Button) findViewById(R.id.btnInviaFeedback);
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();
        Bundle extras = intent.getExtras();
        boxID = extras.getString(MessagingService.BOX_ID_KEY);
        if (user != null) {
            User.load(user.getUid(),(User _user) -> {
                Box.load(_user.getId(),boxID,(Box box)->{
                    pacco = box;
                    box.setState(BoxState.RECEIVED,(completion)->{

                    });
                    User.load(box.getBoxter(),(User _puser)->{
                        if(_puser != null){
                            boxter = _puser;
                            btnInviaFeedback.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if(boxter.getRating()== null){
                                        double newr = (0 * CONST_RATING) + (1 - CONST_RATING);
                                        boxter.setRating(newr, (completion)->{
                                            Toast.makeText(ProprietaFeedbackActivity.this,getString(R.string.Tnanks_for_feedback),Toast.LENGTH_LONG).show();
                                        });
                                    }else{
                                        double oldr = boxter.getRating();
                                        double newr = (oldr * CONST_RATING) + (1 - CONST_RATING);
                                        boxter.setRating(newr, (completion)->{
                                            Toast.makeText(ProprietaFeedbackActivity.this,getString(R.string.Tnanks_for_feedback),Toast.LENGTH_LONG).show();
                                        });
                                    }
                                    Intent startNewActivity = new Intent(ProprietaFeedbackActivity.this, MainActivity.class);
                                    finishAffinity();
                                    startActivity(startNewActivity);
                                }
                            });
                            boxter_profile_name.setText(_puser.getName());
                            user_profile_name.setText(_puser.getEmail());
                            if(_puser.getPhoto() != null){
                                if (boxterImg != null)
                                    new DownloadImageTask(boxterImg).execute(_puser.getPhoto());
                            }
                        }
                    },false);
                },false);
            },false);
        }
    }
}
