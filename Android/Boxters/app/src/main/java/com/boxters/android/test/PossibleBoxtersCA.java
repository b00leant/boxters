package com.boxters.android.test;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.boxters.android.DownloadImageTask;
import com.boxters.android.MainActivity;
import com.boxters.android.Manifest;
import com.boxters.android.ProprietaPaccoActivity;
import com.boxters.android.ProprietaViaggioActivity;
import com.boxters.android.R;
import com.boxters.model.Box;
import com.boxters.model.BoxState;
import com.boxters.model.PublicTravel;
import com.boxters.model.PublicUser;
import com.boxters.model.User;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import static com.facebook.FacebookSdk.getApplicationContext;


public class PossibleBoxtersCA extends ArrayAdapter<PublicTravel> {
    public ViaggiatoriQueryFragment ma;
    //public PossibleBoxtersFragment ma;
    private Context mContext;
    private User myuser;
    private ProprietaPaccoActivity activity;

    public void writeCityName(Context ctx, TextView tv, double lat, double lng){
        Geocoder geoCoder = new Geocoder(ctx, Locale.getDefault());

        try {
            List<Address> addresses = geoCoder.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0)
            {
                tv.setText(addresses.get(0).getLocality().toString());
                System.out.println(addresses.get(0).getLocality());
            }
            else
            {
                // do your staff
            }

        } catch (IOException e) {
            ;
        }
    }
    /* STEFANO */
    /*

    sto rendendo la ProprietaPaccoActivity più snella
    accorpando le PossibleBoxtersCA insieme a LVQCA

     */
    /* OLD
    public PossibleBoxtersCA(PossibleBoxtersFragment _ma, PublicTravelList tl) {
        super(_ma.getActivity().getApplicationContext(), R.layout.potential_boxters_list_item_card);
        ma = _ma;
        if (tl != null)
            importa(tl);
    }

    */

    public PossibleBoxtersCA(ViaggiatoriQueryFragment _ma, PublicTravelList tl) {
        super(_ma.getActivity().getApplicationContext(), R.layout.potential_boxters_list_item_card);
        ma = _ma;
        if (tl != null)
            importa(tl);
    }
    protected void importa(PublicTravelList tl) {
        for (PublicTravel t : tl) {
            this.add(t);
        }
    }

    protected PublicTravel getElement(int pos) {
        return this.getItem(pos);
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        PublicTravel t = this.getItem(position);

        convertView = ma.getActivity().getLayoutInflater().inflate(R.layout.potential_boxters_list_item_card, null);
        String tel;
        TextView firstLine = (TextView) convertView.findViewById(R.id.firstLine);
        TextView secondLineA = (TextView) convertView.findViewById(R.id.secondLineA);
        TextView secondLineB = (TextView) convertView.findViewById(R.id.secondLineB);
        TextView timeLineA = (TextView) convertView.findViewById(R.id.timeLineA);
        TextView timeLineB = (TextView) convertView.findViewById(R.id.timeLineB);
        ImageButton button = (ImageButton) convertView.findViewById(R.id.confirm_button);
        ImageButton button2 = (ImageButton) convertView.findViewById(R.id.call_button);
        RatingBar ratingBarA = (RatingBar) convertView.findViewById(R.id.ratingBarA);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alert = new AlertDialog.Builder(ma.getActivity());
                alert.setTitle(ma.getActivity().getString(R.string.confirm));
                alert.setMessage(ma.getActivity().getString(R.string.dialog_confirm_boxter));
                alert.setNegativeButton(ma.getActivity().getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(
                                    DialogInterface dialog,
                                    int whichButton) {
                                dialog.cancel();
                            }
                        });
                alert.setPositiveButton(ma.getActivity().getString(R.string.Sure),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(
                                    DialogInterface dialog,
                                    int whichButton) { /* FARE COSE */

                                if (t != null) {

                                    ProprietaPaccoActivity.boxAttuale.setTravel(t,(save) -> {

                                        Toast.makeText(ma.getContext(),ma.getString(R.string.NewBoxter),Toast.LENGTH_LONG).show();

                                        Box.load(ProprietaPaccoActivity.boxAttuale.getSender(),ProprietaPaccoActivity.boxAttuale.getId(),(Box box)->{
                                            if(box != null){
                                                ProprietaPaccoActivity.boxAttuale = box;
                                                PublicTravel.load(box.getBoxter(),box.getTravelId(),(PublicTravel travel)->{
                                                    ProprietaPaccoActivity.mytravel = travel;
                                                    Intent startNewActivity = new Intent(ma.getContext(),ProprietaPaccoActivity.class);
                                                    ma.getActivity().finishAffinity();
                                                    ma.getActivity().startActivity(startNewActivity);
                                                },false);
                                            }
                                        },false);
                                    });

                                }


                            }
                        });
                alert.create().show(); // btw show() creates and shows it..
            }
        });


        View finalConvertView = convertView;
        User.load(t.getBoxter(),(User user) -> {
            if(user != null){
                myuser = user;
                ratingBarA.setRating(user.getRating() != null ? user.getRating().floatValue() : 0);
                firstLine.setText(user.getName());
                if (user.getPhoto() != null) {
                    ImageView iconViaggiatore = (ImageView) finalConvertView.findViewById(R.id.iconViaggiatore);
                    if (iconViaggiatore != null)
                        new DownloadImageTask(iconViaggiatore).execute(user.getPhoto());
                }
            }
        },false);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_CALL); //use ACTION_CALL class
                callIntent.setData(Uri.parse("tel:"+myuser.getPhone().trim()));    //this is the phone number calling
                //check permission
                //If the device is running Android 6.0 (API level 23) and the app's targetSdkVersion is 23 or higher,
                //the system asks the user to grant approval.
                if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    //request permission from user if the app hasn't got the required permission
                    ActivityCompat.requestPermissions(ma.getActivity(),
                            new String[]{android.Manifest.permission.CALL_PHONE},   //request specific permission from user
                            10);
                    return;
                }else {     //have got permission
                    try{
                        ma.getActivity().startActivity(callIntent);  //call activity and make phone call
                    }
                    catch (android.content.ActivityNotFoundException ex){
                        Toast.makeText(getApplicationContext(),"yourActivity is not founded",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        ImageView mezzoImg = (ImageView) finalConvertView.findViewById(R.id.mezzoImg);
        TextView spazioDisponibile = (TextView) finalConvertView.findViewById(R.id.spazio);

        /*
        Temporaneo e NON SCALABILE. Dobbiamo parametrizzare gli strings array  multilingua
         */

        switch(t.getTransports().toString().replace("[","").replace("]","")){
            case "PULLMAN":
                mezzoImg.setImageResource(R.drawable.ic_directions_bus_blue_24dp);
                break;
            case "BUS":
                mezzoImg.setImageResource(R.drawable.ic_directions_bus_blue_24dp);
                break;
            case "AEREO":
                mezzoImg.setImageResource(R.drawable.ic_flight_takeoff_blue);
                break;
            case "AIRPLANE":
                mezzoImg.setImageResource(R.drawable.ic_flight_takeoff_blue);
                break;
            case "AUTO":
                mezzoImg.setImageResource(R.drawable.ic_directions_car_blue_24dp);
                break;
            case "CAR":
                mezzoImg.setImageResource(R.drawable.ic_directions_car_blue_24dp);
                break;
            case "TRENO":
                mezzoImg.setImageResource(R.drawable.ic_train_blue_24dp);
                break;
            case "TRAIN":
                mezzoImg.setImageResource(R.drawable.ic_train_blue_24dp);
                break;
        }

        if(t.getSpace() == 10.00){
            spazioDisponibile.setText("S");
            //spazioDisponibile.setText(getResources().getStringArray(R.array.spazio)[0].replace("[","").replace("]",""));
        }
        if(t.getSpace() == 50.00){
            spazioDisponibile.setText("M");
            //spazioDisponibile.setText(getResources().getStringArray(R.array.spazio)[1].replace("[","").replace("]",""));
        }
        if(t.getSpace() == 100.00){
            spazioDisponibile.setText("L");
            //spazioDisponibile.setText(getResources().getStringArray(R.array.spazio)[2].replace("[","").replace("]",""));
        }

        SimpleDateFormat localDateFormat = new SimpleDateFormat("MM/dd/yyyy");

        Places.GeoDataApi.getPlaceById(ProprietaPaccoActivity.mGoogleApiClient, t.getArrival().getName())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);
                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                            secondLineB.setText(myPlace.getName());
                        } else {
                            Log.e("PLACES DETAILS", "Place not found");
                            Log.e("PLACES DETAILS",places.getStatus().getStatusMessage());
                        }
                        places.release();
                    }
                });
        Places.GeoDataApi.getPlaceById(ProprietaPaccoActivity.mGoogleApiClient, t.getDeparture().getName())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);
                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                            secondLineA.setText(myPlace.getName());
                        } else {
                            Log.e("PLACES DETAILS", "Place not found");
                        }
                        places.release();
                    }
                });

        //writeCityName(ma.getContext(),secondLineA,t.getDeparture().getLat(),t.getDeparture().getLon());
        //writeCityName(ma.getContext(),secondLineB,t.getArrival().getLat(),t.getArrival().getLon());
        //secondLineB.setText("to " + t.getArrival().getName());
        timeLineA.setText(localDateFormat.format(t.getStartDate()));
        timeLineB.setText(localDateFormat.format(t.getEndDate()));


        return convertView;
    }

}
