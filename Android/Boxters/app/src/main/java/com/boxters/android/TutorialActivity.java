package com.boxters.android;

import android.annotation.SuppressLint;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.vision.text.Line;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class TutorialActivity extends AppCompatActivity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private int currentStep = 1;
    private static final int MODE_BOX = 0;
    private static final int MODE_TRAVEL = 1;
    private int mode;
    private boolean animated = false;
    private ImageButton ic_box_boxters;
    private ImageButton ic_directions_blue_24dp;
    private LinearLayout steps_layout;
    private LinearLayout meaning_icons_travel;
    private LinearLayout meaning_icons_box;
    private LinearLayout use_layout_box;
    private LinearLayout two_images_layout;
    private ImageView single_image;
    private ImageView halfimagebottom;
    private ImageView halfimagetop;
    private ImageView done_qr;
    private ImageButton next_step;
    private ImageButton prev_step;
    private TextView center_explanation;
    private TextView step_header;
    private TextView step1;
    private TextView step2;
    private TextView step3;
    private TextView step4;
    private TextView step5;
    private TextView step6;
    private Animation a;
    private Animation b;

    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private View mControlsView;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tutorial2);

        mVisible = true;
        mControlsView = findViewById(R.id.fullscreen_content_controls);
        mContentView = findViewById(R.id.how_to_text);
        ic_box_boxters = (ImageButton) findViewById(R.id.ic_box_boxters);
        ic_directions_blue_24dp = (ImageButton) findViewById(R.id.ic_directions_blue_24dp);
        steps_layout = (LinearLayout) findViewById(R.id.steps_layout);
        next_step = (ImageButton) findViewById(R.id.next_step);
        prev_step = (ImageButton) findViewById(R.id.prev_step);
        use_layout_box = (LinearLayout) findViewById(R.id.use_layout_box);
        step_header = (TextView) findViewById(R.id.step_header);
        meaning_icons_box = (LinearLayout) findViewById(R.id.meaning_icons_box);
        meaning_icons_travel = (LinearLayout) findViewById(R.id.meaning_icons_travel);
        two_images_layout = (LinearLayout) findViewById(R.id.two_images_layout);
        single_image = (ImageView) findViewById(R.id.single_image);
        halfimagebottom = (ImageView) findViewById(R.id.halfimagebottom);
        halfimagetop = (ImageView) findViewById(R.id.halfimagetop);
        done_qr = (ImageView) findViewById(R.id.done_qr);
        center_explanation = (TextView) findViewById(R.id.center_explanation);
        step1 = (TextView) findViewById(R.id.step1);
        step2 = (TextView) findViewById(R.id.step2);
        step3 = (TextView) findViewById(R.id.step3);
        step4 = (TextView) findViewById(R.id.step4);
        step5 = (TextView) findViewById(R.id.step5);
        step6 = (TextView) findViewById(R.id.step6);
        a = AnimationUtils.loadAnimation(this, R.anim.scale_circle);
        b = AnimationUtils.loadAnimation(this, R.anim.scale_circle_2);

        //Setto il listener per iniziare il tutorial come Mittente
        ic_box_boxters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mode = MODE_BOX;
                mControlsView.setVisibility(View.GONE);
                meaning_icons_travel.setVisibility(View.GONE);
                meaning_icons_box.setVisibility(View.VISIBLE);
                steps_layout.setVisibility(View.VISIBLE);
            }
        });

        //Setto il listener per iniziare il tutorial come Boxter
        ic_directions_blue_24dp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mode = MODE_TRAVEL;
                mControlsView.setVisibility(View.GONE);
                meaning_icons_travel.setVisibility(View.VISIBLE);
                meaning_icons_box.setVisibility(View.GONE);
                steps_layout.setVisibility(View.VISIBLE);
            }
        });

        next_step.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (currentStep){
                    case 1:
                        single_image.setVisibility(View.VISIBLE);
                        meaning_icons_travel.setVisibility(View.GONE);
                        meaning_icons_box.setVisibility(View.GONE);
                        switch (mode){
                            //MOSTRARE MAPPA
                            case MODE_BOX:
                                single_image.setImageResource(R.drawable.mapdrawable);
                                step_header.setText(getString(R.string.TUTORIAL_HEADER_FORM_BOX));
                                break;
                            //MOSTRARE MAPA
                            case MODE_TRAVEL:
                                single_image.setImageResource(R.drawable.mapdrawable);
                                step_header.setText(getString(R.string.TUTORIAL_HEADER_FORM_TRAVEL));
                                break;
                        }
                        step1.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.tw__solid_white));
                        step2.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.colorAccent));
                        currentStep = 2;
                        meaning_icons_box.setVisibility(View.GONE);
                        use_layout_box.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        switch (mode){
                            //MOSTRARE FORM
                            case MODE_BOX:
                                single_image.setImageResource(R.drawable.formdrawable);
                                step_header.setText(getString(R.string.TUTORIAL_HEADER_FORM2_BOX));
                                break;
                            //MOSTRARE FORM
                            case MODE_TRAVEL:
                                single_image.setImageResource(R.drawable.formdrawable);
                                step_header.setText(getString(R.string.TUTORIAL_HEADER_FORM2_TRAVEL));
                                break;
                        }
                        step2.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.tw__solid_white));
                        step3.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.colorAccent));
                        currentStep = 3;
                        break;
                    case 3:
                        single_image.setVisibility(View.GONE);
                        two_images_layout.setVisibility(View.VISIBLE);
                        switch (mode){
                            //MOSTRARE RICHIESTA
                            case MODE_BOX:
                                step_header.setText(getString(R.string.TUTORIAL_HEADER_REQUEST_BOX));
                                halfimagetop.setImageResource(R.drawable.requesttravel_draeable);
                                center_explanation.setText(getString(R.string.TUTORIAL_HEADER_REQUEST2_BOX));
                                halfimagebottom.setImageResource(R.drawable.confirmtravel_drawable);
                                break;
                            //MOSTRARE RICHIESTA
                            case MODE_TRAVEL:
                                step_header.setText(getString(R.string.TUTORIAL_HEADER_REQUEST_TRAVEL));
                                halfimagetop.setImageResource(R.drawable.requestbox_drawable);
                                center_explanation.setText(getString(R.string.TUTORIAL_HEADER_REQUEST2_TRAVEL));
                                halfimagebottom.setImageResource(R.drawable.confirmbox_drawable);
                                break;
                        }
                        step3.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.tw__solid_white));
                        step4.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.colorAccent));
                        currentStep = 4;
                        break;
                    case 4:
                        halfimagetop.setImageResource(R.drawable.notificationdrawable);
                        switch (mode){
                            //MOSTRARE NOTIFICA
                            case MODE_BOX:
                                step_header.setText(getString(R.string.TUTORIAL_HEADER_NOTIFICATION_BOX));
                                center_explanation.setText(getString(R.string.TUTORIAL_HEADER_NOTIFICATION2_BOX));
                                halfimagebottom.setImageResource(R.drawable.notificationbuttondrawable);
                                break;
                            //MOSTRARE NOTIFICA
                            case MODE_TRAVEL:
                                step_header.setText(getString(R.string.TUTORIAL_HEADER_NOTIFICATION_TRAVEL));
                                center_explanation.setText(getString(R.string.TUTORIAL_HEADER_NOTIFICATION2_TRAVEL));
                                halfimagebottom.setImageResource(R.drawable.notificationtravelbutton);
                                break;
                        }
                        step4.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.tw__solid_white));
                        step5.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.colorAccent));
                        currentStep = 5;
                        break;
                    case 5:
                        single_image.setVisibility(View.VISIBLE);
                        two_images_layout.setVisibility(View.GONE);
                        switch (mode){
                            //MOSTRARE QR
                            case MODE_BOX:
                                single_image.setImageResource(R.drawable.qrdrawablephone);
                                step_header.setText(getString(R.string.TUTORIAL_HEADER_QR));
                                break;
                            //MOSTRARE SCAN
                            case MODE_TRAVEL:
                                single_image.setImageResource(R.drawable.phonedrawable);

                                if(animated == false){
                                    animated = true;
                                    new CountDownTimer(2000, 1000) { //40000 milli seconds is total time, 1000 milli seconds is time interval

                                        public void onTick(long millisUntilFinished) {
                                        }
                                        public void onFinish() {
                                            if(currentStep==6){
                                                done_qr.setVisibility(View.VISIBLE);
                                                done_qr.clearAnimation();
                                                done_qr.startAnimation(a);
                                                new CountDownTimer(2000, 1000) { //40000 milli seconds is total time, 1000 milli seconds is time interval

                                                    public void onTick(long millisUntilFinished) {
                                                    }
                                                    public void onFinish() {
                                                        if(currentStep == 6){
                                                            done_qr.setImageResource(R.drawable.ic_done_all_white_24dp);
                                                            done_qr.startAnimation(b);
                                                        }
                                                    }
                                                }.start();
                                            }
                                        }
                                    }.start();
                                }
                                step_header.setText(getString(R.string.TUTORIAL_HEADER_SCAN));
                                break;
                        }
                        step5.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.tw__solid_white));
                        step6.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.colorAccent));
                        currentStep = 6;
                        break;
                    case 6:
                        animated = false;
                        done_qr.setVisibility(View.GONE);
                        done_qr.setImageResource(R.drawable.ic_done_white_24dp);
                        step6.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.tw__solid_white));
                        step1.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.colorAccent));
                        meaning_icons_box.setVisibility(View.VISIBLE);
                        use_layout_box.setVisibility(View.GONE);
                        currentStep = 1;
                        mControlsView.setVisibility(View.VISIBLE);
                        steps_layout.setVisibility(View.GONE);
                        break;
                }
            }
        });

        prev_step.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (currentStep){
                    case 1:
                        switch (mode){
                            case MODE_BOX:
                                meaning_icons_travel.setVisibility(View.GONE);
                                meaning_icons_box.setVisibility(View.VISIBLE);
                                break;
                            case MODE_TRAVEL:
                                meaning_icons_travel.setVisibility(View.VISIBLE);
                                meaning_icons_box.setVisibility(View.GONE);
                                break;
                        }
                        use_layout_box.setVisibility(View.GONE);
                        done_qr.setImageResource(R.drawable.ic_done_all_white_24dp);
                        done_qr.setVisibility(View.GONE);
                        currentStep = 1;
                        mControlsView.setVisibility(View.VISIBLE);
                        steps_layout.setVisibility(View.GONE);
                        break;
                    case 2:
                        single_image.setVisibility(View.GONE);
                        meaning_icons_box.setVisibility(View.VISIBLE);
                        switch (mode){
                            //MOSTRARE SIGNIFICATO ICONE
                            case MODE_BOX:
                                break;
                            //MOSTRARE SIGNIFICATO ICONE
                            case MODE_TRAVEL:
                                break;
                        }
                        step1.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.colorAccent));
                        step2.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.tw__solid_white));
                        meaning_icons_box.setVisibility(View.VISIBLE);
                        use_layout_box.setVisibility(View.GONE);
                        currentStep = 1;
                        break;
                    case 3:
                        switch (mode){
                            //MOSTRARE MAPPA
                            case MODE_BOX:
                                single_image.setImageResource(R.drawable.mapdrawable);
                                step_header.setText(getString(R.string.TUTORIAL_HEADER_FORM_BOX));
                                break;
                            //MOSTRARE MAPPA
                            case MODE_TRAVEL:
                                single_image.setImageResource(R.drawable.mapdrawable);
                                step_header.setText(getString(R.string.TUTORIAL_HEADER_FORM_TRAVEL));
                                break;
                        }
                        step2.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.colorAccent));
                        step3.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.tw__solid_white));
                        currentStep = 2;
                        break;
                    case 4:
                        two_images_layout.setVisibility(View.GONE);
                        single_image.setVisibility(View.VISIBLE);
                        switch (mode){
                            //MOSTRARE FORM
                            case MODE_BOX:
                                single_image.setImageResource(R.drawable.formdrawable);
                                step_header.setText(getString(R.string.TUTORIAL_HEADER_FORM2_BOX));
                                break;
                            //MOSTRARE FORM
                            case MODE_TRAVEL:
                                single_image.setImageResource(R.drawable.formdrawable);
                                step_header.setText(getString(R.string.TUTORIAL_HEADER_FORM2_TRAVEL));
                                break;
                        }
                        step3.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.colorAccent));
                        step4.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.tw__solid_white));
                        currentStep = 3;
                        break;
                    case 5:
                        switch (mode){
                            //MOSTRARE RICHIESTA
                            case MODE_BOX:
                                halfimagetop.setImageResource(R.drawable.requesttravel_draeable);
                                step_header.setText(getString(R.string.TUTORIAL_HEADER_REQUEST_BOX));
                                center_explanation.setText(getString(R.string.TUTORIAL_HEADER_REQUEST2_BOX));
                                halfimagebottom.setImageResource(R.drawable.confirmtravel_drawable);
                                break;
                            //MOSTRARE RICHIESTA
                            case MODE_TRAVEL:
                                halfimagetop.setImageResource(R.drawable.requestbox_drawable);
                                step_header.setText(getString(R.string.TUTORIAL_HEADER_REQUEST_TRAVEL));
                                center_explanation.setText(getString(R.string.TUTORIAL_HEADER_REQUEST2_TRAVEL));
                                halfimagebottom.setImageResource(R.drawable.confirmbox_drawable);
                                break;
                        }
                        currentStep = 4;
                        step4.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.colorAccent));
                        step5.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.tw__solid_white));
                        break;
                    case 6:
                        animated = false;
                        done_qr.setVisibility(View.GONE);
                        done_qr.setImageResource(R.drawable.ic_done_white_24dp);
                        single_image.setVisibility(View.GONE);
                        two_images_layout.setVisibility(View.VISIBLE);
                        halfimagetop.setImageResource(R.drawable.notificationdrawable);
                        switch (mode){
                            //NOTIFICA
                            case MODE_BOX:
                                step_header.setText(getString(R.string.TUTORIAL_HEADER_NOTIFICATION_BOX));
                                center_explanation.setText(getString(R.string.TUTORIAL_HEADER_NOTIFICATION2_BOX));
                                halfimagebottom.setImageResource(R.drawable.notificationbuttondrawable);
                                break;
                            //NOTIFICA
                            case MODE_TRAVEL:
                                step_header.setText(getString(R.string.TUTORIAL_HEADER_NOTIFICATION_TRAVEL));
                                center_explanation.setText(getString(R.string.TUTORIAL_HEADER_NOTIFICATION2_TRAVEL));
                                halfimagebottom.setImageResource(R.drawable.notificationtravelbutton);
                                done_qr.setVisibility(View.GONE);
                                break;
                        }
                        currentStep = 5;
                        step5.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.colorAccent));
                        step6.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.tw__solid_white));
                        break;
                }
            }
        });


        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        //findViewById(R.id.dummy_button).setOnTouchListener(mDelayHideTouchListener);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mControlsView.setVisibility(View.GONE);
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        //mHideHandler.removeCallbacks(mHideRunnable);
        //mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
}
