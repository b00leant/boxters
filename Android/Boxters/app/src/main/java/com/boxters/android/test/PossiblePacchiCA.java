package com.boxters.android.test;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.boxters.android.DownloadImageTask;
import com.boxters.android.MainActivity;
import com.boxters.android.MapActivity;
import com.boxters.android.ProprietaPaccoActivity;
import com.boxters.android.ProprietaViaggioActivity;
import com.boxters.android.R;
import com.boxters.model.AuthorizedBox;
import com.boxters.model.BoxState;
import com.boxters.model.IllegalActionException;
import com.boxters.model.PublicBox;
import com.boxters.model.PublicTravel;
import com.boxters.model.PublicUser;
import com.boxters.model.Travel;
import com.boxters.model.TravelState;
import com.boxters.model.User;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.vision.text.Text;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import static com.facebook.FacebookSdk.getApplicationContext;


public class PossiblePacchiCA extends ArrayAdapter<AuthorizedBox> {
    //public PossiblePacchiFragment ma;
    public PacchiQueryFragment ma;
    private User myreceiver;
    private User mysender;
    private Context mContext;
    private ListView lv;



    public void writeCityName(Context ctx, TextView tv, double lat, double lng){
        Geocoder geoCoder = new Geocoder(ctx, Locale.getDefault());

        try {
            List<Address> addresses = geoCoder.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0)
            {
                tv.setText(addresses.get(0).getLocality().toString());
                System.out.println(addresses.get(0).getLocality());
            }
            else
            {
                // do your staff
            }

        } catch (IOException e) {
            ;
        }
    }
    /* STEFANO */
    /*

    sto rendendo la ProprietaPaccoActivity più snella
    accorpando le PossibleBoxtersCA insieme a LVQCA

     */
    /* OLD
    public PossiblePacchiCA(PossiblePacchiFragment _ma, AuthorizedBoxList lp) {
        super(_ma.getActivity().getApplicationContext(), R.layout.pacchi_potenziali_listview_item);
        ma = _ma;
        if (lp != null)
            importa(lp);
    }
    */

    public PossiblePacchiCA(PacchiQueryFragment _ma, AuthorizedBoxList lp) {
        super(_ma.getActivity().getApplicationContext(), R.layout.pacchi_potenziali_listview_item);
        ma = _ma;
        if (lp != null)
            importa(lp);
    }


    protected void importa(AuthorizedBoxList pb) {
        for (AuthorizedBox b : pb) {
            this.add(b);
        }
    }


    protected AuthorizedBox getElement(int pos) {
        return this.getItem(pos);
    }


    public View getView(final int position, View convertView, ViewGroup parent) {

        AuthorizedBox b = this.getItem(position);
        mContext = parent.getContext();
        lv = (ListView) parent;
        convertView = ma.getActivity().getLayoutInflater().inflate(R.layout.pacchi_potenziali_listview_item, null);
        TextView nomePacco = (TextView) convertView.findViewById(R.id.nome_pacco);
        TextView senderName= (TextView) convertView.findViewById(R.id.sender_name);
        TextView receiverName= (TextView) convertView.findViewById(R.id.receiver_name);
        TextView secondLineA = (TextView) convertView.findViewById(R.id.secondLineA);
        TextView secondLineB = (TextView) convertView.findViewById(R.id.secondLineB);
        TextView dimensionLine = (TextView) convertView.findViewById(R.id.dimensionLine);
        TextView peso = (TextView) convertView.findViewById(R.id.peso_pacco);
        TextView expiration = (TextView) convertView.findViewById(R.id.expiration_date);
        ImageButton button = (ImageButton) convertView.findViewById(R.id.confirm_button);
        ImageButton callSender = (ImageButton) convertView.findViewById(R.id.call_one);
        ImageButton callReceiver = (ImageButton) convertView.findViewById(R.id.call_two);
        RatingBar ratingBarA = (RatingBar) convertView.findViewById(R.id.ratingBarA);
        RatingBar ratingBarB = (RatingBar) convertView.findViewById(R.id.ratingBarB);



        // ProprietaViaggioActivity.travelAttuale.getBox(b.getSender(),b.getId()).getState()
        //per ragioni di debug al momento uso b.getState()
        switch (ProprietaViaggioActivity.travelAttuale.getBox(b.getSender(),b.getId()).getState()){
            case CREATED:
                button.setImageResource(R.drawable.ic_done_white_24dp);
                break;
            case CONFIRMED:
                button.setImageResource(R.drawable.ic_camera_alt_white_24dp);
                break;
            case TRAVELLING:
                button.setImageResource(R.drawable.ic_qr_boxter);
                break;
            case RECEIVED:
                button.setImageResource(R.drawable.ic_done_all_white_24dp);
                break;
        }

        callReceiver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User.load(b.getReceiver(),(User user) ->{
                    if(user != null){
                        myreceiver = user;
                        Intent callIntent = new Intent(Intent.ACTION_CALL); //use ACTION_CALL class
                        callIntent.setData(Uri.parse("tel:"+myreceiver.getPhone().trim()));    //this is the phone number calling
                        //check permission
                        //If the device is running Android 6.0 (API level 23) and the app's targetSdkVersion is 23 or higher,
                        //the system asks the user to grant approval.
                        if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            //request permission from user if the app hasn't got the required permission
                            ActivityCompat.requestPermissions(ma.getActivity(),
                                    new String[]{android.Manifest.permission.CALL_PHONE},   //request specific permission from user
                                    10);
                            return;
                        }else {     //have got permission
                            try{
                                ma.getActivity().startActivity(callIntent);  //call activity and make phone call
                            }
                            catch (android.content.ActivityNotFoundException ex){
                                Toast.makeText(getApplicationContext(),"yourActivity is not founded",Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                },false);
            }
        });

        callSender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User.load(b.getSender(),(User user) ->{
                    if(user != null){
                        mysender = user;
                        Intent callIntent = new Intent(Intent.ACTION_CALL); //use ACTION_CALL class
                        callIntent.setData(Uri.parse("tel:"+mysender.getPhone().trim()));    //this is the phone number calling
                        //check permission
                        //If the device is running Android 6.0 (API level 23) and the app's targetSdkVersion is 23 or higher,
                        //the system asks the user to grant approval.
                        if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            //request permission from user if the app hasn't got the required permission
                            ActivityCompat.requestPermissions(ma.getActivity(),
                                    new String[]{android.Manifest.permission.CALL_PHONE},   //request specific permission from user
                                    10);
                            return;
                        }else {     //have got permission
                            try{
                                ma.getActivity().startActivity(callIntent);  //call activity and make phone call
                            }
                            catch (android.content.ActivityNotFoundException ex){
                                Toast.makeText(getApplicationContext(),"yourActivity is not founded",Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                },false);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                // ProprietaViaggioActivity.travelAttuale.getBox(b.getSender(),b.getId()).getState()
                //per ragioni di debug al momento uso b.getState()
                switch (ProprietaViaggioActivity.travelAttuale.getBox(b.getSender(),b.getId()).getState()) {

                    case CREATED: {
                        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
                        alert.setTitle(ma.getString(R.string.confirm));
                        alert.setMessage(ma.getString(R.string.request_to_be_boxter));
                        alert.setNegativeButton(ma.getString(R.string.cancel),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(
                                            DialogInterface dialog,
                                            int whichButton) {

                                        dialog.cancel();
                                    }
                                });
                        alert.setPositiveButton(ma.getString(R.string.Sure),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(
                                            DialogInterface dialog,
                                            int whichButton) {

                                        ProprietaViaggioActivity.travelAttuale.updateBoxState(BoxState.CONFIRMED,b.getSender(),b.getId(),(save) ->{
                                            //ProprietaViaggioActivity.travelAttuale.setState(TravelState.STARTED,null);
                                            ma.getActivity().recreate();
                                        });
                                        //((Activity) mContext).recreate();
                                    }
                                });
                        alert.create().show();
                        break;
                    }

                    case CONFIRMED: {
                        Intent startNewActivity = new Intent(ma.getActivity(), QRScannerActivity.class);
                        String QR = b.getQRCode().trim().toUpperCase();
                        startNewActivity.putExtra("QR",QR);
                        startNewActivity.putExtra("Sender",b.getSender());
                        startNewActivity.putExtra("Box",b.getId());
                        ((Activity) mContext).startActivityForResult(startNewActivity, PossiblePacchiFragment.RC_GET_SHIP_CODE);
                        break;
                    }

                    case TRAVELLING: {
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ma.getActivity());
                        LayoutInflater inflater = LayoutInflater.from(ma.getActivity());
                        View dialogView = inflater.inflate(R.layout.qr_dialog, null);
                        dialogBuilder.setView(dialogView);
                        AlertDialog alertDialog = dialogBuilder.create();
                        ImageView qr_code = (ImageView) dialogView.findViewById(R.id.qr);
                        WindowManager wm = (WindowManager) ma.getActivity().getSystemService(Context.WINDOW_SERVICE);
                        Display display = wm.getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int width = size.x;
                        int height = size.y;

                        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                        try {
                            BitMatrix bitMatrix = multiFormatWriter.encode(b.getQRCode(), BarcodeFormat.QR_CODE,width/2,width/2);
                            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                            qr_code.setImageBitmap(bitmap);
                        } catch (WriterException e) {
                            e.printStackTrace();
                        }

                        ImageButton send = (ImageButton) dialogView.findViewById(R.id.sendBox);
                        send.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                AlertDialog.Builder alert = new AlertDialog.Builder(ma.getActivity());
                                alert.setTitle(ma.getActivity().getString(R.string.QR_Scan));
                                alert.setMessage(ma.getActivity().getString(R.string.Question_Assigned));
                                alert.setNegativeButton(ma.getActivity().getString(R.string.cancel),
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int whichButton) {
                                                dialog.cancel();
                                            }
                                        });
                                alert.setPositiveButton(ma.getActivity().getString(R.string.Sure),
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int whichButton) {
                                                    PublicUser.load(b.getReceiver(),(rec) -> {
                                                    rec.waitReceivedConfirm(b.getSender(),b.getId(), (res) -> {
                                                        ProprietaViaggioActivity.travelAttuale.updateBoxState(BoxState.RECEIVED,b.getSender(),b.getId(),null);
                                                        alertDialog.cancel();
                                                        ma.getActivity().recreate();
                                                        Toast.makeText(ma.getContext(),ma.getActivity().getString(R.string.Box_delivered),Toast.LENGTH_LONG).show();
                                                    });
                                                },false);
                                            }
                                        });
                                alert.create().show();
                            }
                        });
                        alertDialog.show();
                        /*
                        Intent startNewActivity = new Intent(v.getContext(), ScanActivity.class);
                        String QR = b.getQRCode().trim().toUpperCase();
                        startNewActivity.putExtra("QR",QR);
                        startNewActivity.putExtra("Box",b.getId());
                        startNewActivity.putExtra("Receiver",b.getReceiver());
                        startNewActivity.putExtra("Sender",b.getSender());
                        ma.getActivity().startActivityForResult(startNewActivity, PossiblePacchiFragment.RC_GIVE_SHIP_CODE);
                        */
                        break;
                    }

                }

//                startActivity(startNewActivity);
            }


        });

        /*
        ImageButton call1 = (ImageButton) convertView.findViewById(R.id.call_one);
        call1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "000000000"));
                ma.startActivity(intent);
            }
        });
        ImageButton call2 = (ImageButton) convertView.findViewById(R.id.call_two);
        call2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "000000000"));
                ma.startActivity(intent);
            }
        });
        */

        View finalConvertView = convertView;

        PublicUser.load(b.getSender(),(user) -> {
            senderName.setText(user.getName());
            ratingBarA.setRating(user.getRating() != null ? user.getRating().floatValue() : 0);
            if (user.getPhoto() != null) {
                ImageView iconSender = (ImageView) finalConvertView.findViewById(R.id.iconSender);
                if (iconSender != null)
                    new DownloadImageTask(iconSender).execute(user.getPhoto());
            }

        },false);
        PublicUser.load(b.getReceiver(),(user) -> {
            ratingBarB.setRating(user.getRating() != null ? user.getRating().floatValue() : 0);
            receiverName.setText(user.getName());
            if (user.getPhoto() != null) {
                ImageView iconReceiver = (ImageView) finalConvertView.findViewById(R.id.iconReceiver);
                if (iconReceiver != null)
                    new DownloadImageTask(iconReceiver).execute(user.getPhoto());
            }

        },false);
        nomePacco.setText(b.getObject());
        SimpleDateFormat localDateFormat = new SimpleDateFormat("MM/dd/yyyy");

        if(b.getWeight() <= 1.0){
            peso.setText("<1");
        }
        else if(b.getWeight() <= 2.0){
            //pesoPacco.setText(getResources().getStringArray(R.array.peso)[1].replace("[","").replace("]",""));
            peso.setText(">1");
        }
        else if(b.getWeight() <= 3.0){
            //pesoPacco.setText(getResources().getStringArray(R.array.peso)[2].replace("[","").replace("]",""));
            peso.setText(">5");
        }
        else if(b.getWeight() <= 4.0){
            peso.setText(">10");
        }else{
            peso.setText("???");
        }


        if(b.getSize() <= 1.0){
            System.out.println("LO SPAZIO È STATO SETTATO S");
            //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[0].replace("[","").replace("]","").split("\\(")[0]);
            dimensionLine.setText("S");
        }
        else if(b.getSize() <= 2.0){
            System.out.println("LO SPAZIO È STATO SETTATO M");
            dimensionLine.setText("M");
            //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[1].replace("[","").replace("]","").split("\\(")[0]);
        }
        else if(b.getSize() <= 3.0){
            System.out.println("LO SPAZIO È STATO SETTATO L");
            dimensionLine.setText("L");
            //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[2].replace("[","").replace("]","").split("\\(")[0]);
        }else{
            System.out.println("IL VERO SPAZIO È "+ProprietaPaccoActivity.boxAttuale.getSize());
            dimensionLine.setText("?");
        }


        Places.GeoDataApi.getPlaceById(ProprietaViaggioActivity.mGoogleApiClient, b.getArrival().getName())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);
                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                            secondLineB.setText(myPlace.getName());
                        } else {
                            Log.e("PLACES DETAILS", "Place not found");
                            Log.e("PLACES DETAILS",places.getStatus().getStatusMessage());
                        }
                        places.release();
                    }
                });
        Places.GeoDataApi.getPlaceById(ProprietaViaggioActivity.mGoogleApiClient, b.getDeparture().getName())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);
                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                            secondLineA.setText(myPlace.getName());
                        } else {
                            Log.e("PLACES DETAILS", "Place not found");
                        }
                        places.release();
                    }
                });

        //writeCityName(ma.getContext(),secondLineA,b.getDeparture().getLat(),b.getDeparture().getLon());
        //writeCityName(ma.getContext(),secondLineB,b.getArrival().getLat(),b.getArrival().getLon());
        //secondLineB.setText("to " + t.getArrival().getName());
        expiration.setText(localDateFormat.format(b.getExpiration()));
        return convertView;
    }
}
