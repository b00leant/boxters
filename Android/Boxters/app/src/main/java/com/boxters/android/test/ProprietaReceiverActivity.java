package com.boxters.android.test;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.boxters.android.BoxterProfileReceiverFragment;
import com.boxters.android.MainActivity;
import com.boxters.android.R;
import com.boxters.android.ViewPagerAdapter;
import com.boxters.model.AuthorizedBox;
import com.boxters.model.Box;
import com.boxters.model.PublicTravel;
import com.boxters.model.Travel;
import com.boxters.model.User;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;

public class ProprietaReceiverActivity extends AppCompatActivity{

    public static AuthorizedBox boxAttuale;
    TextView paccoFrom;
    TextView paccoTo;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private User myreceiver;
    private DataReceiverFragment proprietaPacco;
    public static GoogleApiClient mGoogleApiClient;
    public static PublicTravel mytravel;
    private BoxterProfileReceiverFragment boxterProfileFragment;
    private ReceiverConfirmBoxterFragment receiverConfirmBoxterFragment;
    private RicezionePaccoFragment ricezionePaccoFragment;

    @Override
    public void onBackPressed() {
        Intent startNewActivity = new Intent( this,MainActivity.class);
        finishAffinity();
        startActivity(startNewActivity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proprieta_pacco);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_proprieta_pacco_activity);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        //paccoTo = (TextView) findViewById(R.id.paccoTo);

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage((FragmentActivity) this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        // your code here
                    }
                })
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        User.load(boxAttuale.getReceiver(), (User _user) -> {
            if (_user != null){
                this.myreceiver = _user;
            }
        }, true);

        User.load(boxAttuale.getReceiver(), (User _user) -> {
            if (_user != null){
                this.myreceiver = _user;
            }
        }, true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#FFFFFF"));
        tabLayout.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
        tabLayout.setTabTextColors(Color.WHITE,Color.WHITE);
    }
    public String getResString(int id) {
        return (String) getResources().getText(id);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        MainActivity.user.getReceiver((receiver) -> {

            switch (receiver.getState(ProprietaReceiverActivity.boxAttuale.getSender(),ProprietaReceiverActivity.boxAttuale.getId())) {

                case CREATED:

                    this.proprietaPacco  = new DataReceiverFragment();
                    //this.ricezionePaccoFragment = new RicezionePaccoFragment();

                    adapter.addFragment(this.proprietaPacco, getResString(R.string.title_properties));
                    if (ProprietaReceiverActivity.boxAttuale.getBoxter() != null) {
                        this.boxterProfileFragment = new BoxterProfileReceiverFragment();

                        adapter.addFragment(this.boxterProfileFragment, getResString(R.string.title_boxter));
                    }
                    // NON HA SENSO AVERE UN FRAGMENT DI SCAN PER IL RECEIVER QUANDO IL PACCO NON LO HA NEANCHE IL BOXTER
                    //adapter.addFragment(this.ricezionePaccoFragment, getResString(R.string.title_scan));




                    break;

                case CONFIRMED:

                    this.proprietaPacco  = new DataReceiverFragment();
                    this.boxterProfileFragment = new BoxterProfileReceiverFragment();
                    this.ricezionePaccoFragment = new RicezionePaccoFragment();

                    adapter.addFragment(this.proprietaPacco, getResString(R.string.title_properties));
                    adapter.addFragment(this.boxterProfileFragment, getResString(R.string.title_boxter));
                    adapter.addFragment(this.ricezionePaccoFragment, getResString(R.string.title_scan));

                    break;

                case TRAVELLING:

                    this.proprietaPacco  = new DataReceiverFragment();
                    this.ricezionePaccoFragment = new RicezionePaccoFragment();
                    this.boxterProfileFragment = new BoxterProfileReceiverFragment();

                    adapter.addFragment(this.proprietaPacco, getResString(R.string.title_properties));
                    adapter.addFragment(this.ricezionePaccoFragment, getResString(R.string.title_scan));
                    adapter.addFragment(this.boxterProfileFragment, getResString(R.string.title_boxter));
                    break;


                case RECEIVED:

                    this.proprietaPacco  = new DataReceiverFragment();

                    adapter.addFragment(this.proprietaPacco, getResString(R.string.title_properties));
                    break;


            }

        });


        viewPager.setAdapter(adapter);
    }

    public void btnClose(View view) {
        finish();
    }

    public void setPossibleBoxters(View view){

    }

}