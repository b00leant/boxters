package com.boxters.android.test;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.boxters.android.ProprietaPaccoActivity;
import com.boxters.android.ProprietaViaggioActivity;
import com.boxters.android.R;
import com.boxters.model.BoxState;
import com.boxters.model.PublicTravel;
import com.boxters.model.TravelState;

public class ViaggiatoriQueryFragment extends Fragment {

    public LVQCA aa;
    public PossibleBoxtersCA pba;
    private ImageView spacer;
    private TextView tv;
    private TextView tv2;
    private ListView lv;
    private ListView lvAddedTravels;

    public ViaggiatoriQueryFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.double_fragment_list_container, container, false);
        lv = (ListView) view.findViewById(R.id.listViewContainer);
        lvAddedTravels = (ListView) view.findViewById(R.id.listViewContainer2);
        spacer = (ImageView) view.findViewById(R.id.spacer);
        if(ProprietaPaccoActivity.boxAttuale.getState() != BoxState.CREATED){
            lv.setVisibility(View.GONE);
        }
        aa = new LVQCA(this, null);
        pba = new PossibleBoxtersCA(this, null);
        tv = (TextView) view.findViewById(R.id.tvListContainer);
        tv2 = (TextView) view.findViewById(R.id.tvListContainer2);
        tv.setText(getContext().getString(R.string.fragment_no_boxes_yet));
        tv2.setText(getContext().getString(R.string.fragment_no_boxes_yet));
        lvAddedTravels.setAdapter(pba);
        lv.setAdapter(aa);
        lv.setEmptyView(tv);
        lvAddedTravels.setEmptyView(tv2);
        updateAddedTravelsList();
        updateList();

        //ListUtility.setListViewHeightBasedOnChildren(lv);
        //ListUtility.setListViewHeightBasedOnChildren(lvAddedTravels);
        return view;


    }

    public void updateAddedTravelsClear() {
        ProprietaPaccoActivity.boxAttuale.loadPossibleTravels((travel) -> {
            pba.clear();
            if (travel != null) {
                pba.add(travel);
                pba.notifyDataSetChanged();
                ListUtility.setDynamicHeight(lvAddedTravels);
                if(aa.getCount() > 0){
                    spacer.setVisibility(View.VISIBLE);
                    //lvAuth.setVisibility(View.VISIBLE);
                }
                if(pba.getCount() == 0){
                    lvAddedTravels.setVisibility(View.GONE);
                    spacer.setVisibility(View.GONE);
                }
            }
        },false);
    }

    public void updateListClear() {
        PublicTravel.queryForBox(ProprietaPaccoActivity.boxAttuale, 3.0, (travel) -> {
            aa.clear();
            if (travel != null) {
                aa.add(travel);
                aa.notifyDataSetChanged();
                ListUtility.setDynamicHeight(lv);
            }
        }, false);

    }

    private void updateAddedTravelsList() {
        ProprietaPaccoActivity.boxAttuale.loadPossibleTravels((travel) -> {
            if (travel != null) {
                pba.add(travel);
                pba.notifyDataSetChanged();
                ListUtility.setDynamicHeight(lvAddedTravels);
                if(aa.getCount() > 0){
                    spacer.setVisibility(View.VISIBLE);
                    //lvAuth.setVisibility(View.VISIBLE);
                }
                if(pba.getCount() == 0){
                    lvAddedTravels.setVisibility(View.GONE);
                    spacer.setVisibility(View.GONE);
                }
            }
        },false);
    }
    private void updateList() {
        PublicTravel.queryForBox(ProprietaPaccoActivity.boxAttuale, 3.0, (travel) -> {
            if (travel != null) {
                aa.add(travel);
                aa.notifyDataSetChanged();
                ListUtility.setDynamicHeight(lv);
                if(aa.getCount() > 0){
                    spacer.setVisibility(View.VISIBLE);
                    //lvAuth.setVisibility(View.VISIBLE);
                }
                if(pba.getCount() == 0){
                    lvAddedTravels.setVisibility(View.GONE);
                    spacer.setVisibility(View.GONE);
                }
            }
        }, false);

    }

}

