package com.boxters.android.test;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by undeclinable on 29/03/17.
 */

public class FindVicinityTask extends AsyncTask<LatLng, Void, String> {
    String vicinity;
    private static final String OUT_JSON = "/json";
    private static final String RADIUS = "500";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place/nearbysearch";
    private static String API_KEY = "AIzaSyDh7RzrrF5TR0Hi-AtHGtvUAVMc-lIbjUI";

    public FindVicinityTask(String vicinity){
        this.vicinity = vicinity;
    }



    public String doInBackground(LatLng... latLngs) {
        LatLng latLng = latLngs[0];
        String id = vicinity;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&radius=" + RADIUS);
            String input = latLng.latitude+","+latLng.longitude;
            sb.append("&location=" + URLEncoder.encode(input, "utf8"));
            System.out.println("L'url dell'API è:");
            URL url = new URL(sb.toString());
            System.out.println(url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e("Autocomplete", "Error processing Places API URL", e);
        } catch (IOException e) {
            Log.e("Autocomplete", "Error connecting to Places API", e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        try {
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("results");

            for (int i = 0; i < 1; i++) {

                try {
                    JSONObject jsonObj2 = predsJsonArray.getJSONObject(i);
                    id = jsonObj2.getString("vicinity");
                    System.out.println("Il luogo vicino è: "+id);
                    System.out.println("Il luogo vicino è: "+ jsonObj2.getString("vicinity"));
                }catch (JSONException e) {
                    Log.e("LOG", "Cannot process JSON results", e);
                }
            }
        } catch (JSONException e) {
            Log.e("LOG", "Cannot process JSON results", e);
        }
        return id;
    }
    public void onPostExecute(String result_vicinity) {
        this.vicinity = result_vicinity;
    }
}
