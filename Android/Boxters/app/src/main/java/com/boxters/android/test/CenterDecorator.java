package com.boxters.android.test;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import com.boxters.android.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * Created by undeclinable on 15/03/17.
 */

public class CenterDecorator implements DayViewDecorator {
    private final Drawable dc;
    private final ArrayList<Date> disabled_dates;
    private final ArrayList<CalendarDay> dates;

    public CenterDecorator(List<Date> disabled_dates, List<CalendarDay> dates, Activity context) {
        dc = context.getResources().getDrawable(R.drawable.ic_just_center);
        this.disabled_dates = new ArrayList<>(disabled_dates);
        this.dates = new ArrayList<>(dates);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        if(dates.size()>0){
            if(this.dates.size() > 2 && !disabled_dates.contains(day.getDate())){
                ArrayList<CalendarDay> newal = new ArrayList<>(this.dates.subList(1, this.dates.size() - 1));
                return newal.contains(day);
            }
        }else if(dates.size() == 0){
            return false;
        }
        return false;
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setSelectionDrawable(dc);
    }
}
