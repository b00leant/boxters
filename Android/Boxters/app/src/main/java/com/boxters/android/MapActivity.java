package com.boxters.android;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.boxters.model.Box;
import com.boxters.model.Location;
import com.boxters.model.PublicBox;
import com.boxters.model.Travel;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, AdapterView.OnItemClickListener{


    public static final int TIPO_PROCESSO_PACCO = 1;
    public static final int TIPO_PROCESSO_VIAGGIO = 2;
    private ProgressBar from_progress;
    private ProgressBar to_progress;
    private ImageButton toMarker;
    private ImageButton fromMarker;
    private Bundle b;
    public static Travel travelAttuale;
    public static Box boxAttuale;
    private static final String TAG = "MapActivity";
    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String PLACES_DETAIL_BASE = "https://maps.googleapis.com/maps/api/place/details";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyDh7RzrrF5TR0Hi-AtHGtvUAVMc-lIbjUI";
    private boolean triggered;
    private AutocompleteFilter typeFilter;
    private int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private PlaceAutocompleteFragment autocompleteFragment;
    private GoogleMap mMap;
    private LatLng from;
    private LatLng to;
    private GoogleApiClient mGoogleApiClient;
    private Marker daMark;
    private Marker aMark;
    private String image_bitmap;

    //stringhe che servono nell'intent di modifica pacco/viaggio

    private String from_id;
    private String to_id;
    private String box_id;
    private String travel_id;
    private double from_lat,from_lon;
    private double to_lat,to_lon;
    private String user_id;

    private Polyline percorso;
    private Location fromLocation;
    private Location toLocation;
    AutoCompleteTextView fromText;
    AutoCompleteTextView toText;
    private CustomPlace fromPlace;
    private CustomPlace toPlace;
    private int tipoProcesso;


    // Request code to use when launching the resolution activity
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    // Unique tag for the error dialog fragment
    private static final String DIALOG_ERROR = "dialog_error";
    // Bool to track whether the app is already resolving an error
    private boolean mResolvingError = false;

    // ...


    @Override
    public void onBackPressed() {
        if(box_id != null || travel_id != null){
            Intent startNewActivity = new Intent( this,MainActivity.class);
            finishAffinity();
            startActivity(startNewActivity);
        }else{
            super.onBackPressed();
        }
    }
    public static LatLng reverseGeocoding(Context context, String locationName) {
        if (!Geocoder.isPresent()) {
            Log.w("asd", "Geocoder implementation not present !");
        }
        Geocoder geoCoder = new Geocoder(context, Locale.getDefault());

        try {
            List<Address> addresses = geoCoder.getFromLocationName(locationName, 1);
            int tentatives = 0;
            while (addresses.size() == 0 && (tentatives < 10)) {
                addresses = geoCoder.getFromLocationName("<address goes here>", 1);
                tentatives++;
            }


            if (addresses.size() > 0) {
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                //t.setText(address + ", " + city);Log.d("asd", "Geocoding inverso: locationName " + locationName + ", Latitude " + addresses.get(0).getLatitude() + ", Longitude: " + addresses.get(0).getLongitude());
                return new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
            } else {
                //use http api
            }

        } catch (IOException e) {
            ;
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tipoProcesso = getIntent().getIntExtra("tipoProcesso", 0);

        setContentView(R.layout.activity_map);

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage((FragmentActivity) this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        // your code here
                    }
                })
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        String mode ="";
        b = getIntent().getExtras();
        if(b != null){
            from_id = b.getString("from_id");
            to_id = b.getString("to_id");
            box_id = b.getString("box_id");
            travel_id = b.getString("travel_id");
            from_lat = b.getDouble("from_lat");
            from_lon = b.getDouble("from_lon");
            to_lat = b.getDouble("to_lat");
            to_lon = b.getDouble("to_lon");
            user_id = b.getString("user_id");
            image_bitmap = b.getString("image_bitmap");
            mode = b.getString("mode");
        }
        if(mode != null){
            if(mode == "boxes"){
                travel_id = null;
                System.out.println("TRAVEL_ID: "+travel_id);
                System.out.println("BOX_ID: "+box_id);
            }
            if(mode == "travels"){
                box_id = null;
                System.out.println("TRAVEL_ID: "+travel_id);
                System.out.println("BOX_ID: "+box_id);
            }
        }
        System.out.println("FROM_ID: "+from_id);
        System.out.println("TO_ID: "+to_id);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_map_activity);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        if(boxAttuale != null || travelAttuale != null){
            toolbar.setTitle("Edit");
        }

        from_progress = (ProgressBar) findViewById(R.id.from_progress);
        to_progress = (ProgressBar) findViewById(R.id.to_progress);

        toMarker = (ImageButton) findViewById(R.id.to_marker);
        fromMarker = (ImageButton) findViewById(R.id.from_marker);

        fromText = (AutoCompleteTextView) findViewById(R.id.FromText);
        toText = (AutoCompleteTextView) findViewById(R.id.ToText);
        fromText.setSingleLine(true);
        toText.setSingleLine(true);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        toText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    toMarker.setImageResource(R.drawable.ic_clear_white_24dp);
                    toText.setOnKeyListener(new OnKeyListener() {

                        @Override
                        public boolean onKey(View arg0, int arg1, KeyEvent arg2) {

                            if (toText.getText().toString() != "") {
                                toMarker.setImageResource(R.drawable.ic_clear_white_24dp);
                                toMarker.setVisibility(View.GONE);
                                to_progress.setVisibility(View.VISIBLE);
                            }
                            if (toText.getText().toString() == "" || toText.getText().toString().length() < 2 || toText.getText().toString() == null) {
                                toMarker.setImageResource(R.drawable.ic_place_white_24dp);
                                toMarker.setVisibility(View.VISIBLE);
                                to_progress.setVisibility(View.GONE);
                            }
                            return false;
                        }
                    });
                    toMarker.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toText.setText("");
                            to = null;
                            toPlace = null;
                            if (percorso != null) {
                                percorso.remove();
                            }
                            if (aMark != null) {
                                aMark.remove();
                            }
                            aMark = null;
                            toLocation = null;
                        }
                    });
                } else {
                    toMarker.setImageResource(R.drawable.ic_place_white_24dp);
                }
            }
        });
        triggered = true;

        fromMarker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (triggered) {
                    fromText.setText("");
                    from = null;
                    if (percorso != null) {
                        percorso.remove();
                    }
                    if (daMark != null) {
                        daMark.remove();
                    }
                    fromPlace = null;
                    daMark = null;
                    fromLocation = null;
                }

            }
        });
        fromText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    fromMarker.setImageResource(R.drawable.ic_clear_white_24dp);
                    fromText.setOnKeyListener(new OnKeyListener() {
                        @Override
                        public boolean onKey(View arg0, int arg1, KeyEvent arg2) {

                            if (fromText.getText().toString() != "") {
                                fromMarker.setImageResource(R.drawable.ic_clear_white_24dp);
                                fromMarker.setVisibility(View.GONE);
                                from_progress.setVisibility(View.VISIBLE);
                            }
                            if (fromText.getText().toString() == "" || fromText.getText().toString().length() < 2 || fromText.getText().toString() == null) {
                                fromMarker.setImageResource(R.drawable.ic_adjust_white_24dp);
                                fromMarker.setVisibility(View.VISIBLE);
                                from_progress.setVisibility(View.GONE);
                            }
                            return false;
                        }
                    });
                    fromMarker.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            fromText.setText("");
                            from = null;
                            if (percorso != null) {
                                percorso.remove();
                            }
                            if (daMark != null) {
                                daMark.remove();
                            }
                            fromPlace = null;
                            daMark = null;
                            fromLocation = null;
                        }
                    });
                } else {
                    fromMarker.setImageResource(R.drawable.ic_adjust_white_24dp);
                }
            }
        });

        int layoutItemId = android.R.layout.simple_dropdown_item_1line;

        fromText.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.place_autocomplete_item));
        fromText.setOnItemClickListener(this);

        toText.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.place_autocomplete_item));
        toText.setOnItemClickListener(this);

        fromText.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(View arg0, int arg1, KeyEvent arg2) {

                if (fromText.getText().toString() != "") {
                    fromMarker.setImageResource(R.drawable.ic_clear_white_24dp);
                    fromMarker.setVisibility(View.GONE);
                    from_progress.setVisibility(View.VISIBLE);
                }
                if (fromText.getText().toString() == "" || fromText.getText().toString().length() < 2 || fromText.getText().toString() == null) {
                    fromMarker.setImageResource(R.drawable.ic_adjust_white_24dp);
                    fromMarker.setVisibility(View.VISIBLE);
                    from_progress.setVisibility(View.GONE);
                }
                return false;
            }
        });
        toText.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(View arg0, int arg1, KeyEvent arg2) {

                if (toText.getText().toString() != "") {
                    toMarker.setImageResource(R.drawable.ic_clear_white_24dp);
                    toMarker.setVisibility(View.GONE);
                    to_progress.setVisibility(View.VISIBLE);
                }
                if (toText.getText().toString() == "" || toText.getText().toString().length() < 2 || toText.getText().toString() == null) {
                    toMarker.setImageResource(R.drawable.ic_place_white_24dp);
                    toMarker.setVisibility(View.VISIBLE);
                    to_progress.setVisibility(View.GONE);
                }
                return false;
            }
        });

        //autocompleteFragment = (PlaceAutocompleteFragment)
        //getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                .build();

        //autocompleteFragment.setFilter(typeFilter);

    }

    public void findPlace(View view) {

        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(typeFilter)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i(TAG, "Place: " + place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, "Place error: " + status.getStatusMessage());
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    /*
    @Override
    public boolean onKey(View view, int keyCode, KeyEvent event) {

        //TextView responseText = (TextView) findViewById(R.id.responseText);
        EditText myEditText = (EditText) view;

        if (keyCode == EditorInfo.IME_ACTION_SEARCH ||
                keyCode == EditorInfo.IME_ACTION_DONE ||
                event.getAction() == KeyEvent.ACTION_DOWN &&
                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

            if (!event.isShiftPressed()) {
                Log.v("AndroidEnterKeyActivity", "Enter Key Pressed!");
                switch (view.getId()) {
                    case R.id.FromText:
                        String responseText1 = ((EditText) findViewById(R.id.FromText)).getText().toString();
                        from = reverseGeocoding(this, responseText1);

                        if (from != null) {
                            if(daMark != null){daMark.remove();}
                            if(aMark != null){
                                daMark = mMap.addMarker(new MarkerOptions().position(from).title("Provenienza"));
                                if(percorso != null ){percorso.remove();}
                                //polyline is huge and it has the main app's color
                                percorso = mMap.addPolyline(new PolylineOptions().add(from,to).width(5).color(Color.rgb(18, 96, 147)));

                                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                                builder.include(daMark.getPosition());
                                builder.include(aMark.getPosition());
                                LatLngBounds bounds = builder.build();
                                int width = getResources().getDisplayMetrics().widthPixels;
                                int height = getResources().getDisplayMetrics().heightPixels;
                                int padding = (int) (width * 0.30);
                                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
                                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                in.hideSoftInputFromWindow(fromText.getApplicationWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
                                mMap.moveCamera(cameraUpdate);
                                //mMap.moveCamera(CameraUpdateFactory.newLatLng(to));
                            }else{
                                daMark = mMap.addMarker(new MarkerOptions().position(from).title("Provenienza"));
                                mMap.moveCamera(CameraUpdateFactory.newLatLng(from));
                            }
                        }

                        break;
                    case R.id.ToText:String responseText2 = ((EditText) findViewById(R.id.ToText)).getText().toString();
                        to = reverseGeocoding(this, responseText2);

                        if (to != null) {
                            if(aMark != null){aMark.remove();}
                            if(daMark != null){
                                aMark = mMap.addMarker(new MarkerOptions().position(to).title("Destinazione"));
                                if(percorso != null ){percorso.remove();}
                                percorso = mMap.addPolyline(new PolylineOptions().add(from,to).width(5).color(Color.rgb(18, 96, 147)));

                                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                                builder.include(daMark.getPosition());
                                builder.include(aMark.getPosition());
                                LatLngBounds bounds = builder.build();
                                int width = getResources().getDisplayMetrics().widthPixels;
                                int height = getResources().getDisplayMetrics().heightPixels;
                                int padding = (int) (width * 0.30);
                                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
                                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                in.hideSoftInputFromWindow(toText.getApplicationWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
                                mMap.moveCamera(cameraUpdate);
                            }else{
                                aMark = mMap.addMarker(new MarkerOptions().position(to).title("Destinazione"));
                                mMap.moveCamera(CameraUpdateFactory.newLatLng(to));
                            }

                        }

                        break;
                }
                return true;
            }

        }
        return false;

    }
    */

    @Override
    public void onItemClick(AdapterView adapterView, View view, int position, long id) {
        AutoCompleteTextView currentClickedAutoCompleteTextView;
        if( adapterView.getAdapter()==fromText.getAdapter()) {

            fromPlace = (CustomPlace) adapterView.getAdapter().getItem(position);
            fromText.setText(fromPlace.getName());
            from = fromPlace.getLatLng();
            from_id = fromPlace.getPlaceId();
            from_lat = fromPlace.getLatLng().latitude;
            from_lon = fromPlace.getLatLng().longitude;
            //from = reverseGeocoding(MapActivity.this, selection);

            if (from != null) {
                System.out.println("from LATLNG è stato settato a lat: "+from.latitude+", lng: "+from.longitude);
                if(daMark != null){daMark.remove();percorso.remove();}
                if(aMark != null){
                    daMark = mMap.addMarker(new MarkerOptions().position(from).title("Provenienza"));
                    if(percorso != null ){percorso.remove();}
                    //polyline is huge and it has the main app's color
                    percorso = mMap.addPolyline(new PolylineOptions().add(from,to).width(8).color(Color.rgb(18, 96, 147)));
                    fromLocation = new Location(fromPlace.getPlaceId(),fromPlace.getLatLng().latitude,fromPlace.getLatLng().longitude);
                    from_id = fromPlace.getPlaceId();
                    from_lat = fromPlace.getLatLng().latitude;
                    from_lon = fromPlace.getLatLng().longitude;
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    builder.include(daMark.getPosition());
                    builder.include(aMark.getPosition());
                    LatLngBounds bounds = builder.build();
                    int width = getResources().getDisplayMetrics().widthPixels;
                    int height = getResources().getDisplayMetrics().heightPixels;
                    int padding = (int) (width * 0.33);
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(fromText.getApplicationWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
                    mMap.moveCamera(cameraUpdate);
                    float zoom = mMap.getCameraPosition().zoom;
                    mMap.animateCamera( CameraUpdateFactory.zoomTo(zoom - 1));
                    fromText.clearFocus();
                    System.out.println("fromLocation: "+fromLocation.toString());
                    System.out.println("toLocation: "+toLocation.toString());
                    //mMap.moveCamera(CameraUpdateFactory.newLatLng(to));
                }else{
                    fromLocation = new Location(fromPlace.getPlaceId(),fromPlace.getLatLng().latitude,fromPlace.getLatLng().longitude);
                    from_id = fromPlace.getPlaceId();
                    from_lat = fromPlace.getLatLng().latitude;
                    from_lon = fromPlace.getLatLng().longitude;
                    toText.requestFocus();
                    daMark = mMap.addMarker(new MarkerOptions().position(from).title("Provenienza"));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(from));
                }
            }else{
                System.out.println("from LATLNG è stato settato a null!!");
            }
        }else {
            toPlace = (CustomPlace) adapterView.getAdapter().getItem(position);
            toText.setText(toPlace.getName());
            to = toPlace.getLatLng();
            to_id = toPlace.getPlaceId();
            to_lat = toPlace.getLatLng().latitude;
            to_lon = toPlace.getLatLng().longitude;
            System.out.println("TO: "+to.toString());

            //to = reverseGeocoding(MapActivity.this, selection);


            if (to != null) {
                System.out.println("to LATLNG è stato settato a lat: "+to.latitude+", lng: "+to.longitude);
                if(aMark != null){aMark.remove();percorso.remove();}
                if(daMark != null){
                    aMark = mMap.addMarker(new MarkerOptions().position(to).title("Destinazione"));
                    if(percorso != null ){percorso.remove();}
                    percorso = mMap.addPolyline(new PolylineOptions().add(from,to).width(8).color(Color.rgb(18, 96, 147)));
                    toLocation = new Location(toPlace.getPlaceId(),toPlace.getLatLng().latitude,toPlace.getLatLng().longitude);
                    to_id = toPlace.getPlaceId();
                    to_lat = toPlace.getLatLng().latitude;
                    to_lon = toPlace.getLatLng().longitude;
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    builder.include(daMark.getPosition());
                    builder.include(aMark.getPosition());
                    LatLngBounds bounds = builder.build();
                    int width = getResources().getDisplayMetrics().widthPixels;
                    int height = getResources().getDisplayMetrics().heightPixels;
                    int padding = (int) (width * 0.33);
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(toText.getApplicationWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
                    mMap.moveCamera(cameraUpdate);
                    float zoom = mMap.getCameraPosition().zoom;
                    mMap.animateCamera( CameraUpdateFactory.zoomTo(zoom - 1));
                    toText.clearFocus();
                    System.out.println("fromLocation: "+fromLocation.toString());
                    System.out.println("toLocation: "+toLocation.toString());
                }else{
                    to_id = toPlace.getPlaceId();
                    to_lat = toPlace.getLatLng().latitude;
                    to_lon = toPlace.getLatLng().longitude;
                    toLocation = new Location(toPlace.getPlaceId(),toPlace.getLatLng().latitude,toPlace.getLatLng().longitude);
                    fromText.requestFocus();
                    aMark = mMap.addMarker(new MarkerOptions().position(to).title("Destinazione"));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(to));
                }

            }else{
                System.out.println("to LATLNG è stato settato a NULL");
            }
        }
        CustomPlace cp = (CustomPlace) adapterView.getAdapter().getItem(position);
        String str = cp.getName();
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

    public void onMapReady(GoogleMap googleMap) {
        float zoomLevel = 10;
        boolean success = googleMap.setMapStyle(new MapStyleOptions(getResources()
                .getString(R.string.map_style_json)));
        if (!success) {
            Log.e(TAG, "Style parsing failed.");
        }

        mMap = googleMap;
        if (b.getString("travel_id") != null && b.getString("box_id") == null) {
            Log.e("EDIT_VIAGGIO","HO RICONOSCIUTO UN EDIT VIAGGIO");
            Places.GeoDataApi.getPlaceById(mGoogleApiClient, b.getString("to_id"))
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                final Place myPlace = places.get(0);
                                Log.i("PLACES DETAILS MAP", "Place found: " + myPlace.getName());
                                toText.setText(myPlace.getName().toString());
                            } else {
                                Log.e("PLACES DETAILS MAP", "Place not found and id was: " + places.get(0).getName());
                                Log.e("PLACES DETAILS MAP",places.getStatus().getStatusMessage());
                            }
                            places.release();
                        }
                    });
            Places.GeoDataApi.getPlaceById(mGoogleApiClient, b.getString("from_id"))
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                final Place myPlace = places.get(0);
                                Log.i("PLACES DETAILS MAP", "Place found: " + myPlace.getName());
                                fromText.setText(myPlace.getName());
                            } else {
                                Log.e("PLACES DETAILS MAP", "Place not found and id was: " + places.get(0).getName());
                                Log.e("PLACES DETAILS MAP", "Place not found");
                            }
                            places.release();
                        }
                    });
            fromText.clearFocus();
            from = new LatLng(from_lat,from_lon);
            to = new LatLng(to_lat,to_lon);
            daMark = mMap.addMarker(new MarkerOptions().position(from).title(getString(R.string.Destination)));
            aMark = mMap.addMarker(new MarkerOptions().position(to).title(getString(R.string.Destination)));
            percorso = mMap.addPolyline(new PolylineOptions().add(from,to).width(8).color(Color.rgb(18, 96, 147)));
            fromLocation = new Location(from_id,from_lat,from_lon);
            toLocation = new Location(to_id,to_lat,to_lon);
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(daMark.getPosition());
            builder.include(aMark.getPosition());
            LatLngBounds bounds = builder.build();
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;
            int padding = (int) (width * 0.33);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
            InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(fromText.getApplicationWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
            mMap.moveCamera(cameraUpdate);
            float zoom = mMap.getCameraPosition().zoom;
            mMap.animateCamera( CameraUpdateFactory.zoomTo(zoom - 1));
            fromText.clearFocus();
            System.out.println("fromLocation: "+fromLocation.toString());
            System.out.println("toLocation: "+toLocation.toString());
        }
        if(b.getString("travel_id") == null && b.getString("box_id") != null){
            Log.e("EDIT_VIAGGIO","HO RICONOSCIUTO UN EDIT VIAGGIO");
            Places.GeoDataApi.getPlaceById(mGoogleApiClient, b.getString("to_id"))
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                final Place myPlace = places.get(0);
                                Log.i("PLACES DETAILS MAP", "Place found: " + myPlace.getName());
                                toText.setText(myPlace.getName().toString());
                            } else {
                                Log.e("PLACES DETAILS MAP", "Place not found and id was: " + places.get(0).getName());
                                Log.e("PLACES DETAILS MAP",places.getStatus().getStatusMessage());
                            }
                            places.release();
                        }
                    });
            Places.GeoDataApi.getPlaceById(mGoogleApiClient, b.getString("from_id"))
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                final Place myPlace = places.get(0);
                                Log.i("PLACES DETAILS MAP", "Place found: " + myPlace.getName());
                                fromText.setText(myPlace.getName());
                            } else {
                                Log.e("PLACES DETAILS MAP", "Place not found and id was: " + places.get(0).getName());
                                Log.e("PLACES DETAILS MAP", "Place not found");
                            }
                            places.release();
                        }
                    });
            fromText.clearFocus();
            from = new LatLng(b.getDouble("from_lat"),b.getDouble("from_lon"));
            to = new LatLng(b.getDouble("to_lat"),b.getDouble("to_lon"));
            daMark = mMap.addMarker(new MarkerOptions().position(from).title(getString(R.string.Destination)));
            aMark = mMap.addMarker(new MarkerOptions().position(to).title(getString(R.string.Destination)));
            percorso = mMap.addPolyline(new PolylineOptions().add(from,to).width(8).color(Color.rgb(18, 96, 147)));
            fromLocation = new Location(b.getString("from_id"),b.getDouble("from_lat"),b.getDouble("from_lon"));
            toLocation = new Location(b.getString("to_id"),b.getDouble("to_lat"),b.getDouble("to_lon"));
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(daMark.getPosition());
            builder.include(aMark.getPosition());
            LatLngBounds bounds = builder.build();
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;
            int padding = (int) (width * 0.33);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
            InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(fromText.getApplicationWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
            mMap.moveCamera(cameraUpdate);
            float zoom = mMap.getCameraPosition().zoom;
            mMap.animateCamera( CameraUpdateFactory.zoomTo(zoom - 1));
            fromText.clearFocus();
            System.out.println("fromLocation: "+fromLocation.toString());
            System.out.println("toLocation: "+toLocation.toString());
        }
        if(b.getString("travel_id") == null && b.getString("box_id") == null){
            LatLng rome = new LatLng(41.9498953, 12.4727082);
            fromText.requestFocus();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(rome, zoomLevel, 0f, 0f)));
        }
    }



    public void btnOk(View view){
        if(travel_id != null && box_id == null){
            Bundle bundle = new Bundle();
            bundle.putString("from_id",from_id);
            bundle.putString("to_id",to_id);
            bundle.putString("travel_id",travel_id);
            bundle.putString("box_id",null);
            bundle.putDouble("from_lat",from_lat);
            bundle.putDouble("from_lon",from_lon);
            bundle.putDouble("to_lat",to_lat);
            bundle.putDouble("to_lon",to_lon);
            bundle.putString("user_id",user_id);
            //MapActivity.boxAttuale = (Box) arg0.getItemAtPosition(index);
            Intent startNewActivity = new Intent(this, FormViaggioActivity.class);
            startNewActivity.putExtras(bundle);
            startActivity(startNewActivity);
        }
        if(travel_id == null && box_id != null){
            Bundle bundle = new Bundle();
            bundle.putString("from_id",from_id);
            bundle.putString("to_id",to_id);
            bundle.putString("travel_id",null);
            bundle.putString("box_id",box_id);
            bundle.putDouble("from_lat",from_lat);
            bundle.putDouble("from_lon",from_lon);
            bundle.putDouble("to_lat",to_lat);
            bundle.putDouble("to_lon",to_lon);
            bundle.putString("user_id",user_id);
            if(image_bitmap == null){
                bundle.putString("image_bitmap",null);
            }else{
                bundle.putString("image_bitmap",image_bitmap);
            }
            //MapActivity.boxAttuale = (Box) arg0.getItemAtPosition(index);
            Intent startNewActivity = new Intent(this, FormPaccoActivity.class);
            startNewActivity.putExtras(bundle);
            startActivity(startNewActivity);
        }if(travel_id == null && box_id == null){
            if (fromPlace == null || toPlace == null) {
                AlertDialog.Builder miaAlert = new AlertDialog.Builder(this);
                miaAlert.setTitle(getResString(R.string.alert_attention));
                miaAlert.setMessage(getResString(R.string.alert_insert));
                AlertDialog alert = miaAlert.create();
                alert.show();
                return;
            } else {
                switch ( tipoProcesso) {
                    case TIPO_PROCESSO_PACCO:
                        System.out.println("FROM_PLACE_ID: "+fromPlace.getPlaceId());
                        System.out.println("TO_PLACE_ID: "+toPlace.getPlaceId());
                        Global.getInstance().attributiPacco.partenza = new Location(fromPlace.getPlaceId(),fromLocation.getLat(),fromLocation.getLon());
                        Global.getInstance().attributiPacco.arrivo = new Location(toPlace.getPlaceId(),toLocation.getLat(),toLocation.getLon());
                        startActivity(FormPaccoActivity.class );
                        break;
                    case TIPO_PROCESSO_VIAGGIO:
                        // // TODO: 10/02/2017
                        System.out.println("FROM_PLACE_ID: "+fromPlace.getPlaceId());
                        System.out.println("TO_PLACE_ID: "+toPlace.getPlaceId());
                        Global.getInstance().attributiViaggio.partenza = new Location(fromPlace.getPlaceId(),fromLocation.getLat(),fromLocation.getLon());
                        Global.getInstance().attributiViaggio.arrivo = new Location(toPlace.getPlaceId(),toLocation.getLat(),toLocation.getLon());
                        startActivity(FormViaggioActivity.class );
                        break;
                }
            }
        }
    }

    public void startActivity(Class cls) {
        Intent startNewActivity = new Intent(this, cls);
        startActivity(startNewActivity);
    }


    public String getResString(int id) {
        return (String) getResources().getText(id);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                boxAttuale = null;
                travelAttuale = null;
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                            // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    // ProgressBar fp, ProgressBar tp, ImageButton fb, ImageButton tb,
    public static ArrayList<CustomPlace> autocomplete(String input) {
        String mainTitle = "";
        ArrayList<CustomPlace> resultList = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e("Autocomplete", "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e("Autocomplete", "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        try {
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");
            resultList = new ArrayList(predsJsonArray.length());
                for (int i = 0; i < 2; i++) {
                    ArrayList resultList2 = null;
                    HttpURLConnection conn2 = null;
                    StringBuilder jsonResults2 = new StringBuilder();
                    double lat = 0;
                    double lng = 0;
                    try {
                        String id = predsJsonArray.getJSONObject(i).getString("place_id");
                        JSONObject strctform = predsJsonArray.getJSONObject(i).getJSONObject("structured_formatting");
                        mainTitle = strctform.getString("main_text");
                        StringBuilder sb2 = new StringBuilder(PLACES_DETAIL_BASE + OUT_JSON);
                        sb2.append("?key=" + API_KEY);
                        sb2.append("&placeid=" + URLEncoder.encode(id, "utf8"));
                        URL url2 = new URL(sb2.toString());
                        conn2 = (HttpURLConnection) url2.openConnection();
                        InputStreamReader in2 = new InputStreamReader(conn2.getInputStream());
                        int read2;
                        char[] buff = new char[1024];
                        while ((read2 = in2.read(buff)) != -1) {
                            jsonResults2.append(buff, 0, read2);
                        }
                    } catch (MalformedURLException e) {
                        Log.e("Autocomplete", "Error processing Places API URL", e);
                        return resultList;
                    } catch (IOException e) {
                        Log.e("Autocomplete", "Error connecting to Places API", e);
                        return resultList;
                    } finally {
                        if (conn2 != null) {
                            conn2.disconnect();
                        }
                    }
                    try {
                        System.out.println(jsonResults2.toString());
                        JSONObject jsonObj2 = new JSONObject(jsonResults2.toString());
                        JSONObject predsJsonArray2 = jsonObj2.getJSONObject("result");
                        JSONObject geometry = predsJsonArray2.getJSONObject("geometry");
                        JSONObject location = geometry.getJSONObject("location");
                        lat = location.getDouble("lat");
                        lng = location.getDouble("lng");

                    }catch (JSONException e) {
                        Log.e("LOG", "Cannot process JSON results", e);
                    }
                    System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                    System.out.println("ID ---> "+predsJsonArray.getJSONObject(i).getString("place_id"));

                    System.out.println("============================================================");
                    LatLng latLng = new LatLng(lat,lng);
                    //String final_result = predsJsonArray.getJSONObject(i).getString("description")+"["+latLng.latitude+","+latLng.longitude+"]";
                    CustomPlace place2add = new CustomPlace(predsJsonArray.getJSONObject(i).getString("description"),latLng,mainTitle,predsJsonArray.getJSONObject(i).getString("place_id"));
                    resultList.add(place2add);
                }

        } catch (JSONException e) {
            Log.e("LOG", "Cannot process JSON results", e);
        }
        return resultList;
    }

    public static class CustomPlace{
        private String placeid;
        private String main_title;
        private String name;
        private LatLng latLng;

        public CustomPlace(String name, LatLng latLng, String main_title, String placeid){
            this.placeid = placeid;
            this.name = name;
            this.main_title = main_title;
            this.latLng = latLng;
        }
        public String getPlaceId(){ return placeid; }
        public String getName(){
            return  name;
        }
        public String getMainTitle(){
            return main_title;
        }
        public LatLng getLatLng(){
            return latLng;
        }
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<CustomPlace> implements Filterable {
        private ArrayList<CustomPlace> resultList;
        private final String MY_DEBUG_TAG = "CustomerAdapter";
        private int viewResourceId;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
            this.viewResourceId = textViewResourceId;
        }
        @Override
        public int getCount() {
            return resultList.size();
        }
        @Override
        public CustomPlace getItem(int index) {
            return resultList.get(index);
        }
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(viewResourceId, null);
            }
            CustomPlace customPlace = resultList.get(position);
            if (customPlace != null) {
                TextView customNameLabel = (TextView) v.findViewById(R.id.customNameLabel);
                if (customNameLabel != null) {
                    customNameLabel.setText(String.valueOf(customPlace.getName()));

                }
            }
            return v;
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // from_progress, to_progress, fromMarker, toMarker,
                        resultList = autocomplete(constraint.toString());
                        // Assign the data to the FilterResults
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                fromMarker.setVisibility(View.VISIBLE);
                                toMarker.setVisibility(View.VISIBLE);
                                to_progress.setVisibility(View.GONE);
                                from_progress.setVisibility(View.GONE);
                            }
                        });


                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }
                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }



}