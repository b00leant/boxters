package com.boxters.android.test;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import com.boxters.android.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by undeclinable on 15/03/17.
 */

public class RightDecorator implements DayViewDecorator {
    private final Drawable dr;
    private final ArrayList<Date> disabled_dates;
    private final ArrayList<CalendarDay> dates;

    public RightDecorator(List<Date> disabled_dates, List<CalendarDay> dates, Activity context) {
        dr = context.getResources().getDrawable(R.drawable.ic_just_ended);
        this.dates = new ArrayList<>(dates);
        this.disabled_dates = new ArrayList<>(disabled_dates);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        if(dates.size()>0){
            CalendarDay last = this.dates.get(this.dates.size() - 1);
            if(!disabled_dates.contains(day.getDate())){
                return last.equals(day);
            }
        }else if(dates.size() == 0){
            return false;
        }
        return false;
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setSelectionDrawable(dr);
    }
}