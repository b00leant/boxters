package com.boxters.android.controller.messaging;

import com.boxters.model.messaging.BoxPayload;
import com.boxters.model.messaging.BoxtersPayload;
import com.boxters.model.messaging.RequestBoxInfoPayload;
import com.boxters.model.messaging.SelectedAsPossibleBoxterPayload;
import com.boxters.model.messaging.TestPayload;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;

import org.jetbrains.annotations.NotNull;

/**
 * Created by daniele on 01/03/17.
 */

public class MessageSender {
    private static final String SENDER_ID = "555864418922";

    public static void send(@NotNull SelectedAsPossibleBoxterPayload payload) {
        RemoteMessage.Builder builder = create(payload);
        builder = builder.addData(BoxPayload.BOX_ID_KEY, payload.getBoxId());
        builder = builder.addData(SelectedAsPossibleBoxterPayload.TRAVEL_ID_KEY, payload.getTravelId());
        send(builder.build());
    }

    public static void send(@NotNull RequestBoxInfoPayload payload) {
        RemoteMessage.Builder builder = create(payload);
        builder = builder.addData(BoxPayload.BOX_ID_KEY, payload.getBoxId());
        builder = builder.addData(RequestBoxInfoPayload.TRAVEL_ID_KEY, payload.getTravelId());
        send(builder.build());
    }

    public static void send(@NotNull BoxPayload payload) {
        RemoteMessage.Builder builder = create(payload);
        builder = builder.addData(BoxPayload.BOX_ID_KEY, payload.getBoxId());
        send(builder.build());
    }

    public static void send(@NotNull TestPayload payload) {
        send(create(payload).build());
    }

    private static void send(@NotNull RemoteMessage message) {
        FirebaseMessaging fm = FirebaseMessaging.getInstance();
        fm.send(message);
    }


    private static RemoteMessage.Builder create(@NotNull BoxtersPayload payload) {
        return new RemoteMessage.Builder(SENDER_ID + "@gcm.googleapis.com")
                .setMessageId(payload.computeMessageId(false))
                .addData(BoxtersPayload.SENDER_KEY, payload.getSender())
                .addData(BoxtersPayload.RECEIVER_TOKEN_KEY, payload.getReceiverToken())
                .addData(BoxtersPayload.TYPE_KEY, payload.getType().name());
    }
}
