package com.boxters.android.test;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.boxters.android.DownloadImageTask;
import com.boxters.android.MainActivity;
import com.boxters.android.ProprietaPaccoActivity;
import com.boxters.android.R;
import com.boxters.model.PublicUser;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.boxters.android.R.id.iconSender;


/**
 * Created by David on 27/02/2017.
 */

public class DataReceiverFragment extends Fragment {
    TextView statoPacco;
    TextView nomePacco;
    TextView descrizionePacco;
    TextView emailDestinatario;
    TextView pesoPacco;
    TextView spazioPacco;
    CircleImageView imageSR;
    TextView scadenzaPacco;
    TextView descrizioneArrivo;
    TextView descrizioneProvenienza;


    public Bitmap convert2Bitmap(String base64Str) throws IllegalArgumentException
    {
        byte[] decodedBytes = Base64.decode(
                base64Str.substring(base64Str.indexOf(",")  + 1),
                Base64.DEFAULT
        );

        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    public String convert2String(Bitmap bitmap)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }
    public DataReceiverFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data_pacco, container, false);


        SimpleDateFormat localDateFormat = new SimpleDateFormat("MM/dd/yyyy");
        ImageView boxState= (ImageView) view.findViewById(R.id.box_state_img);
        ImageView boxImage = (ImageView) view.findViewById(R.id.image_layout);
        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        if(ProprietaReceiverActivity.boxAttuale.getPhoto()!=null){
            Bitmap bitmap = convert2Bitmap(ProprietaReceiverActivity.boxAttuale.getPhoto());
            boxImage.setImageBitmap(bitmap);
        }{
            //TODO: Gestire foto anteprima generale
        }


        MainActivity.user.getReceiver((receiver) -> {
            if (receiver != null) {

                //statoPacco = (TextView) view.findViewById(R.id.box_state);
                //statoPacco.setText(receiver.getState(ProprietaReceiverActivity.boxAttuale.getSender(),ProprietaReceiverActivity.boxAttuale.getId()) + "");


            }

        });

        //nomePacco = (TextView) view.findViewById(R.id.nomePacco);
        //nomePacco.setText(ProprietaReceiverActivity.boxAttuale.getObject().toString());

        descrizioneArrivo = (TextView) view.findViewById(R.id.posizioneArrivo);

        descrizioneArrivo = (TextView) view.findViewById(R.id.posizioneArrivo);
        writeCityName(view.getContext(), descrizioneArrivo, ProprietaReceiverActivity.boxAttuale.getArrival().getLat(),ProprietaReceiverActivity.boxAttuale.getArrival().getLon());


        descrizioneProvenienza = (TextView) view.findViewById(R.id.posizionePartenza);
        writeCityName(view.getContext(), descrizioneProvenienza, ProprietaReceiverActivity.boxAttuale.getDeparture().getLat(), ProprietaReceiverActivity.boxAttuale.getDeparture().getLon());

        descrizioneProvenienza = (TextView) view.findViewById(R.id.posizionePartenza);

        String descrizione = ProprietaReceiverActivity.boxAttuale.getDescription();
        if (descrizione == null) {
            descrizione = "";
            }

        descrizionePacco = (TextView) view.findViewById(R.id.descrizionePacco);
        descrizionePacco.setText(descrizione);


        emailDestinatario = (TextView) view.findViewById(R.id.email);



        PublicUser.load(ProprietaReceiverActivity.boxAttuale.getSender(),(user) -> {
            ratingBar.setRating(user.getRating() != null ? user.getRating().floatValue() : 0);
            TextView otheruser = (TextView) view.findViewById(R.id.sender_name);
            otheruser.setText(user.getName());
            emailDestinatario.setText(user.getEmail());
            if (user.getPhoto() != null) {
                imageSR = (CircleImageView) view.findViewById(R.id.iconSR);
                if (imageSR != null)
                    new DownloadImageTask(imageSR).execute(user.getPhoto());
            }

        },false);



        switch (ProprietaReceiverActivity.boxAttuale.getState()){
            case CREATED:
                boxState.setImageResource(R.drawable.ic_hourglass_full_blue_24dp);
                //statoPacco.setText(getString(R.string.Created));
                break;
            case CONFIRMED:
                boxState.setImageResource(R.drawable.ic_done_blue_24dp);
                //statoPacco.setText(getString(R.string.Confirmed));
                break;
            case TRAVELLING:
                boxState.setImageResource(R.drawable.ic_my_location_blue_24dp);
                //statoPacco.setText(getString(R.string.Travelling));
                break;
            case RECEIVED:
                boxState.setImageResource(R.drawable.ic_done_all_blue_24dp);
                //statoPacco.setText(getString(R.string.Received));
                break;
        }

        pesoPacco = (TextView) view.findViewById(R.id.peso);

        if(ProprietaReceiverActivity.boxAttuale.getWeight() <= 1.0){
            pesoPacco.setText("<1");
        }
        else if(ProprietaReceiverActivity.boxAttuale.getWeight() <= 2.0){
            //pesoPacco.setText(getResources().getStringArray(R.array.peso)[1].replace("[","").replace("]",""));
            pesoPacco.setText(">1");
        }
        else if(ProprietaReceiverActivity.boxAttuale.getWeight() <= 3.0){
            //pesoPacco.setText(getResources().getStringArray(R.array.peso)[2].replace("[","").replace("]",""));
            pesoPacco.setText(">5");
        }
        else if(ProprietaReceiverActivity.boxAttuale.getWeight() <= 4.0){
            pesoPacco.setText(">10");
        }else{
            pesoPacco.setText("???");
        }
        spazioPacco = (TextView) view.findViewById(R.id.spazio);
        double s = 1.0;
        double m = 2.0;
        double l = 3.0;

        if(ProprietaReceiverActivity.boxAttuale.getSize() <= 1.0){
            System.out.println("LO SPAZIO È STATO SETTATO S");
            //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[0].replace("[","").replace("]","").split("\\(")[0]);
            spazioPacco.setText("S");
        }
        else if(ProprietaReceiverActivity.boxAttuale.getSize() <= 2.0){
            System.out.println("LO SPAZIO È STATO SETTATO M");
            spazioPacco.setText("M");
            //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[1].replace("[","").replace("]","").split("\\(")[0]);
        }
        else if(ProprietaReceiverActivity.boxAttuale.getSize() <= 3.0){
            System.out.println("LO SPAZIO È STATO SETTATO L");
            spazioPacco.setText("L");
            //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[2].replace("[","").replace("]","").split("\\(")[0]);
        }else{
            System.out.println("IL VERO SPAZIO È "+ProprietaReceiverActivity.boxAttuale.getSize());
            spazioPacco.setText("?");
        }



        scadenzaPacco = (TextView) view.findViewById(R.id.scadenza);
        scadenzaPacco.setText(localDateFormat.format(ProprietaReceiverActivity.boxAttuale.getExpiration()));

        return view;
    }

    public void writeCityName(Context ctx, TextView tv, double lat, double lng){
        Geocoder geoCoder = new Geocoder(ctx, Locale.getDefault());

        try {
            List<Address> addresses = geoCoder.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0)
            {
                tv.setText(addresses.get(0).getLocality().toString());
                System.out.println(addresses.get(0).getLocality());
            }
            else
            {
                // do your staff
            }

        } catch (IOException e) {
            ;
        }
    }
}
