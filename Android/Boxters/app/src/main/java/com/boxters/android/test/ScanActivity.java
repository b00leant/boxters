package com.boxters.android.test;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.boxters.android.MainActivity;
import com.boxters.android.ProprietaViaggioActivity;
import com.boxters.android.R;
import com.boxters.android.Utility;
import com.boxters.android.ViewPagerAdapter;
import com.boxters.model.Travel;

public class ScanActivity extends AppCompatActivity {


    private TabLayout tabLayout;
    private ViewPager viewPager;

    private QRCodeFragmentB qrCodeFragment;
    private String QR;
    public static String boxID;
    public static String sender;
    public static String receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blank);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_proprieta_viaggio_activity);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        //actionBar.setDisplayHomeAsUpEnabled(true);

        this.QR=this.getIntent().getStringExtra("QR");
        this.boxID = this.getIntent().getStringExtra("Box");
        this.sender = this.getIntent().getStringExtra("Sender");
        this.receiver = this.getIntent().getStringExtra("Receiver");
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#FFFFFF"));
        tabLayout.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
        tabLayout.setTabTextColors(Color.WHITE,Color.WHITE);


    }

    public String getResString(int id) {
        return (String) getResources().getText(id);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        this.qrCodeFragment  = new QRCodeFragmentB();
        this.qrCodeFragment.setBox(this,this.QR);


        adapter.addFragment(this.qrCodeFragment, "QRCODE");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        Intent startNewActivity = new Intent( this,ProprietaViaggioActivity.class);
        finishAffinity();
        startActivity(startNewActivity);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                            // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
