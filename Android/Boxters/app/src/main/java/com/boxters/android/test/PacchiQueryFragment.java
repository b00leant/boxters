package com.boxters.android.test;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.boxters.android.MainActivity;
import com.boxters.android.ProprietaPaccoActivity;
import com.boxters.android.ProprietaViaggioActivity;
import com.boxters.android.R;
import com.boxters.model.Box;
import com.boxters.model.BoxState;
import com.boxters.model.PublicBox;
import com.boxters.model.PublicTravel;
import com.boxters.model.TravelState;

import java.util.List;



public class PacchiQueryFragment  extends Fragment {

    public LPQCA aa;
    public PossiblePacchiCA ppa;
    private TextView tv;
    private TextView tv2;
    private ListView lv;
    private ImageView spacer;
    private String boxID;
    public static final int RC_GET_SHIP_CODE = 200;
    public static final int RC_GIVE_SHIP_CODE = 777;
    private ListView lvAuth;

    public PacchiQueryFragment () {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.double_fragment_list_container, container, false);
        lv = (ListView) view.findViewById(R.id.listViewContainer);
        spacer = (ImageView) view.findViewById(R.id.spacer);
        if(ProprietaViaggioActivity.travelAttuale.getState() != TravelState.CREATED){
            lv.setVisibility(View.GONE);
        }
        lvAuth = (ListView) view.findViewById(R.id.listViewContainer2);
        tv = (TextView) view.findViewById(R.id.tvListContainer);
        tv2 = (TextView) view.findViewById(R.id.tvListContainer2);
        tv.setText(getResString(R.string.fragment_no_boxes_yet));
        tv2.setText(getResString(R.string.fragment_no_boxes_yet));
        aa = new LPQCA(this, null);
        ppa = new PossiblePacchiCA(this,null);
        lvAuth.setAdapter(ppa);
        lv.setAdapter(aa);
        lv.setEmptyView(tv);
        lvAuth.setEmptyView(tv2);

        updateAuthorizedBoxList();
        updateList();
        //ListUtility.setListViewHeightBasedOnChildren(lv);
        //ListUtility.setListViewHeightBasedOnChildren(lvAuth);





        return view;


    }
    private void updateAuthorizedBoxList() {
        if(ProprietaViaggioActivity.travelAttuale != null) {
            ProprietaViaggioActivity.travelAttuale.loadBoxes((box) -> {
                if (box != null) {
                    ppa.add(box);
                    ppa.notifyDataSetChanged();
                    ListUtility.setDynamicHeight(lvAuth);
                    if(aa.getCount() > 0){
                        spacer.setVisibility(View.VISIBLE);
                        //lvAuth.setVisibility(View.VISIBLE);
                    }
                    if(ppa.getCount() == 0){
                        lvAuth.setVisibility(View.GONE);
                        spacer.setVisibility(View.GONE);
                    }
                }
            }, false);
        }
    }

    public void updateAuthorizedBoxListClear() {
        ppa.clear();
        if(ProprietaViaggioActivity.travelAttuale != null) {
            ProprietaViaggioActivity.travelAttuale.loadBoxes((box) -> {
                if (box != null) {
                    ppa.add(box);
                    ppa.notifyDataSetChanged();
                    if(aa.getCount() > 0){
                        spacer.setVisibility(View.VISIBLE);
                        //lvAuth.setVisibility(View.VISIBLE);
                    }
                    if(ppa.getCount() == 0){
                        lvAuth.setVisibility(View.GONE);
                        spacer.setVisibility(View.GONE);
                    }
                    //ListUtility.setListViewHeightBasedOnChildren(lvAuth);
                }
            }, false);
        }
    }

    private void updateList() {
        if (ProprietaViaggioActivity.travelAttuale != null) {
            PublicBox.queryForTravel(ProprietaViaggioActivity.travelAttuale, 3.0, (box) -> {
                if (box != null) {
                    aa.add(box);
                    aa.notifyDataSetChanged();
                    ListUtility.setDynamicHeight(lv);
                }
            }, false);
        }


    }

    public void updateListClear() {
        if (ProprietaViaggioActivity.travelAttuale != null) {
            aa.clear();
            PublicBox.queryForTravel(ProprietaViaggioActivity.travelAttuale, 3.0, (box) -> {
                if (box != null) {
                    aa.add(box);
                    aa.notifyDataSetChanged();
                    //ListUtility.setListViewHeightBasedOnChildren(lv);
                }
            }, false);
        }
    }

    private String getResString(int id) {
        return (String) getResources().getText(id);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode ){
            case RC_GET_SHIP_CODE: {
                if (resultCode == Activity.RESULT_OK) {
                    //cambio stato dell'authorizedbox

                    boxID = data.getStringExtra("Box");

                    if (boxID != null) {
                        Box.load(MainActivity.user.getId(),boxID, (Box box) -> {

                            ProprietaViaggioActivity.travelAttuale.updateBoxState(BoxState.TRAVELLING,box.getSender(),box.getId(),null);
                            box.waitTravellingConfirm((result) -> {

                            } );
                        },false);
                        Toast.makeText(getContext(),getActivity().getString(R.string.You_have_the_box),Toast.LENGTH_SHORT).show();

                        Intent startNewActivity = new Intent(getContext(),ProprietaViaggioActivity.class);
                        getActivity().finishAffinity();
                        getActivity().startActivity(startNewActivity);
                    }

                }
                break;
            }

            case RC_GIVE_SHIP_CODE: {
                if (resultCode == -1) {
                    //cambio stato dell'authorizedbox

                }
                break;
            }


        }
    }

}