package com.boxters.android.test;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;

import com.boxters.android.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * Created by undeclinable on 15/03/17.
 */

public class LeftDecorator implements DayViewDecorator{
    private final ArrayList<CalendarDay> dates;
    private final ArrayList<Date> disabled_dates;
    private final Drawable dl;

    public LeftDecorator(List<Date> disabled_dates, List<CalendarDay> dates, Activity context) {
        dl = context.getResources().getDrawable(R.drawable.ic_just_start_started);
        this.dates = new ArrayList<>(dates);
        this.disabled_dates = new ArrayList<>(disabled_dates);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        if(dates.size()>0){
            CalendarDay first = this.dates.get(0);
            if(!disabled_dates.contains(day.getDate())){
                return first.equals(day);
            }
        }else if(dates.size() == 0){
            return false;
        }
        return false;
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setSelectionDrawable(dl);
    }
}
/*


    de = context.getResources().getDrawable(R.drawable.ic_just_ended);
    dc = context.getResources().getDrawable(R.drawable.ic_just_center);
* */