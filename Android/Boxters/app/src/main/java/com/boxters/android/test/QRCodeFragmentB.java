package com.boxters.android.test;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.boxters.android.MainActivity;
import com.boxters.android.ProprietaPaccoActivity;
import com.boxters.android.ProprietaViaggioActivity;
import com.boxters.android.R;
import com.boxters.android.Utility;
import com.boxters.model.AuthorizedBox;
import com.boxters.model.Box;
import com.boxters.model.BoxState;
import com.boxters.model.PublicTravel;
import com.boxters.model.PublicUser;

/**
 * Created by giuse on 09/03/2017.
 */

public class QRCodeFragmentB extends WebViewFragment {

    private Context c;




    public void setBox(Context _c, String qr) {

        c=_c;

        String s= Utility.readResourceTextFileEx(c, R.raw.qr_code);
        s = s.replace("{qrcode}",String.valueOf( qr.trim().toUpperCase()));
        setHTML(s);
    }



    @android.webkit.JavascriptInterface
    public void jsRicevuto() {

        AlertDialog.Builder alert = new AlertDialog.Builder(c);
        alert.setTitle("QR Scan");
        alert.setMessage("Hai consegnato il pacco?");
        alert.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(
                            DialogInterface dialog,
                            int whichButton) {
                        dialog.cancel();
                    }
                });
        alert.setPositiveButton("SI",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(
                            DialogInterface dialog,
                            int whichButton) {

                        ProprietaViaggioActivity.travelAttuale.updateBoxState(BoxState.RECEIVED,ScanActivity.sender,ScanActivity.boxID,null);
                        PublicUser.load(ScanActivity.receiver,(rec) -> {
                            rec.waitReceivedConfirm(ScanActivity.sender,ScanActivity.boxID, (res) -> {

                                android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(getContext());
                                alert.setTitle("Scan OK");
                                alert.setMessage("Hai Consegnato il pacco");
                                alert.setNegativeButton("CANCEL",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int whichButton) {

                                                getActivity().finish();
                                            }
                                        });
                                alert.setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int whichButton) {

                                                getActivity().finish();
                                            }
                                        });
                                alert.create().show();

                            });

                        },false);


                    }
                });
        alert.create().show();




    }
}