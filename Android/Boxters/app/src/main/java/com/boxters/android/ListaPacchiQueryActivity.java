package com.boxters.android;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;


import com.boxters.android.test.LPQCA;
import com.boxters.model.PublicBox;
import com.boxters.model.Travel;
import com.boxters.android.test.ListaPacchiQueryCustomArrayAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.vision.text.Text;

public class ListaPacchiQueryActivity extends AppCompatActivity {

    ListaPacchiQueryCustomArrayAdapter aa;
    public static Travel currentTravel = null;
    public static GoogleApiClient mGoogleApiClient;

    @Override
    public void onBackPressed() {
        Intent startNewActivity = new Intent( this,MainActivity.class);
        finishAffinity();
        startActivity(startNewActivity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_lista_pacchi_query);
        ListView lv = (ListView) findViewById(R.id.listViewPacchiQuery);
        TextView emptyView = (TextView) findViewById(R.id.tvQueryPacchi);
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage((FragmentActivity) this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        // your code here
                    }
                })
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        aa = new ListaPacchiQueryCustomArrayAdapter(this, null);
        lv.setEmptyView(emptyView);
        lv.setAdapter(aa);
        updateList();


    }

    private void updateList() {
        PublicBox.queryForTravel(currentTravel, 3.0, (box) -> {
            if (box != null) {
                aa.add(box);
                aa.notifyDataSetChanged();
            }
        }, false);

    }


    public void startActivity(Class cls) {
        Intent startNewActivity = new Intent(this, cls);
        startActivity(startNewActivity);
    }


    public void onQuit(View view) {
        finishAffinity();
        startActivity(MainActivity.class);
        TabLayout.Tab tab = MainActivity.tabLayout.getTabAt(1);
        tab.select();
    }
}
