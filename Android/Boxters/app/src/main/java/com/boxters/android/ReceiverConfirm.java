package com.boxters.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.boxters.android.controller.messaging.MessagingService;
import com.boxters.model.AuthorizedBox;
import com.boxters.model.Box;
import com.boxters.model.BoxState;
import com.boxters.model.User;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Logger;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class ReceiverConfirm extends AppCompatActivity {
    private User sender;
    private AuthorizedBox boxID;
    private Button acceptButton;
    private Button denyButton;
    private TextView pesoPacco;
    private FirebaseUser user;
    private static boolean started = false;
    private static final boolean DEBUG = true;
    private TextView spazioPacco;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();

        setContentView(R.layout.activity_receiver_confirm);

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage((FragmentActivity) this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        // your code here
                    }
                })
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();


        SimpleDateFormat localDateFormat = new SimpleDateFormat("MM/dd/yyyy");

        CircleImageView iconBoxter = (CircleImageView) findViewById(R.id.iconSR);

        acceptButton = (Button) findViewById(R.id.accept);

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainActivity.user != null) {
                    Log.d("LOGIN", "ALREADY SIGNED IN");
                    User.load(MainActivity.user.getId(), (User _user) -> {
                        if (_user != null) {
                            _user.getReceiver((rec) -> {
                                if (rec != null) {
                                    rec.addBox(sender.getId(), boxID.getId() ,(save) -> {
                                        CharSequence text = "Box accepted!";
                                        int duration = Toast.LENGTH_SHORT;
                                        Toast toast = Toast.makeText(ReceiverConfirm.this, text, duration);
                                        toast.show();
                                        Intent startNewActivity = new Intent(ReceiverConfirm.this, MainActivity.class);
                                        finishAffinity();
                                        startActivity(startNewActivity);
                                    });

                                }

                            });
                        }
                    }, false);
                }
            }
        });

        denyButton = (Button) findViewById(R.id.deny);

        denyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startNewActivity = new Intent(ReceiverConfirm.this, MainActivity.class);
                finishAffinity();
                startActivity(startNewActivity);
            }
        });

        Bundle extras = intent.getExtras();

        String senderString = extras.getString(MessagingService.SENDER_KEY);
        User.load(senderString,(User user) -> {
            this.sender = user;

            if (user.getPhoto() != null) {
                if (iconBoxter != null)
                    new DownloadImageTask(iconBoxter).execute(user.getPhoto());
            }

            TextView nome = (TextView) findViewById(R.id.sender_name);
            nome.setText(sender.getName());

            TextView email = (TextView) findViewById(R.id.email);
            email.setText(sender.getEmail());

            RatingBar rating = (RatingBar) findViewById(R.id.ratingBar);
            rating.setMax(5);
            if(sender.getRating()==null){
                double rating_s1 = 0;
                rating.setRating( (float) rating_s1);
            }else{
                double rating_s2 = sender.getRating();
                rating.setRating( (float) rating_s2);
            }


            TextView senderDesc = (TextView) findViewById(R.id.descrizionePacco);
            //senderDesc.append(sender.getDescription());
        }, false);
        String boxString = extras.getString(MessagingService.BOX_ID_KEY);
        AuthorizedBox.load(senderString, boxString, (AuthorizedBox box) -> {
            this.boxID = box; //ID??

            TextView boxName = (TextView) findViewById(R.id.nomePacco);
            boxName.setText(boxID.getObject());

            Places.GeoDataApi.getPlaceById(mGoogleApiClient, boxID.getArrival().getName())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                final Place myPlace = places.get(0);
                                Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                                TextView posArr = (TextView) findViewById(R.id.posizioneArrivo);
                                posArr.setText(myPlace.getName());
                            } else {
                                Log.e("PLACES DETAILS", "Place not found");
                                Log.e("PLACES DETAILS",places.getStatus().getStatusMessage());
                            }
                            places.release();
                        }
                    });
            Places.GeoDataApi.getPlaceById(mGoogleApiClient, boxID.getDeparture().getName())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                final Place myPlace = places.get(0);
                                Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                                TextView posPar = (TextView) findViewById(R.id.posizionePartenza);
                                posPar.setText(myPlace.getName());
                            } else {
                                Log.e("PLACES DETAILS", "Place not found");
                            }
                            places.release();
                        }
                    });

            TextView desc = (TextView) findViewById(R.id.descrizionePacco);
            desc.setText((boxID.getDescription()));

            ImageView boxState= (ImageView) findViewById(R.id.box_state_img);

            switch (boxID.getState()){
                case CREATED:
                    boxState.setImageResource(R.drawable.ic_hourglass_full_blue_24dp);
                    //statoPacco.setText(getString(R.string.Created));
                    break;
                case CONFIRMED:
                    boxState.setImageResource(R.drawable.ic_done_blue_24dp);
                    //statoPacco.setText(getString(R.string.Confirmed));
                    break;
                case TRAVELLING:
                    boxState.setImageResource(R.drawable.ic_my_location_blue_24dp);
                    //statoPacco.setText(getString(R.string.Travelling));
                    break;
                case RECEIVED:
                    boxState.setImageResource(R.drawable.ic_done_all_blue_24dp);
                    //statoPacco.setText(getString(R.string.Received));
                    break;
            }

            pesoPacco = (TextView) findViewById(R.id.peso);

            if(boxID.getWeight() <= 1.0){
                pesoPacco.setText("<1");
            }
            else if(boxID.getWeight() <= 2.0){
                //pesoPacco.setText(getResources().getStringArray(R.array.peso)[1].replace("[","").replace("]",""));
                pesoPacco.setText(">1");
            }
            else if(boxID.getWeight() <= 3.0){
                //pesoPacco.setText(getResources().getStringArray(R.array.peso)[2].replace("[","").replace("]",""));
                pesoPacco.setText(">5");
            }
            else if(boxID.getWeight() <= 4.0){
                pesoPacco.setText(">10");
            }else{
                pesoPacco.setText("???");
            }
            spazioPacco = (TextView) findViewById(R.id.spazio);
            double s = 1.0;
            double m = 2.0;
            double l = 3.0;

            if(boxID.getSize() <= 1.0){
                System.out.println("LO SPAZIO È STATO SETTATO S");
                //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[0].replace("[","").replace("]","").split("\\(")[0]);
                spazioPacco.setText("S");
            }
            else if(boxID.getSize() <= 2.0){
                System.out.println("LO SPAZIO È STATO SETTATO M");
                spazioPacco.setText("M");
                //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[1].replace("[","").replace("]","").split("\\(")[0]);
            }
            else if(boxID.getSize() <= 3.0){
                System.out.println("LO SPAZIO È STATO SETTATO L");
                spazioPacco.setText("L");
                //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[2].replace("[","").replace("]","").split("\\(")[0]);
            }else{
                System.out.println("IL VERO SPAZIO È "+boxID.getSize());
                spazioPacco.setText("?");
            }




            TextView scadenza = (TextView) findViewById(R.id.scadenza);
            scadenza.setText(localDateFormat.format(boxID.getExpiration()));
        }, false);





    }


    @Override
    protected void onNewIntent(Intent intent) {
        Bundle extras = intent.getExtras();

        SimpleDateFormat localDateFormat = new SimpleDateFormat("MM/dd/yyyy");
        CircleImageView iconBoxter = (CircleImageView) findViewById(R.id.iconSR);


        acceptButton = (Button) findViewById(R.id.accept);

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainActivity.user != null) {
                    Log.d("LOGIN", "ALREADY SIGNED IN");
                    User.load(MainActivity.user.getId(), (User _user) -> {
                        if (_user != null) {
                            _user.getReceiver((rec) -> {
                                if (rec != null) {
                                    rec.addBox(sender.getId(), boxID.getId() ,(save) -> {
                                        CharSequence text = "Box accepted!";
                                        int duration = Toast.LENGTH_SHORT;
                                        Toast toast = Toast.makeText(ReceiverConfirm.this, text, duration);
                                        toast.show();
                                        Intent startNewActivity = new Intent(ReceiverConfirm.this, MainActivity.class);
                                        finishAffinity();
                                        startActivity(startNewActivity);
                                    });

                                }

                            });
                        }
                    }, false);
                }
            }
        });

        denyButton = (Button) findViewById(R.id.deny);

        denyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startNewActivity = new Intent(ReceiverConfirm.this, MainActivity.class);
                finishAffinity();
                startActivity(startNewActivity);
            }
        });

        String senderString = extras.getString(MessagingService.SENDER_KEY);
        User.load(senderString,(User user) -> {

            if (user.getPhoto() != null) {
                if (iconBoxter != null)
                    new DownloadImageTask(iconBoxter).execute(user.getPhoto());
            }

            this.sender = user;
            TextView nome = (TextView) findViewById(R.id.sender_name);
            nome.setText(sender.getName());

            TextView email = (TextView) findViewById(R.id.email);
            email.setText(sender.getEmail());

            RatingBar rating = (RatingBar) findViewById(R.id.ratingBar);
            rating.setMax(5);
            if(sender.getRating()==null){
                double rating_s1 = 0;
                rating.setRating( (float) rating_s1);
            }else{
                double rating_s2 = sender.getRating();
                rating.setRating( (float) rating_s2);
            }


            TextView senderDesc = (TextView) findViewById(R.id.descrizionePacco);
            //senderDesc.append(sender.getDescription());
        }, false);
        String boxString = extras.getString(MessagingService.BOX_ID_KEY);
        AuthorizedBox.load(senderString, boxString, (AuthorizedBox box) -> {
            this.boxID = box; //ID??

            TextView boxName = (TextView) findViewById(R.id.nomePacco);
            boxName.setText(boxID.getObject());

            Places.GeoDataApi.getPlaceById(mGoogleApiClient, boxID.getArrival().getName())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                final Place myPlace = places.get(0);
                                Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                                TextView posArr = (TextView) findViewById(R.id.posizioneArrivo);
                                posArr.setText(myPlace.getName());
                            } else {
                                Log.e("PLACES DETAILS", "Place not found");
                                Log.e("PLACES DETAILS",places.getStatus().getStatusMessage());
                            }
                            places.release();
                        }
                    });
            Places.GeoDataApi.getPlaceById(mGoogleApiClient, boxID.getDeparture().getName())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                final Place myPlace = places.get(0);
                                Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                                TextView posPar = (TextView) findViewById(R.id.posizionePartenza);
                                posPar.setText(myPlace.getName());
                            } else {
                                Log.e("PLACES DETAILS", "Place not found");
                            }
                            places.release();
                        }
                    });

            TextView desc = (TextView) findViewById(R.id.descrizionePacco);
            desc.setText((boxID.getDescription()));

            ImageView boxState= (ImageView) findViewById(R.id.box_state_img);

            switch (boxID.getState()){
                case CREATED:
                    boxState.setImageResource(R.drawable.ic_hourglass_full_blue_24dp);
                    //statoPacco.setText(getString(R.string.Created));
                    break;
                case CONFIRMED:
                    boxState.setImageResource(R.drawable.ic_done_blue_24dp);
                    //statoPacco.setText(getString(R.string.Confirmed));
                    break;
                case TRAVELLING:
                    boxState.setImageResource(R.drawable.ic_my_location_blue_24dp);
                    //statoPacco.setText(getString(R.string.Travelling));
                    break;
                case RECEIVED:
                    boxState.setImageResource(R.drawable.ic_done_all_blue_24dp);
                    //statoPacco.setText(getString(R.string.Received));
                    break;
            }

            pesoPacco = (TextView) findViewById(R.id.peso);

            if(boxID.getWeight() <= 1.0){
                pesoPacco.setText("<1");
            }
            else if(boxID.getWeight() <= 2.0){
                //pesoPacco.setText(getResources().getStringArray(R.array.peso)[1].replace("[","").replace("]",""));
                pesoPacco.setText(">1");
            }
            else if(boxID.getWeight() <= 3.0){
                //pesoPacco.setText(getResources().getStringArray(R.array.peso)[2].replace("[","").replace("]",""));
                pesoPacco.setText(">5");
            }
            else if(boxID.getWeight() <= 4.0){
                pesoPacco.setText(">10");
            }else{
                pesoPacco.setText("???");
            }
            spazioPacco = (TextView) findViewById(R.id.spazio);
            double s = 1.0;
            double m = 2.0;
            double l = 3.0;

            if(boxID.getSize() <= 1.0){
                System.out.println("LO SPAZIO È STATO SETTATO S");
                //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[0].replace("[","").replace("]","").split("\\(")[0]);
                spazioPacco.setText("S");
            }
            else if(boxID.getSize() <= 2.0){
                System.out.println("LO SPAZIO È STATO SETTATO M");
                spazioPacco.setText("M");
                //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[1].replace("[","").replace("]","").split("\\(")[0]);
            }
            else if(boxID.getSize() <= 3.0){
                System.out.println("LO SPAZIO È STATO SETTATO L");
                spazioPacco.setText("L");
                //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[2].replace("[","").replace("]","").split("\\(")[0]);
            }else{
                System.out.println("IL VERO SPAZIO È "+boxID.getSize());
                spazioPacco.setText("?");
            }




            TextView scadenza = (TextView) findViewById(R.id.scadenza);
            scadenza.setText(localDateFormat.format(boxID.getExpiration()));
        }, false);
    }

    public void onDeny(View view){
        Intent startNewActivity = new Intent(this, MainActivity.class);
        finishAffinity();
        startActivity(startNewActivity);
    }

    public void writeCityName(Context ctx, TextView tv, double lat, double lng){
        Geocoder geoCoder = new Geocoder(ctx, Locale.getDefault());

        try {
            List<Address> addresses = geoCoder.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0)
            {
                tv.setText(addresses.get(0).getLocality().toString());
                System.out.println(addresses.get(0).getLocality());
            }
            else
            {
                // do your staff
            }

        } catch (IOException e) {
            ;
        }
    }

    public void onAccetta(View view) {

        MainActivity.user.getReceiver((rec) -> {
            if (rec != null) {
                rec.addBox(sender.getId(), boxID.getId() ,(save) -> {
                    CharSequence text = "Box accepted!";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(ReceiverConfirm.this, text, duration);
                    toast.show();
                    Intent startNewActivity = new Intent(this, MainActivity.class);
                    finishAffinity();
                    startActivity(startNewActivity);
                });

            }

        });

    }
}
