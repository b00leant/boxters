package com.boxters.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.boxters.android.test.LVQCA;
import com.boxters.model.Box;
import com.boxters.model.PublicTravel;
import com.boxters.android.test.ListaViaggiatoriQueryCustomArrayAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;

public class ListaViaggiatoriQueryActivity extends AppCompatActivity {
    ListaViaggiatoriQueryCustomArrayAdapter aa;


    Global G = Global.getInstance();
    public static Box currentBox = null;
    private static final String TAG = "CardListActivity";
    private ListView listView;
    public static GoogleApiClient mGoogleApiClient;

    @Override
    public void onBackPressed() {
        Intent startNewActivity = new Intent( this,MainActivity.class);
        finishAffinity();
        startActivity(startNewActivity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_viaggiatori_query);

        TextView emptyView = (TextView) findViewById(R.id.tvQueryViaggiatori);
        ListView lv = (ListView) findViewById(R.id.listViewViaggiatori);
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage((FragmentActivity) this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        // your code here
                    }
                })
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        lv.addHeaderView(new View(this));
        lv.addFooterView(new View(this));

        aa = new ListaViaggiatoriQueryCustomArrayAdapter(this, null);

        lv.setEmptyView(emptyView);
        lv.setAdapter(aa);
        updateList();
    }


    private void updateList() {
        PublicTravel.queryForBox(currentBox, 3.0, (travel) -> {
            if (travel != null) {
                aa.add(travel);
                aa.notifyDataSetChanged();
            }
        }, false);

    }




    public void startActivity(Class cls) {
        Intent startNewActivity = new Intent(this, cls);
        startActivity(startNewActivity);
    }


    public void onQuit(View view) {


        finishAffinity();
        startActivity(MainActivity.class);
    }
}
