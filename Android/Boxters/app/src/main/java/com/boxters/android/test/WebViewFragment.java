package com.boxters.android.test;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.boxters.android.R;


public class WebViewFragment extends Fragment {
    protected WebView wv;
    public WebViewFragment() {}
    protected String standByUrl="";
    protected String standByHtml="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= (View) inflater.inflate(R.layout.fragment_web_browser, container, false);
        wv= (WebView) view.findViewById(R.id.web_view);

        wv.getSettings().setJavaScriptEnabled(true);
        wv.addJavascriptInterface( this, "JSInterface");


        if (standByUrl!="") loadUrl(standByUrl);
        if (standByHtml !="")  setHTML(standByHtml );

        return view;
    }

    public void loadUrl(String url) {
        if (wv != null)
            wv.loadUrl(url);
        else
            standByUrl=url;
    }

    public void setHTML(String html) {
        if (wv != null)
            wv.loadData(html, "text/html", "UTF8");
        else
            standByHtml=html;
    }


}