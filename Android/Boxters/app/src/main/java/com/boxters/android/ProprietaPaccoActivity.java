package com.boxters.android;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.boxters.android.controller.messaging.MessageSender;
import com.boxters.android.test.PossibleBoxtersFragment;
import com.boxters.model.AuthorizedBox;
import com.boxters.model.Box;
import com.boxters.android.test.BoxterProfileFragment;
import com.boxters.android.test.RicezionePaccoFragment;
import com.boxters.android.test.DataPaccoFragment;
import com.boxters.android.test.FeedBackFragment;
import com.boxters.android.test.QRCodeFragment;
import com.boxters.android.test.ViaggiatoriQueryFragment;
import com.boxters.model.PublicTravel;
import com.boxters.model.PublicUser;
import com.boxters.model.Travel;
import com.boxters.model.User;
import com.boxters.model.messaging.RequestBoxInfoPayload;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;

import static com.boxters.android.R.styleable.MenuItem;
import static com.boxters.model.BoxState.CONFIRMED;
import static com.boxters.model.BoxState.CREATED;

public class ProprietaPaccoActivity extends AppCompatActivity {

    public static Box boxAttuale;
    TextView paccoFrom;
    TextView paccoTo;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private User myreceiver;
    public static PublicTravel mytravel;
    private DataPaccoFragment proprietaPacco;
    //private InfoPaccoFragment proprietaPacco;
    public static GoogleApiClient mGoogleApiClient;
    //private ViaggiFragment  viaggiPacco;
    private QRCodeFragment qrcode;
    private ViaggiatoriQueryFragment viaggiatoriQuery;
    private BoxterProfileFragment boxterProfileFragment;
    private RicezionePaccoFragment qrBoxterRicevePacco;
    private PossibleBoxtersFragment possibleBoxtersFragment;
    private FeedBackFragment feedBackFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proprieta_pacco);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_proprieta_pacco_activity);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(boxAttuale.getObject());
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        //paccoTo = (TextView) findViewById(R.id.paccoTo);
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage((FragmentActivity) this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        // your code here
                    }
                })
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        User.load(boxAttuale.getReceiver(), (User _user) -> {
            if (_user != null){
                this.myreceiver = _user;
            }
        }, true);

        //paccoTo.setText(boxAttuale.getArrival().getName());

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#FFFFFF"));
        tabLayout.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
        tabLayout.setTabTextColors(Color.WHITE,Color.WHITE);
    }
    public String getResString(int id) {
        return (String) getResources().getText(id);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        switch (this.boxAttuale.getState()) {

            case CREATED:

                this.proprietaPacco  = new DataPaccoFragment();
                this.qrcode = new QRCodeFragment();
                this.viaggiatoriQuery = new ViaggiatoriQueryFragment();
                //this.possibleBoxtersFragment = new PossibleBoxtersFragment();
                this.qrcode.setBox(this,boxAttuale);

                adapter.addFragment(this.proprietaPacco, getResString(R.string.title_properties));
                adapter.addFragment(this.viaggiatoriQuery, getResString(R.string.title_boxters));
                //adapter.addFragment(this.possibleBoxtersFragment, getResString(R.string.title_boxters));
                adapter.addFragment(this.qrcode, getResString(R.string.title_ship));
                break;

            case CONFIRMED:

                this.proprietaPacco  = new DataPaccoFragment();
                this.qrcode = new QRCodeFragment();
                this.boxterProfileFragment = new BoxterProfileFragment();
                this.feedBackFragment = new  FeedBackFragment();
                this.qrcode.setBox(this,boxAttuale);

                adapter.addFragment(this.proprietaPacco, getResString(R.string.title_properties));
                adapter.addFragment(this.boxterProfileFragment, getResString(R.string.title_boxter));
                adapter.addFragment(this.qrcode, getResString(R.string.title_ship));
                //adapter.addFragment(this.feedBackFragment, getResString(R.string.title_feedback));
                break;

            case TRAVELLING:

                this.proprietaPacco  = new DataPaccoFragment();
                this.boxterProfileFragment = new BoxterProfileFragment();

                adapter.addFragment(this.proprietaPacco, getResString(R.string.title_properties));
                adapter.addFragment(this.boxterProfileFragment, getResString(R.string.title_boxter));
                break;

            case RECEIVED:

                this.proprietaPacco  = new DataPaccoFragment();
                //this.boxterProfileFragment = new BoxterProfileFragment();

                adapter.addFragment(this.proprietaPacco, getResString(R.string.title_properties));
                //adapter.addFragment(this.boxterProfileFragment, getResString(R.string.title_boxter));
                //adapter.addFragment(this.feedBackFragment, getResString(R.string.title_feedback));
                break;


        }

        viewPager.setAdapter(adapter);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(ProprietaPaccoActivity.boxAttuale.getState() == CREATED) {
            MenuInflater inflater=getMenuInflater();
            inflater.inflate(R.menu.edit_delete_menu, menu);

        }
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public void onBackPressed() {
        Intent startNewActivity = new Intent( this,MainActivity.class);
        finishAffinity();
        startActivity(startNewActivity);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent startNewActivity2 = new Intent( this,MainActivity.class);
                finishAffinity();
                startActivity(startNewActivity2);
                /*
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                            // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                */
                return true;
            case R.id.edit:
                Bundle bundle = new Bundle();
                bundle.putString("from_id",boxAttuale.getDeparture().getName());
                bundle.putString("to_id",boxAttuale.getArrival().getName());
                bundle.putString("box_id",boxAttuale.getId());
                bundle.putString("travel_id",null);
                bundle.putDouble("from_lat",boxAttuale.getDeparture().getLat());
                bundle.putDouble("from_lon",boxAttuale.getDeparture().getLon());
                bundle.putDouble("to_lat",boxAttuale.getArrival().getLat());
                bundle.putDouble("to_lon",boxAttuale.getArrival().getLon());
                bundle.putString("user_id",boxAttuale.getSender());
                bundle.putString("mode","boxes");
                //MapActivity.boxAttuale = (Box) arg0.getItemAtPosition(index);
                Intent startNewActivity = new Intent(this, MapActivity.class);
                startNewActivity.putExtras(bundle);
                startActivity(startNewActivity);
                break;
            case R.id.delete:
                    AlertDialog.Builder alert = new AlertDialog.Builder(ProprietaPaccoActivity.this);
                    alert.setTitle("Delete Box?");
                    alert.setMessage("Do you want to delete this box?");
                    alert.setNegativeButton("CANCEL",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(
                                        DialogInterface dialog,
                                        int whichButton) {
                                    dialog.cancel();
                                }
                            });
                    alert.setPositiveButton("Delete",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(
                                        DialogInterface dialog,
                                        int whichButton) {
                                    /* FARE COSE */
                                    boxAttuale.delete((box) ->{
                                        Intent mainIntent = new Intent(ProprietaPaccoActivity.this, MainActivity.class);
                                        startActivity(mainIntent);
                                        Toast.makeText(ProprietaPaccoActivity.this, "Box removed! (maybe)", Toast.LENGTH_SHORT).show();
                                    });
                                }
                            });
                    alert.create().show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    public void btnClose(View view) {
        finish();
    }

    public void setPossibleBoxters(View view){

    }

}
