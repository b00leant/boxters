package com.boxters.android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;



import java.io.IOException;
import java.net.URL;

public class TermsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        TextView tv = (TextView) findViewById(R.id.txt_terms);
        String text = Utility.readRawTextFile(this, R.raw.boxters_terms );
        tv.setText(text);
     }

    public String getResString(int id) {
        return (String) getResources().getText(id);
    }
}
