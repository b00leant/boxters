package com.boxters.android.test;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.boxters.android.ProprietaPaccoActivity;
import com.boxters.android.R;

/**
 * Created by giuse on 27/02/2017.
 */

public class PossibleBoxtersFragment extends Fragment {

    public PossibleBoxtersCA aa;

    public PossibleBoxtersFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_list_container, container, false);
        ListView lv = (ListView) view.findViewById(R.id.listViewContainer);
        //aa = new PossibleBoxtersCA(this, null);

        //lv.setAdapter(aa);
        updateList();

        return view;


    }
    private void updateList() {

        ProprietaPaccoActivity.boxAttuale.loadPossibleTravels((travel) -> {
            if (travel != null) {
                aa.add(travel);
                aa.notifyDataSetChanged();
            }
        },false);
        /*
        PublicTravel.queryForBox(ProprietaPaccoActivity.boxAttuale, 3.0, (travel) -> {
            if (travel != null) {
                aa.add(travel);
                aa.notifyDataSetChanged();
            }
        }, false);
        */

    }




}