package com.boxters.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PhoneNumberActivity extends AppCompatActivity {
    private EditText phone;

    @Override
    public void onBackPressed() {
        Intent startNewActivity = new Intent( this,MainActivity.class);
        finishAffinity();
        startActivity(startNewActivity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        phone = (EditText) findViewById(R.id.phone);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setTitle(getString(R.string.Number_verification));
        setSupportActionBar(toolbar);
        Button confirmNumberButton = (Button) findViewById(R.id.sms_send_in_button);
        confirmNumberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(phone.getText() == null || phone.getText().toString() == "" || phone.getText().length() < 10){
                    //TODO: tradurre stringa!
                    Toast.makeText(PhoneNumberActivity.this,"Number incorrect",Toast.LENGTH_SHORT);
                }else{
                    Intent startNewActivity = new Intent(PhoneNumberActivity.this,VerificationActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("phone",phone.getText().toString());
                    startNewActivity.putExtras(bundle);
                    startActivity(startNewActivity);
                }
            }
        });

    }

}