package com.boxters.android;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.boxters.android.controller.messaging.MessagingService;
import com.boxters.model.AuthorizedBox;
import com.boxters.model.Box;
import com.boxters.model.PublicTravel;
import com.boxters.model.Travel;
import com.boxters.model.User;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class BoxterConfirm extends AppCompatActivity {
    private User sender;
    private AuthorizedBox boxID;
    private GoogleApiClient mGoogleApiClient;

    public BoxterConfirm() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boxter_confirm);
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage((FragmentActivity) this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        // your code here
                    }
                })
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        SimpleDateFormat localDateFormat = new SimpleDateFormat("MM/dd/yyyy");

        CircleImageView iconBoxter = (CircleImageView) findViewById(R.id.iconB);

        Intent intent = getIntent();

        Bundle extras = intent.getExtras();

        String senderString = extras.getString(MessagingService.SENDER_KEY);
        User.load(senderString,(User user) -> {
            this.sender = user;
            TextView nome = (TextView) findViewById(R.id.boxter_name);
            nome.setText(sender.getName());

            TextView email = (TextView) findViewById(R.id.email);
            email.setText(sender.getEmail());

            RatingBar rating = (RatingBar) findViewById(R.id.ratingBar);
            rating.setMax(5);
            if(sender.getRating()==null){
                double rating_s1 = 0;
                rating.setRating( (float) rating_s1);
            }else{
                double rating_s2 = sender.getRating();
                rating.setRating( (float) rating_s2);
            }
            if (user.getPhoto() != null) {
                if (iconBoxter != null)
                    new DownloadImageTask(iconBoxter).execute(user.getPhoto());
            }



        }, false);
        String boxString = extras.getString(MessagingService.BOX_ID_KEY);
        AuthorizedBox.load(senderString, boxString, (AuthorizedBox box) -> {
            this.boxID = box;

            TextView posPar = (TextView) findViewById(R.id.posizionePartenza);
            TextView posArr = (TextView) findViewById(R.id.posizioneArrivo);


            TextView spazioPacco = (TextView) findViewById(R.id.spazio);
            spazioPacco.setText("SML");



            Places.GeoDataApi.getPlaceById(mGoogleApiClient, boxID.getArrival().getName())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                final Place myPlace = places.get(0);
                                Log.i("PLACES DETAILS", "Place found: " + myPlace.getAddress());
                                posArr.setText(myPlace.getName().toString());
                            } else {
                                Log.e("PLACES DETAILS", "Place not found");
                            }
                            places.release();
                        }
                    });
            Places.GeoDataApi.getPlaceById(mGoogleApiClient, boxID.getDeparture().getName())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                final Place myPlace = places.get(0);
                                Log.i("PLACES DETAILS", "Place found: " + myPlace.getAddress());
                                posPar.setText(myPlace.getName().toString());
                            } else {
                                Log.e("PLACES DETAILS", "Place not found");
                            }
                            places.release();
                        }
                    });
            //writeCityName(BoxterConfirm.this, posPar, boxID.getDeparture().getLat(), boxID.getDeparture().getLon());
            //writeCityName(BoxterConfirm.this, posArr, boxID.getArrival().getLat(), boxID.getArrival().getLon());

            TextView boxName = (TextView) findViewById(R.id.nomePacco);
            boxName.setText(boxID.getObject());

            TextView desc = (TextView) findViewById(R.id.descrizionePacco);
            desc.setText((boxID.getDescription()));

            TextView pesoPacco = (TextView) findViewById(R.id.peso);

            if(boxID.getWeight() <= 1.0){
                pesoPacco.setText("<1");
            }
            else if(boxID.getWeight() <= 2.0){
                //pesoPacco.setText(getResources().getStringArray(R.array.peso)[1].replace("[","").replace("]",""));
                pesoPacco.setText(">1");
            }
            else if(boxID.getWeight() <= 3.0){
                //pesoPacco.setText(getResources().getStringArray(R.array.peso)[2].replace("[","").replace("]",""));
                pesoPacco.setText(">5");
            }
            else if(boxID.getWeight() <= 4.0){
                pesoPacco.setText(">10");
            }else{
                pesoPacco.setText("???");
            }

            spazioPacco = (TextView) findViewById(R.id.spazio);
            double s = 1.0;
            double m = 2.0;
            double l = 3.0;

            if(boxID.getSize() <= 1.0){
                System.out.println("LO SPAZIO È STATO SETTATO S");
                //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[0].replace("[","").replace("]","").split("\\(")[0]);
                spazioPacco.setText("S");
            }
            else if(boxID.getSize() <= 2.0){
                System.out.println("LO SPAZIO È STATO SETTATO M");
                spazioPacco.setText("M");
                //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[1].replace("[","").replace("]","").split("\\(")[0]);
            }
            else if(boxID.getSize() <= 3.0){
                System.out.println("LO SPAZIO È STATO SETTATO L");
                spazioPacco.setText("L");
                //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[2].replace("[","").replace("]","").split("\\(")[0]);
            }else{
                System.out.println("IL VERO SPAZIO È "+boxID.getSize());
                spazioPacco.setText("?");
            }

            TextView scadenza = (TextView) findViewById(R.id.scadenza);
            scadenza.setText(localDateFormat.format(boxID.getExpiration()));


        }, false);





    }


    @Override
    protected void onNewIntent(Intent intent) {
        Bundle extras = intent.getExtras();

        SimpleDateFormat localDateFormat = new SimpleDateFormat("MM/dd/yyyy");

        CircleImageView iconBoxter = (CircleImageView) findViewById(R.id.iconB);
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage((FragmentActivity) this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        // your code here
                    }
                })
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        String senderString = extras.getString(MessagingService.SENDER_KEY);
        User.load(senderString,(User user) -> {
            this.sender = user;
            TextView nome = (TextView) findViewById(R.id.boxter_name);
            nome.setText(sender.getName());

            TextView email = (TextView) findViewById(R.id.email);
            email.setText(sender.getEmail());

            RatingBar rating = (RatingBar) findViewById(R.id.ratingBar);
            rating.setMax(5);
            if(sender.getRating()==null){
                double rating_s1 = 0;
                rating.setRating( (float) rating_s1);
            }else{
                double rating_s2 = sender.getRating();
                rating.setRating( (float) rating_s2);
            }
            if (user.getPhoto() != null) {
                if (iconBoxter != null)
                    new DownloadImageTask(iconBoxter).execute(user.getPhoto());
            }

        }, false);
        String boxString = extras.getString(MessagingService.BOX_ID_KEY);
        AuthorizedBox.load(senderString, boxString, (AuthorizedBox box) -> {
            this.boxID = box;

            TextView posPar = (TextView) findViewById(R.id.posizionePartenza);
            TextView posArr = (TextView) findViewById(R.id.posizioneArrivo);


            TextView spazioPacco = (TextView) findViewById(R.id.spazio);
            spazioPacco.setText("SML");


            Places.GeoDataApi.getPlaceById(mGoogleApiClient, boxID.getArrival().getName())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                final Place myPlace = places.get(0);
                                Log.i("PLACES DETAILS", "Place found: " + myPlace.getAddress());
                                posArr.setText(myPlace.getName().toString());
                            } else {
                                Log.e("PLACES DETAILS", "Place not found");
                            }
                            places.release();
                        }
                    });
            Places.GeoDataApi.getPlaceById(mGoogleApiClient, boxID.getDeparture().getName())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                final Place myPlace = places.get(0);
                                Log.i("PLACES DETAILS", "Place found: " + myPlace.getAddress());
                                posPar.setText(myPlace.getName().toString());
                            } else {
                                Log.e("PLACES DETAILS", "Place not found");
                            }
                            places.release();
                        }
                    });
            //writeCityName(BoxterConfirm.this, posPar, boxID.getDeparture().getLat(), boxID.getDeparture().getLon());
            //writeCityName(BoxterConfirm.this, posArr, boxID.getArrival().getLat(), boxID.getArrival().getLon());

            TextView boxName = (TextView) findViewById(R.id.nomePacco);
            boxName.setText(boxID.getObject());

            TextView desc = (TextView) findViewById(R.id.descrizionePacco);
            desc.setText((boxID.getDescription()));
            //ImageView boxState = (ImageView) findViewById(R.id.box_state_img);
            TextView pesoPacco = (TextView) findViewById(R.id.peso);
            /*
            switch (boxID.getState()){
                case CREATED:
                    boxState.setImageResource(R.drawable.ic_hourglass_full_blue_24dp);
                    //statoPacco.setText(getString(R.string.Created));
                    break;
                case CONFIRMED:
                    boxState.setImageResource(R.drawable.ic_done_blue_24dp);
                    //statoPacco.setText(getString(R.string.Confirmed));
                    break;
                case TRAVELLING:
                    boxState.setImageResource(R.drawable.ic_my_location_blue_24dp);
                    //statoPacco.setText(getString(R.string.Travelling));
                    break;
                case RECEIVED:
                    boxState.setImageResource(R.drawable.ic_done_all_blue_24dp);
                    //statoPacco.setText(getString(R.string.Received));
                    break;
            }
            */

            pesoPacco = (TextView) findViewById(R.id.peso);

            if(boxID.getWeight() <= 1.0){
                pesoPacco.setText("<1Kg");
            }
            else if(boxID.getWeight() <= 2.0){
                //pesoPacco.setText(getResources().getStringArray(R.array.peso)[1].replace("[","").replace("]",""));
                pesoPacco.setText(">1kg");
            }
            else if(boxID.getWeight() <= 3.0){
                //pesoPacco.setText(getResources().getStringArray(R.array.peso)[2].replace("[","").replace("]",""));
                pesoPacco.setText(">5kg");
            }
            else if(boxID.getWeight() <= 4.0){
                pesoPacco.setText(">10Kg");
            }else{
                pesoPacco.setText("???");
            }
            spazioPacco = (TextView) findViewById(R.id.spazio);
            double s = 1.0;
            double m = 2.0;
            double l = 3.0;


            if(boxID.getSize() <= 1.0){
                System.out.println("LO SPAZIO È STATO SETTATO S");
                //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[0].replace("[","").replace("]","").split("\\(")[0]);
                spazioPacco.setText("S");
            }
            else if(boxID.getSize() <= 2.0){
                System.out.println("LO SPAZIO È STATO SETTATO M");
                spazioPacco.setText("M");
                //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[1].replace("[","").replace("]","").split("\\(")[0]);
            }
            else if(boxID.getSize() <= 3.0){
                System.out.println("LO SPAZIO È STATO SETTATO L");
                spazioPacco.setText("L");
                //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[2].replace("[","").replace("]","").split("\\(")[0]);
            }else{
                System.out.println("IL VERO SPAZIO È "+boxID.getSize());
                spazioPacco.setText("?");
            }

            TextView scadenza = (TextView) findViewById(R.id.scadenza);
            scadenza.setText(localDateFormat.format(boxID.getExpiration()));


        }, false);






    }

    public void writeCityName(Context ctx, TextView tv, double lat, double lng){
        Geocoder geoCoder = new Geocoder(ctx, Locale.getDefault());

        try {
            List<Address> addresses = geoCoder.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0)
            {
                tv.setText(addresses.get(0).getLocality().toString());
                System.out.println(addresses.get(0).getLocality());
            }
            else
            {
                // do your staff
            }

        } catch (IOException e) {
            ;
        }
    }

    public void onAccetta(View view) {

        Intent startNewActivity = new Intent(this, MainActivity.class);
        finishAffinity();
        startActivity(startNewActivity);
    }
}
