package com.boxters.android.test;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.boxters.android.DownloadImageTask;
import com.boxters.android.ListaPacchiQueryActivity;
import com.boxters.android.ListaViaggiatoriQueryActivity;
import com.boxters.android.MainActivity;
import com.boxters.android.ProprietaPaccoActivity;
import com.boxters.android.R;
import com.boxters.android.controller.messaging.MessageSender;
import com.boxters.model.PublicTravel;
import com.boxters.model.PublicUser;
import com.boxters.model.Travel;
import com.boxters.model.messaging.SelectedAsPossibleBoxterPayload;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class ListaViaggiatoriQueryCustomArrayAdapter extends ArrayAdapter<PublicTravel> {
    public ListaViaggiatoriQueryActivity ma;

    private static final String TAG = "CardArrayAdapter";

    public void writeCityName(Context ctx, TextView tv, double lat, double lng){
        Geocoder geoCoder = new Geocoder(ctx, Locale.getDefault());

        try {
            List<Address> addresses = geoCoder.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0)
            {
                tv.setText(addresses.get(0).getLocality().toString());
                System.out.println(addresses.get(0).getLocality());
            }
            else
            {
                // do your staff
            }

        } catch (IOException e) {
            ;
        }
    }

    static class CardViewHolder {
        TextView line1;
        TextView line2;
    }

    public ListaViaggiatoriQueryCustomArrayAdapter(ListaViaggiatoriQueryActivity _ma, PublicTravelList tl) {
        super(_ma.getApplicationContext(), R.layout.pboxter_list_item_card);
        ma = _ma;
        if (tl != null)
            importa(tl);
    }



    protected void importa(PublicTravelList tl) {
        for (PublicTravel t : tl) {
            this.add(t);
        }
    }

    protected PublicTravel getElement(int pos) {
        return this.getItem(pos);
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        PublicTravel t = this.getItem(position);

        View row = convertView;

        convertView = ma.getLayoutInflater().inflate(R.layout.pboxter_list_item_card, null);

        TextView firstLine = (TextView) convertView.findViewById(R.id.firstLine);
        TextView secondLineA = (TextView) convertView.findViewById(R.id.secondLineA);
        TextView secondLineB = (TextView) convertView.findViewById(R.id.secondLineB);
        //TextView thirdLine = (TextView) convertView.findViewById(R.id.thirdLine);
        TextView timeLineA = (TextView) convertView.findViewById(R.id.timeLineA);
        TextView timeLineB = (TextView) convertView.findViewById(R.id.timeLineB);
        RatingBar ratingBarA = (RatingBar) convertView.findViewById(R.id.ratingBarA);

        ImageButton buttonNotification = (ImageButton) convertView.findViewById((R.id.buttonNotification));
        buttonNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(ma);
                alert.setTitle(ma.getString(R.string.confirm)+"?");
                alert.setMessage(ma.getString(R.string.selected_as_possible_boxter_body_sender));
                alert.setNegativeButton(ma.getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(
                                    DialogInterface dialog,
                                    int whichButton) {
                                dialog.cancel();
                            }
                        });
                alert.setPositiveButton(ma.getString(R.string.Sure),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(
                                    DialogInterface dialog,
                                    int whichButton) {
                                /* FARE COSE */

                                if (t != null) {
                                    PublicUser.load(t.getBoxter(),(user) -> {
                                        String userToken = user.getMessagingToken();
                                        if (userToken != null)
                                            MessageSender.send(new SelectedAsPossibleBoxterPayload(MainActivity.user.getId(),userToken, ListaViaggiatoriQueryActivity.currentBox.getId(), t.getId()));

                                    },false);
                                    Set<PublicTravel> travels = new HashSet<PublicTravel>();
                                    travels.add(t);

                                    ListaViaggiatoriQueryActivity.currentBox.addPossibleTravels(travels,(save) -> {
                                        if (save) {

                                            int i=10;
                                            System.out.println(i);
                                        }
                                        else {


                                        }

                                    });
                                }
                            }
                        });
                alert.create().show();


                /*

                */
            }
        });

        View finalConvertView = convertView;
        PublicUser.load(t.getBoxter(),(user) -> {
            firstLine.setText(user.getName());
            ratingBarA.setRating(user.getRating() != null ? user.getRating().floatValue() : 0);
            if (user.getPhoto() != null) {
                ImageView iconViaggiatore = (ImageView) finalConvertView.findViewById(R.id.iconViaggiatore);
                if (iconViaggiatore != null)
                    new DownloadImageTask(iconViaggiatore).execute(user.getPhoto());
            }

        },false);

        SimpleDateFormat localDateFormat = new SimpleDateFormat("MM/dd/yyyy ");

        ImageView mezzoImg = (ImageView) finalConvertView.findViewById(R.id.mezzoImg);
        TextView spazioDisponibile = (TextView) finalConvertView.findViewById(R.id.spazio);

        /*
        Temporaneo e NON SCALABILE. Dobbiamo parametrizzare gli strings array  multilingua
         */

        switch(t.getTransports().toString().replace("[","").replace("]","")){
            case "PULLMAN":
                mezzoImg.setImageResource(R.drawable.ic_directions_bus_blue_24dp);
                break;
            case "BUS":
                mezzoImg.setImageResource(R.drawable.ic_directions_bus_blue_24dp);
                break;
            case "AEREO":
                mezzoImg.setImageResource(R.drawable.ic_flight_takeoff_blue);
                break;
            case "AIRPLANE":
                mezzoImg.setImageResource(R.drawable.ic_flight_takeoff_blue);
                break;
            case "AUTO":
                mezzoImg.setImageResource(R.drawable.ic_directions_car_blue_24dp);
                break;
            case "CAR":
                mezzoImg.setImageResource(R.drawable.ic_directions_car_blue_24dp);
                break;
            case "TRENO":
                mezzoImg.setImageResource(R.drawable.ic_train_blue_24dp);
                break;
            case "TRAIN":
                mezzoImg.setImageResource(R.drawable.ic_train_blue_24dp);
                break;
        }

        if(t.getSpace() == 10.00){
            spazioDisponibile.setText("S");
            //spazioDisponibile.setText(getResources().getStringArray(R.array.spazio)[0].replace("[","").replace("]",""));
        }
        if(t.getSpace() == 50.00){
            spazioDisponibile.setText("M");
            //spazioDisponibile.setText(getResources().getStringArray(R.array.spazio)[1].replace("[","").replace("]",""));
        }
        if(t.getSpace() == 100.00){
            spazioDisponibile.setText("L");
            //spazioDisponibile.setText(getResources().getStringArray(R.array.spazio)[2].replace("[","").replace("]",""));
        }

        Places.GeoDataApi.getPlaceById(ListaViaggiatoriQueryActivity.mGoogleApiClient, t.getArrival().getName())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);
                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                            secondLineB.setText(myPlace.getName());
                        } else {
                            Log.e("PLACES DETAILS", "Place not found");
                            Log.e("PLACES DETAILS",places.getStatus().getStatusMessage());
                        }
                        places.release();
                    }
                });
        Places.GeoDataApi.getPlaceById(ListaViaggiatoriQueryActivity.mGoogleApiClient, t.getDeparture().getName())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);
                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                            secondLineA.setText(myPlace.getName());
                        } else {
                            Log.e("PLACES DETAILS", "Place not found");
                        }
                        places.release();
                    }
                });

        timeLineA.setText(localDateFormat.format(t.getStartDate()));
        timeLineB.setText(localDateFormat.format(t.getEndDate()));
        return convertView;


    }
}