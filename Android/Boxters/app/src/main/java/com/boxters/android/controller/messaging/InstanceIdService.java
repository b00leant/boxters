package com.boxters.android.controller.messaging;

import com.boxters.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by daniele on 12/02/17.
 */

public class InstanceIdService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        FirebaseUser _user = FirebaseAuth.getInstance().getCurrentUser();
        if(_user != null) {
            User.load(_user.getUid(), (User user) -> user.setMessagingToken(refreshedToken, null), false);
        }
    }

}
