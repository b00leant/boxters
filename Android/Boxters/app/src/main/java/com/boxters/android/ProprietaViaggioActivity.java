package com.boxters.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.boxters.android.test.DataViaggioFragment;
import com.boxters.model.AuthorizedBox;
import com.boxters.model.Box;
import com.boxters.model.BoxState;
import com.boxters.model.PublicBox;
import com.boxters.model.Travel;
import com.boxters.android.test.PacchiQueryFragment;
import com.boxters.android.test.PossiblePacchiFragment;
import com.boxters.android.test.WebViewFragment;
import com.boxters.model.TravelState;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;

import static com.boxters.android.FormPaccoActivity.REQUEST_CAMERA;
import static com.boxters.android.test.PacchiQueryFragment.RC_GIVE_SHIP_CODE;
import static com.boxters.model.TravelState.CREATED;
import static java.security.AccessController.getContext;

public class ProprietaViaggioActivity extends AppCompatActivity {

    public static Travel travelAttuale;
    TextView travelFrom;
    TextView travelTo;
    private int n_boxes = 0;
    public Polyline percorso;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public static GoogleApiClient mGoogleApiClient;
    public static final int RC_GET_SHIP_CODE = 200;
    private DataViaggioFragment proprietaPacco;
    private PacchiQueryFragment pacchiQueryFragment;
    private static final String TAG = "PropritaViaggioActivity";
    private PossiblePacchiFragment possiblePacchiFragment;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(ProprietaViaggioActivity.travelAttuale.getState() == CREATED) {
            MenuInflater inflater=getMenuInflater();
            inflater.inflate(R.menu.edit_delete_menu, menu);

        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode ){
            case RC_GET_SHIP_CODE: {
                if (resultCode == Activity.RESULT_OK) {
                    n_boxes = 0;
                    ProprietaViaggioActivity.travelAttuale.loadBoxes((AuthorizedBox box)->{
                        if(box!= null && box.getState() == BoxState.CONFIRMED){
                            n_boxes++;
                        }
                    },false);
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setTitle(getString(R.string.Start_travel));
                    alert.setMessage(getString(R.string.Start_travel_dialog)
                            + getString(R.string.you_still_have) + " "+ n_boxes + " "+
                            getString(R.string.missing_boxes_to_carry));
                    alert.setNegativeButton(getString(R.string.cancel),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(
                                        DialogInterface dialog,
                                        int whichButton) {

                                    dialog.cancel();
                                }
                            });
                    alert.setPositiveButton(getString(R.string.Start_travel),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(
                                        DialogInterface dialog,
                                        int whichButton) {
                                        ProprietaViaggioActivity.travelAttuale.setState(TravelState.STARTED,null);
                                        recreate();
                                }
                            });
                    alert.create().show();
                    String boxID = data.getStringExtra("Box");
                    String senderID = data.getStringExtra("Sender");

                }
                break;
            }

            case RC_GIVE_SHIP_CODE: {
                if (resultCode == -1) {
                    //cambio stato dell'authorizedbox

                }
                break;
            }


        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proprieta_viaggio);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_proprieta_viaggio_activity);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        //travelFrom = (TextView) findViewById(R.id.viaggioFrom);
        //travelTo = (TextView) findViewById(R.id.viaggioTo);

        //travelFrom.setText(travelAttuale.getDeparture().getName());
        //travelTo.setText("from "+travelAttuale.getDeparture().getName()+" to "+travelAttuale.getArrival().getName());

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage((FragmentActivity) this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        // your code here
                    }
                })
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#FFFFFF"));
        tabLayout.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
        tabLayout.setTabTextColors(Color.WHITE,Color.WHITE);


    }
    public String getResString(int id) {
        return (String) getResources().getText(id);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        this.proprietaPacco  = new DataViaggioFragment();
        this.pacchiQueryFragment = new PacchiQueryFragment();
        //this.possiblePacchiFragment = new PossiblePacchiFragment();
        String s= Utility.readResourceTextFileEx(this,R.raw.info_pacco);
        //s = s.replace("{partenza}",travelAttuale.getArrival().getName() );


        //this.proprietaPacco.setHTML(s);

        adapter.addFragment(this.proprietaPacco, getResString(R.string.title_properties));
        adapter.addFragment(this.pacchiQueryFragment, getResString(R.string.title_boxes));
        //adapter.addFragment(this.possiblePacchiFragment, getResString(R.string.title_pending_boxes));
        viewPager.setAdapter(adapter);
    }

    public void btnClose(View view) {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent startNewActivity2 = new Intent( this,MainActivity.class);
                finishAffinity();
                startActivity(startNewActivity2);
                /*
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                            // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                */
                return true;
            case R.id.edit:
                Bundle bundle = new Bundle();
                bundle.putString("from_id",travelAttuale.getDeparture().getName());
                bundle.putString("to_id",travelAttuale.getArrival().getName());
                bundle.putString("travel_id",travelAttuale.getId());
                bundle.putString("box_id",null);
                bundle.putDouble("from_lat",travelAttuale.getDeparture().getLat());
                bundle.putDouble("from_lon",travelAttuale.getDeparture().getLon());
                bundle.putDouble("to_lat",travelAttuale.getArrival().getLat());
                bundle.putDouble("to_lon",travelAttuale.getArrival().getLon());
                bundle.putString("user_id",travelAttuale.getBoxter());
                bundle.putString("mode","travels");
                //MapActivity.boxAttuale = (Box) arg0.getItemAtPosition(index);
                Intent startNewActivity = new Intent(this, MapActivity.class);
                startNewActivity.putExtras(bundle);
                startActivity(startNewActivity);
                break;
            case R.id.delete:
                AlertDialog.Builder alert = new AlertDialog.Builder(ProprietaViaggioActivity.this);
                alert.setTitle("Delete Box?");
                alert.setMessage("Do you want to delete this travel?");
                alert.setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(
                                    DialogInterface dialog,
                                    int whichButton) {
                                dialog.cancel();
                            }
                        });
                alert.setPositiveButton("Delete",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(
                                    DialogInterface dialog,
                                    int whichButton) {
                                    /* FARE COSE */
                                travelAttuale.delete((travel) ->{
                                    Intent mainIntent = new Intent(ProprietaViaggioActivity.this, MainActivity.class);
                                    startActivity(mainIntent);
                                    Toast.makeText(ProprietaViaggioActivity.this, "Travel removed! (maybe)", Toast.LENGTH_SHORT).show();
                                });
                                dialog.cancel();
                            }
                        });
                alert.create().show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        Intent startNewActivity = new Intent( this,MainActivity.class);
        finishAffinity();
        startActivity(startNewActivity);
    }
}
