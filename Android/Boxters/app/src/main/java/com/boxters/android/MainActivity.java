package com.boxters.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.boxters.android.controller.messaging.MessageSender;
import com.boxters.android.test.BoxList;
import com.boxters.android.test.ListaPacchiMainCustomArrayAdapter;
import com.boxters.model.DBTester;
import com.boxters.model.User;
import com.boxters.model.messaging.BoxArrivedPayload;
import com.boxters.model.messaging.RequestBoxInfoPayload;
import com.boxters.model.messaging.RequestToBeReceiverPayload;
import com.boxters.model.messaging.SelectedAsPossibleBoxterPayload;
import com.boxters.model.messaging.TestPayload;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.firebase.ui.auth.ResultCodes;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Logger;
import com.google.firebase.iid.FirebaseInstanceId;

import org.jetbrains.annotations.NotNull;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity

        implements NavigationView.OnNavigationItemSelectedListener {

    private ListView notificationList;
    private NavigationView navigationView;
    private boolean checkPhone;
    // Choose an arbitrary request code value for sign-in
    private static final int RC_SIGN_IN = 347;
    //    public Global G= Global.getInstance();
    public static TabLayout tabLayout;
    private FloatingActionButton fab;
    private ViewPager viewPager;
    public static User user;
    private DrawerLayout drawer;
    public static GoogleApiClient mGoogleApiClient;
    private static final boolean DEBUG = true;
    private static boolean started = false;
    public static PacchiFragment pf;
    public static ViaggiFragment vf;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(DEBUG && !started) {
            started = true;
            FirebaseDatabase.getInstance().setLogLevel(Logger.Level.DEBUG);
            Log.d("Test", "Stampo il debug");
        }
        super.onCreate(savedInstanceState);
        // serve ....
        Global.getInstance().mainActivity = this;

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage((FragmentActivity) this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        // your code here
                    }
                })
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        CustomActionBarToggle toggle = new CustomActionBarToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);




        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        fab = (FloatingActionButton) findViewById(R.id.fab_pacco);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#FFFFFF"));
        tabLayout.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
        tabLayout.setTabTextColors(Color.WHITE,Color.WHITE);

        Utility.checkGooglePlayServicesAvailability(this);

        if(false) { //effettuo il log out dell'utente per testare il log in

            FirebaseAuth.getInstance().signOut();
            AuthUI.getInstance()
                    .signOut(this)
                    .addOnCompleteListener(task -> {
                        // user is now signed out
                        loginIfNecessary();
                    });
        }
        else {
            //login dell'utente
            loginIfNecessary();
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = tabLayout.getSelectedTabPosition();

                switch(position) {
                    case 0:
                        pf.creaNuovoPacco();
                        // first tab is selected
                        break;
                    case 1:
                        vf.creaNuovoViaggio();
                        // second tab is selected
                        break;
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (user != null){
            if(user.getPhone().equals("333 3333333")
                    || user.getPhone().toString().equals("333 3333333")
                    || user.getPhone().toString().equals("3333333333")
                    || user.getPhone().equals("3333333333")){
                Log.e("SMS","DEVO MANDARE UN SMS");
                Intent startNewActivity = new Intent(this,PhoneNumberActivity.class);
                finishAffinity();
                startActivity(startNewActivity);
            }else{
                fab.setVisibility(View.VISIBLE);
                aggiornaDatiUtente();
            }
        }



    }

    /** Logga l'utente se non è già loggato
     */
    private void loginIfNecessary() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();

        if (user != null) {
            Log.d("LOGIN","ALREADY SIGNED IN");
            User.load(user.getUid(),(User _user) -> {
                if(_user != null){
                    MainActivity.user = _user;
                    if(_user.getPhone().equals("333 3333333")){
                        Intent startNewActivity = new Intent(MainActivity.this,PhoneNumberActivity.class);
                        finishAffinity();
                        startActivity(startNewActivity);
                    }else{
                        fab.setVisibility(View.VISIBLE);
                        afterLogin(user);
                    }
                }
            },false);
        } else {

            // not signed in
            startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder()
                    .setIsSmartLockEnabled(!DEBUG)
                    .setProviders(Arrays.asList(
                            new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                            new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build(),
                            new AuthUI.IdpConfig.Builder(AuthUI.FACEBOOK_PROVIDER).build()
                    ))
                    .setTosUrl("http://www.danielecominu.com/boxters/terms_and_conditions.html")
                    .setLogo(R.drawable.ic_logo_auth_boxters)
                    .setTheme(R.style.AuthTheme)
                    .build(), RC_SIGN_IN);
        }
    }

    public String getResString(int id) {
       return (String) getResources().getText(id);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        this.pf = new PacchiFragment();
        this.vf = new ViaggiFragment();


        adapter.addFragment(pf, getResString(R.string.title_boxes));
        adapter.addFragment(vf, getResString(R.string.title_travels));
        viewPager.setAdapter(adapter);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    //    getActionBar().setDisplayHomeAsUpEnabled(true);
        switch (requestCode ){
            // RC_SIGN_IN is the request code you passed into startActivityForResult(...) when starting the sign in flow.
            case RC_SIGN_IN:
                IdpResponse response = IdpResponse.fromResultIntent(data);

                // Successfully signed in
                if (resultCode == ResultCodes.OK) {
                    //yeah, ora siamo loggati

                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    fab.setVisibility(View.VISIBLE);
                    Intent startTutorial = new Intent(this, TutorialActivity.class);
                    startActivity(startTutorial);
                    if (user == null) {
                        throw new InternalError("onActivityResult chiamato con resultCode OK frg utente non loggato");
                    } else afterLogin(user);
                } else {
                    // Sign in failed
                    if (response == null) {
                        // User pressed back button
                        //todo: l'utente non può utilizzare l'app senza loggarsi, mostra un dialog
                        loginIfNecessary();
                    } else if (response.getErrorCode() == ErrorCodes.NO_NETWORK) {
                        //todo: non c'è internet, avvisa l'utente che senza non può utilizzare l'applicazione
                        finish();
                    } else if (response.getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                        //todo: si è verificato un errore, fa qualcosa
                        finish();
                    }
                }
            break;


        }


    }

    public static void aggiornaDatiUtente() {
        PacchiFragment.aa.loadAllUserBoxes(MainActivity.user);
        ViaggiFragment.aa.loadAllUserTravels(MainActivity.user);

    }



    private void afterLogin(@NotNull FirebaseUser user) {

        //creo un utente con dati fittizi, giusto per vedere che funziona
        //todo: fare un'activity con una form dove l'utente inserisce i propri dati
        User.load(user.getUid(), (User _user) -> {
            if (_user != null) {
                MainActivity.user =_user;
                /*
                gestisco inserimento telefono per gli utenti già iscritti
                e che hanno hardcodeato il numero "333 3333333"
                */
                Log.d("NUMERO",_user.getPhone());
                if(_user.getPhone().equals("333 3333333")){
                    Log.e("SMS","BISOGNA INSERIRE UN NUMERO VERO");
                    Intent startNewActivity = new Intent(this,PhoneNumberActivity.class);
                    finishAffinity();
                    startActivity(startNewActivity);
                }else{
                    this.user = _user;
                    pf.showProgressBar();
                    vf.showProgressBar();
                    aggiornaDatiUtente();
                    //aggiorno il token in caso sia stato aggiornato
                    _user.setMessagingToken(FirebaseInstanceId.getInstance().getToken(), null);
                }
            }
            else {
                //todo: qui dovresti presentare una form all'utente
                // per inserire i propri dati, operazione asincrona
                String name = (user.getDisplayName() != null ? user.getDisplayName() : "Lillo lallo");
                String email = (user.getEmail() != null ? user.getEmail() : "myEmail@gmail.com");
                String photo = (user.getPhotoUrl() != null ? user.getPhotoUrl().toString() : null);

                User.create(user.getUid(), email, name, photo, null, "333 3333333", null,
                        FirebaseInstanceId.getInstance().getToken(), (newUser) -> this.user = newUser, true);
            }
        }, true);

        Global.getInstance().currentUser = user;
        Global.getInstance().CreaImmagineProfilo();

    }

   @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       if(item.getItemId()==R.id.action_notification){
           NotificationActivity();
           Log.d("Option","2");

       }
       return super.onOptionsItemSelected(item);
    }

    private void NotificationActivity() {
        Intent intent = new Intent(this, NotificationActivity.class);
        startActivity(intent) ;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //MenuInflater mMenuInflater = getMenuInflater();
        //mMenuInflater.inflate(R.menu.main, menu);
        //TODO: DECOMMENTARE QUANDO SI AVRANNO LE NOTIFICHE IN-APP
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            /*
            case R.id.nav_invio:

                Intent startNewActivity1 = new Intent( this, MapActivity.class);
                startNewActivity1.putExtra("tipoProcesso", MapActivity.TIPO_PROCESSO_PACCO );
                startActivity(startNewActivity1);
                break;

            case R.id.nav_viaggio:
                Intent startNewActivity2 = new Intent( this, MapActivity.class);
                startNewActivity2.putExtra("tipoProcesso", MapActivity.TIPO_PROCESSO_VIAGGIO );
                startActivity(startNewActivity2);
            */
                /*Set<Transport> transports = new HashSet<>();
                transports.add(Transport.AIRPLANE);

                Travel.create(user.getId(),new Date(),new Date(), 30.0, transports,
                        new Location(null, 43.6, 23.8), new Location(null, 32.7, 23.8), null, true);*/

                //break;
            case R.id.tutorial:
                Intent startTutorial = new Intent(this, TutorialActivity.class);
                startActivity(startTutorial);
                break;
            case R.id.nav_terms:
                /*DBTester.addTestTravels(user);
                DBTester.addTestBoxes(user, "FPmRzKAW2wPwpSAkHMSmfVcxNKL2");*/
                //MessageSender.send(new RequestToBeReceiverPayload(user.getId(), "fZkm25h9P9U:APA91bGPMvSWAP5VAmVE9i_PwEItXReex2-ytO5TTns5xUfJWsW2GUwxpbwp2NmOCBJvEXp_kvZvZbjqs0SADxoY4-kcaKZb46itU9GJivJEeCC5i32OSBsq9LcC2ZBwy9ZyR315RVu4", "KeL8PUkQVolEQwhy8P5"));

                //MessageSender.send(new SelectedAsPossibleBoxterPayload(user.getId(), "fo8V5jE8fXc:APA91bGX2iDZQEk_I6jHG_rlJGN7u1cw1yvJHuPAJwmnZETVNIrt-9oLZJa0j4iZvbXxEi4FpJ1mwMi9kQRYfRuDJ-GS4BpKl7nezYQIpjkNyekexp6vI6Z95ERRogtI8_fnaiWn7scw", "-Ke8eiaoWMG76jfMIXRy", "-KdMIgEX0nMOhX5Bsglq"));
                //MessageSender.send(new RequestToBeReceiverPayload(user.getId(), "fo8V5jE8fXc:APA91bGX2iDZQEk_I6jHG_rlJGN7u1cw1yvJHuPAJwmnZETVNIrt-9oLZJa0j4iZvbXxEi4FpJ1mwMi9kQRYfRuDJ-GS4BpKl7nezYQIpjkNyekexp6vI6Z95ERRogtI8_fnaiWn7scw", "-Ke8eiaoWMG76jfMIXRy"));

                MessageSender.send(new RequestToBeReceiverPayload(user.getId(), "fZkm25h9P9U:APA91bGPMvSWAP5VAmVE9i_PwEItXReex2-ytO5TTns5xUfJWsW2GUwxpbwp2NmOCBJvEXp_kvZvZbjqs0SADxoY4-kcaKZb46itU9GJivJEeCC5i32OSBsq9LcC2ZBwy9ZyR315RVu4", "-KeL8PUkQVolEQwhy8P5"));
                //MessageSender.send(new RequestBoxInfoPayload(user.getId(), "fZkm25h9P9U:APA91bGPMvSWAP5VAmVE9i_PwEItXReex2-ytO5TTns5xUfJWsW2GUwxpbwp2NmOCBJvEXp_kvZvZbjqs0SADxoY4-kcaKZb46itU9GJivJEeCC5i32OSBsq9LcC2ZBwy9ZyR315RVu4", "-KeL8PUkQVolEQwhy8P5"));
                //DBTester.testTravelQuery(user.getId());

                startActivity(TermsActivity.class);
                break;
            case R.id.nav_profilo:
                Intent startNewActivity3 = new Intent(this, ProfileActivity.class);
                startActivity(startNewActivity3);
                break;
            case R.id.nav_logout:
                FirebaseAuth.getInstance().signOut();
                AuthUI.getInstance()
                        .signOut(this)
                        .addOnCompleteListener(task -> {
                            // user is now signed out
                            loginIfNecessary();
                        });

                break;
        }

        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    public void creaNuovoPacco(View view) {
        pf.creaNuovoPacco();
    }

    public void creaNuovoViaggio(View view) {
        vf.creaNuovoViaggio();
    }

    public void startActivity(Class cls) {
        Intent startNewActivity = new Intent(this, cls);
        startActivity(startNewActivity);
    }
  }
