package com.boxters.android.test;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import com.boxters.android.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by undeclinable on 11/04/17.
 */

public class DisableDecorator implements DayViewDecorator{
    private final Drawable dc;
    private final ArrayList<Date> dates;

    public DisableDecorator(List<Date> dates, Activity context) {
        dc = context.getResources().getDrawable(R.drawable.ic_disabled);
        this.dates = new ArrayList<>(dates);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        Calendar cal = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal.setTime(day.getDate());
        cal2.setTime(day.getDate());
        cal.add( Calendar.DAY_OF_MONTH, -1 );
        cal2.add( Calendar.DAY_OF_MONTH, 1 );
        Date next = cal2.getTime();
        Date previous = cal.getTime();
        return dates.size()>0 && !dates.contains(previous) && dates.contains(day.getDate()) && !dates.contains(next);
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setDaysDisabled(true);
        view.setSelectionDrawable(dc);
    }
}
