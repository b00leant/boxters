package com.boxters.android.test;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.boxters.android.MainActivity;
import com.boxters.android.ProprietaPaccoActivity;
import com.boxters.android.ProprietaViaggioActivity;
import com.boxters.android.R;
import com.boxters.model.AuthorizedBox;
import com.boxters.model.Box;
import com.boxters.model.BoxState;
import com.boxters.model.Travel;

/**
 * Created by giuse on 27/02/2017.
 */

public class PossiblePacchiFragment extends Fragment {

    public PossiblePacchiCA aa;
    public static final int RC_GET_SHIP_CODE = 200;
    public static final int RC_GIVE_SHIP_CODE = 777;

    private TextView tv;
    private String boxID;

    public PossiblePacchiFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_list_container, container, false);
        ListView lv = (ListView) view.findViewById(R.id.listViewContainer);
        //aa = new PossiblePacchiCA(this, null);
        //tv = (TextView) view.findViewById(R.id.tvListContainer);
        //tv.setText(getResString(R.string.fragment_no_boxes_yet));
        lv.setAdapter(aa);
        updateList();


        /*
         lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3)
            {
                authorizedBox = (AuthorizedBox) adapter.getItemAtPosition(position);

                switch (authorizedBox.getState()) {

                    case CREATED: {
                        //per il momento faccio come se fosse nello stato CREATED
                        Intent startNewActivity = new Intent(v.getContext(), QRScannerActivity.class);
                        QR = authorizedBox.getQRCode().trim().toUpperCase();
                        startNewActivity.putExtra("QR",QR);
                        startActivityForResult(startNewActivity, RC_GET_SHIP_CODE  );
                        break;
                    }

                    case CONFIRMED: {
                        Intent startNewActivity = new Intent(v.getContext(), QRScannerActivity.class);
                        QR = authorizedBox.getQRCode().trim().toUpperCase();
                        startNewActivity.putExtra("QR",QR);
                        startActivityForResult(startNewActivity, RC_GET_SHIP_CODE  );
                        break;
                    }

                    case TRAVELLING: {
                        Intent startNewActivity = new Intent(v.getContext(), ScanActivity.class);
                        QR = authorizedBox.getQRCode().trim().toUpperCase();
                        startNewActivity.putExtra("QR",QR);
                        startActivityForResult(startNewActivity, RC_GIVE_SHIP_CODE);
                        break;
                    }

                }

//                startActivity(startNewActivity);
            }
        });

*/

        return view;


    }
    private void updateList() {
        if(ProprietaViaggioActivity.travelAttuale != null) {
            ProprietaViaggioActivity.travelAttuale.loadBoxes((box) -> {
                if (box != null) {
                    aa.add(box);
                    aa.notifyDataSetChanged();
                }
            }, false);
        }

    }

    private String getResString(int id) {
        return (String) getResources().getText(id);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode ){
            case RC_GET_SHIP_CODE: {
                if (resultCode == -1) {
                    //cambio stato dell'authorizedbox

                    boxID = data.getStringExtra("Box");

                    if (boxID != null) {
                        Box.load(MainActivity.user.getId(),boxID, (Box box) -> {

                            ProprietaViaggioActivity.travelAttuale.updateBoxState(BoxState.TRAVELLING,box.getSender(),box.getId(),null);
                            box.waitTravellingConfirm((result) -> {

                            } );
                        },false);



                    }

                }
                break;
            }

            case RC_GIVE_SHIP_CODE: {
                if (resultCode == -1) {
                    //cambio stato dell'authorizedbox

                }
                break;
            }


        }
    }


}