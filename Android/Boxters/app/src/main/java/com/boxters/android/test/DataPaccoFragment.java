package com.boxters.android.test;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.boxters.android.DownloadImageTask;
import com.boxters.android.MainActivity;
import com.boxters.android.ProprietaViaggioActivity;
import com.boxters.android.R;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.boxters.android.ProprietaPaccoActivity;
import com.boxters.model.Box;
import com.boxters.model.PublicUser;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.boxters.android.ProprietaViaggioActivity.travelAttuale;


/**
 * Created by David on 27/02/2017.
 */

public class DataPaccoFragment extends Fragment {
    TextView statoPacco;
    TextView nomePacco;
    ImageView imageLayout;
    TextView descrizionePacco;
    TextView emailDestinatario;
    TextView pesoPacco;
    TextView spazioPacco;
    TextView otheruser;
    TextView scadenzaPacco;
    TextView descrizioneArrivo;
    TextView descrizioneProvenienza;
    CircleImageView iconSenderReceiver;


    public Bitmap convert2Bitmap(String base64Str) throws IllegalArgumentException
    {
        byte[] decodedBytes = Base64.decode(
                base64Str.substring(base64Str.indexOf(",")  + 1),
                Base64.DEFAULT
        );

        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    public String convert2String(Bitmap bitmap)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }
    public DataPaccoFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data_pacco, container, false);


        SimpleDateFormat localDateFormat = new SimpleDateFormat("MM/dd/yyyy");
        ImageView boxState= (ImageView) view.findViewById(R.id.box_state_img);
        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        //statoPacco = (TextView) view.findViewById(R.id.box_state);
        LinearLayout.LayoutParams p2 = new LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAP_CONTENT,2.0f);

        if(ProprietaPaccoActivity.boxAttuale.getState().toString() != "CREATED"){

        }
        if(ProprietaPaccoActivity.boxAttuale.getState().toString() == "TRAVELLING" ||
                ProprietaPaccoActivity.boxAttuale.getState().toString() == "RECEIVED"){
        }
        iconSenderReceiver = (CircleImageView) view.findViewById(R.id.iconSR);

        imageLayout = (ImageView) view.findViewById(R.id.image_layout);
        if(ProprietaPaccoActivity.boxAttuale.getPhoto()!=null){
            Bitmap bitmap = convert2Bitmap(ProprietaPaccoActivity.boxAttuale.getPhoto());
            imageLayout.setImageBitmap(bitmap);
        }{
            //TODO: Gestire foto anteprima generale
        }
        descrizioneArrivo = (TextView) view.findViewById(R.id.posizioneArrivo);
        //writeCityName(view.getContext(), descrizioneArrivo, ProprietaPaccoActivity.boxAttuale.getArrival().getLat(),ProprietaPaccoActivity.boxAttuale.getArrival().getLon());
        Places.GeoDataApi.getPlaceById(ProprietaPaccoActivity.mGoogleApiClient, ProprietaPaccoActivity.boxAttuale.getArrival().getName())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);
                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getAddress());
                            descrizioneArrivo.setText(myPlace.getName().toString());
                        } else {
                            Log.e("PLACES DETAILS", "Place not found");
                        }
                        places.release();
                    }
                });

        descrizioneProvenienza = (TextView) view.findViewById(R.id.posizionePartenza);
        //writeCityName(view.getContext(), descrizioneProvenienza, ProprietaPaccoActivity.boxAttuale.getDeparture().getLat(), ProprietaPaccoActivity.boxAttuale.getDeparture().getLon());
        Places.GeoDataApi.getPlaceById(ProprietaPaccoActivity.mGoogleApiClient, ProprietaPaccoActivity.boxAttuale.getDeparture().getName())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);
                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getAddress());
                            descrizioneProvenienza.setText(myPlace.getName().toString());
                        } else {
                            Log.e("PLACES DETAILS", "Place not found");
                        }
                        places.release();
                    }
                });

        String descrizione = ProprietaPaccoActivity.boxAttuale.getDescription();
        if (descrizione == null) {
            descrizione = "";
            }

        descrizionePacco = (TextView) view.findViewById(R.id.descrizionePacco);
        descrizionePacco.setText(descrizione);
        //nomePacco.setText(ProprietaPaccoActivity.boxAttuale.getObject());

        emailDestinatario = (TextView) view.findViewById(R.id.email);

        Box b = ProprietaPaccoActivity.boxAttuale;

        if(b.getReceiver() == MainActivity.user.getId()){

            PublicUser.load(b.getSender(),(user) -> {
                otheruser = (TextView) view.findViewById(R.id.sender_name);
                otheruser.setText(user.getName());
                RatingBar rating = (RatingBar) view.findViewById(R.id.ratingBar);
                rating.setMax(5);
                if(user.getRating()==null){
                    double rating_s1 = 0;
                    rating.setRating( (float) rating_s1);
                }else{
                    double rating_s2 = user.getRating();
                    rating.setRating( (float) rating_s2);
                }
                //nomePacco.setText(getString(R.string.to_receive));
                emailDestinatario.setText(user.getEmail());
                if (user.getPhoto() != null) {
                    if (iconSenderReceiver != null)
                        new DownloadImageTask(iconSenderReceiver).execute(user.getPhoto());
                }

            },false);

        }else{ //sono sender

            PublicUser.load(b.getReceiver(),(user) -> {
                ratingBar.setRating(user.getRating() != null ? user.getRating().floatValue() : 0);
                otheruser = (TextView) view.findViewById(R.id.sender_name);
                otheruser.setText(user.getName());
                RatingBar rating = (RatingBar) view.findViewById(R.id.ratingBar);
                rating.setMax(5);
                if(user.getRating()==null){
                    double rating_s1 = 0;
                    rating.setRating( (float) rating_s1);
                }else{
                    double rating_s2 = user.getRating();
                    rating.setRating( (float) rating_s2);
                }
                //nomePacco.setText(getString(R.string.to_send));
                emailDestinatario.setText(user.getEmail());
                if (user.getPhoto() != null) {

                    if (iconSenderReceiver != null)
                        new DownloadImageTask(iconSenderReceiver).execute(user.getPhoto());
                }

            },false);

            switch (ProprietaPaccoActivity.boxAttuale.getState()){
                case CREATED:
                    boxState.setImageResource(R.drawable.ic_hourglass_full_blue_24dp);
                    //statoPacco.setText(getString(R.string.Created));
                    break;
                case CONFIRMED:
                    boxState.setImageResource(R.drawable.ic_done_blue_24dp);
                    //statoPacco.setText(getString(R.string.Confirmed));
                    break;
                case TRAVELLING:
                    boxState.setImageResource(R.drawable.ic_my_location_blue_24dp);
                    //statoPacco.setText(getString(R.string.Travelling));
                    break;
                case RECEIVED:
                    boxState.setImageResource(R.drawable.ic_done_all_blue_24dp);
                    //statoPacco.setText(getString(R.string.Received));
                    break;
            }

            pesoPacco = (TextView) view.findViewById(R.id.peso);

            if(ProprietaPaccoActivity.boxAttuale.getWeight() <= 1.0){
                pesoPacco.setText("<1");
            }
            else if(ProprietaPaccoActivity.boxAttuale.getWeight() <= 2.0){
                //pesoPacco.setText(getResources().getStringArray(R.array.peso)[1].replace("[","").replace("]",""));
                pesoPacco.setText(">1");
            }
            else if(ProprietaPaccoActivity.boxAttuale.getWeight() <= 3.0){
                //pesoPacco.setText(getResources().getStringArray(R.array.peso)[2].replace("[","").replace("]",""));
                pesoPacco.setText(">5");
            }
            else if(ProprietaPaccoActivity.boxAttuale.getWeight() <= 4.0){
                pesoPacco.setText(">10");
            }else{
                pesoPacco.setText("???");
            }
            spazioPacco = (TextView) view.findViewById(R.id.spazio);
            double s = 1.0;
            double m = 2.0;
            double l = 3.0;

            if(ProprietaPaccoActivity.boxAttuale.getSize() <= 1.0){
                System.out.println("LO SPAZIO È STATO SETTATO S");
                //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[0].replace("[","").replace("]","").split("\\(")[0]);
                spazioPacco.setText("S");
            }
            else if(ProprietaPaccoActivity.boxAttuale.getSize() <= 2.0){
                System.out.println("LO SPAZIO È STATO SETTATO M");
                spazioPacco.setText("M");
                //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[1].replace("[","").replace("]","").split("\\(")[0]);
            }
            else if(ProprietaPaccoActivity.boxAttuale.getSize() <= 3.0){
                System.out.println("LO SPAZIO È STATO SETTATO L");
                spazioPacco.setText("L");
                //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[2].replace("[","").replace("]","").split("\\(")[0]);
            }else{
                System.out.println("IL VERO SPAZIO È "+ProprietaPaccoActivity.boxAttuale.getSize());
                spazioPacco.setText("?");
            }



            scadenzaPacco = (TextView) view.findViewById(R.id.scadenza);
            scadenzaPacco.setText(localDateFormat.format(ProprietaPaccoActivity.boxAttuale.getExpiration()));

        }

        //PublicUser.load(ProprietaPaccoActivity.boxAttuale.getReceiver(),(user) -> {
        //    if (user != null) //todo: da correggere la completion che spesso è null

            /*if (user.getPhoto() != null) {
                //ImageView iconSender = (ImageView) finalConvertView.findViewById(R.id.iconSender);
                if (iconSender != null)
                    new DownloadImageTask(iconSender).execute(user.getPhoto());
            }*/

        //},false);
        //statoPacco.setText(getString(R.string.CREATED));



        return view;
    }

    public void writeCityName(Context ctx, TextView tv, double lat, double lng){
        Geocoder geoCoder = new Geocoder(ctx, Locale.getDefault());

        try {
            List<Address> addresses = geoCoder.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0)
            {
                tv.setText(addresses.get(0).getLocality().toString());
                System.out.println(addresses.get(0).getLocality());
            }
            else
            {
                // do your staff
            }

        } catch (IOException e) {
            ;
        }
    }
}
