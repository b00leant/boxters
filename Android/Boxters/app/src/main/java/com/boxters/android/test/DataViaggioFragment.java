package com.boxters.android.test;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.boxters.android.Manifest;
import com.boxters.android.MapActivity;
import com.boxters.android.ProprietaViaggioActivity;
import com.boxters.android.R;

import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boxters.android.ProprietaPaccoActivity;
import com.boxters.android.RiepilogoPaccoActivity;
import com.boxters.model.Location;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.PlacePhotoResult;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.boxters.android.ProprietaViaggioActivity.travelAttuale;
import static com.boxters.android.R.id.linearLayout;
import static com.boxters.android.R.id.to;


/**
 * Created by David on 27/02/2017.
 */

public class DataViaggioFragment extends Fragment implements OnMapReadyCallback{
    private TextView statoViaggio;
    private TextView dataPartenza;
    private TextView luogoPartenza;
    private TextView dataArrivo;
    private TextView luogoArrivo;
    private TextView spazioDisponibile;
    private SupportMapFragment supportMapFragment;
    public GoogleMap mMap;
    private Polyline percorso;
    public Marker daMark;
    public Marker aMark;
    private Activity a;
    private TextView mezzoUtilizzato;
    private static final String OUT_JSON = "/json";
    private Context mContext;
    private static final String RADIUS = "10";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place/nearbysearch";
    private static String API_KEY = "AIzaSyDh7RzrrF5TR0Hi-AtHGtvUAVMc-lIbjUI";
    LinearLayout fromLayout;
    LinearLayout toLayout;



    public DataViaggioFragment() {
        // Required empty public constructor
    }

    private ResultCallback<PlacePhotoResult> fromDisplayPhotoResultCallback
            = new ResultCallback<PlacePhotoResult>() {
        @Override
        public void onResult(PlacePhotoResult placePhotoResult) {
            if (!placePhotoResult.getStatus().isSuccess()) {
                return;
            }
            BitmapDrawable background = new BitmapDrawable(placePhotoResult.getBitmap());
            fromLayout.setBackgroundDrawable(background);
        }
    };
    private ResultCallback<PlacePhotoResult> toDisplayPhotoResultCallback
            = new ResultCallback<PlacePhotoResult>() {
        @Override
        public void onResult(PlacePhotoResult placePhotoResult) {
            if (!placePhotoResult.getStatus().isSuccess()) {
                return;
            }

            BitmapDrawable background = new BitmapDrawable(placePhotoResult.getBitmap());
            toLayout.setBackgroundDrawable(background);
        }
    };
    private void placePhotosAsync(String placeId, String ft) {
        Places.GeoDataApi.getPlacePhotos(ProprietaViaggioActivity.mGoogleApiClient, placeId)
                .setResultCallback(new ResultCallback<PlacePhotoMetadataResult>() {


                    @Override
                    public void onResult(PlacePhotoMetadataResult photos) {
                        switch (ft){
                            case "from":
                                if (!photos.getStatus().isSuccess()) {
                                    return;
                                }
                                PlacePhotoMetadataBuffer photoMetadataBuffer1 = photos.getPhotoMetadata();
                                if (photoMetadataBuffer1.getCount() > 0) {
                                    // Display the first bitmap in an ImageView in the size of the view
                                    photoMetadataBuffer1.get(0)
                                            .getScaledPhoto(ProprietaViaggioActivity.mGoogleApiClient, fromLayout.getWidth(),
                                                    fromLayout.getHeight())
                                            .setResultCallback(fromDisplayPhotoResultCallback);
                                }
                                photoMetadataBuffer1.release();
                                break;
                            case "to":
                                if (!photos.getStatus().isSuccess()) {
                                    return;
                                }
                                PlacePhotoMetadataBuffer photoMetadataBuffer2 = photos.getPhotoMetadata();
                                if (photoMetadataBuffer2.getCount() > 0) {
                                    // Display the first bitmap in an ImageView in the size of the view
                                    photoMetadataBuffer2.get(0)
                                            .getScaledPhoto(ProprietaViaggioActivity.mGoogleApiClient, toLayout.getWidth(),
                                                    toLayout.getHeight())
                                            .setResultCallback(toDisplayPhotoResultCallback);
                                }
                                photoMetadataBuffer2.release();
                                break;
                        }
                    }
                });
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);



    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mContext = getActivity();

        FragmentManager fm = getActivity().getSupportFragmentManager();/// getChildFragmentManager();
        supportMapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
        if (supportMapFragment == null) {
            supportMapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map_container, supportMapFragment).commit();
        }
        supportMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        boolean success = googleMap.setMapStyle(new MapStyleOptions(getResources()
                .getString(R.string.map_style_json)));
        if (!success) {
            Log.e("MAP_STYLAH", "Style parsing failed.");
        }
        mMap = googleMap;
        LatLng from = new LatLng(ProprietaViaggioActivity.travelAttuale.getDeparture().getLat(),ProprietaViaggioActivity.travelAttuale.getDeparture().getLon());
        daMark = mMap.addMarker(new MarkerOptions().position(from).title(getString(R.string.Origin)));
        LatLng to = new LatLng(ProprietaViaggioActivity.travelAttuale.getArrival().getLat(),ProprietaViaggioActivity.travelAttuale.getArrival().getLon());
        aMark = mMap.addMarker(new MarkerOptions().position(to).title(getString(R.string.Destination)));
        percorso = mMap.addPolyline(new PolylineOptions().add(from,to).width(8).color(Color.rgb(18, 96, 147)));
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(daMark.getPosition());
        builder.include(aMark.getPosition());
        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.33);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
        mMap.moveCamera(cameraUpdate);
        float zoom = mMap.getCameraPosition().zoom;
        mMap.animateCamera( CameraUpdateFactory.zoomTo(zoom - 1));
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data_viaggio, container, false);
        mContext = getActivity();

        LatLng arr = new LatLng(ProprietaViaggioActivity.travelAttuale.getArrival().getLat(),ProprietaViaggioActivity.travelAttuale.getArrival().getLon());
        LatLng dep = new LatLng(ProprietaViaggioActivity.travelAttuale.getDeparture().getLat(),ProprietaViaggioActivity.travelAttuale.getDeparture().getLon());

        luogoArrivo = (TextView) view.findViewById(R.id.posizioneArrivo);
        luogoPartenza = (TextView) view.findViewById(R.id.posizionePartenza);

        SimpleDateFormat localDateFormat = new SimpleDateFormat("MM/dd/yyyy");


        Places.GeoDataApi.getPlaceById(ProprietaViaggioActivity.mGoogleApiClient, ProprietaViaggioActivity.travelAttuale.getArrival().getName())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);
                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getAddress());
                            luogoArrivo.setText(myPlace.getName().toString());
                        } else {
                            Log.e("PLACES DETAILS", "Place not found");
                        }
                        places.release();
                    }
                });
        Places.GeoDataApi.getPlaceById(ProprietaViaggioActivity.mGoogleApiClient, ProprietaViaggioActivity.travelAttuale.getDeparture().getName())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);
                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getAddress());
                            luogoPartenza.setText(myPlace.getName().toString());
                        } else {
                            Log.e("PLACES DETAILS", "Place not found");
                        }
                        places.release();
                    }
                });
        dataPartenza = (TextView) view.findViewById(R.id.dataPartenza);
        dataPartenza.setText(localDateFormat.format(travelAttuale.getStartDate()));

        fromLayout = (LinearLayout) view.findViewById(R.id.from_layout);
        toLayout = (LinearLayout) view.findViewById(R.id.to_layout);

        dataArrivo = (TextView) view.findViewById(R.id.dataArrivo);
        dataArrivo.setText(localDateFormat.format(travelAttuale.getEndDate()));

        /*
        LatLng from = new LatLng(ProprietaViaggioActivity.travelAttuale.getDeparture().getLat(),ProprietaViaggioActivity.travelAttuale.getDeparture().getLon());
        daMark = mMap.addMarker(new MarkerOptions().position(from).title(getString(R.string.Origin)));
        LatLng to = new LatLng(ProprietaViaggioActivity.travelAttuale.getArrival().getLat(),ProprietaViaggioActivity.travelAttuale.getArrival().getLon());
        aMark = mMap.addMarker(new MarkerOptions().position(from).title(getString(R.string.Destination)));
        */

        //writeCityName(view.getContext(), luogoPartenza, ProprietaViaggioActivity.travelAttuale.getDeparture().getLat(),ProprietaViaggioActivity.travelAttuale.getDeparture().getLon());
        //writeCityName(view.getContext(), luogoArrivo, ProprietaViaggioActivity.travelAttuale.getArrival().getLat(),ProprietaViaggioActivity.travelAttuale.getArrival().getLon());

        LatLng arrVicinity = new LatLng(0,0);
        LatLng depVicinity = new LatLng(0,0);
        String vicinity_from = "";
        String vicinity_to = "";
        new FindVicinityTask(vicinity_to).execute(arr);
        new FindVicinityTask(vicinity_from).execute(dep);
        String arrId = "";
        String depId = "";
        new FindIdTask(arrId).execute(arr);
        new FindIdTask(depId).execute(dep);
        LinearLayout card_layout = (LinearLayout) view.findViewById(R.id.arr_dep_layout);
        //ImageView from_bkg = (ImageView) view.findViewById(R.id.from_bkg);
        //ImageView to_bkg = (ImageView) view.findViewById(R.id.to_bkg);
        ViewTreeObserver vto =  card_layout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                card_layout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                //placePhotosTask(ProprietaViaggioActivity.travelAttuale.getDeparture().getName(), from_bkg,card_layout.getMeasuredWidth(),card_layout.getMeasuredHeight());
                //placePhotosTask(ProprietaViaggioActivity.travelAttuale.getArrival().getName(), to_bkg,card_layout.getMeasuredWidth(), card_layout.getMeasuredHeight());
            }
        });

        ImageView travelState = (ImageView) view.findViewById(R.id.travel_state_img);

        switch (ProprietaViaggioActivity.travelAttuale.getState()){
            case CREATED:
                travelState.setImageResource(R.drawable.ic_hourglass_full_blue_24dp);
                //statoPacco.setText(getString(R.string.Created));
                break;
            case STARTED:
                travelState.setImageResource(R.drawable.ic_my_location_blue_24dp);
                //statoPacco.setText(getString(R.string.Travelling));
                break;
        }

        spazioDisponibile = (TextView) view.findViewById(R.id.spazioDisponibile);

        ImageView mezzoImg = (ImageView) view.findViewById(R.id.transImg);


        /*
        Temporaneo e NON SCALABILE. Dobbiamo parametrizzare gli strings array  multilingua
         */

        switch(ProprietaViaggioActivity.travelAttuale.getTransports().toString().replace("[","").replace("]","")){
            case "PULLMAN":
                mezzoImg.setImageResource(R.drawable.ic_directions_bus_blue_24dp);
                break;
            case "BUS":
                mezzoImg.setImageResource(R.drawable.ic_directions_bus_blue_24dp);
                break;
            case "AEREO":
                mezzoImg.setImageResource(R.drawable.ic_flight_takeoff_blue);
                break;
            case "AIRPLANE":
                mezzoImg.setImageResource(R.drawable.ic_flight_takeoff_blue);
                break;
            case "AUTO":
                mezzoImg.setImageResource(R.drawable.ic_directions_car_blue_24dp);
                break;
            case "CAR":
                mezzoImg.setImageResource(R.drawable.ic_directions_car_blue_24dp);
                break;
            case "TRENO":
                mezzoImg.setImageResource(R.drawable.ic_train_blue_24dp);
                break;
            case "TRAIN":
                mezzoImg.setImageResource(R.drawable.ic_train_blue_24dp);
                break;
        }

        if(ProprietaViaggioActivity.travelAttuale.getSpace() == 10.00){
            spazioDisponibile.setText("S");
            //spazioDisponibile.setText(getResources().getStringArray(R.array.spazio)[0].replace("[","").replace("]",""));
        }
        if(ProprietaViaggioActivity.travelAttuale.getSpace() == 50.00){
            spazioDisponibile.setText("M");
            //spazioDisponibile.setText(getResources().getStringArray(R.array.spazio)[1].replace("[","").replace("]",""));
        }
        if(ProprietaViaggioActivity.travelAttuale.getSpace() == 100.00){
            spazioDisponibile.setText("L");
            //spazioDisponibile.setText(getResources().getStringArray(R.array.spazio)[2].replace("[","").replace("]",""));
        }




        //mezzoUtilizzato = (TextView) view.findViewById(R.id.mezzoUtilizzato);
        //mezzoUtilizzato.setText(ProprietaViaggioActivity.travelAttuale.getTransports().toString().replace("[","").replace("]",""));

        //statoViaggio = (TextView) view.findViewById(R.id.tripState);

        String ra1 = getString(R.string.STATE)+":";
        int ra1length = ra1.length();
        String ra2 = ProprietaViaggioActivity.travelAttuale.getState().toString();
        int ra2lenth = ra2.length();
        String r = ra1+ra2;
        SpannableString r2=  new SpannableString(r);
        r2.setSpan(new RelativeSizeSpan(1f), ra1length,ra1length+ra2lenth, 0);
        r2.setSpan(new ForegroundColorSpan(Color.rgb(18, 96, 147)), ra1length,ra1length+ra2lenth, 0);
        //statoViaggio.setText(r2);

        //statoViaggio.setText(ProprietaViaggioActivity.travelAttuale.getState() + "");




        return view;
    }

    private void placePhotosTask(String placeId, ImageView imageView, int width, int height) {
        //final String placeId = "ChIJrTLr-GyuEmsRBfy61i59si0"; // Australian Cruise Group
        // Create a new AsyncTask that displays the bitmap and attribution once loaded.
        new PhotoTask(ProprietaViaggioActivity.mGoogleApiClient, width/2, height) {
            @Override
            protected void onPreExecute() {
                // Display a temporary image to show while bitmap is loading.
                //linearLayout.setBackgroundResource(R.drawable.);
                //mImageView.setImageResource(R.drawable.empty_photo);
            }

            @Override
            protected void onPostExecute(AttributedPhoto attributedPhoto) {
                if (attributedPhoto != null) {
                    // Photo has been loaded, display it.
                    Bitmap bmImg = attributedPhoto.bitmap;
                    imageView.setImageBitmap(bmImg);
                    //linearLayout.setBackgroundDrawable(background);
                    //mImageView.setImageBitmap(attributedPhoto.bitmap);

                    // Display the attribution as HTML content if set.
                    if (attributedPhoto.attribution == null) {
                        //mText.setVisibility(View.GONE);
                    } else {
                        //mText.setVisibility(View.VISIBLE);
                        //mText.setText(Html.fromHtml(attributedPhoto.attribution.toString()));
                    }

                }else{
                    Log.e("PLACES IMAGES", "NESSUNA IMMAGINE È STATA SETTATA PERCHÉ NULL");
                }
            }
        }.execute(placeId);
    }


    public void writeCityName(Context ctx, TextView tv, double lat, double lng){
        Geocoder geoCoder = new Geocoder(ctx, Locale.getDefault());

        try {
            List<Address> addresses = geoCoder.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0)
            {
                tv.setText(addresses.get(0).getLocality().toString());
                System.out.println(addresses.get(0).getLocality());
            }
            else
            {
                // do your staff
            }

        } catch (IOException e) {
            ;
        }
    }
}
