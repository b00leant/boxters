package com.boxters.android.test;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.boxters.android.DownloadImageTask;
import com.boxters.android.ListaPacchiQueryActivity;
import com.boxters.android.MainActivity;
import com.boxters.android.ProprietaPaccoActivity;
import com.boxters.android.ProprietaViaggioActivity;
import com.boxters.android.R;
import com.boxters.android.controller.messaging.MessageSender;
import com.boxters.model.AuthorizedBox;
import com.boxters.model.Box;
import com.boxters.model.BoxState;
import com.boxters.model.CountDown;
import com.boxters.model.PublicBox;
import com.boxters.model.PublicTravel;
import com.boxters.model.PublicUser;
import com.boxters.model.Travel;
import com.boxters.model.messaging.RequestBoxInfoPayload;
import com.boxters.model.messaging.SelectedAsPossibleBoxterPayload;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.vision.text.Text;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Created by giuse on 27/02/2017.
 */
/*
public class LPQCA {
}
*/

public class LPQCA extends ArrayAdapter<PublicBox> {
    public PacchiQueryFragment ma;
    private int i;
    private ListView lv;


    public void writeCityName(Context ctx, TextView tv, double lat, double lng){
        Geocoder geoCoder = new Geocoder(ctx, Locale.getDefault());

        try {
            List<Address> addresses = geoCoder.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0)
            {
                tv.setText(addresses.get(0).getLocality().toString());
                System.out.println(addresses.get(0).getLocality());
            }
            else
            {
                // do your staff
            }

        } catch (IOException e) {
            ;
        }
    }

    public LPQCA(PacchiQueryFragment _ma, PublicBoxList lp) {
        super(_ma.getActivity().getApplicationContext(), R.layout.pacchi_listview_item);
        ma = _ma;
        if (lp != null)
            importa(lp);
    }



    protected void importa(PublicBoxList pb) {
        for (PublicBox b : pb) {
            this.add(b);
        }
    }

    protected PublicBox getElement(int pos) {
        return this.getItem(pos);
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        PublicBox b = this.getItem(position);

        lv = (ListView) parent;



        convertView = ma.getActivity().getLayoutInflater().inflate(R.layout.pacchi_listview_item, null);
        TextView nomePacco = (TextView) convertView.findViewById(R.id.nome_pacco);
        TextView senderName= (TextView) convertView.findViewById(R.id.sender_name);
        TextView receiverName= (TextView) convertView.findViewById(R.id.receiver_name);
        TextView secondLineA = (TextView) convertView.findViewById(R.id.secondLineA);
        TextView secondLineB = (TextView) convertView.findViewById(R.id.secondLineB);
        TextView dimensionLine = (TextView) convertView.findViewById(R.id.dimensionLine);
        //TextView dimensionLine = (TextView) convertView.findViewById(R.id.dimensionLine);
        //TextView peso = (TextView) convertView.findViewById(R.id.peso_pacco);
        RatingBar ratingBar = (RatingBar) convertView.findViewById(R.id.ratingBar);
        TextView expiration = (TextView) convertView.findViewById(R.id.expiration_date);

        String from_id = "";
        String to_id = "";
        Places.GeoDataApi.getPlaceById(ProprietaViaggioActivity.mGoogleApiClient, b.getArrival().getName())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);

                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                            secondLineB.setText(myPlace.getName());
                        } else {
                            Log.e("PLACES DETAILS", "Place found: " + b.getArrival().getName());
                            Log.e("PLACES DETAILS",places.getStatus().getStatusMessage());
                            Log.e("PLACES DETAILS", "Place not found");
                        }
                        places.release();
                    }
                });
        Places.GeoDataApi.getPlaceById(ProprietaViaggioActivity.mGoogleApiClient, b.getDeparture().getName())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);
                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                            secondLineA.setText(myPlace.getName());
                        } else {
                            Log.e("PLACES DETAILS", "Place not found: " + b.getDeparture().getName());
                            Log.e("PLACES DETAILS",places.getStatus().getStatusMessage());
                            Log.e("PLACES DETAILS", "Place not found");
                        }
                        places.release();
                    }
                });
        View finalConvertView = convertView;
        PublicUser.load(b.getSender(),(user) -> {
            ratingBar.setRating(user.getRating() != null ? user.getRating().floatValue() : 0);
            senderName.setText(user.getName());
            if (user.getPhoto() != null) {
                ImageView iconSender = (ImageView) finalConvertView.findViewById(R.id.iconSender);
                if (iconSender != null)
                    new DownloadImageTask(iconSender).execute(user.getPhoto());
            }

        },false);

        ImageButton buttonNotification = (ImageButton) convertView.findViewById((R.id.buttonNotifica));
        buttonNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(ma.getActivity());
                alert.setTitle(ma.getString(R.string.request_box_info_title));
                alert.setMessage(ma.getString(R.string.request_box_info_body_boxter));
                alert.setNegativeButton(ma.getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(
                                    DialogInterface dialog,
                                    int whichButton) {
                                dialog.cancel();
                            }
                        });
                alert.setPositiveButton(ma.getString(R.string.Sure),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(
                                    DialogInterface dialog,
                                    int whichButton) {
                                PublicUser.load(b.getSender(),(user) -> {
                                    String userToken = user.getMessagingToken();
                                    if (userToken != null){
                                        MessageSender.send(new RequestBoxInfoPayload(MainActivity.user.getId(),userToken, b.getId(), ProprietaViaggioActivity.travelAttuale.getId()));

                                        PublicBox.load(b.getSender(), b.getId(), (PublicBox box) ->{
                                            if(box != null){
                                                ProprietaViaggioActivity.travelAttuale.addBox(
                                                        user.getId(),b.getId(),new Travel.TravelBoxProj(box.getReceiver(), BoxState.CREATED),null);

                                            }
                                        },false);
                                        Intent startNewActivity = new Intent(ma.getContext(),ProprietaViaggioActivity.class);
                                        ma.getActivity().finishAffinity();
                                        ma.getActivity().startActivity(startNewActivity);
                                    }
                                },false);
                            }
                        });
                alert.create().show();
            }
        });

        SimpleDateFormat localDateFormat = new SimpleDateFormat("MM/dd/yyyy");
        nomePacco.setText(b.getObject());
        //TODO: BACKEND [@Daniele] Ottenere il peso anche dalla publicBox, è possibile?

        if(b.getSize() <= 1.0){
            System.out.println("LO SPAZIO È STATO SETTATO S");
            //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[0].replace("[","").replace("]","").split("\\(")[0]);
            dimensionLine.setText("S");
        }
        else if(b.getSize() <= 2.0){
            System.out.println("LO SPAZIO È STATO SETTATO M");
            dimensionLine.setText("M");
            //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[1].replace("[","").replace("]","").split("\\(")[0]);
        }
        else if(b.getSize() <= 3.0){
            System.out.println("LO SPAZIO È STATO SETTATO L");
            dimensionLine.setText("L");
            //spazioPacco.setText(getResources().getStringArray(R.array.spazio)[2].replace("[","").replace("]","").split("\\(")[0]);
        }else{
            System.out.println("IL VERO SPAZIO È "+ProprietaPaccoActivity.boxAttuale.getSize());
            dimensionLine.setText("?");
        }

        //writeCityName(ma.getContext(),secondLineA,b.getDeparture().getLat(),b.getDeparture().getLon());
        //writeCityName(ma.getContext(),secondLineB,b.getArrival().getLat(),b.getArrival().getLon());
        //secondLineB.setText("to " + t.getArrival().getName());
        String week = getContext().getString(R.string.Week);
        String days = getContext().getString(R.string.Days);
        String day = getContext().getString(R.string.Day);

        Calendar calCurr = Calendar.getInstance();
        Calendar expirationDate = Calendar.getInstance();
        expirationDate.setTime(b.getExpiration());
        int daysLeft = 0;
        if(expirationDate.after(calCurr)){
            daysLeft = ((expirationDate.get(Calendar.DAY_OF_MONTH)) - (calCurr.get(Calendar.DAY_OF_MONTH)));
            System.out.println("Days Left: " + (expirationDate.get(Calendar.DAY_OF_MONTH) -(calCurr.get(Calendar.DAY_OF_MONTH))) );
        }
        Log.d("DAYSLEFT",daysLeft+" days to "+localDateFormat.format(b.getExpiration()));
        if(daysLeft == 1){
            expiration.setText("-1 "+day);
        }else if(daysLeft <= 14 && daysLeft >= 2){
            if(daysLeft%7 == 0){
                expiration.setText("-"+(daysLeft/7)+" "+week);
            }else{
                expiration.setText("-"+daysLeft+" "+days);
            }
        }else{
            expiration.setText(localDateFormat.format(b.getExpiration()));
        }
        return convertView;
    }
}