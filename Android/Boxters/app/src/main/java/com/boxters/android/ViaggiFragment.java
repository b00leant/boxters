package com.boxters.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.boxters.android.test.ListaPacchiMainCustomArrayAdapter;
import com.boxters.model.Box;
import com.boxters.model.PublicTravel;
import com.boxters.model.Travel;
import com.boxters.android.test.ListaViaggiMainCustomArrayAdapter;
import com.boxters.model.User;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;

import java.util.ArrayList;
import java.util.List;


public class ViaggiFragment extends Fragment {

    public static ListaViaggiMainCustomArrayAdapter aa;
    private GoogleApiClient mGoogleApiClient;
    private ProgressBar pb;
    private ListView lv;
    private TextView tv;
    private SwipeRefreshLayout swipeRefreshLayout;

    public ViaggiFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_lista_viaggi_main, container, false);
        lv = (ListView) view.findViewById(R.id.listViewViaggi);
        aa = new ListaViaggiMainCustomArrayAdapter(this, null);
        tv = (TextView) view.findViewById(R.id.tvFragmentTwo);
        pb = (ProgressBar) view.findViewById(R.id.prgbar);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        aa.loadAllUserTravels(MainActivity.user);
                    }
                }
        );
        lv.setAdapter(aa);
        lv.setEmptyView(tv);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3)
            {
                ProprietaViaggioActivity.travelAttuale = (Travel) adapter.getItemAtPosition(position);
                Intent startNewActivity = new Intent(v.getContext(), ProprietaViaggioActivity.class);
                getActivity().finish();
                startActivity(startNewActivity);
            }
        });
        //Codice David
        lv.setLongClickable(true);

        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> arg0, View v,
                                           int index, long arg3) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = LayoutInflater.from(getContext());
                View dialogView = inflater.inflate(R.layout.item_dialog, null);
                Travel t  = (Travel) arg0.getItemAtPosition(index);
                dialogBuilder.setView(dialogView);
                AlertDialog alertDialog = dialogBuilder.create();
                if(t.getState().toString() == "CREATED"){
                    ImageButton yes = (ImageButton) dialogView.findViewById(R.id.btn_yes);
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //MapActivity.travelAttuale = t;
                            Bundle bundle = new Bundle();
                            bundle.putString("from_id",t.getDeparture().getName());
                            bundle.putString("to_id",t.getArrival().getName());
                            bundle.putString("travel_id",t.getId());
                            bundle.putString("box_id",null);
                            bundle.putDouble("from_lat",t.getDeparture().getLat());
                            bundle.putDouble("from_lon",t.getDeparture().getLon());
                            bundle.putDouble("to_lat",t.getArrival().getLat());
                            bundle.putDouble("to_lon",t.getArrival().getLon());
                            bundle.putString("user_id",t.getBoxter());
                            bundle.putString("mode","travels");
                            //MapActivity.boxAttuale = (Box) arg0.getItemAtPosition(index);
                            Intent startNewActivity = new Intent(v.getContext(), MapActivity.class);
                            startNewActivity.putExtras(bundle);
                            getActivity().finish();
                            startActivity(startNewActivity);
                            alertDialog.cancel();
                        }
                    });
                    ImageButton no = (ImageButton) dialogView.findViewById(R.id.btn_no);
                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Travel box_to_delete = (Travel) t;
                            box_to_delete.delete((box_deleted) -> {
                                ListaViaggiMainCustomArrayAdapter adapter = (ListaViaggiMainCustomArrayAdapter) arg0.getAdapter();
                                adapter.remove(t);
                                alertDialog.cancel();
                                Toast.makeText(getContext(), "Travel deleted! (maybe)",Toast.LENGTH_SHORT);
                            });
                        }
                    });
                    alertDialog.show();
                }
                return true;
            }});


        return view;
    }

    public void showProgressBar(){
        pb.setVisibility(View.VISIBLE);
    }
    public void hideProgressBar(){
        pb.setVisibility(View.GONE);
    }
    public void hideSwipeUpdate() { swipeRefreshLayout.setRefreshing(false);}
    public void showListView() {lv.setVisibility(View.VISIBLE);}
    public void setEmptyView() {lv.setEmptyView(tv);}
    public void showTV() {tv.setVisibility(View.VISIBLE);}

    public void creaNuovoViaggio() {

        Intent startNewActivity = new Intent( this.getContext(), MapActivity.class);
        startNewActivity.putExtra("tipoProcesso", MapActivity.TIPO_PROCESSO_VIAGGIO );
        startActivity(startNewActivity );
    }



}


/*
        ListView lv = (ListView) view.findViewById(R.id.listViewViaggi);
        aa = new ListaViaggiMainCustomArrayAdapter(this.getActivity(), Global.getInstance().listaViaggi);
        lv.setAdapter(aa);


        if (Global.getInstance().listaViaggi != null && Global.getInstance().listaViaggi.size() > 0) {
            TextView tv = (TextView) view.findViewById(R.id.tvFragmentTwo);
            tv.setVisibility(View.INVISIBLE);
        }
*/