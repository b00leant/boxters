package com.boxters.android;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.boxters.model.AuthorizedBox;
import com.boxters.model.Box;
import com.boxters.model.Travel;
import com.boxters.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Logger;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ProfileActivity extends AppCompatActivity {
    private User myuser;
    private int tott = 0;
    private int totb = 0;
    public void loadAllUserTravels(User u ) {

        Log.e("LOADER","CALLED");
        if (u != null) {
            Log.e("USER","USER_NOT_NULL");
            Travel.loadAll(u.getId(), (Travel travel) -> {
                Log.e("COMPLETION","CALLED");
                if (travel != null) {
                    Log.e("TRAVELLIST","TRAVEL_ADDED");
                    tott++;
                    Log.e("TOTT","tott is: "+tott);
                    TextView trips = (TextView) findViewById(R.id.user_profile_trips);
                    String t= ""+tott;
                    /*SpannableString t2=  new SpannableString(t);
                    t2.setSpan(new RelativeSizeSpan(2f), 8,9, 0);
                    t2.setSpan(new ForegroundColorSpan(Color.rgb(18, 96, 147)), 8, 9, 0);*/
                    trips.setText(t);
                }else{
                    Log.e("TRAVELLIST","TRAVEL_NOT_FOUND");
                }
            }, false);
        }else{
            Log.e("USER","USER IS NULL");
        }
    }

    public void loadAllUserBoxes(User u) {
        Log.e("LOADER","CALLED");
        if (u != null) {
            Log.e("USER","USER_NOT_NULL");
            Box.loadAsSender(u.getId(), (Box box) -> {
                Log.e("COMPLETION","CALLED");
                if (box != null) {
                    totb++;
                    Log.e("BOXESLIST","BOX_ADDED");
                    Log.e("TOTB","totb is: "+totb);
                    TextView boxes = (TextView) findViewById(R.id.user_profile_boxes);
                    String b= ""+totb;
                    /*SpannableString b2=  new SpannableString(b);
                    b2.setSpan(new RelativeSizeSpan(2f), 8,9, 0);
                    b2.setSpan(new ForegroundColorSpan(Color.rgb(18, 96, 147)), 8, 9, 0);*/
                    boxes.setText(b);
                }else{
                    Log.e("BOXESLIST","BOX_NOT_FOUND");
                }
            }, false);

            Box.loadAsReceiver(u, (box) -> {
                Log.e("COMPLETION","CALLED");
                if (box != null) {
                    totb++;
                    Log.e("BOXESLIST","BOX_ADDED");
                    Log.e("TOTB","totb is: "+totb);
                    TextView boxes = (TextView) findViewById(R.id.user_profile_boxes);
                    String b= ""+totb;
                    /*SpannableString b2=  new SpannableString(b);
                    b2.setSpan(new RelativeSizeSpan(2f), 8,9, 0);
                    b2.setSpan(new ForegroundColorSpan(Color.rgb(18, 96, 147)), 8, 9, 0);*/
                    boxes.setText(b);
                }else{
                    Log.e("BOXESLIST","BOX_NOT_FOUND");
                }
            }, false);
        }else{
            Log.e("USER","USER IS NULL");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                            // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_profile);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();

        if (user != null) {
            Log.d("LOGIN","ALREADY SIGNED IN");
            User.load(user.getUid(),(User _user) -> {
                if(_user != null) {
                    this.myuser = _user;
                    String ra1 = "Rating: ";
                    int ra1length = ra1.length();
                    String ra2 = (myuser.getRating() != null ? myuser.getRating().toString() : "0") + "%";
                    int ra2lenth = ra2.length();
                    String r = ra1+ra2;
                    TextView phone_utente = (TextView) findViewById(R.id.phone);
                    phone_utente.setText(_user.getPhone());
                    SpannableString r2=  new SpannableString(r);
                    r2.setSpan(new RelativeSizeSpan(2f), 8,ra1length+ra2lenth, 0);
                    r2.setSpan(new ForegroundColorSpan(Color.rgb(18, 96, 147)), 8,ra1length+ra2lenth, 0);

                    RatingBar rating = (RatingBar) findViewById(R.id.ratingBar);
                    rating.setMax(5);
                    if(_user.getRating()==null){
                        double rating_s1 = 0;
                        rating.setRating( (float) rating_s1);
                    }else{
                        double rating_s2 = _user.getRating();
                        rating.setRating( (float) rating_s2);
                    }
                    loadAllUserBoxes(_user);
                    loadAllUserTravels(_user);


                }else{
                    Log.d("USER","NEED A LOGIN!?");
                }
            },false);
        }else{
            throw new InternalError("onActivityResult chiamato con resultCode OK frg utente non loggato");
        }
        ImageButton editPhone = (ImageButton) findViewById(R.id.editPhone);
        editPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startNewActivity = new Intent(ProfileActivity.this,PhoneNumberActivity.class);
                finishAffinity();
                startActivity(startNewActivity);
            }
        });
        String imageURL = user.getPhotoUrl() != null ? user.getPhotoUrl().toString() : null;
        new DownloadImageTask((ImageView) findViewById(R.id.user_profile_photo)).execute(imageURL);
        TextView nome_utente = (TextView) findViewById(R.id.user_profile_name);
        TextView email_utente = (TextView) findViewById(R.id.user_profile_email);
        nome_utente.setText(user.getDisplayName());
        email_utente.setText(user.getEmail());
    }



}
