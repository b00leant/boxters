package com.boxters.android;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.boxters.model.Location;
import com.boxters.model.Transport;
import com.boxters.model.Travel;
import com.boxters.android.test.TravelList;
import com.boxters.android.test.Viaggio;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;


public class RiepilogoViaggioActivity extends AppCompatActivity implements OnMapReadyCallback{

    public Global G = Global.getInstance();
    private TextView locationDa;
    private TextView dataPartenza;
    private SupportMapFragment supportMapFragment;
    public GoogleMap mMap;
    private Polyline percorso;
    public Marker daMark;
    public Marker aMark;
    private TextView dataArrivo;
    private Location da;
    private Location a;
    private TextView locationA;
    private TextView spazioDisponibile;
    private TextView mezzoUtilizzato;

    private GoogleApiClient mGoogleApiClient;



    @Override
    public void onMapReady(GoogleMap googleMap) {
        boolean success = googleMap.setMapStyle(new MapStyleOptions(getResources()
                .getString(R.string.map_style_json)));
        if (!success) {
            Log.e("MAP_STYLAH", "Style parsing failed.");
        }
        mMap = googleMap;
        LatLng from = new LatLng(da.getLat(),da.getLon());
        daMark = mMap.addMarker(new MarkerOptions().position(from).title(getString(R.string.Origin)));
        LatLng to = new LatLng(a.getLat(),a.getLon());
        aMark = mMap.addMarker(new MarkerOptions().position(to).title(getString(R.string.Destination)));
        percorso = mMap.addPolyline(new PolylineOptions().add(from,to).width(8).color(Color.rgb(18, 96, 147)));
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(daMark.getPosition());
        builder.include(aMark.getPosition());
        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.33);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
        mMap.moveCamera(cameraUpdate);
        float zoom = mMap.getCameraPosition().zoom;
        mMap.animateCamera( CameraUpdateFactory.zoomTo(zoom - 1));
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riepilogo_viaggio);

        da = G.attributiViaggio.partenza;
        a = G.attributiViaggio.arrivo;
        FragmentManager fm = getSupportFragmentManager();
        supportMapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
        if (supportMapFragment == null) {
            supportMapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map_container, supportMapFragment).commit();
        }
        supportMapFragment.getMapAsync(this);

        mGoogleApiClient = new GoogleApiClient
                .Builder(RiepilogoViaggioActivity.this)
                .enableAutoManage((FragmentActivity) RiepilogoViaggioActivity.this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        // your code here
                    }
                })
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();

        Places.GeoDataApi.getPlaceById(mGoogleApiClient, G.attributiViaggio.arrivo.getName())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);
                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                            locationA = (TextView) findViewById(R.id.locationA);
                            locationA.setText(myPlace.getName());
                        } else {
                            Log.e("PLACES DETAILS", "Place not found");
                            Log.e("PLACES DETAILS",places.getStatus().getStatusMessage());
                        }
                        places.release();
                    }
                });
        Places.GeoDataApi.getPlaceById(mGoogleApiClient, G.attributiViaggio.partenza.getName())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);
                            Log.i("PLACES DETAILS", "Place found: " + myPlace.getName());
                            locationDa = (TextView) findViewById(R.id.locationDa);
                            locationDa.setText(myPlace.getName());
                        } else {
                            Log.e("PLACES DETAILS", "Place not found");
                        }
                        places.release();
                    }
                });

        dataPartenza = (TextView) findViewById(R.id.dataPartenza);
        dataPartenza.setText(G.attributiViaggio.string_inizio_viaggio);


        //writeCityName(RiepilogoViaggioActivity.this, locationDa, da.getLat(),da.getLon());

        System.out.println(da.getName().toString()+" lat:"+da.getLat()+" lon:"+da.getLon());
        System.out.println(a.getName().toString()+" lat:"+a.getLat()+" lon:"+a.getLon());

        dataArrivo = (TextView) findViewById(R.id.dataArrivo);
        dataArrivo.setText(G.attributiViaggio.string_fine_viaggio);


        //writeCityName(RiepilogoViaggioActivity.this, locationA, a.getLat(),a.getLon());

        spazioDisponibile = (TextView) findViewById(R.id.spazioDisponibile);


        ImageView mezzoImg = (ImageView) findViewById(R.id.mezzoImg);


        /*
        Temporaneo e NON SCALABILE. Dobbiamo parametrizzare gli strings array  multilingua
         */

        switch(G.attributiViaggio.trasporto){
            case "BUS":
                mezzoImg.setImageResource(R.drawable.ic_directions_bus_blue_24dp);
                break;
            case "PLANE":
                mezzoImg.setImageResource(R.drawable.ic_flight_takeoff_blue);
                break;
            case "CAR":
                mezzoImg.setImageResource(R.drawable.ic_directions_car_blue_24dp);
                break;
            case "TRAIN":
                mezzoImg.setImageResource(R.drawable.ic_train_blue_24dp);
                break;
        }
        switch (G.attributiViaggio.spazio_disponibile){
            case "S":
                spazioDisponibile.setText("S");
                break;
            case "M":
                spazioDisponibile.setText("M");
                break;
            case "L":
                spazioDisponibile.setText("L");
                break;
        }

    }

    public void startActivity(Class cls) {
        Intent startNewActivity = new Intent(this, cls);
        startActivity(startNewActivity);
    }


    public void onConfirm(View view) {


        if (G.listaViaggi == null) {
            G.listaViaggi = new TravelList();
        }

        Set<Transport> transports = new HashSet<Transport>();

        switch(G.attributiViaggio.trasporto){
            case "BUS":
                transports.add(Transport.BUS);
                break;
            case "PLANE":
                transports.add(Transport.AIRPLANE);
                break;
            case "CAR":
                transports.add(Transport.CAR);
                break;
            case "TRAIN":
                transports.add(Transport.TRAIN);
                break;
        }
        //G.listaViaggi.add(G.attributiViaggio);
        double space = 10.00;
        switch (G.attributiViaggio.spazio_disponibile){
            case "S":
                space = 10.00;
                break;
            case "M":
                space = 50.00;
                break;
            case "L":
                space = 100.00;
                break;
        }


        Travel.create(G.currentUser.getUid(),G.attributiViaggio.inizio_viaggio,G.attributiViaggio.fine_viaggio, space,transports,
                G.attributiViaggio.partenza,G.attributiViaggio.arrivo,
                (travel) -> {

                    ListaPacchiQueryActivity.currentTravel = travel;

                    G.attributiViaggio = new Viaggio();
                    Intent startNewActivity = new Intent( this, ListaPacchiQueryActivity.class);
                    finishAffinity();
                    startActivity(startNewActivity);

                }

                , true);

    }

    public void writeCityName(Context ctx, TextView tv, double lat, double lng){
        Geocoder geoCoder = new Geocoder(ctx, Locale.getDefault());

        try {
            List<Address> addresses = geoCoder.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0)
            {
                tv.setText(addresses.get(0).getLocality().toString());
                System.out.println(addresses.get(0).getLocality());
            }
            else
            {
                // do your staff
            }

        } catch (IOException e) {
            ;
        }
    }

}












/* public static void create(@NotNull String boxter, @NotNull Date startDate, @NotNull Date endDate,
                              double space, @NonNull Set<Transport> transports,
                              @NotNull Location departure, @NotNull Location arrival,
                              @Nullable TravelCompletion completion, boolean autoUpdate)*/