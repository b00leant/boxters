package com.boxters.android;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.boxters.android.test.CenterDecorator;
import com.boxters.android.test.CenterDisableDecorator;
import com.boxters.android.test.RightDisableDecorator;
import com.boxters.android.test.DisableDecorator;
import com.boxters.android.test.LeftDecorator;
import com.boxters.android.test.LeftDisableDecorator;
import com.boxters.android.test.RightDecorator;
import com.boxters.model.Location;
import com.boxters.model.Transport;
import com.boxters.model.Travel;
import com.boxters.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnRangeSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class FormViaggioActivity extends AppCompatActivity {

    private MaterialCalendarView cv;
    public Global G = Global.getInstance();
    private Bundle b;
    public static Travel travelAttuale;
    Spinner spinner_mezzo_di_trasporto;
    Spinner spinner_spazio;
    private RadioGroup spaceGroup;
    private RadioGroup vehicleGroup;
    private Location fromLocation;
    private Location toLocation;
    DatePicker picker_partenza;
    DatePicker picker_arrivo;
    Button btn1;
    EditText fromDate, toDate;
    private String dateOfTheTravel;
    SeekBar seek_bar;
    TextView text_view;
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    private Date fromTravelDate;
    private Date toTravelDate;
    private String from_id;
    private String to_id;
    private String box_id;
    private String travel_id;
    private double from_lat,from_lon;
    private double to_lat,to_lon;
    private String user_id;
    private List<Date> disabled_dates;
    private Calendar cal;

    public void seekbarr( ){
        //seek_bar = (SeekBar)findViewById(R.id.seekBarViaggio);

        //seek_bar.setMax(15);

        //text_view =(TextView)findViewById(R.id.kmProgressViaggio);
        //text_view.setText("KM : " + seek_bar.getProgress() + " / " +seek_bar.getMax());



        /*seek_bar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {

                    int progress_value;
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        progress_value = progress;
                        text_view.setText("KM : " + progress + " / " +seek_bar.getMax());

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        text_view.setText("KM : " + progress_value + " / " +seek_bar.getMax());
                    }
                }
        );*/

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
        setContentView(R.layout.activity_form_viaggio);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        spaceGroup = (RadioGroup) findViewById(R.id.travel_space);
        vehicleGroup = (RadioGroup) findViewById(R.id.travel_vehicle);
        toolbar.setTitle(getString(R.string.Compile_Travel));
        setSupportActionBar(toolbar);
        seekbarr();
        b = getIntent().getExtras();
        if(b != null){
            from_id = b.getString("from_id");
            to_id = b.getString("to_id");
            box_id = b.getString("box_id");
            travel_id = b.getString("travel_id");
            from_lat = b.getDouble("from_lat");
            from_lon = b.getDouble("from_lon");
            to_lat = b.getDouble("to_lat");
            to_lon = b.getDouble("to_lon");
            user_id = b.getString("user_id");
            System.out.println("---------------------------------");
            System.out.println("from_id: "+from_id);
            System.out.println("from_lat: "+from_lat+" from_lon: "+from_lon);
            System.out.println("to_id: "+to_id);
            System.out.println("to_lat: "+to_lat+" to_lon: "+to_lon);
            System.out.println("---------------------------------");
        }
        findViewsById();
        setDateTimeField();

        /*
         *  Inizializzo le strutture dati per prendermi i giorni non disponibili per
         *  Creare/Editare il viaggio
         */

        disabled_dates = new ArrayList<Date>();
        cal = Calendar.getInstance();
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();

        /*
         *  Mi prendo un utente dall'istanza auth di firebase e carico in modo asincrono
         *  i giorni di arrivo e partenza di ogni viaggio badando che il viaggio corrente non
         *  venga inserito nella lista dei giorni non disponibili in quanto dobbiamo essere liberi,
         *  in caso di modifica, di settare il range dei giorni. Ergo, non do per scontato
         *  che il viaggio che sto per modificare possa avere come date riprogrammate, le stesse
         *  identiche date scelte in precedenza (esempio: un viaggio che magari deve essere
         *  prolungato di un solo giorno)
         */

        if (user != null) {
            User.load(user.getUid(), (User _user) -> {
                if (_user != null) {
                    Travel.loadAll(_user.getId(), (Travel travel) -> {
                        Log.e("COMPLETION","CALLED");
                        if (travel != null && !travel.getId().equals(travel_id)) {
                            Date fromTravelDate = travel.getStartDate();
                            Date toTravelDate = travel.getEndDate();
                            cal.setTime(fromTravelDate);
                            disabled_dates.add(cal.getTime());
                            cal.add(Calendar.DATE, 1);
                            while (cal.getTime().before(toTravelDate) || cal.getTime().equals(toTravelDate)) {
                                disabled_dates.add(cal.getTime());
                                Log.d("DISABLED_DATES","ADDED"+cal.getTime());
                                cal.add(Calendar.DATE, 1);
                            }

                        }else{
                            Log.e("TRAVELLIST","TRAVEL_NOT_FOUND");
                        }
                    }, false);
                }
            },false);
        }


        fromLocation = new Location(from_id,from_lat,from_lon);
        toLocation = new Location(to_id,to_lat,to_lon);

        /*
         *  Setto il listener che al click del form di tipo EditText mi apre il dialog customizzato
         *  di selezione data e range
         */
        fromDate.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LayoutInflater inflater = (LayoutInflater)getApplicationContext().getSystemService
                                (Context.LAYOUT_INFLATER_SERVICE);
                        LinearLayout ll= (LinearLayout)inflater.inflate(R.layout.calendar_picker, null, false);
                        cv = (MaterialCalendarView) ll.getChildAt(0);

                        /*
                         *  Setto la modalità range
                         */

                        cv.setSelectionMode(MaterialCalendarView.SELECTION_MODE_RANGE);
                        cv.state().edit().setMinimumDate(Calendar.getInstance()).commit();
                        cv.removeDecorators();
                        /*
                         *  Applico i decoratori che disabiliteranno la selezione di date
                         *  già occupate da altri viaggi, risiedenti nella struttura dati
                         *  disabled_dates
                         */
                        cv.addDecorators(
                                new DisableDecorator(disabled_dates, FormViaggioActivity.this),
                                new CenterDisableDecorator(disabled_dates, FormViaggioActivity.this),
                                new LeftDisableDecorator(disabled_dates, FormViaggioActivity.this),
                                new RightDisableDecorator(disabled_dates, FormViaggioActivity.this)
                        );
                        cv.setTopbarVisible(true);

                        /*
                         *  Setto il listener della data selezionata (NON RANGE) che gestirà
                         *  a sua volta altri decoratori dello stesso tipo/classe utilizzati
                         *  precedentemente al caricamento del dialog
                         */

                        cv.setOnDateChangedListener(new OnDateSelectedListener() {
                            @Override
                            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                                dateOfTheTravel = dateFormatter.format(date.getDate().getTime()) + " to " + dateFormatter.format(date.getDate().getTime());
                                toTravelDate = new Date(date.getDate().getTime());
                                fromTravelDate = new Date(date.getDate().getTime());
                                widget.removeDecorators();
                                widget.addDecorators(
                                        new DisableDecorator(disabled_dates, FormViaggioActivity.this),
                                        new CenterDisableDecorator(disabled_dates, FormViaggioActivity.this),
                                        new LeftDisableDecorator(disabled_dates, FormViaggioActivity.this),
                                        new RightDisableDecorator(disabled_dates, FormViaggioActivity.this)
                                );
                                widget.invalidateDecorators();
                            }
                        });

                        /*
                         *  Setto il listener del range selezionato e badando questa volta, oltre
                         *  che a richiamare i decoratori, anche ad impedire l'accavallamento
                         *  o meglio, l'intersezione delle due liste che rappresentano
                         *  rispettivamente le date disabilitate già occupate e le date selezionate
                         *  come range e passate in questo listener.
                         */

                        cv.setOnRangeSelectedListener(new OnRangeSelectedListener() {
                            @Override public void onRangeSelected(@NonNull MaterialCalendarView widget, @NonNull List<CalendarDay> dates)
                            {
                                toTravelDate = new Date(dates.get(dates.size()-1).getDate().getTime());
                                fromTravelDate = new Date(dates.get(0).getDate().getTime());
                                cal.setTime(fromTravelDate);
                                List<Date> matchList = new ArrayList<Date>();
                                while (cal.getTime().before(toTravelDate) || cal.getTime().equals(toTravelDate)) {
                                    matchList.add(cal.getTime());
                                    cal.add(Calendar.DATE, 1);
                                }
                                widget.removeDecorators();
                                List<Date> commonMatch = new ArrayList<Date>(matchList);
                                /*if(matchList.contains(disabled_dates.get(0))
                                        || matchList.contains(disabled_dates.get(disabled_dates.size()-1)))*/
                                /*if(commonMatch.retainAll(disabled_dates) && disabled_dates.size()>0){*/
                                if(!Collections.disjoint(commonMatch,disabled_dates)){
                                    widget.clearSelection();
                                    widget.setSelectedDate(dates.get(0));
                                    dateOfTheTravel = dateFormatter.format(dates.get(0).getDate().getTime()) + " to " + dateFormatter.format(dates.get(0).getDate().getTime());
                                    toTravelDate = new Date(dates.get(0).getDate().getTime());
                                    fromTravelDate = new Date(dates.get(0).getDate().getTime());
                                    widget.setOnDateChangedListener(new OnDateSelectedListener() {
                                        @Override
                                        public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                                            dateOfTheTravel = dateFormatter.format(date.getDate().getTime()) + " to " + dateFormatter.format(date.getDate().getTime());
                                            toTravelDate = new Date(date.getDate().getTime());
                                            fromTravelDate = new Date(date.getDate().getTime());
                                            widget.removeDecorators();
                                            widget.addDecorators(
                                                    new DisableDecorator(disabled_dates, FormViaggioActivity.this),
                                                    new CenterDisableDecorator(disabled_dates, FormViaggioActivity.this),
                                                    new LeftDisableDecorator(disabled_dates, FormViaggioActivity.this),
                                                    new RightDisableDecorator(disabled_dates, FormViaggioActivity.this)
                                            );
                                            widget.invalidateDecorators();
                                        }
                                    });
                                    List<CalendarDay> emptyList = new ArrayList<CalendarDay>(0);
                                    widget.addDecorators(
                                            new LeftDecorator(disabled_dates, emptyList, FormViaggioActivity.this),
                                            new CenterDecorator(disabled_dates, emptyList, FormViaggioActivity.this),
                                            new RightDecorator(disabled_dates, emptyList, FormViaggioActivity.this),
                                            new DisableDecorator(disabled_dates, FormViaggioActivity.this),
                                            new CenterDisableDecorator(disabled_dates, FormViaggioActivity.this),
                                            new LeftDisableDecorator(disabled_dates, FormViaggioActivity.this),
                                            new RightDisableDecorator(disabled_dates, FormViaggioActivity.this)
                                    );
                                }else if(disabled_dates.size()==0){
                                    dateOfTheTravel = dateFormatter.format(dates.get(0).getDate().getTime()) + " to " + dateFormatter.format(dates.get(dates.size() - 1).getDate().getTime());
                                    toTravelDate = new Date(dates.get(dates.size() - 1).getDate().getTime());
                                    fromTravelDate = new Date(dates.get(0).getDate().getTime());
                                    widget.addDecorators(
                                            new LeftDecorator(disabled_dates, dates, FormViaggioActivity.this),
                                            new CenterDecorator(disabled_dates, dates, FormViaggioActivity.this),
                                            new RightDecorator(disabled_dates, dates, FormViaggioActivity.this),
                                            new DisableDecorator(disabled_dates, FormViaggioActivity.this),
                                            new CenterDisableDecorator(disabled_dates, FormViaggioActivity.this),
                                            new LeftDisableDecorator(disabled_dates, FormViaggioActivity.this),
                                            new RightDisableDecorator(disabled_dates, FormViaggioActivity.this)
                                    );
                                }else{
                                    dateOfTheTravel = dateFormatter.format(dates.get(0).getDate().getTime()) + " to " + dateFormatter.format(dates.get(dates.size() - 1).getDate().getTime());
                                    toTravelDate = new Date(dates.get(dates.size() - 1).getDate().getTime());
                                    fromTravelDate = new Date(dates.get(0).getDate().getTime());
                                    widget.addDecorators(
                                            new LeftDecorator(disabled_dates, dates, FormViaggioActivity.this),
                                            new CenterDecorator(disabled_dates, dates, FormViaggioActivity.this),
                                            new RightDecorator(disabled_dates, dates, FormViaggioActivity.this),
                                            new DisableDecorator(disabled_dates, FormViaggioActivity.this),
                                            new CenterDisableDecorator(disabled_dates, FormViaggioActivity.this),
                                            new LeftDisableDecorator(disabled_dates, FormViaggioActivity.this),
                                            new RightDisableDecorator(disabled_dates, FormViaggioActivity.this)
                                    );
                                }
                                widget.invalidateDecorators();
                            }
                        });

                        /*
                         *  Setto infine il titolo del dialog di selezione data
                         */

                        new AlertDialog.Builder(FormViaggioActivity.this)
                                //TODO: TRADURRE CON STRINGHE MULTILINGUA
                                .setTitle("Partenza & arrivo")
                                .setMessage("Select the departure and arrival dates")
                                .setView(ll)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        fromDate.setText(dateOfTheTravel);
                                    }
                                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        // Do nothing.
                                    }
                                }
                        ).show();
                    }
                }
        );

    }

    private void findViewsById() {
        //spinner_mezzo_di_trasporto = (Spinner) findViewById(R.id.spinner_mezzo_di_trasporto);
        //spinner_spazio= (Spinner) findViewById(R.id.spinner_spazio_disponibile);

        fromDate = (EditText) findViewById(R.id.from_date);
        fromDate.setInputType(InputType.TYPE_NULL);
        fromDate.requestFocus();
    }

    private void setDateTimeField() {
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromDate.setText(dateFormatter.format(newDate.getTime()));
            }
        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0){
            return false;
        }
        return true;
    }

    public void onContinue(View view) {
        if(travel_id != null){
            if (isEmpty(fromDate)) {
                AlertDialog.Builder miaAlert = new AlertDialog.Builder(this);
                miaAlert.setTitle(getString(R.string.alert_attention));
                miaAlert.setMessage(getString(R.string.alert_data_missing));
                AlertDialog alert = miaAlert.create();
                alert.show();
                return;
            } else {
                Travel.load(user_id,travel_id,(Travel travel) -> {
                    if(travel != null){
                        Log.i("TRAVEL_COMPL","TRAVEL_FOUND");
                        travel.setArrival(new Location(b.getString("to_id"),b.getDouble("to_lat"),b.getDouble("to_lon")), (completion) -> {
                        });
                        travel.setDeparture(new Location(b.getString("from_id"),b.getDouble("from_lat"),b.getDouble("from_lon")), (completion) -> {
                        });
                        //TODO: implementare settaggio del mezzo senza aggiungerne altri alla lista <Transport>
                        String fromDateString = fromDate.getText().toString().split(" to ")[0];
                        String toDateString = fromDate.getText().toString().split(" to ")[1];
                        Date fromDate = fromTravelDate; // GregorianCalendar(Integer.parseInt(fromDateString.split("-")[2]), Integer.parseInt(fromDateString.split("-")[1]), Integer.parseInt(fromDateString.split("-")[0])).getTime();
                        Date toDate = toTravelDate; //new GregorianCalendar(Integer.parseInt(toDateString.split("-")[2]), Integer.parseInt(toDateString.split("-")[1]), Integer.parseInt(toDateString.split("-")[0])).getTime();
                        travel.setStartDate(fromDate,(completion)->{

                        });
                        travel.setEndDate(toDate,(completion)->{

                        });
                        final int id2 = spaceGroup.getCheckedRadioButtonId();
                        double SIZE = 5.0;
                        switch(id2){
                            case R.id.travel_s:
                                SIZE = 1.00;
                                break;
                            case R.id.travel_m:
                                SIZE = 2.00;
                                break;
                            case R.id.travel_l:
                                SIZE = 3.00;
                                break;
                        }
                        travel.setSpace(SIZE,(completion)->{

                        });
                        Set<Transport> transports = new HashSet<Transport>();
                        final int id3 = vehicleGroup.getCheckedRadioButtonId();
                        switch(id3){
                            case R.id.car:
                                transports.add(Transport.CAR);
                                break;
                            case R.id.train:
                                transports.add(Transport.TRAIN);
                                break;
                            case R.id.bus:
                                transports.add(Transport.BUS);
                                break;
                            case R.id.plane:
                                transports.add(Transport.AIRPLANE);
                                break;
                        }
                    }else{
                        Log.e("TRAVEL_COMPL","TRAVEL_NOT_FOUND");
                    }
                },false);
                Intent startNewActivity = new Intent(this, MainActivity.class);
                finishAffinity();
                startActivity(startNewActivity);
                TabLayout.Tab tab = MainActivity.tabLayout.getTabAt(1);
                tab.select();
            }
        }else {
            String fromDateString = fromDate.getText().toString().split(" to ")[0];
            String toDateString = fromDate.getText().toString().split(" to ")[1];
            G.attributiViaggio.string_inizio_viaggio = fromDateString.split("-")[0] + "/" + fromDateString.split("-")[1] + "/" + fromDateString.split("-")[2];
            G.attributiViaggio.string_fine_viaggio = toDateString.split("-")[0] + "/" + toDateString.split("-")[1] + "/" + toDateString.split("-")[2];
            G.attributiViaggio.inizio_viaggio = fromTravelDate;
            G.attributiViaggio.fine_viaggio = toTravelDate;
            final int id2 = spaceGroup.getCheckedRadioButtonId();
            String SIZE_TO_CREATE = "?";
            switch(id2){
                case R.id.travel_s:
                    SIZE_TO_CREATE = "S";
                case R.id.travel_m:
                    SIZE_TO_CREATE = "M";
                    break;
                case R.id.travel_l:
                    SIZE_TO_CREATE = "L";
                    break;
            }
            final int id = vehicleGroup.getCheckedRadioButtonId();
            String VEHICLE_TO_CREATE = "?";
            switch(id){
                case R.id.car:
                    VEHICLE_TO_CREATE = "CAR";
                    break;
                case R.id.train:
                    VEHICLE_TO_CREATE = "TRAIN";
                    break;
                case R.id.bus:
                    VEHICLE_TO_CREATE = "BUS";
                    break;
                case R.id.plane:
                    VEHICLE_TO_CREATE = "PLANE";
                    break;
            }
            G.attributiViaggio.spazio_disponibile = SIZE_TO_CREATE;
            G.attributiViaggio.trasporto = VEHICLE_TO_CREATE;
            startActivity(RiepilogoViaggioActivity.class);
        }
    }

    public void startActivity(Class cls) {
        Intent startNewActivity = new Intent(this, cls);
        startActivity(startNewActivity);
    }
}