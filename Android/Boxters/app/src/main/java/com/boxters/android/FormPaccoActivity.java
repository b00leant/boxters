package com.boxters.android;

import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.boxters.android.controller.messaging.MessageSender;
import com.boxters.model.AuthorizedBox;
import com.boxters.model.Box;
import com.boxters.model.Location;
import com.boxters.model.PublicUser;
import com.boxters.model.User;
import com.boxters.model.messaging.RequestToBeReceiverPayload;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class FormPaccoActivity extends AppCompatActivity implements View.OnClickListener{

    public Global G = Global.getInstance();
    private Box boxAttuale;
    private Bundle b;
    private Location fromLocation;
    private Bitmap imageBitmap;
    private Location toLocation;
    public EditText edit_nome_pacco, edit_descrizione_pacco;
    public AutoCompleteTextView edit_email_dest;
    DatePicker picker_scadenza_pacco;
    private DatePickerDialog expirationDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    private EditText expirationDate;
    Button camera_button;
    private ImageView preview;
    private RadioGroup sizeGroup;
    private RadioGroup weightGroup;
    private String from_id;
    private String to_id;
    private String box_id;
    private Date expirationBoxDate;
    private String travel_id;
    private double from_lat,from_lon;
    private double to_lat,to_lon;
    private String user_id;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_CAMERA = 1;

    private void findViewsById() {
        expirationDate = (EditText) findViewById(R.id.expiration_date);
        expirationDate.setInputType(InputType.TYPE_NULL);
        camera_button = (Button) findViewById(R.id.camera);
    }

    private void setDateTimeField() {
        expirationDate.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        expirationDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                view.setMinDate(newDate.getTimeInMillis());
                newDate.set(year, monthOfYear, dayOfMonth);
                expirationBoxDate = new Date(newDate.getTime().getTime());
                expirationDate.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    public void seekbarr( ){
        //seek_bar = (SeekBar)findViewById(R.id.seekBar);

        //seek_bar.setMax(15);

        //text_view =(TextView)findViewById(R.id.kmProgress);
        //text_view.setText("KM : " + seek_bar.getProgress() + " / " +seek_bar.getMax());

        /*
        seek_bar.setOnSeekBarChangeListener(

                new SeekBar.OnSeekBarChangeListener() {

                    int progress_value;
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        progress_value = progress;
                        text_view.setText("KM : " + progress + " / " +seek_bar.getMax());

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        text_view.setText("KM : " + progress_value + " / " +seek_bar.getMax());
                    }
                }
        );
        */

    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    private void dispatchTakePictureIntent() {
        // Check permission for CAMERA
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            // Callback onRequestPermissionsResult interceptado na Activity MainActivity
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.CAMERA},
                    REQUEST_CAMERA);
        } else {
            // permission has been granted, continue as usual

            Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(captureIntent, REQUEST_CAMERA);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {
            Log.i("FOTOOO","FOTO_SCATTATA!!!!");

            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            if(imageBitmap != null){
                Log.i("FOTOOO",imageBitmap.toString());
                preview.setImageBitmap(imageBitmap);
            }else{
                imageBitmap = null;
            }

        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
        setContentView(R.layout.activity_form_pacco);
        preview = (ImageView) findViewById(R.id.preview_image);
        preview.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        sizeGroup = (RadioGroup) findViewById(R.id.box_sizes);
        weightGroup = (RadioGroup) findViewById(R.id.box_weight);
        toolbar.setTitle(getString(R.string.Compile_Box));
        setSupportActionBar(toolbar);

        seekbarr();
        findViewsById();
        setDateTimeField();

        camera_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dispatchTakePictureIntent();
                } catch(Exception e) {
                    Toast.makeText(FormPaccoActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });
        edit_nome_pacco = (EditText) findViewById(R.id.edit_nome_pacco);
        edit_descrizione_pacco = (EditText) findViewById(R.id.edit_descrizione_pacco);
        edit_email_dest = (AutoCompleteTextView) findViewById(R.id.edit_email_dest);

        //spinner_peso_pacco = (Spinner) findViewById(R.id.spinner_peso_pacco);
        //spinner_spazio_pacco = (Spinner) findViewById(R.id.spinner_spazio_pacco);





        b = getIntent().getExtras();
        if(b != null){
            from_id = b.getString("from_id");
            to_id = b.getString("to_id");
            box_id = b.getString("box_id");
            travel_id = b.getString("travel_id");
            from_lat = b.getDouble("from_lat");
            from_lon = b.getDouble("from_lon");
            to_lat = b.getDouble("to_lat");
            to_lon = b.getDouble("to_lon");
            user_id = b.getString("user_id");
            if(b.getString("image_bitmap") == null){

            }else{
                imageBitmap = convert2Bitmap(b.getString("image_bitmap"));
                if(imageBitmap!=null){
                    preview.setImageBitmap(imageBitmap);
                }
            }

            FirebaseAuth auth = FirebaseAuth.getInstance();
            FirebaseUser user = auth.getCurrentUser();

            if(box_id != null){
                if (user != null) {
                    Log.d("LOGIN","ALREADY SIGNED IN");
                    User.load(user.getUid(),(User _user) -> {
                        if(_user != null){
                            //non capiterà mai di non essere loggati in quest'actvity
                            Box.load(_user.getId(),box_id,(Box box) -> {
                                if(box.getReceiver() != null){
                                    User.load(box.getReceiver(),(User receiver) ->{
                                        if(receiver != null){
                                            edit_email_dest.setText(receiver.getEmail());
                                        }
                                    },false);
                                }
                                if(box.getPhoto() != null || !box.getPhoto().isEmpty()){
                                    imageBitmap = convert2Bitmap(box.getPhoto());
                                    if(imageBitmap!=null){
                                        preview.setImageBitmap(imageBitmap);
                                    }
                                }
                            },false);
                        }
                    },false);
                }
            }

            fromLocation = new Location(from_id,from_lat,from_lon);
            toLocation = new Location(to_id,to_lat,to_lon);
            System.out.println("---------------------------------");
            System.out.println("from_id: "+from_id);
            System.out.println("from_lat: "+from_lat+" from_lon: "+from_lon);
            System.out.println("from_id: "+to_lon);
            System.out.println("to_lat: "+to_lat+" to_lon: "+to_lon);
            System.out.println("---------------------------------");
            Box.load(user_id,box_id,(Box box)->{
                if(box != null){
                    expirationBoxDate = box.getExpiration();
                    expirationDate.setText(dateFormatter.format(box.getExpiration()));
                    edit_descrizione_pacco.setText(box.getDescription());

                    //TODO: inserire il testo della mail del receiver.

                    edit_nome_pacco.setText(box.getObject());
                    // GESTIONE PESO CON NUOVO RADIOGROUP
                    /*
                    if(box.getWeight() <= 1.0){
                        String compareValue = getResources().getStringArray(R.array.peso)[0];
                        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.peso, android.R.layout.simple_spinner_item);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_peso_pacco.setAdapter(adapter);
                        if (!compareValue.equals(null)) {
                            int spinnerPosition = adapter.getPosition(compareValue);
                            spinner_peso_pacco.setSelection(spinnerPosition);
                        }
                    }
                    else if(box.getWeight() <= 2.0){
                        String compareValue = getResources().getStringArray(R.array.peso)[1];
                        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.peso, android.R.layout.simple_spinner_item);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_peso_pacco.setAdapter(adapter);
                        if (!compareValue.equals(null)) {
                            int spinnerPosition = adapter.getPosition(compareValue);
                            spinner_peso_pacco.setSelection(spinnerPosition);
                        }
                    }
                    else if(box.getWeight() <= 3.0){
                        String compareValue = getResources().getStringArray(R.array.peso)[2];
                        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.peso, android.R.layout.simple_spinner_item);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_peso_pacco.setAdapter(adapter);
                        if (!compareValue.equals(null)) {
                            int spinnerPosition = adapter.getPosition(compareValue);
                            spinner_peso_pacco.setSelection(spinnerPosition);
                        }
                    }else if(box.getWeight() <= 4.0) {
                        String compareValue = getResources().getStringArray(R.array.peso)[3];
                        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.peso, android.R.layout.simple_spinner_item);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_peso_pacco.setAdapter(adapter);
                        if (!compareValue.equals(null)) {
                            int spinnerPosition = adapter.getPosition(compareValue);
                            spinner_peso_pacco.setSelection(spinnerPosition);
                        }
                    }*/

                    double s = 1.0;
                    double m = 2.0;
                    double l = 3.0;

                    /*
                    if(box.getSize() <= 1.0){
                        System.out.println("LO SPAZIO È STATO SETTATO S");
                        String compareValue = getResources().getStringArray(R.array.spazio)[0];
                        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.peso, android.R.layout.simple_spinner_item);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_peso_pacco.setAdapter(adapter);
                        if (!compareValue.equals(null)) {
                            int spinnerPosition = adapter.getPosition(compareValue);
                            spinner_peso_pacco.setSelection(spinnerPosition);
                        }
                    }
                    else if(box.getSize() <= 2.0){
                        System.out.println("LO SPAZIO È STATO SETTATO M");
                        String compareValue = getResources().getStringArray(R.array.spazio)[1];
                        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.peso, android.R.layout.simple_spinner_item);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_peso_pacco.setAdapter(adapter);
                        if (!compareValue.equals(null)) {
                            int spinnerPosition = adapter.getPosition(compareValue);
                            spinner_peso_pacco.setSelection(spinnerPosition);
                        }
                    }
                    if(box.getSize() <= 3.0){
                        System.out.println("LO SPAZIO È STATO SETTATO L");
                        String compareValue = getResources().getStringArray(R.array.spazio)[2];
                        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.peso, android.R.layout.simple_spinner_item);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_peso_pacco.setAdapter(adapter);
                        if (!compareValue.equals(null)) {
                            int spinnerPosition = adapter.getPosition(compareValue);
                            spinner_peso_pacco.setSelection(spinnerPosition);
                        }
                    }
                    */
                }
            },false);
        }
    }


    public void onContinue(View view) {
        if(box_id != null){
            if (isEmpty(edit_nome_pacco) || isEmpty(edit_descrizione_pacco) || isEmpty(edit_email_dest) || isEmpty(expirationDate)) {

                AlertDialog.Builder miaAlert = new AlertDialog.Builder(this);
                miaAlert.setTitle(getResString(R.string.alert_attention));
                miaAlert.setMessage(getResString(R.string.alert_data_missing));
                AlertDialog alert = miaAlert.create();
                alert.show();
                return;
            } else {

                Box.load(user_id,box_id,(Box box)-> {
                    if (box != null) {
                        double WEIGHT = 1.00;
                        final int id = weightGroup.getCheckedRadioButtonId();
                        switch(id){
                            case R.id.weight1:
                                WEIGHT = 1.00;
                                break;
                            case R.id.weight2:
                                WEIGHT = 2.00;
                                break;
                            case R.id.weight3:
                                WEIGHT = 3.00;
                                break;
                            case R.id.weight4:
                                WEIGHT = 4.00;
                                break;
                        }
                        double SIZE = 5.0;
                        final int id2 = sizeGroup.getCheckedRadioButtonId();
                        switch(id2){
                            case R.id.box_s:
                                SIZE = 1.00;
                                break;
                            case R.id.box_m:
                                SIZE = 2.00;
                                break;
                            case R.id.box_l:
                                SIZE = 3.00;
                                break;
                        }
                        /*
                        switch (spinner_peso_pacco.getSelectedItem().toString()) {
                            case "Less than 1Kg":
                                WEIGHT = 1.00;
                                break;
                            case "Meno di 1Kg":
                                WEIGHT = 1.00;
                                break;
                            case "1 - 5Kg":
                                WEIGHT = 2.00;
                                break;
                            case "5 - 10Kg":
                                WEIGHT = 3.00;
                                break;
                            case "More than 10Kg":
                                WEIGHT = 4.00;
                                break;
                            case "Più di 10Kg":
                                WEIGHT = 4.00;
                                break;
                        }*/
                        /*
                        double SIZE = 5.0;
                        switch (spinner_spazio_pacco.getSelectedItem().toString()) {
                            case "S (1 - 10 cm³)":
                                SIZE = 1.0;
                                break;
                            case "M (10 - 50 cm³)":
                                SIZE = 2.0;
                                break;
                            case "L (50+ cm³)":
                                SIZE = 3.0;
                                break;
                        }*/
                        box.setArrival(new Location(b.getString("to_id"),
                                b.getDouble("to_lat"),
                                b.getDouble("to_lon")), (completion) -> {

                        });
                        box.setDeparture(new Location(b.getString("from_id"),
                                b.getDouble("from_lat"),
                                b.getDouble("from_lon")), (completion) -> {

                        });
                        box.setDescription(edit_descrizione_pacco.getText().toString(), (completion) -> {

                        });
                        box.setExpiration(expirationBoxDate,
                                (completion) -> {

                                });
                        /*
                        String expiration_edit = expirationDate.getText().toString();

                        box.setExpiration(
                                new GregorianCalendar(Integer.parseInt(expiration_edit.split("-")[2]),
                                        Integer.parseInt(expiration_edit.split("-")[1]),
                                        Integer.parseInt(expiration_edit.split("-")[0])).getTime(),
                                (completion) -> {

                                });
                        */
                        box.setObject(edit_nome_pacco.getText().toString(), (completion) -> {

                        });
                        box.setSize(SIZE,(completion)->{

                        });
                        box.setWeight(WEIGHT,(completion)->{

                        });
                        if(imageBitmap != null){
                            box.setPhoto(convert2String(imageBitmap),(c)->{

                            });
                        }
                        Intent startNewActivity = new Intent( this,MainActivity.class);
                        User.loadBasicUserByEmail(edit_email_dest.getText().toString(),(user) -> {
                            if(user != null){
                                PublicUser.load(user.getId(),(publicUser) -> {
                                    MessageSender.send(new RequestToBeReceiverPayload(MainActivity.user.getId(),publicUser.getMessagingToken(), box.getId()));
                                    finishAffinity();
                                    startActivity(startNewActivity);
                                },false);
                            }else{
                                Toast.makeText(FormPaccoActivity.this, getString(R.string.error_receiver_mail),Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                },false);
            }
        }else{
            if (isEmpty(edit_nome_pacco) || imageBitmap == null ||isEmpty(edit_descrizione_pacco) || isEmpty(edit_email_dest) || isEmpty(expirationDate)) {

                AlertDialog.Builder miaAlert = new AlertDialog.Builder(this);
                miaAlert.setTitle(getResString(R.string.alert_attention));
                miaAlert.setMessage(getResString(R.string.alert_data_missing));
                AlertDialog alert = miaAlert.create();
                alert.show();
                return;
            } else { // superato test, metto gli elementi nel singleton G
                String expiration = expirationDate.getText().toString();
                G.attributiPacco.nome_pacco = edit_nome_pacco.getText().toString();
                G.attributiPacco.descrizione_pacco = edit_descrizione_pacco.getText().toString();
                G.attributiPacco.email_dest = edit_email_dest.getText().toString();
                final int id = weightGroup.getCheckedRadioButtonId();
                String WEIGHT_BOX_CREATION = "1";
                switch(id){
                    case R.id.weight1:
                        WEIGHT_BOX_CREATION = "1";
                        break;
                    case R.id.weight2:
                        WEIGHT_BOX_CREATION = "2";
                        break;
                    case R.id.weight3:
                        WEIGHT_BOX_CREATION = "3";
                        break;
                    case R.id.weight4:
                        WEIGHT_BOX_CREATION = "4";
                        break;
                }
                String SIZE_BOX_CREATION = "1";
                final int id2 = sizeGroup.getCheckedRadioButtonId();
                switch(id2){
                    case R.id.box_s:
                        SIZE_BOX_CREATION = "S";
                        break;
                    case R.id.box_m:
                        SIZE_BOX_CREATION = "M";
                        break;
                    case R.id.box_l:
                        SIZE_BOX_CREATION = "L";
                        break;
                }
                G.attributiPacco.peso_pacco = WEIGHT_BOX_CREATION;
                G.attributiPacco.spazio_pacco = SIZE_BOX_CREATION;
                G.attributiPacco.string_scadenza_pacco = expiration.split("-")[0] + "/" + expiration.split("-")[1] + "/" + expiration.split("-")[2];
                G.attributiPacco.scadenza_pacco = expirationBoxDate; //new GregorianCalendar(Integer.parseInt(expiration.split("-")[2]),Integer.parseInt(expiration.split("-")[1]),Integer.parseInt(expiration.split("-")[0])).getTime();
                Bundle bundle2 = new Bundle();
                if(imageBitmap != null){
                    bundle2.putString("image_bitmap",convert2String(imageBitmap));
                }else{
                    bundle2.putString("image_bitmap",null);
                }
                Intent startNewActivity = new Intent(this, RiepilogoPaccoActivity.class);
                startNewActivity.putExtras(bundle2);
                startActivity(startNewActivity);
            }
        }
    }


    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;

        return true;
    }


    public void startActivity(Class cls) {
        Intent startNewActivity = new Intent(this, cls);
        startActivity(startNewActivity);
    }
    public String getResString(int id) {
        return (String) getResources().getText(id);
    }

    @Override
    public void onClick(View view) {
        if(view == expirationDate) {
            expirationDatePickerDialog.show();
        }
    }
    public Bitmap convert2Bitmap(String base64Str) throws IllegalArgumentException
    {
        byte[] decodedBytes = Base64.decode(
                base64Str.substring(base64Str.indexOf(",")  + 1),
                Base64.DEFAULT
        );

        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    public String convert2String(Bitmap bitmap)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }
}
