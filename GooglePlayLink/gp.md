<h1 align="center">Ladies and gentlemen, Boxters!</h1>
<table><tr><td>
<iframe width="560" height="315" src="https://www.youtube.com/embed/-HjFGhc2DsA?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
</td></tr></table>
<br><br>
<h1 align="center">Want to be a beta tester?</h1>
<div align="center">
Join the crew, fill <a target="_blank" href="https://docs.google.com/forms/d/e/1FAIpQLSeIOgNP1b_IYjGGNem8Ta8V-gjMR9ifCvesfUtpdj_h-m2RDw/viewform?usp=sf_link">the form</a> and you'll receive an invitation to the BETA download
</div>
<table>
  <tr>
    <td>
      <a target="_blank" href="https://play.google.com/apps/testing/com.boxters.android"><img width="30%" height="auto" src="gpicon.png"/></a>
    </td>
  </tr>
</table>

