package com.boxters.model;

import java.util.Random;

/**
 * Created by daniele on 22/02/17.
 *
 * Implementa il pattern singleton con alcune particolarità per questioni di comodità.
 * é stateless, quindi non comporta i problemi associati in genere all'utilizzo di singleton
 */

public class RandomString {

    private static final char[] symbols;
    private static final Random random = new Random();

    static {
        StringBuilder tmp = new StringBuilder();
        for (char ch = '0'; ch <= '9'; ++ch)
            tmp.append(ch);
        for (char ch = 'a'; ch <= 'z'; ++ch)
            tmp.append(ch);
        symbols = tmp.toString().toCharArray();
    }

    public static String next(int length) {
        char[] buf = new char[length];
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = symbols[random.nextInt(symbols.length)];
        return new String(buf);
    }
}