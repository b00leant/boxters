package com.boxters.model;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * Created by undeclinable on 12/04/17.
 */

public class CountDown {
    private static Calendar cal;

    public static int DaysLeft(Date date) {
        int i = 0;
        cal = Calendar.getInstance();
        while(cal.getTime().before(date)){
            i++;
            cal.add(Calendar.DATE, 1);
        }
        return i;
    }
}