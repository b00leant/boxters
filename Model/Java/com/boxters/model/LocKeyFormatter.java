package com.boxters.model;

import org.jetbrains.annotations.NotNull;

/**
 * Created by daniele on 07/02/17.
 */

abstract class LocKeyFormatter {

    @NotNull
    static String formatDoubleToKey(double value) {
        return String.valueOf(value).replace('.', '_');
    }

    static double parseKeyToDouble(String value) {
        return Double.parseDouble(value.replace('_', '.'));
    }

}
