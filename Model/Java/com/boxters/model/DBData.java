package com.boxters.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.boxters.model.completion.Completion;
import com.boxters.model.completion.SaveCompletion;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by daniele on 29/01/17.

Descrizione del design pattern utilizzato:
 - per creare un oggetto di una classe T si usa T.create(attributiPacco, completionHandler, autoUpdate)
    ove completionHandler è una lambda che prende come parametro l'oggetto T appena creato
    e viene chiamata a creazione ultimata, autoUpdate indica se l'oggetto deve auto aggiornarsi
 - per caricare un oggetto di una classe T dal server si usa
    T.load(identificatori, completionHandler, autoUpdate) (analogo a create)
 - gli attributiPacco di un oggetto vengono acceduti e modificati attraverso getter e setter;
    i setter salvano il risultato in locale, aggiornano il server ed è possibile specificare
    un handler chiamato a fine salvataggio
 - per aggiungere funzioni (o lambda) chiamate ogni volta un oggetto o viene aggiornato,
    si usa la funzione o.onUpdate(funzione)

 note:
 - il synchronized è utilizzato nei getter nei setter e in parse per prevenire race conditions
    nell'aggiornamento concorrente dello stesso oggetto nel client e nel server e per non
    richiedere proprietà di un oggetto durante un aggiornamento
*/

abstract class DBData {

    @NotNull
    final String id;
    @NotNull
    ArrayList<Completion<DBData>> onUpdate = new ArrayList<>();

    boolean autoUpdate = false;

    DBData(@NotNull String id) {
        this.id = id;
    }

    void update(Map<String, Object> updates, @Nullable SaveCompletion completion) {
        DatabaseReference db = DataService.root;
        db.updateChildren(updates, (error, ref) -> {
            if(error != null) {
                error.toException().printStackTrace();
            }

            if(completion != null) completion.run(error == null);
        });
    }

    final void save(@Nullable SaveCompletion completion) {
        Map<String, Object> map = new HashMap<>();
        map.put(getTable(), toMap());
        update(map, completion);
        /*
        Task<Void> t = db.child(getTable()).setValue(toMap());
        t.addOnCompleteListener(task -> {
            if(!task.isSuccessful()) {
                if(task.getException() != null) {
                    task.getException().printStackTrace();
                }
            }

            if(completion != null) completion.run(task.isSuccessful());
        });*/
    }

    @NotNull
    abstract String getTable();
    @NotNull
    abstract Map<String, Object> toMap();

    abstract void parse(@NotNull DataSnapshot snapshot);

    void autoUpdate() {
        autoUpdate = true;

        DatabaseReference db = DataService.root;
        db.child(getTable()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null) {
                    parse(dataSnapshot);

                    for(Completion<DBData> handler: onUpdate) {
                        handler.run(DBData.this);
                    }
                }
                else {
                    for (Completion<DBData> handler : onUpdate) {
                        handler.run(null);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //todo: gestire più accuratamente l'errore
                databaseError.toException().printStackTrace();
            }
        });
    }

    @NotNull
    synchronized public String getId() { return id; }

    void delete(@Nullable SaveCompletion completion) {
        DataService.root.child(getTable()).removeValue((databaseError, databaseReference) -> {
            if(databaseError != null) {
                databaseError.toException().printStackTrace();
            }

            if(completion!= null) completion.run(databaseError != null);
        });
    }

}
