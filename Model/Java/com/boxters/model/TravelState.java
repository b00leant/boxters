package com.boxters.model;

/**
 * Created by daniele on 02/02/17.
 *
 * Gli stati che può assumere un viaggio:
 *  - CREATED : il boxter non è ancora partito
 *  - STARTED : il boxter è già partito e non può più prendere accordi
 */

public enum TravelState {
    CREATED, STARTED
}
