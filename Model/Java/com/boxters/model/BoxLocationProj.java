package com.boxters.model;

import com.firebase.geofire.GeoLocation;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

/**
 * Created by daniele on 02/02/17.
 */

class BoxLocationProj extends LocKeyFormatter {
    @NotNull private static final String LOC_KEY_SEPARATOR = "*";

    @NotNull private final String senderId;
    @NotNull private final String boxId;
    @NotNull private final Location arrival;
    @NotNull private final Location departure;
    @NotNull private final Date expiration;

    public BoxLocationProj(@NotNull String key, @NotNull GeoLocation location) {
        String[] args = key.split("\\" + LOC_KEY_SEPARATOR);
        if(args.length != 5) {
            throw new InternalError("Chiave errata in " + Box.BOXES_LOCATIONS_KEY);
        }

        senderId = args[0];
        boxId = args[1];
        arrival = new Location(location);
        departure = new Location(null, parseKeyToDouble(args[2]), parseKeyToDouble(args[3]));
        expiration = new Date(Long.parseLong(args[4]));
    }

    public static String key(@NotNull Box box) {
        return box.sender + LOC_KEY_SEPARATOR + box.id + LOC_KEY_SEPARATOR
                + formatDoubleToKey(box.departure.getLat()) + LOC_KEY_SEPARATOR
                + formatDoubleToKey(box.departure.getLon()) + LOC_KEY_SEPARATOR + box.expiration.getTime();
    }

    @NotNull
    public String getSenderId() {
        return senderId;
    }

    @NotNull
    public String getBoxId() {
        return boxId;
    }

    @NotNull
    public Location getArrival() {
        return arrival;
    }

    @NotNull
    public Location getDeparture() {
        return departure;
    }

    @NotNull
    public Date getExpiration() {
        return expiration;
    }
}
