package com.boxters.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.boxters.model.completion.Completion;
import com.boxters.model.completion.ReceiverCompletion;
import com.boxters.model.completion.SaveCompletion;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by daniele on 10/02/17.
 */

public class Receiver extends DBData {
    //region: inner workings -----------------------------------------------------------------------

    static final String STATE_KEY = "state";
    private static final String RECEIVERS_KEY = "recipients";

    private final Map<String, Map<String, BoxState>> boxes = new HashMap<>();

    private Receiver(@NotNull String id) {
        super(id);
    }

    @Contract(pure = true)
    static String getTable(@NotNull String id) {
        return RECEIVERS_KEY + "/" + id;
    }

    @NotNull
    @Override
    String getTable() {
        return getTable(id);
    }

    @Override @NotNull
    Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();

        for(Map.Entry<String, Map<String, BoxState>> sender: boxes.entrySet()) {

            Map<String, Map<String, String>> boxes = new HashMap<>();
            for(Map.Entry<String, BoxState> box: sender.getValue().entrySet()) {

                Map<String, String> stateMap = new HashMap<>();
                stateMap.put(STATE_KEY, box.getValue().name());

                boxes.put(box.getKey(), stateMap);
            }

            map.put(sender.getKey(), boxes);
        }

        return map;
    }

    @Override
    synchronized void parse(@NotNull DataSnapshot snapshot) {

        if(snapshot.hasChildren()) {
            for (DataSnapshot sender : snapshot.getChildren()) {

                Map<String, BoxState> senderBoxes = new HashMap<>();
                for (DataSnapshot box : sender.getChildren()) {

                    BoxState state = BoxState.valueOf((String) box.child("state").getValue());
                    senderBoxes.put(box.getKey(), state);
                }

                boxes.put(sender.getKey(), senderBoxes);
            }
        }
    }

    //endregion ------------------------------------------------------------------------------------

    class ReceiverBox {
        final String sender;
        final String id;
        final BoxState state;


        ReceiverBox(String sender, String id, BoxState state) {
            this.sender = sender;
            this.id = id;
            this.state = state;
        }
    }


    //region: public interface ---------------------------------------------------------------------

    /** Carica i dati identificativi dei pacchi che l'utente con id specificato sta ricevendo
     * L'utente deve essere loggato
     *
     * @param id l'id dell'utente da caricare
     * @param completion l'handler che viene chiamato a caricamento ultimato con argomento l'utente caricato
     * @param autoUpdate se l'istanza locale dell'utente creato dovrà aggiornare automaticamente
     *                   i propri campi quando questi vengono modificati sul server
     */
    public static void load(@NotNull String id, @NotNull ReceiverCompletion completion, boolean autoUpdate) {
        Utils.notNull(id);
        Utils.notNull(completion);

        DatabaseReference db = DataService.root;
        db.child(getTable(id)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Receiver user = new Receiver(id);
                if(dataSnapshot.getValue() != null) user.parse(dataSnapshot);
                if (autoUpdate) user.autoUpdate();
                completion.run(user);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException().printStackTrace();
                completion.run(null);
            }
        });
    }

    public List<ReceiverBox> boxes() {
        List<ReceiverBox> l = new ArrayList<>();
        for(Map.Entry<String, Map<String, BoxState>> s: boxes.entrySet()) {
            for(Map.Entry<String, BoxState> b: s.getValue().entrySet()) {
                l.add(new ReceiverBox(s.getKey(), b.getKey(), b.getValue()));
            }
        }
        return l;
    }

    public void onUpdate(ReceiverCompletion completion) {
        Utils.notNull(completion);

        onUpdate.add((data) -> completion.run((Receiver) data));
    }

    public void addBox(@NotNull String sender, @NotNull String boxId,
                       @Nullable SaveCompletion completion) {
        Utils.notNull(sender);
        Utils.notNull(boxId);

        DatabaseReference db = DataService.root;
        db.child(getTable()).child(sender + "/" + boxId + "/" + STATE_KEY)
                .setValue(BoxState.CREATED.name()).addOnCompleteListener(task -> {
                    if(!task.isSuccessful()) {
                        if(task.getException() != null) {
                            task.getException().printStackTrace();
                        }
                    }

                    if(completion != null) completion.run(task.isSuccessful());
                });
    }

    @Nullable
    synchronized public BoxState getState(@NotNull String sender, @NotNull String boxId) {
        Map<String, BoxState> senderBoxes = boxes.get(sender);

        if(senderBoxes == null) return null;
        return senderBoxes.get(boxId);
    }

    //il seguente suppress è dovuto che al fatto che non possiamo utilizzare i metodi indicati
    //in quanto la nostra minApi è la 18
    @SuppressWarnings("Java8CollectionsApi")
    synchronized public void setState(@NotNull String sender, @NotNull String boxId, @NotNull BoxState state,
                         @Nullable SaveCompletion completion) {
        Utils.notNull(sender);
        Utils.notNull(boxId);
        Utils.notNull(state);


        if(boxes.get(sender) == null) boxes.put(sender, new HashMap<>());

        Map<String, BoxState> senderBoxes = boxes.get(sender);

        BoxState oldState = senderBoxes.get(boxId);

        if(state == BoxState.TRAVELLING
                || (state == BoxState.CREATED && oldState != null && oldState != BoxState.CREATED)
                || (state == BoxState.CONFIRMED && oldState != BoxState.CREATED && oldState != BoxState.CONFIRMED)
                || (state == BoxState.RECEIVED && oldState != BoxState.CONFIRMED && oldState != BoxState.RECEIVED) ) {
            //il cambiamento di stato del pacco da parte del mittente non è valido
            throw new IllegalActionException("Operazione non consentita: si sta provando al passare dallo stato "
                    + oldState.name() + " allo stato " + state.name());
        }
        else {
            senderBoxes.put(boxId, state);
            save(completion);
        }

    }



    //endregion ------------------------------------------------------------------------------------
}
