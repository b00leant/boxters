package com.boxters.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.boxters.model.completion.ReceiverCompletion;
import com.boxters.model.completion.SaveCompletion;
import com.boxters.model.completion.UserCompletion;
import com.boxters.model.completion.BasicUserCompletion;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.Contract;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by daniele on 30/01/17.
 *
 * User è una classe che rappresenta un utente.
 * Contiene tutte le informazioni sull'utente e può essere utilizzata solo se l'utente è loggato,
 * a causa dei permessi per la privacy degli utenti.
 */

public class User extends PublicUser {
    //region: inner workings -----------------------------------------------------------------------

    private static final String PHONE_KEY = "phone";
    private static final String DESCRIPTION_KEY = "description";
    private static final String USERS_QUERIES_KEY = "usersPublic";

    @NotNull private String phone;
    @Nullable private String description;
    @Nullable private Receiver receiver; //campo lazy

    @NotNull private Set<String> authUsers = new HashSet<>();

    private User(@NotNull String id, @NotNull String email, @NotNull String name, String photo,
                 Double rating, @NotNull String phone, @Nullable String description,
                 @Nullable String messagingToken) {
        super(id, email, name, photo, rating, messagingToken);

        this.phone = phone;
        this.description = description;
    }

    private User(@NotNull String id) {
        super(id);
    }

    @Contract(pure = true)
    static String getTable(@NotNull String id) {
        return USERS_KEY + "/" + id;
    }

    @Override @NotNull
    String getTable() {
        return getTable(id);
    }

    @Override @NotNull
    Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();

        map.put(PHONE_KEY, phone);
        if(description != null) map.put(DESCRIPTION_KEY, description);

        Map<String, Object> users = new HashMap<>();
        for(String userId: authUsers) {
            users.put(userId, true);
        }
        if(users.isEmpty()) map.put(USERS_KEY, false);
        else map.put(USERS_KEY, users);

        map.put(PUBLIC_KEY, super.toMap());

        return map;
    }

    @Override
    synchronized void parse(@NotNull DataSnapshot snapshot) {

        this.phone = (String) snapshot.child(PHONE_KEY).getValue();
        if(snapshot.hasChild(DESCRIPTION_KEY)) {
            this.description = (String) snapshot.child(DESCRIPTION_KEY).getValue();
        }
        else description = null;

        authUsers.clear();
        for(DataSnapshot authUser: snapshot.child(USERS_KEY).getChildren()) {
            if((Boolean) authUser.getValue()) authUsers.add(authUser.getKey());
        }

        super.parse(snapshot.child(PUBLIC_KEY));
    }

    @Override
    void update(@NotNull Map<String, Object> updates, @Nullable SaveCompletion completion) {
        updates.put(USERS_QUERIES_KEY + "/" + id, new UserQueryResult(this).toMap());
        super.update(updates, completion);
    }

    //endregion ------------------------------------------------------------------------------------


    //region: public interface ---------------------------------------------------------------------

    /** Carica l'utente con id specificato
     * L'utente deve essere loggato
     *
     * @param id l'id dell'utente da caricare
     * @param completion l'handler che viene chiamato a caricamento ultimato con argomento l'utente caricato
     * @param autoUpdate se l'istanza locale dell'utente creato dovrà aggiornare automaticamente
     *                   i propri campi quando questi vengono modificati sul server
     */
    public static void load(@NotNull String id, @NotNull UserCompletion completion, boolean autoUpdate) {
        Utils.notNull(id);
        Utils.notNull(completion);

        DatabaseReference db = DataService.root;
        db.child(getTable(id)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null) {
                    User user = new User(id);
                    user.parse(dataSnapshot);
                    completion.run(user);
                    if (autoUpdate) user.autoUpdate();
                }
                else completion.run(null);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException().printStackTrace();
                completion.run(null);
            }
        });
    }

    /** Crea l'utente con i dati specificati
     * L'utente deve essere già loggato, e l'id specificato deve essere l'uid dell'utente
     *
     * @param id l'uid dell'utente
     * @param email l'email dell'utente
     * @param name il name dell'utente
     * @param photo la foto dell'utente (opzionale)
     * @param rating il rating attuale dell'utente
     * @param phone il telefono dell'utente
     * @param description la descrizione (facoltativa) dell'utente
     * @param completion l'handler che viene chiamato a caricamento ultimato con argomento l'utente creato
     * @param autoUpdate se l'istanza locale dell'utente creato dovrà aggiornare automaticamente
     *                   i propri campi quando questi vengono modificati sul server
     */
    public static void create(@NotNull String id, @NotNull String email, @NotNull String name,
                              @Nullable String photo, @Nullable Double rating, @NotNull String phone,
                              @Nullable String description, @Nullable String messagingToken,
                              @Nullable UserCompletion completion, boolean autoUpdate) {
        Utils.notNull(id);
        Utils.notNull(email);
        Utils.notNull(name);
        Utils.notNull(phone);

        User user = new User(id, email, name, photo, rating, phone, description, messagingToken);

        Map<String, Object> updates = new HashMap<>();
        updates.put(user.getTable(), user.toMap());
        updates.put(Receiver.getTable(id), false);

        user.update(updates, (res) -> {
            if(completion != null) completion.run(res ? user : null);
            if(autoUpdate) user.autoUpdate();
        });
    }

    public void onUpdate(UserCompletion completion) {
        Utils.notNull(completion);

        onUpdate.add((data) -> completion.run((User) data));
    }

    /** Cerca nel database l'utente con l'email specificata, e ritorna un oggetto BasicUser a rappresentarlo
     *
     * @param email l'email dell'utente
     * @param completion la funzione chiamata una volta scaricato l'utente
     */
    public static void loadBasicUserByEmail(@NotNull String email, @NotNull BasicUserCompletion completion) {
        Utils.notNull(email);
        Utils.notNull(completion);

        DatabaseReference db = DataService.root.child(USERS_QUERIES_KEY);

        Query query = db.orderByChild(EMAIL_KEY).equalTo(email).limitToFirst(1);
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(!dataSnapshot.exists()) completion.run(null);
                else {
                    BasicUser user = new UserQueryResult(dataSnapshot);
                    completion.run(user);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException().printStackTrace();
                completion.run(null);
            }
        }); /*new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/
    }

    /** Cerca nel database gli utenti con il nome specificato, e ritorna un oggetto BasicUser a rappresentarlo
     *
     * @param name il nome dell'utente
     * @param completion la funzione chiamata una volta scaricato l'utente
     */
    public static void loadBasicUserByName(@NotNull String name, @NotNull BasicUserCompletion completion) {
        Utils.notNull(name);
        Utils.notNull(completion);

        DatabaseReference db = DataService.root.child(USERS_QUERIES_KEY);

        Query query = db.orderByChild(NAME_KEY).equalTo(name);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()) completion.run(null);
                else {
                    BasicUser user = new UserQueryResult(dataSnapshot);
                    completion.run(user);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException().printStackTrace();
                completion.run(null);
            }
        });
    }

    synchronized public void getReceiver(ReceiverCompletion completion) {
        if(receiver != null) completion.run(receiver);
        else {
            Receiver.load(id, (receiver) -> {
                this.receiver = receiver;
                completion.run(receiver);
            }, autoUpdate);
        }
    }

    @NotNull
    synchronized public String getPhone() {
        return phone;
    }

    @Nullable
    synchronized public String getDescription() {
        return description;
    }

    synchronized public void setPhone(@NotNull String phone, @Nullable SaveCompletion completion) {
        Utils.notNull(phone);

        this.phone = phone;
        save(completion);
    }

    synchronized public void setDescription(@Nullable String description, @Nullable SaveCompletion completion) {
        this.description = description;
        save(completion);
    }

    synchronized public void setEmail(@NotNull String email, @Nullable SaveCompletion completion) {
        Utils.notNull(email);

        this.email = email;
        save(completion);
    }

    synchronized public void setName(@NotNull String name, @Nullable SaveCompletion completion) {
        Utils.notNull(name);

        this.name = name;
        save(completion);
    }

    synchronized public void setPhoto(@Nullable String photo, @Nullable SaveCompletion completion) {
        this.photo = photo;
        save(completion);
    }

    synchronized public void setRating(@Nullable Double rating, @Nullable SaveCompletion completion) {
        this.rating = rating;
        save(completion);
    }

    public void setMessagingToken(@Nullable String messagingToken,
                                  @Nullable SaveCompletion completion) {
        this.messagingToken = messagingToken;
        save(completion);
    }

    public void authorizeUser(@NotNull String userId, @Nullable SaveCompletion completion) {
        this.authUsers.add(userId);
        save(completion);
    }

    //endregion
}

