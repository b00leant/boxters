package com.boxters.model;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by daniele on 27/03/17.
 */

final class DataService {

    public static final DatabaseReference root = FirebaseDatabase.getInstance().getReference();
}
