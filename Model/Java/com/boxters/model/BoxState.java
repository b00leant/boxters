package com.boxters.model;

/**
 * Created by daniele on 30/01/17.
 */

public enum BoxState {
    CREATED, CONFIRMED, TRAVELLING, RECEIVED
}
