package com.boxters.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.boxters.model.completion.AuthorizedBoxCompletion;
import com.boxters.model.completion.BoxCompletion;
import com.boxters.model.completion.Completion;
import com.boxters.model.completion.PublicBoxCompletion;
import com.boxters.model.completion.PublicTravelCompletion;
import com.boxters.model.completion.SaveCompletion;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.Contract;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Created by daniele on 30/01/17.
 *
 * Questa classe rappresenta un pacco dell'utente loggato.
 *
 * Ha al suo interno tutte le informazioni sul pacco,
 * comprese quelle private, ed è utilizzabile solo dall'utente
 * loggato per motivi di privacy
 */

@SuppressWarnings("ConstantConditions")
public class Box extends AuthorizedBox {
    //region: inner workings -----------------------------------------------------------------------

    static final String BOXES_LOCATIONS_KEY = "boxesLocations";
    private static final String POSSIBLE_TRAVELS_KEY = "possibleTravels";


    //per ogni Boxter vengono memorizzati i viaggi in cui può portare il pacco
    @NotNull private final Map<String, Set<String>> possibleTravels = new HashMap<>();


    private Box(@NotNull String sender, @NotNull String id, @NotNull String receiver,
                double weight, @Nullable String description, @NotNull BoxState state,
                @Nullable String photo, @NotNull String object, @NotNull Date expiration,
                double size, @NotNull Location departure, @NotNull Location arrival) {
        super(sender, id, receiver, weight, description, state, photo, object, expiration, size, departure, arrival);
    }

    private Box(@NotNull String sender, @NotNull String id) {
        super(sender, id);
    }


    @Contract(pure = true)
    private static String getTable(@NotNull String sender, @NotNull String id) {
        return BOXES_KEY + "/" + sender + "/" + id;
    }

    @Override @NotNull
    String getTable() {
        return getTable(sender, id);
    }

    @Override @NotNull
    Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();

        Map<String, Map<String, Object>> possibleTravelsMap = new HashMap<>();
        for(Map.Entry<String, Set<String>> t: possibleTravels.entrySet()) {

            HashMap<String, Object> boxterMap = new HashMap<>();
            for(String travel: t.getValue()) {
                boxterMap.put(travel, true);
            }

            possibleTravelsMap.put(t.getKey(), boxterMap);
        }
        map.put(POSSIBLE_TRAVELS_KEY, possibleTravelsMap);

        map.put(AUTHORIZED_KEY, super.toMap());

        return map;
    }

    @Override
    synchronized void parse(@NotNull DataSnapshot snapshot) {

        for(DataSnapshot t: snapshot.child(POSSIBLE_TRAVELS_KEY).getChildren()) {
            Set<String> travels = new HashSet<>();
            for(DataSnapshot trav: t.getChildren()) {
                if((Boolean) trav.getValue()) {
                    travels.add(trav.getKey());
                }
            }

            possibleTravels.put(t.getKey(), travels);
        }

        super.parse(snapshot.child(AUTHORIZED_KEY));
    }

    @Override
    void update(@NotNull Map<String, Object> updates, @Nullable SaveCompletion completion) {
        super.update(updates, (res) -> {
            if(res != null && res) {
                //salva la location per rendere il pacco trovabile se state == CREATED
                if(state == BoxState.CREATED) {
                    DatabaseReference locRef = DataService.root.child(BOXES_LOCATIONS_KEY);
                    GeoFire geoFire = new GeoFire(locRef);

                    geoFire.setLocation(BoxLocationProj.key(this), new GeoLocation(arrival.getLat(), arrival.getLon()), (key, error) -> {
                        //todo: segnala l'errore
                        if (completion != null) completion.run(error == null);
                    });
                }
                else if(completion != null) completion.run(true);
            }
            else if (completion != null) completion.run(false);
        });
    }

    //endregion ------------------------------------------------------------------------------------


    //region: public interface ---------------------------------------------------------------------

    /** Carica dal server il pacco con id specificato
     * é necessario che l'utente loggato sia il sender specificato
     *
     * @param sender il mittente del pacco
     * @param id l'id del pacco da caricare dal server
     * @param completion l'handler che viene chiamato a caricamento ultimato con argomento il pacco
     * @param autoUpdate se il pacco che viene caricato dovrà auto aggiornarsi
     */
    public static void load(@NotNull String sender, @NotNull String id,
                            @NotNull BoxCompletion completion, boolean autoUpdate) {
        Utils.notNull(sender);
        Utils.notNull(id);
        Utils.notNull(completion);

        DatabaseReference db = DataService.root;
        db.child(getTable(sender, id)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null) {
                    Box box = new Box(sender, id);
                    box.parse(dataSnapshot);
                    completion.run(box);
                    if(autoUpdate) box.autoUpdate();
                }
                else completion.run(null);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException().printStackTrace();
                completion.run(null);
            }
        });
    }

    /** La funzione loadAll è stata rimossa in quanto è necessario ritornare due oggetti diversi
     * per i pacchi che si stanno inviando e quelli che si stanno ricevendo
     *
     * Carica tutti i pacchi di un determinato utente (come mittente o destinatario)
     * è necessario che il mittente sia l'utente loggato
     *
     * @param user il mittente dei pacchi
     * @param completion una funzione che viene chiamata con ogni pacco del mittente (null = errore)
     * @param autoUpdate se il pacco che viene caricato dovrà auto aggiornarsi
     *
    public static void loadAll(@NotNull User user, @NotNull BoxCompletion completion,
                               boolean autoUpdate) {

        //i controlli sui valori null avvengono nelle funzioni chiamate

        loadAsSender(user.id, completion, autoUpdate);
        loadAsReceiver(user, completion, autoUpdate);
    }*/

    /** Carica tutti i pacchi che il mittente specificato sta inviando o ha inviato
     * è necessario che il mittente sia l'utente loggato
     *
     * @param sender il mittente dei pacchi
     * @param completion una funzione che viene chiamata con ogni pacco del mittente (null = errore)
     * @param autoUpdate se il pacco che viene caricato dovrà auto aggiornarsi
     */
    public static void loadAsSender(@NotNull String sender, @NotNull BoxCompletion completion,
                                  boolean autoUpdate) {
        Utils.notNull(sender);
        Utils.notNull(completion);

        DatabaseReference db = DataService.root;
        db.child(BOXES_KEY + "/" + sender).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Box box = new Box(sender, dataSnapshot.getKey());
                box.parse(dataSnapshot);
                if(autoUpdate) box.autoUpdate();
                completion.run(box);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {
                completion.run(null);
            }
        });
    }

    /** Carica tutti i pacchi che il destinatario specificato sta ricevendo o ha ricevuto
     * è necessario che il destinatario sia l'utente loggato
     *
     * @param receiver il destinatario dei pacchi
     * @param completion una funzione che viene chiamata con ogni pacco del mittente (null = errore)
     * @param autoUpdate se il pacco che viene caricato dovrà auto aggiornarsi
     */
    public static void loadAsReceiver(@NotNull User receiver, @NotNull AuthorizedBoxCompletion completion,
                                  boolean autoUpdate) {
        Utils.notNull(receiver);
        Utils.notNull(completion);

        receiver.getReceiver((_receiver) -> {
            for(Receiver.ReceiverBox b: _receiver.boxes()) {
                 AuthorizedBox.load(b.sender, b.id, completion, autoUpdate);
            }
        });
    }

    /** Permette di creare un nuovo pacco con mittente userId
     * (che deve essere l'utente attualmente loggato) e con le informazioni specificate
     *
     * @param sender il mittente del pacco
     * @param photo la foto del pacco (eventualmente null)
     * @param object l'oggetto che si sta spedendo
     * @param expiration la data entro cui deve avvenire la spedizione
     * @param departure il luogo di partenza
     * @param arrival il luogo di arrivo
     * @param completion l'handler a cui viene passato l'oggetto creato a creazione ultimata
     * @param autoUpdate specifica se l'oggetto deve auto aggiornarsi quando le info nel server sono aggiornate
     */
    public static void create(@NotNull String sender, @NotNull String receiver,
                              double weight, @Nullable String description,
                              @Nullable String photo, @NotNull String object, @NotNull Date expiration,
                              double size, @NotNull Location departure, @NotNull Location arrival,
                              @Nullable BoxCompletion completion, boolean autoUpdate) {
        Utils.notNull(sender);
        Utils.notNull(receiver);
        Utils.notNull(object);
        Utils.notNull(expiration);
        Utils.notNull(departure);
        Utils.notNull(arrival);

        DatabaseReference db = DataService.root;
        String id = db.child(BOXES_KEY + "/" + sender).push().getKey();

        Box box = new Box(sender, id, receiver, weight, description, BoxState.CREATED,
                photo, object, expiration, size, departure, arrival);

        box.save((res) -> {
            if(res != null) {
                db.child(User.getTable(sender) + "/" + User.USERS_KEY + "/" + receiver).setValue(true, (err, ref)-> {
                    if(err != null) {
                        err.toException().printStackTrace();
                    }
                    else if(ref != null) {
                        if (autoUpdate) box.autoUpdate();
                        if(completion != null) completion.run(box);
                    }
                    else if (completion != null) completion.run(null);
                });

            }
            else if (completion != null) completion.run(null);

        });
    }

    /** Aggiunge una funzione che verrà chiamata ogni qual volta l'oggetto viene modificato
     *
     * @param completion la funzione chiamata ad ogni modifica
     */
    public void onUpdate(@NotNull BoxCompletion completion) {
        Utils.notNull(completion);

        onUpdate.add((data) -> completion.run((Box) data));
    }

    synchronized public void loadPossibleTravels(@NotNull PublicTravelCompletion completion,
                                                boolean autoUpdate) {
        if(state != BoxState.CREATED) return;

        for(Map.Entry<String, Set<String>> boxter: possibleTravels.entrySet()) {
            for(String travelId: boxter.getValue()) {
                PublicTravel.load(boxter.getKey(), travelId, completion, autoUpdate);
            }
        }
    }

    synchronized public void setState(@NotNull BoxState state, @Nullable SaveCompletion completion) {
        Utils.notNull(state);

        if( state == BoxState.CREATED
            || (state == BoxState.CONFIRMED && this.state != BoxState.CREATED && this.state != BoxState.CONFIRMED)
            || (state == BoxState.TRAVELLING && this.state != BoxState.CONFIRMED && this.state != BoxState.TRAVELLING)
            || (state == BoxState.RECEIVED && this.state != BoxState.TRAVELLING && this.state != BoxState.RECEIVED) ) {
            //il cambiamento di stato del pacco da parte del mittente non è valido
            throw new IllegalActionException("Operazione non consentita: si sta provando al passare dallo stato "
                    + this.state.name() + " allo stato " + state.name());
        }
        else {
            //si è scelto un Boxter
            if(state == BoxState.CONFIRMED && this.state == BoxState.CREATED) {
                //è stato confermato il boxter, rimuovi il pacco dai pacchi cercabili dai boxters
                DatabaseReference boxLoc = DataService.root
                        .child(BOXES_LOCATIONS_KEY + "/" + BoxLocationProj.key(this));

                boxLoc.removeValue((databaseError, databaseReference) -> {
                    //todo: logga l'errore
                    if(databaseError != null) {
                        if(completion != null) completion.run(false);
                    }
                    else {
                        this.state = state;
                        save(completion);
                    }
                });
            }
            else {
                this.state = state;
                save(completion);
            }
        }
    }

    synchronized public void setPhoto(@Nullable String photo, @Nullable SaveCompletion completion) {
        if(state != BoxState.CREATED) {
            throw new IllegalActionException("Un pacco può essere modificato solo nello stato CREATED");
        }
        else {
            this.photo = photo;
            save(completion);
        }
    }

    synchronized public void setObject(@NotNull String object, @Nullable SaveCompletion completion) {
        Utils.notNull(object);

        if(state != BoxState.CREATED) {
            throw new IllegalActionException("Un pacco può essere modificato solo nello stato CREATED");
        }
        else {
            this.object = object;
            save(completion);
        }
    }

    synchronized public void setExpiration(@NotNull Date expiration, @Nullable SaveCompletion completion) {
        Utils.notNull(expiration);

        if(state != BoxState.CREATED) {
            throw new IllegalActionException("Un pacco può essere modificato solo nello stato CREATED");
        }
        else {
            //todo: salvare tramite un save speciale location e pacco contemporaneamente e nella stessa operazione rimuovere la location precedente

            //rimuove la chiave corrente in boxesLocations poiché ne verrà salvata una nuova
            DatabaseReference boxLoc = DataService.root
                    .child(BOXES_LOCATIONS_KEY + "/" + BoxLocationProj.key(this));

            boxLoc.removeValue((databaseError, databaseReference) -> {
                //todo: logga l'errore
                if(databaseError != null) {
                    if(completion!= null) completion.run(false);
                }
                else {
                    this.expiration = expiration;
                    save(completion);
                }
            });
        }
    }

    synchronized public void setDeparture(@NotNull Location departure, @Nullable SaveCompletion completion) {
        Utils.notNull(departure);

        if(state != BoxState.CREATED) {
            throw new IllegalActionException("Un pacco può essere modificato solo nello stato CREATED");
        }
        else {
            //todo: salvare tramite un save speciale location e pacco contemporaneamente e nella stessa operazione rimuovere la location precedente

            //rimuove la chiave corrente in boxesLocations poiché ne verrà salvata una nuova
            DatabaseReference boxLoc = DataService.root
                    .child(BOXES_LOCATIONS_KEY + "/" + BoxLocationProj.key(this));

            boxLoc.removeValue((databaseError, databaseReference) -> {
                //todo: logga l'errore
                if(databaseError != null) {
                    if(completion!= null) completion.run(false);
                }
                else {
                    this.departure = departure;
                    save(completion);
                }
            });
        }
    }

    synchronized public void setArrival(@NotNull Location arrival, @Nullable SaveCompletion completion) {
        Utils.notNull(arrival);

        if(state != BoxState.CREATED) {
            throw new IllegalActionException("Un pacco può essere modificato solo nello stato CREATED");
        }
        else {
            //todo: salvare tramite un save speciale location e pacco contemporaneamente e nella stessa operazione rimuovere la location precedente

            //rimuove la chiave corrente in boxesLocations poiché ne verrà salvata una nuova
            DatabaseReference boxLoc = DataService.root
                    .child(BOXES_LOCATIONS_KEY + "/" + BoxLocationProj.key(this));

            boxLoc.removeValue((databaseError, databaseReference) -> {
                //todo: logga l'errore
                if(databaseError != null) {
                    if(completion!= null) completion.run(false);
                }
                else {
                    this.arrival = arrival;
                    save(completion);
                }
            });
        }
    }

    synchronized public void setDescription(@Nullable String description, @Nullable SaveCompletion completion) {
        if(state != BoxState.CREATED) {
            throw new IllegalActionException("Un pacco può essere modificato solo nello stato CREATED");
        }
        else {
            this.description = description;
            save(completion);
        }
    }

    synchronized public void setWeight(double weight, @Nullable SaveCompletion completion) {
        if(state != BoxState.CREATED) {
            throw new IllegalActionException("Un pacco può essere modificato solo nello stato CREATED");
        }
        else {
            this.weight = weight;
            save(completion);
        }
    }

    synchronized public void addPossibleTravels(@NotNull Set<PublicTravel> travels,
                                               @Nullable SaveCompletion completion) {
        Utils.notNull(travels);

        if(state != BoxState.CREATED) {
            throw new IllegalActionException("Un pacco può essere modificato solo nello stato CREATED");
        }
        else {
            Map<String, Object> updates = new HashMap<>();

            for(PublicTravel travel: travels) {
                authUsers.add(travel.boxter);

                if(!possibleTravels.containsKey(travel.boxter))
                    possibleTravels.put(travel.boxter, new HashSet<>());

                this.possibleTravels.get(travel.boxter).add(travel.id);

                updates.put(User.getTable(sender) + "/" + User.USERS_KEY + "/" + travel.boxter, true);
            }

            updates.put(getTable(), toMap());

            DatabaseReference db = DataService.root;

            db.updateChildren(updates, (error, ref) -> {
                if(error != null) {
                    error.toException().printStackTrace();
                }

                if(completion != null) completion.run(error == null);
            });
        }
    }

    public void setTravel(@NotNull PublicTravel travel, @Nullable SaveCompletion completion) {
        Utils.notNull(travel);

        if(state != BoxState.CREATED) {
            throw new IllegalActionException("Un pacco può essere modificato solo nello stato CREATED");
        }
        else {
            this.state = BoxState.CONFIRMED;
            this.boxter = travel.boxter;
            this.travelId = travel.id;
            save(completion);
        }

    }

    public void setSize(double size, @Nullable SaveCompletion completion) {

        if(state != BoxState.CREATED) {
            throw new IllegalActionException("Un pacco può essere modificato solo nello stato CREATED");
        }
        else {
            this.size = size;
            save(completion);
        }
    }

    @Override
    synchronized public void delete(@Nullable SaveCompletion completion) {
        if(state != BoxState.CREATED) {
            throw new IllegalActionException("Un pacco può essere eliminato solo nello stato CREATED");
        }
        else {
            super.delete(completion);
        }
    }

    //endregion

}
