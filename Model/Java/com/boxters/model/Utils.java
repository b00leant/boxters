package com.boxters.model;

import org.jetbrains.annotations.NotNull;

/**
 * Created by daniele on 28/02/17.
 */

public class Utils {
    @SuppressWarnings("ConstantConditions")
    public static void notNull(@NotNull Object obj) {
        if(obj == null) {
            throw new IllegalArgumentException("Il parametro passato non può essere null!");
        }
    }
}
