package com.boxters.model;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Random;
import java.util.Set;

/**
 * Created by daniele on 14/02/17.
 */

public class DBTester {
    private final static Date TODAY = new Date();
    private final static Date TOMORROW = addDays(TODAY, 1);

    private final static Random r = new Random();

    private final static Location ROMA = new Location("Roma", 41.9028, 12.4964);
    private final static Location MILANO = new Location("Milano", 45.4654, 9.1859);
    private final static Location FORMIA = new Location("Formia", 41.2560, 13.6069);
    private final static Location LATINA = new Location("Latina", 41.4676, 12.9036);
    private final static Location GENOVA = new Location("Genova", 44.4056, 8.9463);

    private final static Set<Transport> CAR = new HashSet<>();
    private final static Set<Transport> TRAIN = new HashSet<>();
    private final static Set<Transport> FEET_TRAIN = new HashSet<>();

    static {
        CAR.add(Transport.CAR);
        TRAIN.add(Transport.TRAIN);
        FEET_TRAIN.add(Transport.FEET);
        FEET_TRAIN.add(Transport.TRAIN);
    }

    @NotNull
    private static Date addDays(@NotNull Date date, int days) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, days);
        return c.getTime();
    }


    public static void addTestTravels(@NotNull User user) {

        Date date = addDays(TODAY, 3);

        if(r.nextInt() % 2 == 1)
            Travel.create(user.getId(), date, addDays(date, 1), 5.0, TRAIN, ROMA, MILANO, null, false);
        if(r.nextInt() % 2 == 1)
            Travel.create(user.getId(), TODAY, TOMORROW, 10.0, CAR, LATINA, ROMA, null, false);
        if(r.nextInt() % 2 == 1)
            Travel.create(user.getId(), date, addDays(date, 1), 10.0, CAR, ROMA, LATINA, null, false);
        if(r.nextInt() % 2 == 1)
            Travel.create(user.getId(), TOMORROW, addDays(TOMORROW, 1), 2.0, FEET_TRAIN, ROMA, FORMIA, null, false);
        if(r.nextInt() % 2 == 1)
            Travel.create(user.getId(), TOMORROW, addDays(TOMORROW, 1), 2.0, FEET_TRAIN, MILANO, ROMA, null, false);
        if(r.nextInt() % 2 == 1)
            Travel.create(user.getId(), TOMORROW, addDays(TOMORROW, 1), 12.0, CAR, ROMA, GENOVA, null, false);
        if(r.nextInt() % 2 == 1)
            Travel.create(user.getId(), TODAY, TOMORROW, 12.0, CAR, GENOVA, ROMA, null, false);
        if(r.nextInt() % 2 == 1)
            Travel.create(user.getId(), TOMORROW, addDays(TOMORROW, 2), 12.0, FEET_TRAIN, FORMIA, MILANO, null, false);
        if(r.nextInt() % 2 == 1)
            Travel.create(user.getId(), date, addDays(date, 1), 2.0, FEET_TRAIN, FORMIA, LATINA, null, false);
        if(r.nextInt() % 2 == 1)
            Travel.create(user.getId(), date, date, 2.0, CAR, FORMIA, LATINA, null, false);
        if(r.nextInt() % 2 == 1)
            Travel.create(user.getId(), TODAY, TODAY, 8.0, CAR, LATINA, FORMIA, null, false);
        if(r.nextInt() % 2 == 1)
            Travel.create(user.getId(), TOMORROW, TOMORROW, 9.0, CAR, GENOVA, GENOVA, null, false);
    }

    public static void addTestBoxes(@NotNull User user, @NotNull String receiver) {

        Date date = addDays(TODAY, 4);

        if(r.nextInt() % 2 == 1)
            Box.create(user.getId(), receiver, 1.0,
                "Ho dimenticato la giacca a Roma, e non posso al momento tornarci.",
                "http://cdn.yoox.biz/41/41342836nx_15_n_f.jpg", "Giacca Armani", date,
                2, ROMA, MILANO, null, false);
        if(r.nextInt() % 2 == 1)
            Box.create(user.getId(), receiver, 2.0, null,
                "http://www.troddiusudebidda.it/wordpress/wp-content/uploads/2015/07/bottarga_muggine.jpg",
                "Bottarga di Cabras", date, 5, ROMA, LATINA, null, false);
        if(r.nextInt() % 2 == 1)
            Box.create(user.getId(), receiver, 2.4, null,
                "https://www.prodottitipiciromani.it/wp-content/uploads/2013/11/porchetta-di-ariccia-slide-960.jpg",
                "Porchetta di Ariccia", TOMORROW, 8, ROMA, MILANO, null, false);
        if(r.nextInt() % 2 == 1)
            Box.create(user.getId(), receiver, 2.0, null,
                "http://naturabio.altervista.org/blog/wp-content/uploads/2013/11/aragosta-carota.jpg",
                "Aragosta vegana", date, 12, GENOVA, ROMA, null, false);
        if(r.nextInt() % 2 == 1)
            Box.create(user.getId(), receiver, 7.0, null, null, "Batteria computer", TOMORROW,
                7, MILANO, ROMA, null, false);
        if(r.nextInt() % 2 == 1)
            Box.create(user.getId(), receiver, 2.5, null, null, "Batteria pc", TODAY,
                4, ROMA, GENOVA, null, false);
        if(r.nextInt() % 2 == 1)
            Box.create(user.getId(), receiver, 22.3,
                "Vorrei spedire a mio figlio un comodino che ha dimenticato durante i traslochi",
                "https://venditamobiligiapponesi.it/1309-thickbox_default/comodino-zen.jpg", "Comodino",
                date, 4, ROMA, MILANO, null, false);
        if(r.nextInt() % 2 == 1)
            Box.create(user.getId(), receiver, 3.0, null, null, "Maglioncino blu", date,
                    3, ROMA, GENOVA, null, false);
    }

    public static void testTravelQuery(String userID) {

        Box.create(userID, "zmFBE4zWu3VzNMxNd1FRPmXz1BN2", 2.0, null, null, "Ciao", new Date(1488240000000L), 1,
            ROMA, MILANO, (box) -> {
                PublicTravel.queryForBox(box, 3.0, (travel) -> {
                    System.out.println(travel.getId());
                }, false);
            }, false);
    }

    public static String uiString(@NotNull Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN);
        return dateFormat.format(date);
    }
}
