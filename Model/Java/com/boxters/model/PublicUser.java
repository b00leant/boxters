package com.boxters.model;

import com.boxters.model.completion.PublicUserCompletion;
import com.boxters.model.completion.ResultCompletion;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniele on 29/01/17.
 *
 * Contiene le informazioni di un utente visibili agli altri utenti, i suoi oggetti sono accessibili
 * da tutti gli utenti
 */

public class PublicUser extends DBData implements BasicUser {
    //region: inner workings -----------------------------------------------------------------------

    static final String USERS_KEY = "users";
    static final String EMAIL_KEY = "email";
    static final String NAME_KEY = "name";
    private static final String PHOTO_KEY = "photo";
    private static final String RATING_KEY = "rating";
    static final String PUBLIC_KEY = "public";
    private static final String MESSAGING_TOKEN = "messagingToken";

    @NotNull String email;
    @NotNull String name;
    @Nullable String photo;
    @Nullable Double rating;

    @Nullable String messagingToken;

    PublicUser(@NotNull String id, @NotNull String email, @NotNull String name,
               @Nullable String photo, @Nullable Double rating, @Nullable String messagingToken) {
        super(id);

        this.email = email;
        this.name = name;
        this.photo = photo;
        this.rating = rating;

        this.messagingToken = messagingToken;
    }

    PublicUser(@NotNull String id) {
        super(id);
    }

    @Contract(pure = true)
    private static String getTable(@NotNull String id) {
        return USERS_KEY + "/" + id + "/" + PUBLIC_KEY;
    }

    @Override @NotNull
    String getTable() {
        return getTable(id);
    }

    @Override @NotNull
    Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();

        map.put(EMAIL_KEY, email);
        map.put(NAME_KEY, name);
        if(photo != null) map.put(PHOTO_KEY, photo);
        if(rating != null) map.put(RATING_KEY, rating);
        if(messagingToken != null)
            map.put(MESSAGING_TOKEN, messagingToken);

        return map;
    }

    @Override
    synchronized void parse(@NotNull DataSnapshot snapshot) {

        this.name = (String) snapshot.child(NAME_KEY).getValue();
        this.email = (String) snapshot.child(EMAIL_KEY).getValue();
        if(snapshot.hasChild(RATING_KEY)) {
            rating = ((Number) snapshot.child(RATING_KEY).getValue()).doubleValue();
        }
        else rating = null;
        if(snapshot.hasChild(PHOTO_KEY)) {
            photo = (String) snapshot.child(PHOTO_KEY).getValue();
        }
        else photo = null;
        if(snapshot.hasChild(MESSAGING_TOKEN)) {
            messagingToken = (String) snapshot.child(MESSAGING_TOKEN).getValue();
        }
    }

    private void waitStateConfirm(@NotNull BoxState state, @NotNull String sender,
                                  @NotNull String boxId, @NotNull ResultCompletion completion) {

        DatabaseReference db = DataService.root.child(Receiver.getTable(id))
                .child(sender + "/" + boxId + "/" + Receiver.STATE_KEY);

        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null) {
                    BoxState newState = BoxState.valueOf((String) dataSnapshot.getValue());
                    if(newState == state) {
                        completion.run(true);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                completion.run(false);
            }
        });
    }

    //endregion ------------------------------------------------------------------------------------


    //region: public interface ---------------------------------------------------------------------

    public static void load(@NotNull String id, @NotNull PublicUserCompletion completion, boolean autoUpdate) {
        Utils.notNull(id);
        Utils.notNull(completion);

        DatabaseReference db = DataService.root;
        db.child(getTable(id)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null) {
                    PublicUser user = new PublicUser(id);
                    user.parse(dataSnapshot);
                    if(autoUpdate) user.autoUpdate();
                    completion.run(user);
                }
                else completion.run(null);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException().printStackTrace();
                completion.run(null);
            }
        });
    }

    public void onUpdate(@NotNull PublicUserCompletion completion) {
        Utils.notNull(completion);

        onUpdate.add((data) -> completion.run((PublicUser) data));
    }

    @NotNull
    synchronized public String getEmail() {
        return email;
    }

    @NotNull
    synchronized public String getName() {
        return name;
    }

    @Nullable
    synchronized public String getPhoto() {
        return photo;
    }

    @Nullable
    synchronized public Double getRating() {
        return rating;
    }

    /** Aggiunge un listener sulla proprietà state di un pacco che si sta ricevendo
     *  Deve essere chiamata dal boxter per aspettare che il destinatario confermi
     *  di aver ricevuto il pacco
     *
     * @param sender il mittente del pacco
     * @param boxId l'id del pacco
     * @param completion funzione chiamata quando state è RECEIVED
     */
    public void waitReceivedConfirm(@NotNull String sender, @NotNull String boxId,
                                      @NotNull ResultCompletion completion) {
        Utils.notNull(sender);
        Utils.notNull(boxId);
        Utils.notNull(completion);

        waitStateConfirm(BoxState.RECEIVED, sender, boxId, completion);
    }

    @Nullable
    public String getMessagingToken() {
        return messagingToken;
    }
    //endregion
}
