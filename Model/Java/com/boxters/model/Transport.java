package com.boxters.model;

/**
 * Created by daniele on 10/02/17.
 */

public enum Transport {
    CAR, FEET, TRAIN, AIRPLANE, BUS
}
