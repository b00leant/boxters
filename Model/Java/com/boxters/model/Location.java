package com.boxters.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.firebase.geofire.GeoLocation;
import com.google.firebase.database.DataSnapshot;

import org.jetbrains.annotations.Contract;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniele on 30/01/17.
 *
 * é un'astrazione di valore, rappresenta un luogo sulla Terra, con latitudine lat e longitudine lon
 */

public class Location {
    //region: inner workings -----------------------------------------------------------------------

    //raggio della Terra in kilometri
    static final double R = 6372.8;


    private static final String NAME_KEY = "location";
    private static final String LAT_KEY = "lat";
    private static final String LON_KEY = "lon";

    @Nullable private final String name;
    private final double lat;
    private final double lon;

    Location(@NotNull DataSnapshot snapshot) {
        if(snapshot.hasChild(NAME_KEY)) {
            this.name = (String) snapshot.child(NAME_KEY).getValue();
        }
        else this.name = null;
        this.lat = ((Number) snapshot.child(LAT_KEY).getValue()).doubleValue();
        this.lon = ((Number) snapshot.child(LON_KEY).getValue()).doubleValue();
    }

    Location(@NotNull GeoLocation location) {
        this.name = null;
        this.lat = location.latitude;
        this.lon = location.longitude;
    }

    @NotNull
    Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();

        if(name != null) map.put(NAME_KEY, name);
        map.put(LON_KEY, lon);
        map.put(LAT_KEY, lat);

        return map;
    }

    @NotNull
    GeoLocation toGeoLocation() {
        return new GeoLocation(lat, lon);
    }

    //endregion ------------------------------------------------------------------------------------


    //region: public interface ---------------------------------------------------------------------

    public Location(@Nullable String name, double lat, double lon) {
        this.name = name;
        this.lat = lat;
        this.lon = lon;
    }

    @Nullable
    public String getName() {
        return name;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    /** calcola la distanza tra due punti utilizzando la formula di haversine
     *
     * @param l il secondo punto da cui calcolare la distanza
     * @return la distanza calcolata */
    @Contract(pure = true)
    public double distance(@NotNull Location l) {
        double dLat = Math.toRadians(l.lat - lat);
        double dLon = Math.toRadians(l.lon - lon);
        double lat1 = Math.toRadians(lat);
        double lat2 = Math.toRadians(l.lat);

        double a = Math.pow(Math.sin(dLat / 2),2) + Math.pow(Math.sin(dLon / 2),2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return R * c;
    }

    //endregion ------------------------------------------------------------------------------------
}
