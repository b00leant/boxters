package com.boxters.model.messaging;

import com.boxters.model.Utils;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * Created by daniele on 02/03/17.
 */

public class RequestBoxInfoPayload extends BoxPayload {
    private final static String TAG = "RequestBoxInfoPayload";

    public static final String TRAVEL_ID_KEY = "travelId";

    @NotNull
    private final String travelId;

    public RequestBoxInfoPayload( @NotNull String sender, @NotNull String receiverToken,
                                  @NotNull String boxId, @NotNull String travelId) {
        super(MessageType.REQUEST_BOX_INFO, sender, receiverToken, boxId);
        this.travelId = travelId;

    }

    RequestBoxInfoPayload(@NotNull Map<String, String> dataPayload) {
        super(MessageType.REQUEST_BOX_INFO, dataPayload);

        String travelId = dataPayload.get(TRAVEL_ID_KEY);
        Utils.notNull(travelId);
        this.travelId = travelId;
    }

    @Override
    public String computeMessageId(boolean fromServer) {
        return super.computeMessageId(fromServer)
                + "A" + Integer.toString(travelId.hashCode(), 36);
    }

    @NotNull
    public String getTravelId() {
        return travelId;
    }

    @Override
    public @NotNull Map<String, String> toMap() {
        Map<String, String> map = super.toMap();
        map.put(TRAVEL_ID_KEY, travelId);
        return map;
    }
}
