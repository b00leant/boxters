package com.boxters.model.messaging;

import com.boxters.model.Utils;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by daniele on 28/02/17.
 */
abstract public class BoxtersPayload {
    public static final String TYPE_KEY = "type";
    public static final String RECEIVER_TOKEN_KEY = "receiver";
    public static final String SENDER_KEY = "sender";

    @NotNull private final MessageType type;
    @NotNull private final String sender;
    @NotNull private final String receiverToken;

    BoxtersPayload(@NotNull MessageType type, @NotNull String sender, @NotNull String receiverToken) {
        this.type = type;
        this.sender = sender;
        this.receiverToken = receiverToken;
    }

    BoxtersPayload(@NotNull MessageType type, Map<String, String> dataPayload) {
        this.type = type;

        String sender = dataPayload.get(SENDER_KEY);
        Utils.notNull(sender);
        this.sender = sender;

        String receiverToken = dataPayload.get(RECEIVER_TOKEN_KEY);
        Utils.notNull(receiverToken);
        this.receiverToken = receiverToken;
    }

    public static BoxtersPayload parse(Map<String, String> dataPayload) {

        MessageType type = MessageType.valueOf(dataPayload.get(TYPE_KEY));

        switch (type) {
            case REQUEST_TO_BE_RECEIVER:
                return new RequestToBeReceiverPayload(dataPayload);
            case REQUEST_BOX_INFO:
                return new RequestBoxInfoPayload(dataPayload);
            case BOX_ARRIVED:
                return new BoxArrivedPayload(dataPayload);
            case SELECTED_AS_POSSIBLE_BOXTER:
                return new SelectedAsPossibleBoxterPayload(dataPayload);
            case TEST:
                return new TestPayload(dataPayload);
        }

        throw new IllegalArgumentException("Il messaggio passato non ha un tipo valido, type: " + type);
    }

    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<>();
        map.put(TYPE_KEY, type.name());
        map.put(RECEIVER_TOKEN_KEY, receiverToken);
        map.put(SENDER_KEY, sender);
        return map;
    }

    @NotNull
    public MessageType getType() {
        return type;
    }

    @NotNull
    public String getSender() {
        return sender;
    }

    @NotNull
    public String getReceiverToken() {
        return receiverToken;
    }

    public String computeMessageId(boolean fromServer) {
        return "m-" + Integer.toString(type.name().hashCode(), 36)
                + "A" + Integer.toString(sender.hashCode(), 36)
                + "A" + Integer.toString(receiverToken.hashCode(), 36)
                + "A" + Integer.toString(new Random().nextInt(), 36)
                + (fromServer ? "Afs" : "");
    }

    @NotNull
    private static String toMessageIdentifier(int i) {
        return Integer.toString(i, 36);
    }
}
