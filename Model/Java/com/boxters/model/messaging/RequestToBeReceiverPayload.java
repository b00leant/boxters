package com.boxters.model.messaging;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * Created by daniele on 28/02/17.
 */
public class RequestToBeReceiverPayload extends BoxPayload {
    private final static String TAG = "RequestToBeReceiverPayload";

    public RequestToBeReceiverPayload( @NotNull String sender, @NotNull String receiverToken, @NotNull String boxId) {
        super(MessageType.REQUEST_TO_BE_RECEIVER, sender, receiverToken, boxId);
    }

    RequestToBeReceiverPayload(@NotNull Map<String, String> dataPayload) {
        super(MessageType.REQUEST_TO_BE_RECEIVER, dataPayload);
    }

}
