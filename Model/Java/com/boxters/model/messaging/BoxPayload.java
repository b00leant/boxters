package com.boxters.model.messaging;

import com.boxters.model.Utils;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * Created by daniele on 03/03/17.
 */

abstract public class BoxPayload extends BoxtersPayload {

    public static final String BOX_ID_KEY = "boxId";

    @NotNull
    private final String boxId;

    BoxPayload(@NotNull MessageType type, @NotNull String sender, @NotNull String receiverToken, @NotNull String boxId) {
        super(type, sender, receiverToken);
        this.boxId = boxId;
    }

    BoxPayload(@NotNull MessageType type, @NotNull Map<String, String> dataPayload) {
        super(type, dataPayload);

        String boxId = dataPayload.get(BOX_ID_KEY);
        Utils.notNull(boxId);
        this.boxId = boxId;
    }

    @Override
    public String computeMessageId(boolean fromServer) {
        return super.computeMessageId(fromServer)
                + "A" + Integer.toString(boxId.hashCode(), 36);
    }

    @NotNull
    public String getBoxId() {
        return boxId;
    }

    @NotNull @Override
    public Map<String, String> toMap() {
        Map<String, String> map = super.toMap();
        map.put(BOX_ID_KEY, boxId);
        return map;
    }
}

