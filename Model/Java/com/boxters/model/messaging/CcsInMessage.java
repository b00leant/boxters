package com.boxters.model.messaging;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents an incoming message from FCM CCS
 */
public class CcsInMessage {

	// Sender registration ID
	private final String from;
	// Sender app's package
	private final String category;
	// Unique id for this message
	private final String messageId;

	public CcsInMessage(String from, String category, String messageId) {
		this.from = from;
		this.category = category;
		this.messageId = messageId;
	}

	public String getFrom() {
		return from;
	}

	public String getCategory() {
		return category;
	}

	public String getMessageId() {
		return messageId;
	}
}