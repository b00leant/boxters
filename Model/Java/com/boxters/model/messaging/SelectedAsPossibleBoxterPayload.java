package com.boxters.model.messaging;

import com.boxters.model.Utils;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * Created by daniele on 05/03/17.
 */

public class SelectedAsPossibleBoxterPayload extends BoxPayload {

    private static final MessageType TYPE = MessageType.SELECTED_AS_POSSIBLE_BOXTER;

    public static final String TRAVEL_ID_KEY = "travelId";

    @NotNull
    private final String travelId;

    public SelectedAsPossibleBoxterPayload(@NotNull String sender, @NotNull String receiverToken,
                                    @NotNull String boxId, @NotNull String travelId) {
        super(TYPE, sender, receiverToken, boxId);
        this.travelId = travelId;
    }

    SelectedAsPossibleBoxterPayload(@NotNull Map<String, String> dataPayload) {
        super(TYPE, dataPayload);

        String travelId = dataPayload.get(TRAVEL_ID_KEY);
        Utils.notNull(travelId);
        this.travelId = travelId;
    }

    @NotNull
    public String getTravelId() {
        return travelId;
    }

    @NotNull @Override
    public Map<String, String> toMap() {
        Map<String, String> map = super.toMap();
        map.put(TRAVEL_ID_KEY, travelId);
        return map;
    }

}
