package com.boxters.model.messaging;

/**
 * Created by daniele on 26/02/17.
 */

public enum MessageType {
    TEST, REQUEST_TO_BE_RECEIVER, REQUEST_BOX_INFO, BOX_ARRIVED,
    SELECTED_AS_POSSIBLE_BOXTER

}
