package com.boxters.model.messaging;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * Created by daniele on 28/02/17.
 */
public class TestPayload extends BoxtersPayload {
    public TestPayload(@NotNull String sender, @NotNull String receiverToken) {
        super(MessageType.TEST, sender, receiverToken);
    }

    TestPayload(@NotNull Map<String, String> dataPayload) {
        super(MessageType.TEST, dataPayload);
    }
}
