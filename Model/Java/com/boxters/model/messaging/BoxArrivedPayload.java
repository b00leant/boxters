package com.boxters.model.messaging;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * Created by daniele on 03/03/17.
 */

public class BoxArrivedPayload extends BoxPayload {
    private final static String TAG = "BoxArrivedPayload";


    public BoxArrivedPayload(@NotNull String sender, @NotNull String receiverToken, @NotNull String boxId) {
        super(MessageType.BOX_ARRIVED, sender, receiverToken, boxId);
    }

    BoxArrivedPayload(@NotNull Map<String, String> dataPayload) {
        super(MessageType.BOX_ARRIVED, dataPayload);
    }
}
