package com.boxters.model;

import org.jetbrains.annotations.NotNull;

/**
 * Created by daniele on 30/01/17.
 *
 * Questa eccezione viene lanciata quando viene chiamato un metodo in uno stato in cui l'operazione
 * non può essere eseguita (es. cambio delle ifnormazioni di un pacco mentre questo sta venendo
 * trasportato)
 */

public class IllegalActionException extends RuntimeException {
    public IllegalActionException(@NotNull String message) {
        super(message);
    }
}
