package com.boxters.model;

import org.jetbrains.annotations.NotNull;

import com.boxters.model.completion.PublicTravelCompletion;
import com.boxters.model.completion.ResultCompletion;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by daniele on 01/02/17.
 *
 * Rappresenta le informazioni di un viaggio accessibili da altri utenti
 */

public class PublicTravel extends DBData {
    //region: inner workings -----------------------------------------------------------------------

    static final String TRAVELS_KEY = "travels";
    private static final String START_DATE_KEY = "startDate";
    private static final String END_DATE_KEY = "endDate";
    private static final String DEPARTURE_KEY = "departure";
    private static final String ARRIVAL_KEY = "arrival";
    private static final String STATE_KEY = "state";
    private static final String SPACE_KEY = "space";
    private static final String TRANSPORTS_KEY = "transports";
    static final String PUBLIC_KEY = "public";


    @NotNull final String boxter;
    @NotNull Date startDate;
    @NotNull Date endDate;
    @NotNull Location departure;
    @NotNull Location arrival;
    @NotNull TravelState state;
    double space;
    @NotNull Set<Transport> transports = new HashSet<>();

    PublicTravel(@NotNull String boxter, @NotNull String id,
                 @NotNull Date startDate, @NotNull Date endDate,
                 double space, @NotNull Set<Transport> transports,
                 @NotNull Location departure, @NotNull Location arrival,
                 @NotNull TravelState state) {
        super(id);

        this.boxter = boxter;
        this.startDate = startDate;
        this.endDate = endDate;
        this.departure = departure;
        this.arrival = arrival;
        this.state = state;
        this.space = space;
        this.transports = transports;
    }

    PublicTravel(@NotNull String boxter, @NotNull String id) {
        super(id);
        this.boxter = boxter;
    }

    @Contract(pure = true)
    private static String getTable(@NotNull String boxter, @NotNull String id) {
        return TRAVELS_KEY + "/" + boxter + "/" + id +"/"+PUBLIC_KEY;
    }

    @NotNull
    @Override
    String getTable() {
        return getTable(boxter, id);
    }

    @NotNull
    @Override
    Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();

        map.put(START_DATE_KEY, startDate.getTime());
        map.put(END_DATE_KEY, endDate.getTime());
        map.put(DEPARTURE_KEY, departure.toMap());
        map.put(ARRIVAL_KEY, arrival.toMap());
        map.put(STATE_KEY, state.name());
        map.put(SPACE_KEY, space);

        Map<String, Boolean> transports = new HashMap<>();
        for(Transport transport: this.transports) {
            transports.put(transport.name(), true);
        }
        map.put(TRANSPORTS_KEY, transports);

        return map;
    }

    @Override
    synchronized void parse(@NotNull DataSnapshot snapshot) {

        this.startDate = new Date((Long) snapshot.child(START_DATE_KEY).getValue());
        this.endDate = new Date((Long) snapshot.child(END_DATE_KEY).getValue());
        this.departure = new Location(snapshot.child(DEPARTURE_KEY));
        this.arrival = new Location(snapshot.child(ARRIVAL_KEY));
        this.state = TravelState.valueOf((String) snapshot.child(STATE_KEY).getValue());
        this.space = ((Number) snapshot.child(SPACE_KEY).getValue()).doubleValue();

        transports.clear();
        for(DataSnapshot transport: snapshot.child(TRANSPORTS_KEY).getChildren()) {
            transports.add(Transport.valueOf(transport.getKey()));
        }
    }

    @NotNull
    String debugId() {
        return debugId(boxter, id);
    }

    @Contract(pure = true)
    @NotNull
    static String debugId(@NotNull String boxter, @NotNull String id) {
        return "(" + boxter + ", " + id + ")";
    }

    private void waitStateConfirm(@NotNull BoxState state, @NotNull String sender,
                                  @NotNull String boxId, @NotNull ResultCompletion completion) {

        DatabaseReference db = DataService.root.child(Travel.getTable(boxter, id))
                .child(Travel.BOXES_KEY + "/" + sender + "/" + boxId + "/" + Travel.TravelBoxProj.STATE_KEY);

        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot != null) {
                    BoxState newState = BoxState.valueOf((String) dataSnapshot.getValue());
                    if(newState == state) {
                        completion.run(true);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                completion.run(false);
            }
        });
    }

    //endregion ------------------------------------------------------------------------------------


    //region: public interface ---------------------------------------------------------------------

    /** Carica dal server i dati visibili di un viaggio da un utente
     * che non si è ancora accordato con il Boxter
     *
     * @param boxter il viaggiatore
     * @param id l'id del viaggio
     * @param completion l'handler che viene chiamato a caricamento ultimato con argomento il viaggio
     * @param autoUpdate se il viaggio che viene caricato dovrà auto aggiornarsi
     */
    public static void load(@NotNull String boxter, @NotNull String id,
                            @NotNull PublicTravelCompletion completion, boolean autoUpdate) {
        Utils.notNull(boxter);
        Utils.notNull(id);
        Utils.notNull(completion);

        DatabaseReference db = DataService.root;
        db.child(getTable(boxter, id)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null) {
                    PublicTravel travel = new PublicTravel(boxter, id);
                    travel.parse(dataSnapshot);
                    if (autoUpdate) travel.autoUpdate();
                    completion.run(travel);
                }
                else completion.run(null);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException().printStackTrace();
                completion.run(null);
            }
        });
    }

    public static void queryForBox(@NotNull Box box, final double maxDistance,
                                   @NotNull PublicTravelCompletion completion,
                                   boolean autoUpdate) {
        Utils.notNull(box);
        Utils.notNull(completion);

        DatabaseReference locRef = DataService.root.child(Travel.TRAVELS_LOCATIONS_KEY);
        GeoFire geoFire = new GeoFire(locRef);


        geoFire.queryAtLocation(box.getArrival().toGeoLocation(), maxDistance)
                .addGeoQueryEventListener(new GeoQueryEventListener() {
                    @Override
                    public void onKeyEntered(String key, GeoLocation location) {
                        //per prima cosa controllo se il viaggio è corretto per il pacco indicato
                        TravelLocationProj travelProj = new TravelLocationProj(key, location);

                        if(!box.sender.equals(travelProj.getBoxter())
                                && !box.getReceiver().equals(travelProj.getBoxter())
                                && travelProj.getArrival().distance(box.getArrival()) <= maxDistance
                                && travelProj.getDeparture().distance(box.getDeparture()) <= maxDistance
                                && travelProj.getEndDate().before(box.getExpiration())) {

                            //il viaggio soddisfa la mia ricerca, quindi lo scarico e lo ritorno
                            PublicTravel.load(travelProj.getBoxter(),
                                    travelProj.getTravelId(), completion, autoUpdate);
                        }
                    }

                    @Override
                    public void onKeyExited(String key) {}

                    @Override
                    public void onKeyMoved(String key, GeoLocation location) {}

                    @Override
                    public void onGeoQueryReady() {}

                    @Override
                    public void onGeoQueryError(DatabaseError error) {
                        //todo: segnala l'errore
                        completion.run(null);
                    }
                }
        );
    }

    /** Aggiunge una funzione che verrà chiamata ogni qual volta l'oggetto viene modificato
     *
     * @param completion la funzione chiamata ad ogni modifica
     */
    public void onUpdate(@NotNull PublicTravelCompletion completion) {
        Utils.notNull(completion);

        onUpdate.add((data) -> completion.run((PublicTravel) data));
    }

    @NotNull
    synchronized public String getBoxter() {
        return boxter;
    }

    @NotNull
    synchronized public Date getStartDate() {
        return startDate;
    }

    @NotNull
    synchronized public Date getEndDate() {
        return endDate;
    }

    @NotNull
    synchronized public Location getDeparture() {
        return departure;
    }

    @NotNull
    synchronized public Location getArrival() {
        return arrival;
    }

    synchronized public double getSpace() {
        return space;
    }

    @NotNull
    synchronized public TravelState getState() {
        return state;
    }

    @NotNull
    public Set<Transport> getTransports() {
        return new HashSet<>(transports);
    }


    /** Aggiunge un listener sulla proprietà state di un pacco del viaggio
     *  Deve essere chiamata dal mittente per aspettare che il boxter confermi
     *  di aver ricevuto il pacco
     *
     * @param sender il mittente del pacco
     * @param boxId l'id del pacco
     * @param completion funzione chiamata quando state è TRAVELLING
     */
    public void waitTravellingConfirm(@NotNull String sender, @NotNull String boxId,
                                      @NotNull ResultCompletion completion) {
        Utils.notNull(sender);
        Utils.notNull(boxId);
        Utils.notNull(completion);

        waitStateConfirm(BoxState.TRAVELLING, sender, boxId, completion);
    }

    public void waitTravellingConfirm(@NotNull AuthorizedBox box,
                                      @NotNull ResultCompletion completion) {
        Utils.notNull(box);
        Utils.notNull(completion);

        waitStateConfirm(BoxState.TRAVELLING, box.sender, box.id, completion);
    }

    public void waitReceivedConfirm(@NotNull String sender, @NotNull String boxId,
                                      @NotNull ResultCompletion completion) {
        Utils.notNull(sender);
        Utils.notNull(boxId);
        Utils.notNull(completion);

        waitStateConfirm(BoxState.RECEIVED, sender, boxId, completion);
    }

    public void waitReceivedConfirm(@NotNull AuthorizedBox box,
                                    @NotNull ResultCompletion completion) {
        Utils.notNull(box);
        Utils.notNull(completion);

        waitStateConfirm(BoxState.RECEIVED, box.sender, box.id, completion);
    }

    //endregion
}
