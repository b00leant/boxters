package com.boxters.model;

import com.boxters.model.completion.AuthorizedBoxCompletion;
import com.boxters.model.completion.ResultCompletion;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by daniele on 30/01/17.
 *
 * Rappresenta le informazioni di un pacco visibili da gli utenti autorizzati dal mittente;
 * non sono presenti setter in quanto le info di un pacco sono settabili solo dal mittente,
 * che utilizzerà un oggetto Box
 */

public class AuthorizedBox extends PublicBox {
    //region: inner workings -----------------------------------------------------------------------

    private static final String DESCRIPTION_KEY = "description";
    private static final String WEIGHT_KEY = "weight";
    private static final String USERS_KEY = "users";
    private static final int QRCODE_LENGTH = 16;
    private static final String QRCODE_KEY = "qrcode";

    private static final String TRAVEL_KEY = "travel";
    private static final String BOXTER_KEY = "boxter";
    private static final String TRAVEL_ID_KEY = "id";

    @Nullable String description;
    double weight;
    private @NotNull String qrCode;

    @Nullable String boxter;
    @Nullable String travelId;

    @NotNull Set<String> authUsers = new HashSet<>();


    AuthorizedBox(@NotNull String sender, @NotNull String id, @NotNull String receiver,
                  double weight, @Nullable String description, @NotNull BoxState state,
                  @Nullable String photo, @NotNull String object, @NotNull Date expiration,
                  double size, @NotNull Location departure, @NotNull Location arrival) {
        super(sender, id, receiver, state, photo, object, expiration, size,  departure, arrival);

        this.weight = weight;
        this.description = description;
        this.qrCode = RandomString.next(QRCODE_LENGTH);

        authUsers.add(receiver);

        boxter = null;
        travelId = null;
    }

    AuthorizedBox(@NotNull String sender, @NotNull String id) {
        super(sender, id);
    }

    @Contract(pure = true)
    private static String getTable(@NotNull String sender, @NotNull String id) {
        return BOXES_KEY + "/" + sender + "/" + id + "/" + AUTHORIZED_KEY;
    }

    @Override @NotNull
    String getTable() {
        return getTable(sender, id);
    }

    @Override @NotNull
    Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();

        map.put(WEIGHT_KEY, weight);
        if(description != null) map.put(DESCRIPTION_KEY, description);

        Map<String, Object> users = new HashMap<>();
        for(String userId: authUsers) {
            users.put(userId, true);
        }
        if(users.isEmpty()) map.put(USERS_KEY, false);
        else map.put(USERS_KEY, users);

        if(boxter != null) {
            Map<String, Object> travelMap = new HashMap<>();
            travelMap.put(TRAVEL_ID_KEY, travelId);
            travelMap.put(BOXTER_KEY, boxter);

            map.put(TRAVEL_KEY, travelMap);
        }

        map.put(QRCODE_KEY, qrCode);

        map.put(PUBLIC_KEY, super.toMap());

        return map;
    }

    @Override
    synchronized void parse(@NotNull DataSnapshot snapshot) {

        this.weight = ((Number) snapshot.child(WEIGHT_KEY).getValue()).doubleValue();
        if(snapshot.hasChild(DESCRIPTION_KEY)) {
            this.description = (String) snapshot.child(DESCRIPTION_KEY).getValue();
        }
        else description = null;

        authUsers.clear();
        for(DataSnapshot authUser: snapshot.child(USERS_KEY).getChildren()) {
            if((Boolean) authUser.getValue()) authUsers.add(authUser.getKey());
        }

        this.qrCode = (String) snapshot.child(QRCODE_KEY).getValue();

        if(snapshot.hasChild(TRAVEL_KEY)) {
            DataSnapshot travelS = snapshot.child(TRAVEL_KEY);

            boxter = (String) travelS.child(BOXTER_KEY).getValue();
            travelId = (String) travelS.child(TRAVEL_ID_KEY).getValue();
        }

        super.parse(snapshot.child(PUBLIC_KEY));
    }

    //endregion ------------------------------------------------------------------------------------


    //region: public interface ---------------------------------------------------------------------


    /** Carica dal server i dati visibili di un pacco da un utente
     * che è stato autorizzato da parte del mittente
     *
     * @param sender il mittente del pacco
     * @param id l'id del pacco
     * @param completion l'handler che viene chiamato a caricamento ultimato con argomento il pacco
     * @param autoUpdate se il pacco che viene caricato dovrà auto aggiornarsi
     */
    public static void load(@NotNull String sender, @NotNull String id,
                            @NotNull AuthorizedBoxCompletion completion, boolean autoUpdate) {
        Utils.notNull(sender);
        Utils.notNull(id);
        Utils.notNull(completion);

        DatabaseReference db = DataService.root;
        db.child(getTable(sender, id)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null) {
                    AuthorizedBox box = new AuthorizedBox(sender, id);
                    box.parse(dataSnapshot);
                    if(autoUpdate) box.autoUpdate();
                    completion.run(box);
                }
                else completion.run(null);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException().printStackTrace();
                completion.run(null);
            }
        });
    }

    /** Aggiunge una funzione che verrà chiamata ogni qual volta l'oggetto viene modificato
     *
     * @param completion la funzione chiamata ad ogni modifica
     */
    public void onUpdate(@NotNull AuthorizedBoxCompletion completion) {
        Utils.notNull(completion);

        onUpdate.add((data) -> completion.run((AuthorizedBox) data));
    }

    @Nullable
    synchronized public String getDescription() {
        return description;
    }

    synchronized public double getWeight() {
        return weight;
    }

    synchronized public String getQRCode() {
        return qrCode;
    }

    /** Aggiunge un listener sulla proprietà state del pacco
     *  Deve essere chiamata dal boxter per attendere che 
     *  il mittente dia conferma di aver consegnato il pacco
     *
     * @param completion funzione chiamata quando state è TRAVELLING
     */
    public void waitTravellingConfirm(@NotNull ResultCompletion completion) {
        Utils.notNull(completion);

        DatabaseReference db = DataService.root.child(getTable())
                .child(PUBLIC_KEY + "/" + STATE_KEY);

        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot != null) {
                    BoxState state = BoxState.valueOf((String) dataSnapshot.getValue());
                    if(state == BoxState.TRAVELLING) {
                        completion.run(true);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                completion.run(false);
            }
        });

    }

    @Nullable
    synchronized public String getBoxter() {
        return boxter;
    }

    @Nullable
    synchronized public String getTravelId() {
        return travelId;
    }

    //endregion
}
