package com.boxters.model;

import org.jetbrains.annotations.NotNull;

import com.boxters.model.BasicUser;
import com.boxters.model.PublicUser;
import com.google.firebase.database.DataSnapshot;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniele on 13/02/17.
 *
 * Si è deciso di rendere questa classe una semplice astrazione di valore per semplicità e prestazioni
 */

class UserQueryResult implements BasicUser {

    @NotNull private final String id;
    @NotNull private final String name;
    @NotNull private final String email;

    UserQueryResult(@NotNull DataSnapshot snapshot) {
        id = snapshot.getKey();
        name = (String) snapshot.child(PublicUser.NAME_KEY).getValue();
        email = (String) snapshot.child(PublicUser.EMAIL_KEY).getValue();

    }

    UserQueryResult(@NotNull PublicUser user) {
        id = user.id;
        name = user.name;
        email = user.email;
    }

    @NotNull
    Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();

        map.put(PublicUser.NAME_KEY, name);
        map.put(PublicUser.EMAIL_KEY, email);

        return map;
    }

    @NotNull
    @Override
    public String getId() {
        return id;
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @NotNull
    @Override
    public String getEmail() {
        return email;
    }
}
