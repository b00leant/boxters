package com.boxters.model;

import com.boxters.model.completion.PublicBoxCompletion;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniele on 30/01/17.
 *
 * Contiene le informazioni di un pacco accessibili da tutti gli utenti
 */

public class PublicBox extends DBData {
    //region: inner workings -----------------------------------------------------------------------

    static final String STATE_KEY = "state";
    private static final String PHOTO_KEY = "photo";
    private static final String OBJECT_KEY = "object";
    private static final String EXPIRATION_KEY = "expiration";
    private static final String SIZE_KEY = "size";
    private static final String DEPARTURE_KEY = "departure";
    private static final String ARRIVAL_KEY = "arrival";
    static final String BOXES_KEY = "boxes";
    private static final String RECEIVER_KEY = "receiver";
    static final String PUBLIC_KEY = "public";
    static final String AUTHORIZED_KEY = "authorized";

    @NotNull
    final String sender;
    @NotNull BoxState state;
    @Nullable String photo;
    @NotNull String object;
    @NotNull Date expiration;

    double size;
    private @NotNull String receiver;

    @NotNull Location departure;
    @NotNull Location arrival;

    PublicBox(@NotNull String sender, @NotNull String id, @NotNull String receiver, @NotNull BoxState state,
                     @Nullable String photo, @NotNull String object, @NotNull Date expiration,
                     double size, @NotNull Location departure, @NotNull Location arrival) {
        super(id);
        this.sender = sender;
        this.receiver = receiver;

        this.state = state;
        this.photo = photo;
        this.object = object;
        this.expiration = expiration;

        this.size = size;

        this.departure = departure;
        this.arrival = arrival;
    }

    PublicBox(@NotNull String sender, @NotNull String id) {
        super(id);
        this.sender = sender;
    }

    @Contract(pure = true)
    private static String getTable(@NotNull String sender, @NotNull String id) {
        return BOXES_KEY + "/" + sender + "/" + id + "/"+AUTHORIZED_KEY+"/"+PUBLIC_KEY;
    }

    @NotNull @Override
    String getTable() {
        return getTable(sender, id);
    }

    @NotNull
    @Override
    Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();

        map.put(STATE_KEY, state.name());
        map.put(OBJECT_KEY, object);
        map.put(EXPIRATION_KEY, expiration.getTime());
        map.put(SIZE_KEY, size);
        map.put(DEPARTURE_KEY, departure.toMap());
        map.put(ARRIVAL_KEY, arrival.toMap());

        map.put(RECEIVER_KEY, receiver);
        if(photo != null) map.put(PHOTO_KEY, photo);

        return map;
    }

    @Override
    synchronized void parse(@NotNull DataSnapshot snapshot) {

        this.state = BoxState.valueOf((String) snapshot.child(STATE_KEY).getValue());
        this.object = (String) snapshot.child(OBJECT_KEY).getValue();
        this.expiration = new Date((Long) snapshot.child(EXPIRATION_KEY).getValue());

        this.size = ((Number) snapshot.child(SIZE_KEY).getValue()).doubleValue();

        this.departure = new Location(snapshot.child(DEPARTURE_KEY));
        this.arrival = new Location(snapshot.child(ARRIVAL_KEY));

        this.receiver = (String) snapshot.child(RECEIVER_KEY).getValue();

        if(snapshot.hasChild(PHOTO_KEY)) {
            this.photo = (String) snapshot.child(PHOTO_KEY).getValue();
        }
        else photo = null;
    }

    @NotNull
    String debugId() {
        return debugId(sender, id);
    }

    @Contract(pure = true)
    @NotNull
    static String debugId(@NotNull String sender, @NotNull String id) {
        return "(" + sender + ", " + id + ")";
    }

    //endregion ------------------------------------------------------------------------------------


    //region: public interface ---------------------------------------------------------------------

    /** Carica dal server i dati visibili di un pacco da un utente
     * che non ha ancora ricevuto alcuna autorizzazione da parte del mittente
     *
     * @param sender il mittente del pacco
     * @param id l'id del pacco
     * @param completion l'handler che viene chiamato a caricamento ultimato con argomento il pacco
     * @param autoUpdate se il pacco che viene caricato dovrà auto aggiornarsi
     */
    public static void load(@NotNull String sender, @NotNull String id,
                            @NotNull PublicBoxCompletion completion, boolean autoUpdate) {
        Utils.notNull(sender);
        Utils.notNull(id);
        Utils.notNull(completion);

        DatabaseReference db = DataService.root;
        db.child(getTable(sender, id)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null) {
                    PublicBox box = new PublicBox(sender, id);
                    box.parse(dataSnapshot);
                    if(autoUpdate) box.autoUpdate();
                    completion.run(box);
                }
                else completion.run(null);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException().printStackTrace();
                completion.run(null);
            }
        });
    }

    /** Ritorna tutti i pacchi che soddisfano un determinato viaggio
     *
     * @param travel il viaggio di cui si effettua la query
     * @param completion la funzione chiamata per ogni pacco trovato
     * @param autoUpdate se il pacco si dovrà autoAggiornare
     */
    public static void queryForTravel(@NotNull Travel travel, final double maxDistance,
                                      @NotNull PublicBoxCompletion completion,
                                      boolean autoUpdate) {
        Utils.notNull(travel);
        Utils.notNull(completion);

        DatabaseReference locRef = DataService.root.child(Box.BOXES_LOCATIONS_KEY);
        GeoFire geoFire = new GeoFire(locRef);

        geoFire.queryAtLocation(travel.getArrival().toGeoLocation(), maxDistance)
                .addGeoQueryEventListener(new GeoQueryEventListener() {
                    @Override
                    public void onKeyEntered(String key, GeoLocation location) {
                        BoxLocationProj boxProj = new BoxLocationProj(key, location);
                        if(!travel.boxter.equals(boxProj.getSenderId())
                                && boxProj.getArrival().distance(travel.getArrival()) < maxDistance
                                && boxProj.getDeparture().distance(travel.getDeparture()) < maxDistance
                                && boxProj.getExpiration().after(travel.getEndDate())) {

                            PublicBox.load(boxProj.getSenderId(), boxProj.getBoxId(),
                                    completion, autoUpdate);
                        }
                    }

                    @Override
                    public void onKeyExited(String key) {}

                    @Override
                    public void onKeyMoved(String key, GeoLocation location) {}

                    @Override
                    public void onGeoQueryReady() {}

                    @Override
                    public void onGeoQueryError(DatabaseError error) {
                        //todo: segnala l'errore
                        completion.run(null);
                    }
                }
        );

    }

    /** Aggiunge una funzione che verrà chiamata ogni qual volta l'oggetto viene modificato
     *
     * @param completion la funzione chiamata ad ogni modifica
     */
    public void onUpdate(@NotNull PublicBoxCompletion completion) {
        Utils.notNull(completion);

        onUpdate.add((data) -> completion.run((PublicBox) data));
    }

    @NotNull
    synchronized public String getSender() { return sender; }

    @NotNull
    synchronized public String getReceiver() {
        return receiver;
    }

    @NotNull
    synchronized public BoxState getState() {
        return state;
    }

    @Nullable
    synchronized public String getPhoto() {
        return photo;
    }

    @NotNull
    synchronized public String getObject() {
        return object;
    }

    @NotNull
    synchronized public Date getExpiration() {
        return expiration;
    }

    synchronized public double getSize() { return size; }

    @NotNull
    synchronized public Location getDeparture() {
        return departure;
    }

    @NotNull
    synchronized public Location getArrival() {
        return arrival;
    }
    //endregion
}
