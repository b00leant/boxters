package com.boxters.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.boxters.model.completion.AuthorizedBoxCompletion;
import com.boxters.model.completion.Completion;
import com.boxters.model.completion.SaveCompletion;
import com.boxters.model.completion.TravelCompletion;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.Contract;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by daniele on 01/02/17.
 *
 * Rappresenta un viaggio dell'utente loggato, possiede tutte le informazioni sul viaggio ed è
 * utilizzate solo se l'utente loggato è la persona che effettua il viaggio
 */

public class Travel extends PublicTravel {
    //region: inner workings -----------------------------------------------------------------------

    static final String TRAVELS_LOCATIONS_KEY = "travelsLocations";

    static final String BOXES_KEY = "boxes";

    public static class TravelBoxProj {
        private static final String RECEIVER_KEY = "receiver";
        static final String STATE_KEY = "state";

        @NotNull private final String receiver;
        @NotNull private final BoxState state;


        public TravelBoxProj(@NotNull String receiver, @NotNull BoxState state) {
            this.receiver = receiver;
            this.state = state;
        }

        Map<String, Object> toMap() {
            Map<String, Object> map = new HashMap<>();

            map.put(RECEIVER_KEY, receiver);
            map.put(STATE_KEY, state.name());

            return map;
        }

        TravelBoxProj(@NotNull DataSnapshot snapshot) {
            this.receiver = (String) snapshot.child(RECEIVER_KEY).getValue();
            this.state = BoxState.valueOf((String) snapshot.child(STATE_KEY).getValue());
        }

        @NotNull
        public String getReceiver() {
            return receiver;
        }

        @NotNull
        public BoxState getState() {
            return state;
        }
    }

    @NotNull private final Map<String, Map<String, TravelBoxProj>> boxes = new HashMap<>();

    private Travel(@NotNull String boxter, @NotNull String id,
                   @NotNull Date startDate, @NotNull Date endDate,
                   double space, @NotNull Set<Transport> transports,
                   @NotNull Location departure, @NotNull Location arrival,
                   @NotNull TravelState state) {
        super(boxter, id, startDate, endDate, space, transports, departure, arrival, state);
    }

    private Travel(@NotNull String boxter, @NotNull String id) {
        super(boxter, id);
    }

    @Contract(pure = true)
    static String getTable(@NotNull String boxter, @NotNull String id) {
        return TRAVELS_KEY + "/" + boxter + "/" + id;
    }

    @NotNull @Override
    String getTable() {
        return getTable(boxter, id);
    }

    @NotNull @Override
    Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();

        Map<String, Object> sendersMap = new HashMap<>();
        for(Map.Entry<String, Map<String, TravelBoxProj>> t: boxes.entrySet()) {

            Map<String, Map<String, Object>> boxesMap = new HashMap<>();
            for(Map.Entry<String, TravelBoxProj> travel: t.getValue().entrySet()) {
                boxesMap.put(travel.getKey(), travel.getValue().toMap());
            }

            sendersMap.put(t.getKey(), boxesMap);
        }
        if(sendersMap.isEmpty()) map.put(BOXES_KEY, false);
        else map.put(BOXES_KEY, sendersMap);

        map.put(PUBLIC_KEY, super.toMap());

        return map;
    }

    @Override
    synchronized void parse(@NotNull DataSnapshot snapshot) {

        DataSnapshot boxesS = snapshot.child(BOXES_KEY);

        for(DataSnapshot senderS: boxesS.getChildren()) {
            Map<String, TravelBoxProj> boxesMap = new HashMap<>();

            for(DataSnapshot boxS: senderS.getChildren()) {
                boxesMap.put(boxS.getKey(), new TravelBoxProj(boxS));
            }

            this.boxes.put(senderS.getKey(), boxesMap);
        }

        super.parse(snapshot.child(PUBLIC_KEY));
    }

    @Override
    void update(@NotNull Map<String, Object> updates, @Nullable SaveCompletion completion) {
        super.update(updates, (res) -> {
            if(res != null && res) {
                //salva la location per rendere il pacco trovabile se state == CREATED
                if(state == TravelState.CREATED) {
                    DatabaseReference locRef = DataService.root.child(TRAVELS_LOCATIONS_KEY);
                    GeoFire geoFire = new GeoFire(locRef);

                    geoFire.setLocation(TravelLocationProj.key(this),
                            new GeoLocation(arrival.getLat(), arrival.getLon()), (key, error) -> {
                                //todo: segnala l'errore
                                if (completion != null) completion.run(error == null);
                            });
                }
            }
            else if (completion != null) completion.run(false);
        });
    }

    private void checkModifiability() {
        if(state != TravelState.CREATED) {
            throw new IllegalActionException("Un viaggio può essere modificato solo prima della partenza");

        }
    }

    /** setta una proprietà dell'oggetto per cui è necessario aggiornare anche la chiave della locazione
     *
     * @param completion la funzione chiamata a set terminato
     * @param setter la funzione in cui vengono settati i valori
     */
    private void setUpdatingLoc(SaveCompletion completion, Completion<Void> setter) {
        checkModifiability();

        DatabaseReference travelLoc = DataService.root
                .child(TRAVELS_LOCATIONS_KEY + "/" + TravelLocationProj.key(this));

        travelLoc.removeValue((databaseError, databaseReference) -> {
            if(databaseError != null) {
                //todo: logga l'errore
                if(completion!= null) completion.run(false);
            }
            else {
                setter.run(null);
                save(completion);
            }
        });
    }

    //endregion ------------------------------------------------------------------------------------


    //region: public interface ---------------------------------------------------------------------

    /** Carica dal server il pacco con id specificato
     *
     * @param boxter il viaggiatore
     * @param id l'id del viaggio da caricare dal server
     * @param completion l'handler che viene chiamato a caricamento ultimato con argomento il viaggio
     * @param autoUpdate se il viaggio che viene caricato dovrà auto aggiornarsi
     */
    public static void load(@NotNull String boxter, @NotNull String id,
                            @NotNull TravelCompletion completion, boolean autoUpdate) {
        Utils.notNull(boxter);
        Utils.notNull(id);
        Utils.notNull(completion);

        DatabaseReference db = DataService.root;
        db.child(getTable(boxter, id)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null) {
                    Travel travel = new Travel(boxter, id);
                    travel.parse(dataSnapshot);
                    if(autoUpdate) travel.autoUpdate();
                    completion.run(travel);
                }
                else completion.run(null);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException().printStackTrace();
                completion.run(null);
            }
        });
    }


    /** Carica tutti i viaggi di un dato utente.
     * L'utente deve essere loggato, in caso contrario si verificherà un errore nell'accesso al db
     *
     * @param boxter l'utente di cui interessano i viaggi
     * @param completion la funzione chiamata per ogni viaggio trovato
     * @param autoUpdate se ogni pacco trovato si deve aggiornare
     */
    public static void loadAll(@NotNull String boxter, @NotNull TravelCompletion completion,
                               boolean autoUpdate) {
        Utils.notNull(boxter);
        Utils.notNull(completion);

        DatabaseReference db = DataService.root;
        db.child(TRAVELS_KEY + "/" + boxter).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Travel travel = new Travel(boxter, dataSnapshot.getKey());
                travel.parse(dataSnapshot);
                if(autoUpdate) travel.autoUpdate();
                completion.run(travel);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {
                completion.run(null);
            }
        });
    }


    /** Permette di creare un nuovo viaggio dell'utente loggato (che deve essere boxter)
     *
     * @param boxter il viaggiatore
     * @param startDate la data di partenza
     * @param endDate la data di arrivo
     * @param departure il luogo di partenza
     * @param arrival il luogo di arrivo
     * @param completion l'handler a cui viene passato il viaggio creato a creazione ultimata
     * @param autoUpdate specifica se il viaggio deve auto aggiornarsi quando le info nel server sono aggiornate
     */
    public static void create(@NotNull String boxter, @NotNull Date startDate, @NotNull Date endDate,
                              double space, @NotNull Set<Transport> transports,
                              @NotNull Location departure, @NotNull Location arrival,
                              @Nullable TravelCompletion completion, boolean autoUpdate) {
        Utils.notNull(boxter);
        Utils.notNull(startDate);
        Utils.notNull(endDate);
        Utils.notNull(departure);
        Utils.notNull(arrival);
        Utils.notNull(transports);

        //todo: aggiungere gli attributi space e transport

        DatabaseReference db = DataService.root;
        String id = db.child(BOXES_KEY + "/" + boxter).push().getKey();

        Travel travel = new Travel(boxter, id, startDate, endDate, space,
                new HashSet<>(transports), departure, arrival, TravelState.CREATED);

        travel.save((res) -> {
            if(res != null && res) {
                if (autoUpdate) travel.autoUpdate();
                if(completion != null) completion.run(travel);

            }
            else if (completion != null) completion.run(null);

        });
    }


    /** Aggiunge una funzione che verrà chiamata ogni qual volta l'oggetto viene modificato
     *
     * @param completion la funzione chiamata ad ogni modifica
     */
    public void onUpdate(@NotNull TravelCompletion completion) {
        Utils.notNull(completion);

        onUpdate.add((data) -> completion.run((Travel) data));
    }


    @Nullable
    synchronized public TravelBoxProj getBox(@NotNull String sender, @NotNull String id) {
        Utils.notNull(sender);
        Utils.notNull(id);


        Map<String, TravelBoxProj> senderMap = this.boxes.get(sender);
        if(senderMap == null) return null;

        return senderMap.get(id);
    }

    synchronized public void loadBoxes(@NotNull AuthorizedBoxCompletion completion,
                                       boolean autoUpdate) {
        Utils.notNull(completion);

        for(Map.Entry<String, Map<String, TravelBoxProj>> sender: boxes.entrySet()) {
            for(Map.Entry<String, TravelBoxProj> box: sender.getValue().entrySet()) {
                AuthorizedBox.load(sender.getKey(), box.getKey(), completion, autoUpdate);
            }
        }
    }

    synchronized public void addBox(@NotNull String sender, @NotNull String id,
                       @NotNull TravelBoxProj box, @Nullable SaveCompletion completion) {
        Utils.notNull(sender);
        Utils.notNull(id);
        Utils.notNull(box);

        checkModifiability();

        if (!boxes.containsKey(sender)) boxes.put(sender, new HashMap<>());

        Map<String, TravelBoxProj> senderMap = this.boxes.get(sender);
        senderMap.put(id, box);

        save(completion);
    }

    synchronized public void setStartDate(@NotNull Date startDate, @Nullable SaveCompletion completion) {
        Utils.notNull(startDate);

        checkModifiability();

        this.startDate = startDate;
        save(completion);
    }

    synchronized public void setEndDate(@NotNull Date endDate, @Nullable SaveCompletion completion) {
        Utils.notNull(endDate);

        setUpdatingLoc(completion, (_void) -> this.endDate = endDate);
    }

    synchronized public void setDeparture(@NotNull Location departure, @Nullable SaveCompletion completion) {
        Utils.notNull(departure);

        setUpdatingLoc(completion, (_void) -> this.departure = departure);
    }

    synchronized public void setArrival(@NotNull Location arrival, @Nullable SaveCompletion completion) {
        Utils.notNull(arrival);

        setUpdatingLoc(completion, (_void) -> this.arrival = arrival);
    }

    synchronized public void setState(@NotNull TravelState state, @Nullable SaveCompletion completion) {
        Utils.notNull(state);

        if(state == TravelState.CREATED) {
            throw new IllegalActionException("Operazione non consentita: si sta provando al passare dallo stato "
                    + this.state.name() + " allo stato " + state.name());
        }

        setUpdatingLoc(completion, (_void) -> this.state = state);
    }

    synchronized public void setSpace(double space, @Nullable SaveCompletion completion) {
        setUpdatingLoc(completion, (_void) -> this.space = space);
    }

    synchronized public void addTransport(@NotNull Transport transport,
                                          @Nullable SaveCompletion completion) {
        Utils.notNull(transport);

        setUpdatingLoc(completion, (_void) -> this.transports.add(transport));
    }

    synchronized public void removeTransport(@NotNull Transport transport,
                                          @Nullable SaveCompletion completion) {
        Utils.notNull(transport);

        setUpdatingLoc(completion, (_void) -> this.transports.remove(transport));
    }

    synchronized public void updateBoxState(@NotNull BoxState newState, @NotNull String sender,
                                            @NotNull String boxId, @Nullable SaveCompletion completion) {

        Utils.notNull(newState);
        Utils.notNull(sender);
        Utils.notNull(boxId);

        Map<String, TravelBoxProj> boxes = this.boxes.get(sender);
        if(boxes == null || boxes.get(boxId) == null)
            throw new IllegalActionException(
                    "Si è chiesto di modificare lo stato del pacco " + PublicBox.debugId(sender, boxId)
                            + " non portato dal viaggio " + debugId());

        TravelBoxProj box = boxes.get(boxId);

        //controllo che il cambiamento di stato sia ammissibile
        if(newState == BoxState.CREATED
                || (newState == BoxState.CONFIRMED && box.state != BoxState.CREATED && box.state != BoxState.CONFIRMED )
                || (newState == BoxState.TRAVELLING && box.state != BoxState.CONFIRMED && box.state != BoxState.TRAVELLING )
                || (newState == BoxState.RECEIVED && box.state != BoxState.TRAVELLING && box.state != BoxState.RECEIVED )
                ) {
            throw new IllegalActionException(
                    "Si sta provando a modificare lo stato del pacco " + PublicBox.debugId(sender, boxId)
                    + " nel viaggio " + debugId() + " da " + box.state + " a " + newState
            );
        }

        //la modifica è lecita
        TravelBoxProj updated = new TravelBoxProj(box.receiver, newState);
        boxes.put(boxId, updated);
        save(completion);

    }

    public void delete(@Nullable SaveCompletion completion) {

        checkModifiability();

        for(Map<String, TravelBoxProj> boxTrav: boxes.values()) {
            for(TravelBoxProj boxProj: boxTrav.values()) {
                if(boxProj.getState() != BoxState.CREATED) {
                    throw new IllegalActionException("Si sta provando a cancellare un viaggio che ha già confermato uno dei pacchi");
                }
            }
        }

        super.delete(completion);

    }

    //endregion ------------------------------------------------------------------------------------


}
