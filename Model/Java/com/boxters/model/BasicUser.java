package com.boxters.model;

import org.jetbrains.annotations.NotNull;

/**
 * Created by daniele on 13/02/17.
 */

public interface BasicUser {
    @NotNull String getId();
    @NotNull String getName();
    @NotNull String getEmail();
}
