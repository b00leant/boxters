package com.boxters.model.completion;

/**
 * Created by daniele on 22/02/17.
 */

public interface ResultCompletion extends Completion<Boolean> {
}
