package com.boxters.model.completion;

import com.boxters.model.User;

public interface UserCompletion extends Completion<User> {}
