package com.boxters.model.completion;

import com.boxters.model.BasicUser;

/**
 * Created by daniele on 13/02/17.
 */

public interface BasicUserCompletion extends Completion<BasicUser> {}
