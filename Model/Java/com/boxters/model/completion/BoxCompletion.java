package com.boxters.model.completion;

import com.boxters.model.Box;

public interface BoxCompletion extends Completion<Box> {}
