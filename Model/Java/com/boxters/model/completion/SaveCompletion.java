package com.boxters.model.completion;

/**
 * Created by daniele on 21/02/17.
 */

public interface SaveCompletion extends Completion<Boolean> {}
