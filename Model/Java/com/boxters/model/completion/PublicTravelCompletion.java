package com.boxters.model.completion;

import com.boxters.model.PublicTravel;

/**
 * Created by daniele on 01/02/17.
 */

public interface PublicTravelCompletion extends Completion<PublicTravel> {}
