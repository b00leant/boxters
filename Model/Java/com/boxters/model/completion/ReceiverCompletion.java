package com.boxters.model.completion;

import com.boxters.model.Receiver;

/**
 * Created by daniele on 10/02/17.
 */

public interface ReceiverCompletion extends Completion<Receiver> {

}
