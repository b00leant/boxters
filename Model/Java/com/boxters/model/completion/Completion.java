package com.boxters.model.completion;

/**
 * Created by daniele on 29/01/17.
 *
 * Le interfaccie completion sono utilizzate per passare come parametro una funzione
 * che sarà eseguita una volta terminata l'operazione chiamata
 */

public interface Completion<T> {
    void run(T par);
}

