package com.boxters.model.completion;

import com.boxters.model.PublicBox;

public interface PublicBoxCompletion extends Completion<PublicBox> {}
