package com.boxters.model.completion;

import com.boxters.model.Travel;

/**
 * Created by daniele on 01/02/17.
 */

public interface TravelCompletion extends Completion<Travel> {
}
