package com.boxters.model.completion;

import com.boxters.model.PublicUser;

public interface PublicUserCompletion extends Completion<PublicUser> {}
