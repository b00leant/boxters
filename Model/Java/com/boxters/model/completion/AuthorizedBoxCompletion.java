package com.boxters.model.completion;

import com.boxters.model.AuthorizedBox;

public interface AuthorizedBoxCompletion extends Completion<AuthorizedBox> {}
