package com.boxters.model;

import com.firebase.geofire.GeoLocation;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

/**
 * Created by daniele on 05/02/17.
 */

class TravelLocationProj extends LocKeyFormatter {
    @NotNull private static final String LOC_KEY_SEPARATOR = "*";

    @NotNull private final String boxter;
    @NotNull private final String travelId;
    @NotNull private final Location arrival;
    @NotNull private final Location departure;
    @NotNull private final Date endDate;

    public TravelLocationProj(@NotNull String key, @NotNull GeoLocation location) {
        String[] args = key.split("\\" + LOC_KEY_SEPARATOR);
        if(args.length != 5) {
            throw new InternalError("Chiave errata in " + Box.BOXES_LOCATIONS_KEY);
        }

        boxter = args[0];
        travelId = args[1];
        arrival = new Location(location);
        departure = new Location(null, parseKeyToDouble(args[2]), parseKeyToDouble(args[3]));
        endDate = new Date(Long.parseLong(args[4]));
    }

    static String key(@NotNull Travel travel) {
        return travel.boxter + LOC_KEY_SEPARATOR + travel.id + LOC_KEY_SEPARATOR
                + formatDoubleToKey(travel.departure.getLat()) + LOC_KEY_SEPARATOR
                + formatDoubleToKey(travel.departure.getLon()) + LOC_KEY_SEPARATOR
                + travel.endDate.getTime();
    }

    @NotNull
    public String getBoxter() {
        return boxter;
    }

    @NotNull
    public String getTravelId() {
        return travelId;
    }

    @NotNull
    public Location getArrival() {
        return arrival;
    }

    @NotNull
    public Location getDeparture() {
        return departure;
    }

    @NotNull
    public Date getEndDate() {
        return endDate;
    }
}
