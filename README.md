## What is Boxters

Boxters is an application developed for the Google Technologies for Cloud and Web Development Workshop 2016/2017.
![](https://github.com/danielecominu/Boxters/blob/master/Idea/19577384_1825084877819889_891124188272444154_o.jpg)

This app gives the opportunity to users to send/receive objects delivered by a third person who is travelling, the "Boxter".
 
## Motivation

The idea of using general travelers to deliver objects for the users was born during winter 2016, when one of the students was shipping
something for his cousin.

## Presentation

Check our [presentation video](https://danielecominu.github.io/Boxters/GooglePlayLink/gp.html)

## Installation

You can try our project cloning the Android directory.

## Links

[Official Website](http://www.boxters.it)

[Facebook Page](https://www.facebook.com/BoxtersIta)

## Our material

[UI/UX evolution](https://danielecominu.github.io/Boxters/UIUX/uiux.html)

[Video explanation](https://www.youtube.com/watch?v=WhucV2r2JJY)

[Survey](https://docs.google.com/presentation/d/1iu1A640R2IyGRnvowDw6mT_MrWc10yvvfbQfEC1uGIo/edit#slide=id.p) (101 answers by now)

[Terms & Conditions](https://danielecominu.github.io/Boxters/Idea/terms_and_conditions.html)

[Used Technologies](https://danielecominu.github.io/Boxters/Idea/diagram.html)

[Coding Calendar](https://docs.google.com/spreadsheets/d/1uDLg2FEunlYqWCAHvKguqiBP_KvIN4F1u_PqajLQ_Eo/edit#gid=0)

[Our Plans](https://danielecominu.github.io/Boxters/Idea/presentation.html)

[Mockup](https://danielecominu.github.io/Boxters/Mockup/Mockup.html)

[Documentation](https://danielecominu.github.io/Boxters/DocumentationBoxters.pdf)

 
## Contributors

##### Students:
- Daniele Cominu (backend, client/server interaction, authentication)

- David Ghedalia (forms, planning, mockups)

- Davide Gimondo (main activity, menu summaries, joined queries, logo)

- Stefano Latini (frontend, profile activity, map activity, forms)


##### Mentors:

- Daniele Paolacci

- Bruno Ambrogio
