package com.boxters.server.server;

/**
 * Created by daniele on 28/02/17.
 */
class Const {
    // For the GCM connection
    static final String FCM_SERVER = "fcm-xmpp.googleapis.com";
    static final int FCM_PORT = 5236;
    static final String FCM_ELEMENT_NAME = "gcm";
    static final String FCM_NAMESPACE = "google:mobile:data";
    static final String FCM_SERVER_CONNECTION = "gcm.googleapis.com";

    // For the app common payload message attributes (android - xmpp server)
}
