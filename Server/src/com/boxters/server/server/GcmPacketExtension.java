package com.boxters.server.server;

import org.jivesoftware.smack.packet.DefaultPacketExtension;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;

/**
 * XMPP Packet Extension for GCM Cloud Connection Server
 */
class GcmPacketExtension extends DefaultPacketExtension {

	private String json;

	GcmPacketExtension(String json) {
		super(Const.FCM_ELEMENT_NAME, Const.FCM_NAMESPACE);
		this.json = json;
	}

	String getJson() {
		return json;
	}

	@Override
	public String toXML() {
		// TODO: Do we need to scape the json? StringUtils.escapeForXML(json)
		return String.format("<%s xmlns=\"%s\">%s</%s>", Const.FCM_ELEMENT_NAME, Const.FCM_NAMESPACE, json,
				Const.FCM_ELEMENT_NAME);
	}

	Packet toPacket() {
		Message message = new Message();
		message.addExtension(this);
		return message;
	}
}
