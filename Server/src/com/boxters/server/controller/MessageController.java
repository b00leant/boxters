package com.boxters.server.controller;

import com.boxters.model.messaging.BoxtersPayload;
import com.boxters.model.messaging.CcsInMessage;
import com.boxters.server.server.CcsClient;
import org.jetbrains.annotations.NotNull;

/**
 * Created by daniele on 28/02/17.
 */
public interface MessageController {
    void onMessageReceived(@NotNull CcsInMessage message,
                           @NotNull BoxtersPayload payload);


    void setClient(@NotNull CcsClient ccsClient);
}
