package com.boxters.server.controller;

import com.boxters.model.messaging.BoxtersPayload;
import com.boxters.model.messaging.CcsInMessage;
import com.boxters.model.messaging.CcsOutMessage;
import com.boxters.server.server.CcsClient;
import com.boxters.server.server.MessageHelper;
import org.jetbrains.annotations.NotNull;

/**
 * Created by daniele on 28/02/17.
 */
public class BoxtersMessageController implements MessageController {

    @NotNull private CcsClient client;

    @Override
    public void onMessageReceived(@NotNull CcsInMessage message,
                                  @NotNull BoxtersPayload payload) {
        System.out.println("Message received on server: " + payload.getType().name());

        CcsOutMessage outMessage = new CcsOutMessage(payload.getReceiverToken(), payload.computeMessageId(true));
        String jsonRequest = MessageHelper.createJsonOutMessage(outMessage, payload);
        client.send(jsonRequest);
    }

    @Override
    public void setClient(@NotNull CcsClient client) {
        this.client = client;
    }
}
