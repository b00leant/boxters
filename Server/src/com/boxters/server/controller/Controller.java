package com.boxters.server.controller;

import com.boxters.model.messaging.BoxtersPayload;
import com.boxters.model.messaging.CcsOutMessage;
import com.boxters.model.messaging.TestPayload;
import com.boxters.server.server.CcsClient;
import com.boxters.server.server.MessageHelper;
import com.boxters.server.utils.Utils;
import org.jetbrains.annotations.NotNull;
import org.jivesoftware.smack.XMPPException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by daniele on 28/02/17.
 */
public class Controller {
    private static final String fcmProjectSenderId = "555864418922";
    private static final String fcmServerKey = "AAAAgWwZ6mo:APA91bGoPvKUBt80IC9y8W9Z2pci4TtC_6y-viHjW-YfBSc32WhJjhCK5gi0eSismO4Xn_9YrIcFYltkNRWdYjgDdaOTPRL00dGdVvt3oI2tBPgvic3LOcinUsNhz6Tkr4YaTTovRnUw";

    public static void main(String[] args) {
        startServer();
    }

    private static void createXMPPServer() {
        MessageController messageController = new BoxtersMessageController();

        CcsClient ccsClient = CcsClient.prepareClient(messageController, fcmProjectSenderId, fcmServerKey, false);

        try {
            ccsClient.connect();
        } catch (XMPPException e) {
            e.printStackTrace();
        }

        ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
        ses.scheduleAtFixedRate(ccsClient::reconnect, 6, 6, TimeUnit.HOURS);
    }

    private static void sendTestMessage(@NotNull CcsClient client) {

        final String testReceiver = "fo8V5jE8fXc:APA91bGX2iDZQEk_I6jHG_rlJGN7u1cw1yvJHuPAJwmnZETVNIrt-9oLZJa0j4iZvbXxEi4FpJ1mwMi9kQRYfRuDJ-GS4BpKl7nezYQIpjkNyekexp6vI6Z95ERRogtI8_fnaiWn7scw";

        // Send a sample downstream message to a device
        String messageId = Utils.getUniqueMessageId();

        BoxtersPayload dataPayload = new TestPayload("Il mittente", testReceiver);

        CcsOutMessage message = new CcsOutMessage(testReceiver, messageId);
        String jsonRequest = MessageHelper.createJsonOutMessage(message, dataPayload);
        client.send(jsonRequest);
    }

    private static void startServer() {

        createXMPPServer();

        try {
            ServerSocket socket = new ServerSocket(7774, 0, InetAddress.getLoopbackAddress());

            boolean closed = false;

            while(!closed) {
                Socket client = socket.accept();

                PrintWriter out = new PrintWriter(client.getOutputStream());
                BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));

                String req = "";
                String row;
                while(!"".equals(row = in.readLine())) {
                    req += row;
                }
                req = req.substring(0, 6).toUpperCase();

                if(req.startsWith("GET")) {
                    closed = true;

                    String response = "---------------------------\r\nIl server e' stato spento\r\n---------------------------\r\n\r\n";

                    out.println("HTTP/1.1 200 OK");
                    out.println("Content-Length: " + response.length());

                    out.println("Server: Apache/2.2.14 (Win32)");
                    out.println("Content-Type: text/html");
                    out.println("Connection: Closed\r\n\r\n");
                    out.println(response + "\r\n");
                }

                out.close();
                in.close();
                client.close();
            }

            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
