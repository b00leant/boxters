package com.boxters.server.utils;

import java.util.UUID;

/**
 * Created by daniele on 28/02/17.
 */
public class Utils {
    /**
     * Returns a random message id to uniquely identify a message
     */
    public static String getUniqueMessageId() {
        // TODO: replace for your own random message ID that the DB generates
        return "m-" + UUID.randomUUID().toString();
    }
}
