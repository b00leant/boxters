<h1>UI/UX Evolution in Boxters</h1>
<table>
  <tr>
    <td><img src="screenshots/old_box_list.png"/></td>
    <td><img src="screenshots/new_box_list.png"/></td>
  </tr>
  <tr>
    <td><img src="screenshots/old_fragment_map.png"/></td>
    <td><img src="screenshots/new_fragment_map.png"/></td>
  </tr>
  <tr>
    <td><img src="screenshots/old_travel_list.png"/></td>
    <td><img src="screenshots/new_travel_list.png"/></td>
  </tr>
</table>
