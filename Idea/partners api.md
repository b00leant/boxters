# Boxters

# Uber APIs

uber offers:

1a) curl our request
curl -H 'Authorization: Token <TOKEN>' \
'https://api.uber.com/v1/products?latitude=37.7759792&longitude=-122.41823' <--- THIS MIGHT BE INTRESTING:

the previous API can allow us to make requests (once we authorized out app via OAuth) with a given lat&lon parameters
(products aren't more that meres car paths that we should filter in order to find the easiest one, cheapest one & so on..)

1b) once we've found the right product we can finally do this
curl  -X POST -H "Authorization: Bearer <TOKEN>" \
     -H "Content-Type: application/json" -d \
     '{"product_id": "821415d8-3bd5-4e27-9604-194e4359a449", "start_latitude":"37.775232", "start_longitude": "-122.4197513", "end_latitude":"37.7899886", "end_longitude": "-122.4021253","seat_count": "2"}' \
      https://api.uber.com/v1.2/requests/estimate

2) a lot of UberDelivery APIS @ https://developer.uber.com/docs/deliveries/introduction.

# BlablaCar APIs

blablacar offers:

1) GET /api/v2/trips?key = --> here we can insert our api key
the response is a xml/json file with a list of car paths
